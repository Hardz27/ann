<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\T_user as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Salary extends Model
{

  use SoftDeletes;

    protected static function boot(){
          parent::boot();
          static::creating(function($model){
               $user = Auth::user();
               $model->created_by = $user->user_name;
          });
     }

    
    protected $table = 't_salary';

    protected $fillable =[
        'salary_value', 'tgl_berlaku', 'tgl_deadline', 'created_by'
    ];

    protected $primaryKey = 'salary_id';

    public $timestamps = true;
}
