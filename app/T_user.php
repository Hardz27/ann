<?php

namespace App;

use App\Admin\T_company;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\T_user as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class T_user extends Authenticatable
{
    use Notifiable, HasRoles, SoftDeletes;


    protected $fillable = [
        'user_name', 'user_email', 'user_password', 
        'user_status','user_nohp', 'role_id', 'salary'
    ];

    protected $dates = ['deleted_at'];

    protected $primaryKey ='user_id';

    protected $hidden = [
        'user_password', 'remember_token',
    ];

    protected $table = 'T_users';

//     protected static function boot(){
//         parent::boot();
//         static::creating(function($model){
//             $role = Role::assignRole();
//              $model->role_id = $role->id;
//              // $model->updated_by = $user->user_name;
//         });

//         static::updating(function($model){
//             $role = Role::assignRole();
//             $model->role_id = $role->id;
//         });
//    }

    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getAuthPassword()
    {
        return $this->attributes['user_password'];
    }

    public function setPasswordAttribute($value){
        $this->attributes['user_password']=$value;
    }

    public function company(){
        return $this->hasMany(T_company::class);
    }

    public function inputers(){
        return $this->hasMany(Inputer::class, 'user_id');
    }


    // public function getRoleNames(): Collection
    // {
    //     return $this->roles->pluck('name');
    // }

    // public function roles(){
    //     // D:\laragon\www\leads-project\vendor\spatie\laravel-permission\src\Models
    //     return $this->belongsTo('vendor\spatie\laravel-permission\src\Models\Role');
    // }

    
}
