<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{

    protected $table = 'jobs';

    protected $fillable = [ 
        'job_id', 'company_id', 'temp_company_id',
        'job_position', 'job_location', 'job_time', 'job_link'
    ];

    protected $primaryKey = 'job_id';

    public $timestamps = true;

    public function company()
    {
        return $this->belongsTo('App\Admin\T_company', 'company_id');
    }

    public function temp_company()
    {
        return $this->belongsTo('App\Temp_company', 'temp_company_id');
    }

}
