<?php

namespace App\Http\Controllers\Admin;

use App\Admin\T_company;
use App\Admin\T_company_domain;
use App\Admin\T_company_email;
use App\Admin\T_company_pic;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Excel;
use App\Exports\ExportPic;
use Illuminate\Support\Facades\Mail;


class PicController extends Controller
{
    public function index(){
    	$pics = T_company_pic::orderBy('pic_id', 'DESC')
        ->where('pic_name', '!=', null)
        ->where('pic_name', '!=', '-')
        ->where('deleted_at', '=', null)
        ->get();
        
        // dd($pics);
        return view('pic.index', compact('pics'))->with(['page' => 'Pic']);;
    }

    public function edit($pic_id)
    {
        $pic = T_company_pic::findOrFail($pic_id);

        // $company_id = T_company::where('company_id','ASC')->get();
        // $id = T_company_pic::where('pic_id', $company_id)->get();
        // // foreach ($id as $companies) {
        //     $company = T_company::where('company_id', $company_id)->first();
        //     $domain = T_company_domain::where('company_id', $company_id)->first();
        //     $email = T_company_email::where('company_id', $company_id)->first();
        // // }
        
        
        return view('pic.edit', compact('pic', 'company', 'domain', 'email'))->with(['page' => 'Pic']);
    }

    public function view($pic_id)
    {
        $pic = T_company_pic::findOrFail($pic_id);
        return view('pic.view', compact('pic'))->with(['page' => 'Pic']);
    }

   

    public function update(Request $request, $pic_id){

        $this->validate($request, [
       
        //PIC
			'pic_name' => 'required|string',
            'pic_position' => 'required|string',
            'pic_division' => 'required|string',
            'pic_nohp' => 'required|string',
            'pic_email' => 'required|email',
            'pic_sosmed' => 'required|string',
            'pic_status' => 'required|in:aktif,nonaktif',
   
        ]);

        $pic = T_company_pic::findOrFail($pic_id);
        // $pic = T_company_pic::where('pic_id',$pic_id)->first();

        $pic->update([
           'pic_name' => $request->pic_name,
           'pic_position' => $request->pic_position,
           'pic_division' => $request->pic_division,
           'pic_nohp' => $request->pic_nohp,
           'pic_email' => $request->pic_email,
           'pic_sosmed' => $request->pic_sosmed,
           'pic_status' => $request->pic_status,
           'pic_id' => $pic->pic_id

        ]);
            
        return redirect(route('pic.index'))->with(['success' => 'PIC berhasil diupdate'])->with(['page' => 'Pic']);
        
    }

    public function detail($pic_id){
        $pic = T_company_pic::findOrFail($pic_id);
        return view('pic.detail', compact('pic'))->with(['page' => 'Pic']);
    }

    public function delete_pic($pic_id){
        $pic = T_company_pic::findOrFail($pic_id);
        return view('pic.delete_pic', compact('pic'));
    }


    public function destroy($pic_id)
    {
        $pic = T_company_pic::findOrFail($pic_id);
        $pic->delete();
        return redirect()->back()->with(['success' => 'PIC: ' . $pic->pic_name . ' Dihapus']);
    }

    // public function email($email){
    //     $message = new Google_Service_Gmail_Message();
    //      // base64url encode the string
    //      //   see http://en.wikipedia.org/wiki/Base64#Implementations_and_history
    //      $email = strtr(base64_encode($email), array('+' => '-', '/' => '_'));
    //      $message->setRaw($email);
    //      return $message;
    // }

    public function export_pic(){
        return Excel::download(new ExportPic, 'PIC.xlsx');
    }

   
}
