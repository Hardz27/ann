<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Request;
use Input;
use App\Admin\T_company;
use Excel;
use App\Exports\exportCompany;
use App\T_user;
use Spatie\Permission\Models\Role;
use Carbon\Carbon;

class CompanyController extends Controller
{
    public function index(Request $request){
        $CompaniesData  = T_company::orderby('created_at', 'DESC')->whereNotNull('company_validation')->get();
        $companies = T_company::orderby('created_at', 'DESC')->with('user');
        $users = T_user::orderBy('user_name', 'ASC')->get();
        // $admin  =  T_user::role('admin')->orderBy('user_name', 'ASC')->get();
        // $inputer = T_user::role('inputer')->orderBy('user_name', 'ASC')->get();


        

        // $customers = Customer::orderBy('name', 'ASC')->get();
        // $users = User::role('kasir')->orderBy('name', 'ASC')->get();
        // $orders = Order::orderBy('created_at', 'DESC')->with('order_detail', 'customer');
        // $orders = T_company::orderBy('created_at', 'DESC')->get();

        // if (!empty($request->user_id)) {
        //     $companies = $companies->where('user_id', $request->user_id);
        // }

        if (!empty($request->user_id)) {
            $companies = $companies->where('user_id', $request->user_id);
        }
        
        if (!empty($request->start_date) && !empty($request->end_date)) {
            $this->validate($request, [
                'start_date' => 'nullable|date',
                'end_date' => 'nullable|date'
            ]);
            $start_date = Carbon::parse($request->start_date)->format('Y-m-d') . ' 00:00:01';
            $end_date = Carbon::parse($request->end_date)->format('Y-m-d') . ' 23:59:59';

            $companies = $companies->whereBetween('created_at', [$start_date, $end_date])->get();
        } else {
            
                
            // $companies = $companies->take(10)->skip(0)->get();
            // $companies = $companies->paginate(10);
            $companies = $companies->get();
        }

        

        return view('company.index', [
            // 'orders' => $orders,
            // 'sold' => $this->countItem($orders),
            // 'total' => $this->countTotal($orders),
            // 'total_customer' => $this->countCustomer($orders),
            'CompaniesData' => $CompaniesData,
            // 'inputer' => $inputer,
            // 'admin' => $admin,
            'users' => $users,
            'companies' => $companies
        ])->with(['page' => 'Company']);
    
    }

    public function edit($company_id)
    {
        $company = T_company::findOrFail($company_id);
        return view('company.edit', compact('company'))->with(['page' => 'Company']);
    }

    public function update(Request $request, $company_id){

        $this->validate($request, [
       
        	'company_name' => 'required|string|max:50|unique:t_companies',
            'company_type' => 'required|in:tekno,nontekno',
            'company_field' => 'required|string',
            'company_desc' => 'required|string',
            'company_address' => 'required|string',
            'company_telpon_office' => 'required|string',
            'company_faximile' => 'required|string',
            'company_status' => 'in:aktif,nonaktif',
        ]);

        $company = T_company::findOrFail($company_id);
        // $pic = T_company_pic::where('pic_id',$pic_id)->first();

        $company->update([
           'company_name' => $request->company_name,
           'company_type' => $request->company_type,
           'company_field' => $request->company_field,
           'company_desc' => $request->company_desc,
           'company_address' => $request->company_address,
           'company_telpon_office' => $request->company_telpon_office,
           'company_faximile' => $request->company_faximile,
           'company_status' => $request->company_status

        ]);
            
        return redirect(route('company.index'))->with(['success' => 'Company berhasil diupdate'])->with(['page' => 'Company']);
        
    }

    public function detail($company_id){
        $companies = T_company::findOrFail($company_id);
        return view('company.detail', compact('companies'))->with(['page' => 'Company']);
    }

    public function delete_form($company_id){
        $company = T_company::findOrFail($company_id);
        $role = Role::orderBy('name', 'ASC')->get();

        return view('company.delete_company', compact('company', 'role'));
    }

    public function destroy($company_id)
    {
        $company = T_company::findOrFail($company_id);
        $company->delete();
        return redirect()->back()->with(['success' => 'Company: ' . $company->company_name . ' Dihapus']);
    }

    // public function destroy() {
    //     $checked = Request::input('checked',[]);
    //     foreach ($checked as $id) {
    //          Todo::where("id",$id)->delete(); //Assuming you have a Todo model. 
    //     }
    //     //Or as @Alex suggested 
    //     Todo::whereIn($checked)->delete();
    //  }

    public function delete_check()
    {
        $checked = Request::input('checked', []);
        foreach ($checked as  $id) {
            T_company::where('company_id', $id)->delete();
        }

        return redirect()->back();
    }

    public function export_excel(){
        return Excel::download(new exportCompany, 'Company.xlsx');
    }
  

}
