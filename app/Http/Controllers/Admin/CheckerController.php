<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin\T_company;
use App\Admin\T_company_domain;
use Illuminate\Support\Facades\DB;
use App\T_user;
use App\Temp_company;
use App\Job;
use App\History_salary;
use App\Inputer;
use App\Admin\T_company_pic;
use App\Admin\T_company_email;
// use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

use Excel;
use App\Exports\ExportDataset;
use App\Imports\ImportDataset;

class CheckerController extends Controller
{
    public function index(Request $request){
        // ini_set('max_input_vars', 999999999);

       
        $user = Auth::user();

        $data = DB::table('normalized')->get();
        
        // $filter =  T_company::where('company_id', $company_id)->first();

         return view('checks.index', [
            'data'  => $data,
            'user'  => $user
             
         ])->with(['page' => 'Check']);
 
    }

        public function generate_normalized(Request $request)
    {        

        $data = explode(';', $request->data);
        
        for ($i = 0; $i < count($data) - 1; $i) { 
            $i++;

            DB::table('normalized')->insert([
                'date' => $data[$i++],
                'time' => $data[$i++],
                'device' => $data[$i++],
                'value_soil_moisture' => $data[$i++],
                'value_leaf_wetness' => $data[$i++],
                'value_air_temprature' => $data[$i++],
                'value_air_humidity' => $data[$i++],
                'value_wind_speed' => $data[$i++],
                'value_wind_direction' => $data[$i++],
                'value_precipitation' => $data[$i++],
                'value_solar_radiation' => $data[$i++]
            ]);
        }

        dd('Sukses!');
    }

    public function generate(Request $request)
    {        

        $data = explode(';', $request->data);
        
        for ($i = 0; $i < count($data) - 1; $i) { 
            // $data[$i] = trim($data[$i]);
            $i++;
            // $date = $data[$i++];
            // $time = $data[$i++];
            // $device = $data[$i++];
            
            DB::table('dataset')->insert([
                'tanggal' => $data[$i++],
                'waktu' => $data[$i++],
                'devices' => $data[$i++],
                'sensor_id' => $data[$i++],
                'sensor_type' => $data[$i++],
                'value' => $data[$i++],
            ]);
            // dd();
            // $i++;

            // dd($data[++$i]);
        }
        // dd($date, $time, $sensor, $device, $value);


        dd('Sukses!');
      

        // return redirect(route('check.website'))
        // ->with(['generated' => 'TRUE'])
        // ->with(['page' => 'Check']);

        // End Generate LinkedIn function
    }

    function log(){
        $line =  file(public_path('log.txt'));
        echo json_encode($line);
        // dd($line);
        // while(!feof($myfile)) {
        //   echo fgets($myfile) . "<br>";
        // }
        // fclose($myfile);
        // $cursor = -1;
        // fseek($f, $cursor, SEEK_END);
        // $char = fgetc($f);
        // //Trim trailing newline characters in the file
        // while ($char === "\n" || $char === "\r") {
        //    fseek($f, $cursor--, SEEK_END);
        //    $char = fgetc($f);
        // }
        // //Read until the next line of the file begins or the first newline char
        // while ($char !== false && $char !== "\n" && $char !== "\r") {
        //    //Prepend the new character
        //    $line = $char . $line;
        //    fseek($f, $cursor--, SEEK_END);
        //    $char = fgetc($f);
        // }
        // $data = json_encode($line, true);
        // echo $data[0];
        // dd(json_encode($line));
    }

    function train(Request $request){
        Excel::store(new ExportDataset(), 'exports/dataset.csv');

        // $path = public_path();
        // $path = str_replace('\\', '/', $path);
        // $pathLog = $path.'/log.txt';
        // $command = 'start /B '.$path.'/PA-ANN.py  > '.$pathLog;
        // pclose( popen( $command, 'r' ) );
        // $this->log();
        
        
    }    

    function import(Request $request)
    {
        DB::connection()->disableQueryLog();

        $this->validate($request, [
            'select_file'  => 'required|mimes:xls,xlsx'
        ]);

        $file = $request->file('select_file');

        $name_file = date('d-m-Y').'__'.$file->getClientOriginalName();
        
        $file->move('dataset',$name_file);


        Excel::import(new ImportDataset, public_path('/dataset/'.$name_file));
        
        
        return back()->with('success', 'Excel Data Imported successfully.');
    }

    public function rawData()
    {        
        $data = DB::table('dataset')->take(5)->get();
        dd($data);
        return Excel::download(new ExportDataset, 'DatasetRaw.csv');
    }

    public function normalized()
    {        
        return Excel::download(new ExportDataset, 'Dataset.xlsx');
    }

    public function normalizing(Request $request)
    {        

        // ================================ NORMALIZING V1 =======================================

        // $data = explode(';', $request->data);
        // // dd($data);
        // for ($i = 0; $i < count($data) - 7; $i) { 
        //     // $data[$i] = trim($data[$i]);

        //     $date = $data[++$i];
        //     $time = $data[++$i];
        //     $device = $data[++$i];
        //     ++$i;
        //     $cek = $data[++$i];
        //     $data_cek = 0;
        //     // dd($date,$time,$device, $data[++$i], $data[++$i]);
        //     while ($data_cek <= 7) {
                
        //         if($cek == 1){
        //             // dd('Masuk Soil');
        //             $value_soil_moisture = $data[++$i];
        //             $data_cek += 1;
        //         }elseif($cek == 7 ){
        //             // dd('Masuk Leaf');
        //             $value_leaf_wetness = $data[++$i];
        //             $data_cek += 1;
        //         }elseif($cek == 8 ){
        //             // dd('Masuk Air Temprature');
        //             $value_air_temprature = $data[++$i];
        //             $data_cek += 1;
        //         }elseif($cek == 9 ){
        //             // dd('Masuk Air Humidity');
        //             $value_air_humidity = $data[++$i];
        //             $data_cek += 1;
        //         }elseif($cek == 10 ){
        //             // dd('Masuk Wind Speed');
        //             $value_wind_speed = $data[++$i];
        //             $data_cek += 1;
        //         }elseif($cek == 11 ){
        //             // dd('Masuk Wind Direction');
        //             $value_wind_direction = $data[++$i];
        //             $data_cek += 1;
        //         }elseif($cek == 12 ){
        //             // dd('Masuk Precipitation');
        //             $value_precipitation = $data[++$i];
        //             $data_cek += 1;
        //         }elseif($cek == 13 ){
        //             // dd('Masuk Solar Radiation');
        //             $value_solar_radiation = $data[++$i];
        //             $data_cek += 1;
        //         }
        //         // else{
        //         //     dd($data);
        //         //     dd('ERROR!', $cek, $data_cek, $i, count($data));

        //         // }

        //         $i += 6;

        //         if (!isset($data[$i])) {
        //             // dd($data_cek);
        //             $i -= 5;
        //             $cek = 99;
        //             $data_cek = 10;
        //             break;
        //         }else{
        //             $cek = $data[$i];    
        //         }
                
                
        //     }

        //     // dd("Sukses!", $value_air_temprature);

        //     // dd("Sukses!", $value_air_temprature, $value_air_humidity, $value_wind_speed, $value_wind_direction, $value_precipitation, $value_soil_moisture, $value_leaf_wetness, $value_solar_radiation);

        //     DB::table('normalized')->insert([
        //         'date' => $date,
        //         'time' => $time,
        //         'device' => $device,
        //         'value_air_temprature' => $value_air_temprature,
        //         'value_air_humidity' => $value_air_humidity,
        //         'value_wind_speed' => $value_wind_speed,
        //         'value_wind_direction' => $value_wind_direction,
        //         'value_precipitation' => $value_precipitation,
        //         'value_soil_moisture' => $value_soil_moisture,
        //         'value_leaf_wetness' => $value_leaf_wetness,
        //         'value_solar_radiation' => $value_solar_radiation
        //     ]);

        //     $i -= 5;

        //     // dd($data[++$i]);
        // }



        // ================================ NORMALIZING V2 =======================================

        // $data = DB::table('dataset')->orderBy('tanggal', 'ASC')->orderBy('waktu', 'ASC')->orderBy('devices', 'ASC')->get();
        // $data = DB::table('dataset')->get();
        // // dd(count($data), $data);
        // for ($i = 0; $i < count($data) - 1; $i) { 
        //     if ($i == count($data)) {
        //         dd('offset up');
        //     }
        //     // $data[$i] = trim($data[$i]);

        //     $date = $data[$i]->tanggal;
        //     $time = $data[$i]->waktu;
        //     $device = $data[$i]->devices;
        //     // ++$i;
        //     $cek = $data[$i]->sensor_type; 
        //     $data_cek = 0;
        //     // dd($date,$time,$device, $cek, $data_cek);
        //     // dd($data[$i]);
        //     while ($data_cek <= 7) {
        //         // dd($cek);   
        //         if($cek == '1'){
        //             // dd('Masuk Soil');
        //             $value_soil_moisture = $data[$i]->value;
        //             $data_cek += 1;
        //         }elseif($cek == '7' ){
        //             // dd('Masuk Leaf');
        //             $value_leaf_wetness = $data[$i]->value;
        //             $data_cek += 1;
        //         }elseif($cek == '8' ){
        //             // dd('Masuk Air Temprature');
        //             $value_air_temprature = $data[$i]->value;
        //             $data_cek += 1;
        //         }elseif($cek == '9' ){
        //             // dd('Masuk Air Humidity');
        //             $value_air_humidity = $data[$i]->value;
        //             $data_cek += 1;
        //         }elseif($cek == '10' ){
        //             // dd('Masuk Wind Speed');
        //             $value_wind_speed = $data[$i]->value;
        //             $data_cek += 1;
        //         }elseif($cek == '11' ){
        //             // dd('Masuk Wind Direction');
        //             $value_wind_direction = $data[$i]->value;
        //             $data_cek += 1;
        //         }elseif($cek == '12' ){
        //             // dd('Masuk Precipitation');
        //             $value_precipitation = $data[$i]->value;
        //             // dd($value_precipitation);
        //             $data_cek += 1;
        //         }elseif($cek == '13' ){
        //             // dd('Masuk Solar Radiation');
        //             $value_solar_radiation = $data[$i]->value;
        //             $data_cek += 1;
        //         }
        //         // else{
        //         //     dd($data);
        //         //     dd('ERROR!', $cek, $data_cek, $i, count($data));

        //         // }
        //         // if ($data_cek > 7) {
        //         //     dd($data_cek);
        //         // }
                
        //         // $i += 6;


        //         if (!isset($data[$i])) {
        //             // dd($data_cek);
        //             $i -= 5;
        //             $cek = 99;
        //             $data_cek = 10;
        //             dd('error');
        //             break;

        //         }else{
        //             $cek = $data[$i];    
        //         }
                
        //         if (($i+1) >= count($data)) {
        //             echo "normalized!";
        //             break;
        //             // dd('ofset');
        //         }else{
        //             // echo "no of";
        //             $cek = $data[$i+=1]->sensor_type; 
        //         }

        //         // dd($cek);
        //     }

        //     // dd($data[$i]);

        //     // dd("Sukses!", $value_air_temprature);

        //     // dd("Sukses!", $value_air_temprature, $value_air_humidity, $value_wind_speed, $value_wind_direction, $value_precipitation, $value_soil_moisture, $value_leaf_wetness, $value_solar_radiation);

        //     DB::table('normalized')->insert([
        //         'date' => $date,
        //         'time' => $time,
        //         'device' => $device,
        //         'value_air_temprature' => $value_air_temprature,
        //         'value_air_humidity' => $value_air_humidity,
        //         'value_wind_speed' => $value_wind_speed,
        //         'value_wind_direction' => $value_wind_direction,
        //         'value_precipitation' => $value_precipitation,
        //         'value_soil_moisture' => $value_soil_moisture,
        //         'value_leaf_wetness' => $value_leaf_wetness,
        //         'value_solar_radiation' => $value_solar_radiation
        //     ]);

        //     // $i -= 5;
        //     if ($i == count($data)) {
        //         dd('offset down');
        //     }
        //     // dd($data[$i]);
        // }

        // ================================ NORMALIZING V3 =======================================

        // $data = DB::table('dataset')->orderBy('tanggal', 'ASC')->orderBy('waktu', 'ASC')->orderBy('devices', 'ASC')->get();
        $data = DB::table('dataset')->get();
        // dd(count($data), $data);
        for ($i = 0; $i < count($data) - 1; $i) { 
            // $data[$i] = trim($data[$i]);

            $date = $data[$i]->tanggal;
            $time = $data[$i]->waktu;
            $device = $data[$i]->devices;
            // ++$i;
            $cek = $data[$i]->sensor_type; 
            $data_cek = 0;

            while ($data_cek <= 7) {
                // dd($cek);   
                if($cek == '1'){
                    // dd('Masuk Soil');
                    $value_soil_moisture = $data[$i]->value;
                    $data_cek += 1;
                    $column[1] = 1;
                }elseif($cek == '7' ){
                    // dd('Masuk Leaf');
                    $value_leaf_wetness = $data[$i]->value;
                    $data_cek += 1;
                    $column[2] = 1;
                }elseif($cek == '8' ){
                    // dd('Masuk Air Temprature');
                    $value_air_temprature = $data[$i]->value;
                    $data_cek += 1;
                    $column[3] = 1;
                }elseif($cek == '9' ){
                    // dd('Masuk Air Humidity');
                    $value_air_humidity = $data[$i]->value;
                    $data_cek += 1;
                    $column[4] = 1;
                }elseif($cek == '10' ){
                    // dd('Masuk Wind Speed');
                    $value_wind_speed = $data[$i]->value;
                    $data_cek += 1;
                    $column[5] = 1;
                }elseif($cek == '11' ){
                    // dd('Masuk Wind Direction');
                    $value_wind_direction = $data[$i]->value;
                    $data_cek += 1;
                    $column[6] = 1;
                }elseif($cek == '12' ){
                    // dd('Masuk Precipitation');
                    $value_precipitation = $data[$i]->value;
                    // dd($value_precipitation);
                    $data_cek += 1;
                    $column[7] = 1;
                }elseif($cek == '13' ){
                    // dd('Masuk Solar Radiation');
                    $value_solar_radiation = $data[$i]->value;
                    $data_cek += 1;
                    $column[8] = 1;
                }

                // else{
                //     dd($data);
                //     dd('ERROR!', $cek, $data_cek, $i, count($data));

                // }
                // if ($data_cek > 7) {
                //     dd($data_cek);
                // }
                
                // $i += 6;


                if (!isset($data[$i])) {
                    // dd($data_cek);
                    $i -= 5;
                    $cek = 99;
                    $data_cek = 10;
                    dd('error');
                    break;

                }else{
                    $cek = $data[$i];    
                }
                
                if (($i+1) >= count($data)) {
                    echo "normalized!";
                    break;
                    // dd('ofset');
                }else{
                    // echo "no of";
                    if ($data[$i]->devices != $device) {
                        $data_cek = 10;
                    }else{
                        $cek = $data[$i+=1]->sensor_type;
                    }
                }

                // dd($cek);
            }

            while ($data[$i]->devices == $device) {
                if (($i+1) >= count($data)) {
                    echo "Data redundan! | ";
                    break;
                    // dd('ofset');
                }else{
                    // echo "no of";
                    $i+=1;
                    // dd($i);
                }

            }
            // dd($data[$i]);

            // dd("Sukses!", $value_air_temprature);

            // dd("Sukses!", $value_air_temprature, $value_air_humidity, $value_wind_speed, $value_wind_direction, $value_precipitation, $value_soil_moisture, $value_leaf_wetness, $value_solar_radiation);

            DB::table('normalized')->insert([
                'date' => $date,
                'time' => $time,
                'device' => $device,
                'value_air_temprature' => $value_air_temprature,
                'value_air_humidity' => $value_air_humidity,
                'value_wind_speed' => $value_wind_speed,
                'value_wind_direction' => $value_wind_direction,
                'value_precipitation' => $value_precipitation,
                'value_soil_moisture' => $value_soil_moisture,
                'value_leaf_wetness' => $value_leaf_wetness,
                'value_solar_radiation' => $value_solar_radiation
            ]);

            // $i -= 5;
            if ($i == count($data)) {
                dd('offset down');
            }
            // dd($data[$i]);
        }

        dd('Sukses!');
      

        // return redirect(route('check.website'))
        // ->with(['generated' => 'TRUE'])
        // ->with(['page' => 'Check']);

        // End Generate LinkedIn function
    }

    // ================ Soil Moisture ==========================

    public function dry($x)
    {
        if ($x <= 50) {
            return 0;
        }else if ($x > 50 && $x < 60) {
            return ($x - 50) / 10;
        }else{
            return 1;
        }
    }

    public function normal($x)
    {
        if ($x <= 30 || $x >= 60 ) {
            return 0;
        }else if ($x > 30 && $x < 40) {
            return ($x - 30) / 10;
        }else if ($x > 50 && $x < 60) {
            return (60 - $x) / 10;
        }else if ($x >= 40 && $x <= 50) {
            return 1;
        }
    }

    public function adq_wet($x)
    {
        if ($x <= 10 || $x >= 40 ) {
            return 0;
        }else if ($x > 10 && $x < 20) {
            return ($x - 10) / 10;
        }else if ($x > 30 && $x < 40) {
            return (40 - $x) / 10;
        }else if ($x >= 20 && $x <= 30) {
            return 1;
        }
    }

    public function satruated($x)
    {
        if ($x <= 10) {
            return 1;
        }else if ($x > 10 && $x < 20) {
            return (20 - $x) / 10;
        }else{
            return 0;
        }
    }

    // ====================== Air Temprature ==========================

    public function v_cold($x)
    {
        if ($x >= 15 ) {
            return 0;
        }
        // else if ($x > (-10) && $x < 0) {
        //     return ( ($x * (-1)) + (-10) ) / 10;
        // }
        else if ($x > 10 && $x < 15) {
            return (15 - $x) / 5;
        }else if ($x <= 10) {
            return 1;
        }
    }

    public function cold($x)
    {
        // $x = '12.3';
        // $x = (float)$x;
        // dd($x);
        if ($x <= 10 || $x >= 25 ) {
            return 0;
        }else if ($x > 10 && $x < 15) {
            return ($x - 10) / 5;
        }else if ($x > 20 && $x < 25) {
            return (25 - $x) / 5;
        }else if ($x >= 15 && $x <= 20) {
            return 1;
        }
    }

    public function normal_temp($x)
    {
        if ($x <= 20 || $x >= 35 ) {
            return 0;
        }else if ($x > 20 && $x < 25) {
            return ($x - 20) / 5;
        }else if ($x > 30 && $x < 35) {
            return (35 - $x) / 5;
        }else if ($x >= 25 && $x <= 30) {
            return 1;
        }
    }

    public function hot($x)
    {
        if ($x <= 30 || $x >= 45 ) {
            return 0;
        }else if ($x > 30 && $x < 35) {
            return ($x - 30) / 5;
        }else if ($x > 40 && $x < 45) {
            return (45 - $x) / 5;
        }else if ($x >= 35 && $x <= 40) {
            return 1;
        }
    }

    public function v_hot($x)
    {
        if ($x <= 40 ) {
            return 0;
        }else if ($x > 40 && $x < 45) {
            return($x - 40) / 5;
        }else if ($x > 45) {
            return 1;
        }
    }

    // ====================== Air Humidity ==========================

    public function low($x)
    {
        if ($x <= 10) {
            return 1;
        }
        // else if ($x > 0 && $x < 10) {
        //     return ( $x - 0 ) / 10;
        // }
        else if ($x > 10 && $x < 20) {
            return (20 - $x) / 10;
        }else if ($x >= 20) {
            return 0;
        }
    }

    public function medium($x)
    {
        if ($x <= 10 || $x >= 40 ) {
            return 0;
        }else if ($x > 10 && $x < 20) {
            return ($x - 10) / 10;
        }else if ($x > 30 && $x < 40) {
            return (40 - $x) / 10;
        }else if ($x >= 20 && $x <= 30) {
            return 1;
        }
    }

    public function high($x)
    {
        if ($x <= 30 || $x >= 60 ) {
            return 0;
        }else if ($x > 30 && $x < 40) {
            return ($x - 30) / 10;
        }else if ($x > 50 && $x < 60) {
            return (60 - $x) / 10;
        }else if ($x >= 40 && $x <= 50) {
            return 1;
        }
    }

    public function ext_high($x)
    {
        if ($x <= 50) {
            return 0;
        }
        // else if ($x > 45 && $x < 55) {
        //     return ($x - 45) / 10;
        // }
        else if ($x > 50 && $x < 60) {
            return (60 - $x) / 10;
        }else if ($x >= 60) {
            return 1;
        }
    }

    // ====================== Output ==========================

    public function onebyfive($alpha)
    {
        $z[] = 30 - ($alpha * 10);

        return $z[0];
    }

    public function twobyfive($alpha)
    {
        $z[] = ($alpha * 10) + 20;
        $z[] = 50 - ($alpha * 10);
        sort($z);

        return $z[0];
    }

    public function threebyfive($alpha)
    {
        $z[] = ($alpha * 10) + 40;
        $z[] = 70 - ($alpha * 10);
        sort($z);

        return $z[0];
    }

    public function fourbyfive($alpha)
    {
        $z[] = ($alpha * 10) + 60;
        $z[] = 80 - ($alpha * 10);
        sort($z);

        return $z[0];
    }

    public function fivebyfive($alpha)
    {
        $z[] = ($alpha * 10) + 80;

        return $z[0];
    }

    public function ten($alpha)
    {
        $z[] = ($alpha * 10) + 0;
        $z[] = 20 - ($alpha * 10);
        sort($z);

        return $z[0];
    }

    public function twentyfive($alpha)
    {
        $z[] = ($alpha * 15) + 15;
        $z[] = 45 - ($alpha * 15);
        sort($z);

        return $z[0];
    }

    public function fifty($alpha)
    {
        $z[] = ($alpha * 15) + 40;
        $z[] = 70 - ($alpha * 15);
        sort($z);

        return $z[0];
    }

    public function seventyfive($alpha)
    {
        $z[] = ($alpha * 10) + 65;
        $z[] = 85 - ($alpha * 10);
        sort($z);

        return $z[0];
    }

    public function openfull($alpha)
    {
        $z[] = ($alpha * 10) + 80;
        $z[] = 100 - ($alpha * 10);
        sort($z);

        return $z[0];
    }

    public function fuzzy(Request $request)
    {
        $max = DB::table('normalized')->get();
        $max = count($max);
        
        $i = 6;

        while ($i <= $max + 6) {
            $data = DB::table('normalized')->where('id_data', $i)->first();

            // $soil_moisture = 8;
            // $air_temprature = 41;
            // $air_humidity = 21;
            // dd($data, $soil_moisture, $air_temprature, $air_humidity);
            $value_soil_moisture  = str_replace(",",".",$data->value_soil_moisture);
            $value_air_temprature = str_replace(",",".",$data->value_air_temprature);
            $value_air_humidity = str_replace(",",".",$data->value_air_humidity);

            $soil_moisture = $value_soil_moisture;
            $air_temprature = $value_air_temprature;
            $air_humidity = $value_air_humidity;
            // echo $soil_moisture .', '. $air_temprature .', '. $air_humidity . '<br>';


            // =========================== Evaluation Rule ===============================

          // R1 Soil Moist Dry dan Air Temprature V.Hot and Air Humid Low Then openfull (fivebyfive) (5)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->v_hot($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 1 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[0] = $miu[0];
            $z[0] = $this->fivebyfive($alpha[0]);

          // R2 Soil Moist Dry dan Air Temprature V.Hot and Air Humid Medium Then openfull (fivebyfive) (4)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->v_hot($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 2 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[1] = $miu[0];
            $z[1] = $this->fivebyfive($alpha[1]);

          // R3 Soil Moist Dry dan Air Temprature V.Hot and Air Humid High Then seperempat(fourbyfive) (3)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->v_hot($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 3 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[2] = $miu[0];
            $z[2] = $this->fourbyfive($alpha[2]);

          // R4 Soil Moist Dry dan Air Temprature V.Hot and Air Humid Extreme cool Then seperempat(fourbyfive) (2)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->v_hot($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 4 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[3] = $miu[0];
            $z[3] = $this->fourbyfive($alpha[3]);

// ==============================================================================

          // R5 Soil Moist Dry dan Air Temprature Hot and Air Humid Low Then openfull(fivebyfive) (5)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->hot($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 5 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[4] = $miu[0];
            $z[4] = $this->fivebyfive($alpha[4]);

          // R6 Soil Moist Dry dan Air Temprature Hot and Air Humid Medium Then openfull(fivebyfive) (4)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->hot($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 6 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[5] = $miu[0];
            $z[5] = $this->fivebyfive($alpha[5]);

          // R7 Soil Moist Dry dan Air Temprature Hot and Air Humid High Then (fourbyfive) (3)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->hot($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 7 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[6] = $miu[0];
            $z[6] = $this->fourbyfive($alpha[6]);

          // R8 Soil Moist Dry dan Air Temprature Hot and Air Humid Extreme Then (fourbyfive) (2)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->hot($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 8 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[7] = $miu[0];
            $z[7] = $this->fourbyfive($alpha[7]);

// ==============================================================================

          // R9 Soil Moist Dry dan Air Temprature Normal_temp and Air Humid Low kering Then (fourbyfive) (4)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->normal_temp($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 9 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[8] = $miu[0];
            $z[8] = $this->fourbyfive($alpha[8]);

          // R10 Soil Moist Dry dan Air Temprature Normal_temp and Air Humid Medium Then (fourbyfive) (3)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->normal_temp($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 10 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[9] = $miu[0];
            $z[9] = $this->fourbyfive($alpha[9]);

          // R11 Soil Moist Dry dan Air Temprature Normal_temp and Air Humid High Then (fourbyfive) (2)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->normal_temp($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 11 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[10] = $miu[0];
            $z[10] = $this->fourbyfive($alpha[10]);

          // R12 Soil Moist Dry dan Air Temprature Normal_temp and Air Humid Extreme Then (threebyfive) (2)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->normal_temp($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 12 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[11] = $miu[0];
            $z[11] = $this->threebyfive($alpha[11]);

// ==============================================================================

          // R13 Soil Moist Dry dan Air Temprature Cold and Air Humid Low Then (fourbyfive) (3)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->cold($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 13 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[12] = $miu[0];
            $z[12] = $this->fourbyfive($alpha[12]);

          // R14 Soil Moist Dry dan Air Temprature Cold and Air Humid Medium Then (fourbyfive) (2)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->cold($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 14 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[13] = $miu[0];
            $z[13] = $this->fourbyfive($alpha[13]);

          // R15 Soil Moist Dry dan Air Temprature Cold and Air Humid High Then (threebyfive) (2)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->cold($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 15 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[14] = $miu[0];
            $z[14] = $this->threebyfive($alpha[14]);

          // R16 Soil Moist Dry dan Air Temprature Cold and Air Humid Extreme Then (threebyfive) (1)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->cold($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 16 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[15] = $miu[0];
            $z[15] = $this->threebyfive($alpha[15]);

// ==============================================================================

          // R17 Soil Moist Dry dan Air Temprature v_cold and Air Humid Low Then (threebyfive) (3)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->v_cold($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 17 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[16] = $miu[0];
            $z[16] = $this->threebyfive($alpha[16]);

          // R18 Soil Moist Dry dan Air Temprature v_cold and Air Humid Medium Then (threebyfive) (3)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->v_cold($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 18 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[17] = $miu[0];
            $z[17] = $this->threebyfive($alpha[17]);

          // R19 Soil Moist Dry dan Air Temprature v_cold and Air Humid High Then (twobyfive) (2)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->v_cold($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 19 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[18] = $miu[0];
            $z[18] = $this->twobyfive($alpha[18]);

          // R20 Soil Moist Dry dan Air Temprature v_cold and Air Humid Extreme Then (twobyfive) (1)
            $miuSoilMoist = $this->dry($soil_moisture);
            $miuAirTemp = $this->v_cold($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 20 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[19] = $miu[0];
            $z[19] = $this->twobyfive($alpha[19]);


// ==============================================================================

// ##############################################################################

// ==============================================================================

          // R21 Soil Moist normal dan Air Temprature V.Hot and Air Humid Low Then (fourbyfive) (4)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->v_hot($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 21 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[20] = $miu[0];
            $z[20] = $this->fourbyfive($alpha[20]);

          // R22 Soil Moist normal dan Air Temprature V.Hot and Air Humid Medium Then (fourbyfive) (4)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->v_hot($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);          
            // echo "Rule 22 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';  
            $alpha[21] = $miu[0];
            $z[21] = $this->fourbyfive($alpha[21]);

          // R23 Soil Moist normal dan Air Temprature V.Hot and Air Humid High Then (threebyfive) (3)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->v_hot($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 23 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[22] = $miu[0];
            $z[22] = $this->threebyfive($alpha[22]);

          // R24 Soil Moist normal dan Air Temprature V.Hot and Air Humid Extreme Then (threebyfive) (3)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->v_hot($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 24 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[23] = $miu[0];
            $z[23] = $this->threebyfive($alpha[23]);

// ==============================================================================

          // R25 Soil Moist normal dan Air Temprature Hot and Air Humid Low Then (fourbyfive) (4)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->hot($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 25 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[24] = $miu[0];
            $z[24] = $this->fourbyfive($alpha[24]);

          // R26 Soil Moist normal dan Air Temprature Hot and Air Humid Medium Then (fourbyfive) (4)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->hot($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 26 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[25] = $miu[0];
            $z[25] = $this->fourbyfive($alpha[25]);

          // R27 Soil Moist normal dan Air Temprature Hot and Air Humid High Then (threebyfive) (3)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->hot($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 27 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[26] = $miu[0];
            $z[26] = $this->threebyfive($alpha[26]);

          // R28 Soil Moist normal dan Air Temprature Hot and Air Humid Extreme Then (threebyfive) (3)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->hot($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 28 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[27] = $miu[0];
            $z[27] = $this->threebyfive($alpha[27]);

// ==============================================================================

          // R29 Soil Moist normal dan Air Temprature Normal_temp and Air Humid Low Then (threebyfive) (4)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->normal_temp($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 29 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[28] = $miu[0];
            $z[28] = $this->threebyfive($alpha[28]);

          // R30 Soil Moist normal dan Air Temprature Normal_temp and Air Humid Medium Then (threebyfive) (4)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->normal_temp($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);           
            // echo "Rule 30 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>'; 
            $alpha[29] = $miu[0];
            $z[29] = $this->threebyfive($alpha[29]);

          // R31 Soil Moist normal dan Air Temprature Normal_temp and Air Humid High Then (twobyfive) (3)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->normal_temp($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            // echo "Rule 31 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[30] = $miu[0];
            $z[30] = $this->twobyfive($alpha[30]);

          // R32 Soil Moist normal dan Air Temprature Normal_temp and Air Humid Extreme Then (twobyfive) (2)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->normal_temp($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            // echo "Rule 32 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $alpha[31] = $miu[0];
            $z[31] = $this->twobyfive($alpha[31]);


// ==============================================================================

            $n = 32;

          // R33 Soil Moist normal dan Air Temprature Cold and Air Humid Low Then (threebyfive) (3)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->cold($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[$n] = $miu[0];
            // echo "Rule ". (($n+1)) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->threebyfive($alpha[$n]);

          // R34 Soil Moist normal dan Air Temprature Cold and Air Humid Medium Then (twobyfive) (2)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->cold($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->twobyfive($alpha[$n]);

          // R35 Soil Moist normal dan Air Temprature Cold and Air Humid High Then (twobyfive) (2)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->cold($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->twobyfive($alpha[$n]);

          // R36 Soil Moist normal dan Air Temprature Cold and Air Humid Extreme Then (onebyfive) (2)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->cold($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);

// ==============================================================================

          // R37 Soil Moist normal dan Air Temprature v_cold and Air Humid Low Then (twobyfive) (4)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->v_cold($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->twobyfive($alpha[$n]);

          // R38 Soil Moist normal dan Air Temprature v_cold and Air Humid Medium Then (twobyfive) (3)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->v_cold($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->twobyfive($alpha[$n]);

          // R39 Soil Moist normal dan Air Temprature v_cold and Air Humid High Then ten (onebyfive) (2)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->v_cold($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);

          // R40 Soil Moist normal dan Air Temprature v_cold and Air Humid Extreme Then ten (onebyfive) (2)
            $miuSoilMoist = $this->normal($soil_moisture);
            $miuAirTemp = $this->v_cold($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);

// ==============================================================================

// ##############################################################################

// ==============================================================================

          // R41 Soil Moist Adq_wet dan Air Temprature V.Hot and Air Humid Low Then openfull (twobyfive) (3)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->v_hot($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->twobyfive($alpha[$n]);

          // R42 Soil Moist Adq_wet dan Air Temprature V.Hot and Air Humid Medium Then openfull (twobyfive) (3)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->v_hot($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->twobyfive($alpha[$n]);
            
          // R43 Soil Moist Adq_wet dan Air Temprature V.Hot and Air Humid High Then seventyfive (onebyfive) (2)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->v_hot($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R44 Soil Moist Adq_wet dan Air Temprature V.Hot and Air Humid Extreme Then seventyfive (onebyfive) (1)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->v_hot($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            

// ==============================================================================

          // R45 Soil Moist Adq_wet dan Air Temprature Hot and Air Humid Low Then seventyfive (twobyfive) (3)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->hot($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->twobyfive($alpha[$n]);
            
          // R46 Soil Moist Adq_wet dan Air Temprature Hot and Air Humid Medium Then seventyfive (twobyfive) (3)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->hot($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->twobyfive($alpha[$n]);
            
          // R47 Soil Moist Adq_wet dan Air Temprature Hot and Air Humid High Then fifty (onebyfive) (3)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->hot($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R48 Soil Moist Adq_wet dan Air Temprature Hot and Air Humid Extreme Then fifty (onebyfive) (2)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->hot($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
// ==============================================================================

          // R49 Soil Moist Adq_wet dan Air Temprature Normal_temp and Air Humid Low Then (onebyfive)  (3)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->normal_temp($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R50 Soil Moist Adq_wet dan Air Temprature Normal_temp and Air Humid Medium Then (onebyfive)  (2)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->normal_temp($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);     
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';       
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R51 Soil Moist Adq_wet dan Air Temprature Normal_temp and Air Humid High Then ten (onebyfive) (1)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->normal_temp($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R52 Soil Moist Adq_wet dan Air Temprature Normal_temp and Air Humid Extreme ten (onebyfive) (1)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->normal_temp($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            

// ==============================================================================

          // R53 Soil Moist Adq_wet dan Air Temprature Cold and Air Humid Low Then twentyfive (onebyfive) (3)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->cold($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R54 Soil Moist Adq_wet dan Air Temprature Cold and Air Humid Medium Then ten (onebyfive) (3)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->cold($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R55 Soil Moist Adq_wet dan Air Temprature Cold and Air Humid High Then ten (onebyfive) (2)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->cold($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R56 Soil Moist Adq_wet dan Air Temprature Cold and Air Humid Extreme Then ten (onebyfive) (1)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->cold($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            

// ==============================================================================

          // R57 Soil Moist Adq_wet dan Air Temprature v_cold and Air Humid Low Then ten (onebyfive) (3)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->v_cold($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R58 Soil Moist Adq_wet dan Air Temprature v_cold and Air Humid Medium Then ten (onebyfive) (2)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->v_cold($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R59 Soil Moist Adq_wet dan Air Temprature v_cold and Air Humid High Then ten (onebyfive) (1)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->v_cold($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);     
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R60 Soil Moist Adq_wet dan Air Temprature v_cold and Air Humid Extreme Then ten (onebyfive) (1)
            $miuSoilMoist = $this->adq_wet($soil_moisture);
            $miuAirTemp = $this->v_cold($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            

// ==============================================================================

// ##############################################################################

// ==============================================================================

          // R61 Soil Moist Satruated dan Air Temprature V.Hot and Air Humid Low Then fifty (onebyfive) (2)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->v_hot($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R62 Soil Moist Satruated dan Air Temprature V.Hot and Air Humid Medium Then fifty (onebyfive) (2)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->v_hot($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R63 Soil Moist Satruated dan Air Temprature V.Hot and Air Humid High Then twentyfive (onebyfive) (1)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->v_hot($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R64 Soil Moist Satruated dan Air Temprature V.Hot and Air Humid Extreme Then twentyfive (onebyfive) (1)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->v_hot($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
// ==============================================================================

          // R65 Soil Moist Satruated dan Air Temprature Hot and Air Humid Low Then fifty (onebyfive) (2)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->hot($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R66 Soil Moist Satruated dan Air Temprature Hot and Air Humid Medium Then twentyfive (onebyfive) (2)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->hot($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R67 Soil Moist Satruated dan Air Temprature Hot and Air Humid High Then twentyfive (onebyfive) (1)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->hot($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R68 Soil Moist Satruated dan Air Temprature Hot and Air Humid Extreme Then twentyfive (onebyfive) (1)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->hot($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
// ==============================================================================

          // R69 Soil Moist Satruated dan Air Temprature Normal_temp and Air Humid Low Then twentyfive (onebyfive) (3)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->normal_temp($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R70 Soil Moist Satruated dan Air Temprature Normal_temp and Air Humid Medium Then twentyfive (onebyfive) (2)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->normal_temp($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R71 Soil Moist Satruated dan Air Temprature Normal_temp and Air Humid High Then ten (onebyfive) (2)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->normal_temp($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R72 Soil Moist Satruated dan Air Temprature Normal_temp and Air Humid Extreme Then ten (onebyfive) (1)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->normal_temp($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            

// ==============================================================================

          // R73 Soil Moist Satruated dan Air Temprature Cold and Air Humid Low Then ten (onebyfive) (2)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->cold($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R74 Soil Moist Satruated dan Air Temprature Cold and Air Humid Medium Then ten (onebyfive) (2)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->cold($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R75 Soil Moist Satruated dan Air Temprature Cold and Air Humid High Then ten (onebyfive) (1)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->cold($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R76 Soil Moist Satruated dan Air Temprature Cold and Air Humid Extreme Then ten (onebyfive) (1)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->cold($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);

// ==============================================================================

          // R77 Soil Moist Satruated dan Air Temprature v_cold and Air Humid Low Then ten (onebyfive) (2)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->v_cold($air_temprature);
            $miuAirHumid = $this->low($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R78 Soil Moist Satruated dan Air Temprature v_cold and Air Humid Medium Then ten (onebyfive) (2)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->v_cold($air_temprature);
            $miuAirHumid = $this->medium($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R79 Soil Moist Satruated dan Air Temprature v_cold and Air Humid High Then ten (onebyfive) (1)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->v_cold($air_temprature);
            $miuAirHumid = $this->high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            
          // R80 Soil Moist Satruated dan Air Temprature v_cold and Air Humid Extreme Then ten (onebyfive) (1)
            $miuSoilMoist = $this->satruated($soil_moisture);
            $miuAirTemp = $this->v_cold($air_temprature);
            $miuAirHumid = $this->ext_high($air_humidity);
            $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
            sort($miu);            
            $alpha[++$n] = $miu[0];
            // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
            $z[$n] = $this->onebyfive($alpha[$n]);
            

// ==============================================================================
            // dd('Nilai Z :', $z,' Alpha : ', $alpha);



          //  -------------------------------- Defuzzyfication ------------------------------
            $result = 0;
            $pembagi = 0;
            for ($j=0; $j < count($alpha); $j++) { 
                $result += $z[$j] * $alpha[$j];
                $pembagi += $alpha[$j];
            }
            // dd($result, $pembagi);
            if ($pembagi == 0) {
                $result = 0;
            }else{
                $result = $result / $pembagi;   
            }
            number_format($result, 1);
            DB::table('normalized')->where('id_data', $i)->update([
                'label' => number_format($result, 1).'%'
            ]);

            $i++;
        }
        // dd($hasil);
    }

    public function coba()
    {
        return "Coba";
    }

    public function website(Request $request){

        // $user = User::find(1);
        // $expirationDate =  $user->created_at->addYears(1);
        // // 2019-04-05 06:59:07
        // $lastUpdate = $user->updated_at->format('d, M Y H:i');

      
        $companies =  Temp_company::orderBy('temp_company_time', 'DESC');
        $jobs = Job::orderBy('job_time', 'DESC');

        if (!empty($request->category == 'LI')) {
            $companies->where('temp_company_code', '=', 'LI');
        }elseif (!empty($request->category == 'GL')) {
            $companies->where('temp_company_code', '=', 'GL');
        }elseif (!empty($request->category == 'JO')) {
            $companies->where('temp_company_code', '=', 'JO');
        }elseif (!empty($request->category == 'LO')){
            $companies->where('temp_company_code', '=', 'LO');
        }else{
            $companies;
        }     

        if (isset($request->due_date)) {

            $parts = explode(' - ' , $request->due_date);
            $start_date = $parts[0];
            $end_date = $parts[1];

            $mulai_tanggal = Carbon::parse($start_date)->format('Y-m-d') . ' 00:00:01';
            $akhir_tanggal = Carbon::parse($end_date)->format('Y-m-d') . ' 23:59:59';
            // dd($mulai_tanggal, $akhir_tanggal);
            if ($request->category == 'LI') {
                $companies =  $companies->where('temp_company_code' ,'=', 'LI')->whereBetween('created_at', [$mulai_tanggal, $akhir_tanggal])->get();
                // dd($companies, $mulai_tanggal, $akhir_tanggal);
            }elseif ($request->category == 'GL') {
                $companies =  $companies->where('temp_company_code' ,'=', 'GL')->whereBetween('created_at', [$mulai_tanggal, $akhir_tanggal])->get();
            }else{
                $companies =  $companies->where('temp_company_code' ,'=', 'GL')->whereBetween('created_at', [$mulai_tanggal, $akhir_tanggal])->get();
            }
        } else {
            $companies = $companies->get();
            $jobs = $jobs->get();
        }

        return view('checks.website', [
             'companies' => $companies,
             'jobs' => $jobs
         ])->with(['page' => 'Check']);
 
    }

    public function Glints($data_html)
    {
        $pisah_list = explode('CompactOpportunityCardsc__CompactJobCard-sc-1xtox99-1', $data_html);

        echo "<h4>Total : ".(count($pisah_list)-1)."</h4>";
        echo "<table>";
        for($a=1; $a<count($pisah_list); $a++) {
            $pisah_job     = explode('gtm-job-card-job-title', $pisah_list[$a]);
            $pisah_loc     = explode('CompactOpportunityCardsc__OpportunityInfo-sc-1xtox99-13', $pisah_list[$a]);
            $pisah_time    = explode('CompactOpportunityCardsc__OpportunityMeta-sc-1xtox99-14', $pisah_list[$a]);
            $pisah_company = explode('CompactOpportunityCardsc__CompanyLink-sc-1xtox99-9', $pisah_list[$a]);
            $pisah_web     = explode('href=', $pisah_company[1]);
            
            $jobs          = explode('>', $pisah_job[1]);
            $loc      = ltrim($jobs[14]," ");
            $loc      = rtrim($loc,"</p");
            
            $jobs          = explode('<', $jobs[1]);
            $jobs          = trim($jobs[0]);
            
            $company_big   = explode('>', $pisah_company[1]);
            $company       = explode('<', $company_big[1]);
            $company       = trim($company[0]);
            $web           = explode('href=', $company_big[0]);
            $web           = str_replace('"', '', $web[1]);
            $web           = str_replace("'", "", $web);
            // $web           = "<a href='https://glints.com".$web."'>".str_replace("/id/companies/", "", $web)."</a>";
            $web           = 'https://glints.com'.$web;
            
            $loc           = explode('>', strip_tags($pisah_loc[1]));
            $loc           = trim($loc[1]);

            $time          = explode('>', strip_tags($pisah_time[1]));
            $time          = trim($time[1]);
            $time          = str_replace('Updated ', '', $time);
            $time          = str_replace(' ago', '', $time);
            $time          = explode(' ', $time);
            if ($time[1] == 'hour' || $time[1] == 'hours') {
                if ($time[0] == 'a') {
                    $time[0] = 1;
                }
                $time = date('Y-m-d');
            }else if($time[1] == 'day' || $time[1] == 'days'){
                if ($time[0] == 'a') {
                    $time[0] = 1;
                }
                $time = date("Y-m-d", strtotime("-".$time[0]." day"));
            }else if ($time[1] == 'month' || $time[1] == 'months') {
                if ($time[0] == 'a') {
                    $time[0] = 1;
                }
                $time = date("Y-m-d", strtotime("-".$time[0]." months"));
                // dd($time);
            }
            
            if ($loc != 'Yogyakarta' && $loc != 'Jogjakarta' && $loc != 'Sleman' && $loc != 'Bantul') {
                $times[] = $time;
                $location[] = $loc;
                $job[] = $jobs;
                $comp[] = $company;
                $url[] = $web;
            }
            
            // echo "<tr><td>".$jobs."</td><td>".$company."</td><td>".$loc."</td><td>".$time."</td><td>".$web."</td></tr>";
        }

        $data = array(
            'comp' => $comp,
            'times' => $times,
            'location' => $location,
            'job' => $job,
            'url' => $url
        );
        $data = json_decode(json_encode($data));
                // echo "</table>";
        // dd($times, $job, $comp, $url, $data);

        return $data;
    }

    public function LinkedIn($data)
    {

        $pisah_list = explode('<artdeco-entity-lockup-content', $data);
        if (count($pisah_list)==1) {
            return 'null';
        }


        for($a=1; $a<count($pisah_list); $a++) {
            $pisah_job     = explode('job-card-search__link-wrapper', $pisah_list[$a]);
            $pisah_company = explode('<h4', $pisah_job[1]);
            $pisah_link    = explode('<a', $pisah_company[1]);
            
            
            $jobs          = strip_tags(str_replace('js-focusable disabled ember-view"> ', '', $pisah_company[0]));
            $jobs          = str_replace('Promoted', '', $jobs);
            $jobs          = trim($jobs);
            // dd($jobs);
            
            $company       = explode('job-card-search__company-name-link', $pisah_company[1]);
            $loc           = $company[1];
            $loc           = explode('<!---->', $loc);
            $loc           = $loc[2];
            $loc           = str_replace('"', '', $loc);
            $loc           = ltrim($loc);
            $loc           = str_replace('</span>', '', $loc);
            $loc           = trim($loc);
            $loc           = explode(',',$loc);
            $loc           = $loc[0];
            // dd($loc);
            $company       = explode('job-card-search__location', $company[1]);
            $company       = strip_tags(str_replace('ember-view"> ', '', $company[0]));
            $company       = str_replace('loading ', '', $company);
            $company       = str_replace('t-normal ', '', $company);
            $company       = trim($company);

            $link = explode('href=', $pisah_link[1]);
            $link = explode('id=', $link[1]);
            $link = trim(str_replace('"', '', $link[0]));

            $time = explode('datetime="', $pisah_list[$a]);
            $time = explode('">', $time[1]);
            $date = $time[1];
            $date = explode('</time>', $date);
            $date = $date[0];
            // dd($date);
            $time = $time[0];
            // $time = strtotime($time);            

            if($link=="#") {
                $link = "-";
            }
            else {
                $link = 'https://www.linkedin.com'.$link;
            }

            if ($loc != 'Yogyakarta' && $loc != 'Jogjakarta' && $loc != 'Sleman' && $loc != 'Bantul') {
                $times[] = $time;
                $location[] = $loc;
                $job[] = $jobs;
                $comp[] = $company;
                $url[] = $link;
            }
            // dd($company);
        }

        $data = array(
            'comp' => $comp,
            'times' => $times,
            'location' => $location,
            'job' => $job,
            'url' => $url
        );
        $data = json_decode(json_encode($data));
        // dd($data);

        return $data;
    }




    public function domain(Request $request)
    {   

        $data = $request->company_domain;
        $company = $request->company_name;

        foreach (array_combine($data, $company) as $domain => $perusahaan) {
      

        $hostname = preg_replace('~^https?://(www\\.)?~i', null, $domain); // shift scheme and www
        $hostname = preg_replace('~#.*$~', null, $hostname); // pop target
        $hostname = preg_replace('~\\?.*$~', null, $hostname); // pop query string
        $hostname = preg_replace('~/.*$~', null, $hostname); // pop uri
        // dd($perusahaan, $domain);

        $domains = DB::table('t_company_domains')->where('domain_name', '=', $hostname)->first();


        $companies = DB::table('t_companies')->where('company_name', '=', $perusahaan)->first();

        

        if($companies === null && $domains === null && $hostname != '-'){
            
            // $create = DB::table('temp_companies')->select('created_at')->first();
            
            $company = T_company::create([
              'company_name' => $perusahaan
              // 'created_at' => $create
            ]);
            $domain = T_company_domain::create([
                'domain_name' => $hostname,
                'company_id' => $company->company_id
            ]);
            $inputer = Inputer::Create([
                'company_id' => $company->company_id
            ]);

            $email = T_company_email::create([
                 'company_id' => $company->company_id
             ]);

            $pic = T_company_pic::Create([           
                 'company_id' => $company->company_id
             ]);

            $temp_company = DB::table('temp_companies')->where('temp_company_name', 'LIKE', '%'.$perusahaan.'%')->first();
            // dd($temp_company, $perusahaan);
            $job = Job::where('temp_company_id',$temp_company->temp_company_id)->first();
            // $job = DB::table('jobs')->where('temp_company_id', '=', $temp_company->temp_company_id)->first();

            

            $job->update([
                'company_id' => $company->company_id
            ]);

        }
        
        else if($domains){
            $temp_company = DB::table('temp_companies')->where('temp_company_name', 'LIKE', '%'.$perusahaan.'%')->first();
            $job = Job::where('temp_company_id',$temp_company->temp_company_id)->first();
            // dd($companies);
            $job->update([
                'company_id' => $domains->company_id
            ]);
          
        }

      }

      return redirect(route('check.website'))->with(['page' => 'Check']);
        
    }



public function checkInput(Request $request){

$url = $request->domain_name;
  

$hostname = preg_replace('~^https?://(www\\.)?~i', null, $url); // ambil https// & www

$hostname = preg_replace('~#.*$~', null, $hostname); // ambil target domain

$hostname = preg_replace('~\\?.*$~', null, $hostname); // pop query string

$hostname = preg_replace('~/.*$~', null, $hostname); // pop uri

$hostname = preg_replace('~^(www\\.)?~i', null, $hostname); // ambil www




    $companies = DB::table('t_companies')->where('company_name', '=', $request->company_name)->first();

    $domains = DB::table('t_company_domains')->where('domain_name', '=', $hostname)->first();

    //    
     $data1 = $request->company_name;
     $data2 = $hostname;

    if($companies === null && $domains === null ){
        return redirect(route('getForm',  ['data1'=> $data1 , 'data2'=> $data2]))->with(['success' => 'data dapat Didaftarkan']);
    }else{
        return redirect()->back()->with(['error' => 'data terdaftar']);
    }
    
    
}

  public function getForm($data1, $data2){

    return view('checks.form', compact('data1', 'data2'))->with(['page' => 'Check']);  
 }


    public function store(Request $request){

        $this->validate($request, [
           
            'company_name' => 'nullable|string|max:50|unique:t_companies',
            'company_type' => 'nullable|in:tekno,nontekno',
        //     'company_field' => 'string',
        //     'company_desc' => 'string',
        //     'company_address' => 'string',
            'company_telpon_office' => 'nullable|string|max:50',
        //     'company_faximile' => 'string',
        //     'company_status' => 'in:aktif,nonaktif',
        
        // //Company Contact
            'domain_name' => 'nullable|string:max:50|unique:t_company_domains',
            'email_name' => 'nullable|email:max50|unique:t_company_emails',
        
        // //PIC
        //     'pic_name' => 'string',
        //     'pic_position' => 'string',
        //     'pic_division' => 'string',
            'pic_nohp' => 'nullable|numeric',
            'pic_email' => 'nullable|email',
        //     'pic_sosmed' => 'string',
            'pic_status' => 'nullable|in:aktif,nonaktif'
   
        ]);

       $company = T_company::create([
           'company_name' => $request->company_name,
           'company_type' => $request->company_type,
           'company_field' => $request->company_field,
           'company_desc' => $request->company_desc,
           'company_address' => $request->company_address,
           'company_telpon_office' => $request->company_telpon_office,
           'company_faximile' => $request->company_faximile,
           'company_status' => $request->company_status,
           
       ]);



       $domain = T_company_domain::create([
           'domain_name' => $request->domain_name,
           'company_id' => $company->company_id
       ]);
   
       $email = T_company_email::create([
           'email_name' => $request->email_name,
           'company_id' => $company->company_id
       ]);

       $pic = T_company_pic::create([           
        'pic_name' => $request->pic_name,
        'pic_position' => $request->pic_position,
        'pic_division' => $request->pic_division,
        'pic_nohp' => $request->pic_nohp,
        'pic_email' => $request->pic_email,
        'pic_sosmed' => $request->pic_sosmed,
        'pic_status' => $request->pic_status,
        'company_id' => $company->company_id
    ]);
   
       $pic = T_company_pic::create([           
           'pic_name' => $request->excel_pic_name,
           'pic_position' => $request->excel_pic_position,
           'pic_division' => $request->excel_pic_division,
           'pic_nohp' => $request->excel_pic_nohp,
           'pic_email' => $request->excel_pic_email,
           'pic_sosmed' => $request->excel_pic_sosmed,
           'pic_status' => $request->excel_pic_status,
           'company_id' => $company->company_id
       ]);

       $inputer = Inputer::Create([
            'company_id' => $company->company_id
        ]);

       // dd("baru masuk kontroller");
     
       return redirect(route('check.index'))->with(['success' => 'Data berhasil diinput']);
        
    }

    public function edit($company_id){
        $company = T_company::findOrfail($company_id);
        return view('checks.edit', compact('company'))->with(['page' => 'Check']);
    }

    public function update(Request $request, $company_id){

        $this->validate($request, [
        
            'company_name' => 'nullable|string|max:50',
            'company_type' => 'nullable|in:tekno,nontekno',
            'company_telpon_office' => 'nullable|string|max:50',
        
        // //Company Contact
            'domain_name[]' => 'nullable|string:max:50|unique:t_company_domains',
            'email_name[]' => 'nullable|email:max50|unique:t_company_emails',
        
        // //PIC
            'pic_nohp' => 'nullable|numeric',
            'pic_email' => 'nullable|email',
            'pic_status' => 'nullable|in:aktif,nonaktif'
   
        ]);

        $company = T_company::findOrFail($company_id);
        $domain = T_company_domain::where('company_id',$company_id)->first();
        $email = T_company_email::where('company_id',$company_id)->first();
        $pic = T_company_pic::where('company_id',$company_id)->first();

        // @foreach ($rows as $row)
        // @if ($loop->first) @continue @endif
        // {{ $row->name }}<br/>
        // @endforeach


        $url = $request->domain_name;

        if ($url == null) {
            return redirect()->back()->with(['error' => 'data tidak boleh kosong']);
        }

        foreach ($url as $key => $url_domain) {
            if ($key > 0) {


                // $hostname = preg_replace('~^https?://(www\\.)?~i', null, $url_domain); // shift scheme and www
                // $hostname = preg_replace('~#.*$~', null, $hostname); // pop target
                // $hostname = preg_replace('~\\?.*$~', null, $hostname); // pop query string
                // $hostname = preg_replace('~/.*$~', null, $hostname); // pop uri
                // $hostname = preg_replace('~^(www\\.)?~i', null, $hostname); // ambil www

                $domain = T_company_domain::where('domain_name', '=', $url_domain)->first();

                if ($domain == null) {  

                //     if (is_array($hostname)){

                //         if ($company->domain()->delete()) {    
                //         foreach($hostname as $domain_name) {
                //         if($domain_name) {
                //             $company->domain()->save(new T_company_domain(['domain_name' => $domain_name]));
                //            }
                //         }
                //     }
                // }
                    
                    if($company->domain()->delete()) {

                        $url = $request->domain_name;


                        if ($key > 0) {

                        $hostname = preg_replace('~^https?://(www\\.)?~i', null, $url_domain); // shift scheme and www
                        $hostname = preg_replace('~#.*$~', null, $hostname); // pop target
                        $hostname = preg_replace('~\\?.*$~', null, $hostname); // pop query string
                        $hostname = preg_replace('~/.*$~', null, $hostname); // pop uri
                        $hostname = preg_replace('~^(www\\.)?~i', null, $hostname); // ambil www

                    foreach($request->domain_name as $domain_name) {
                        
                        if($domain_name) {

                            $company->domain()->save(new T_company_domain(['domain_name' => $domain_name]));

                        }
                    }
                }
            }
                
            } else {
                // dd("data terdaftar");
                return redirect()->back()->with(['error' => 'Data terdaftar']);
            }
            if ($key < 1) {
            

                $hostname = preg_replace('~^https?://(www\\.)?~i', null, $url_domain); // shift scheme and www
                $hostname = preg_replace('~#.*$~', null, $hostname); // pop target
                $hostname = preg_replace('~\\?.*$~', null, $hostname); // pop query string
                $hostname = preg_replace('~/.*$~', null, $hostname); // pop uri
                $hostname = preg_replace('~^(www\\.)?~i', null, $hostname); // ambil www

                
                $domain = T_company_domain::where('domain_name', '=', $hostname)->first();
                
                if ($domain === null) {  
                    dd("data terupdate1");
                if($company->domain()->delete()) {
                    
                    foreach($request->domain_name as $domain_name) {
                        if($domain_name) {
                            
                            $company->domain()->save(new T_company_domain(['domain_name' => $domain_name]));
                        }
                    }
                }
                
            }
            
            else {
                dd("data terdaftar1");
                return redirect()->back()->with(['error' => 'Data terdaftar']);
            }
            
        }
    
    }
    
}
                if($company->email()->delete()) {
                    foreach($request->email_name as $email_name) {
                        if($email_name) {
                    $company->email()->save(new T_company_email(['email_name' => $email_name]));
                }
            }
        }
        

        $pic->pic_name = $request->pic_name;
        $pic->pic_position = $request->pic_position;
        $pic->pic_division = $request->pic_division;
        $pic->pic_nohp = $request->pic_nohp;
        $pic->pic_email = $request->pic_email;
        $pic->pic_sosmed = $request->pic_sosmed;
        $pic->pic_status = $request->pic_status;

        $company->pic()->save($pic);

        $company->update([
           'company_name' => $request->company_name,
           'company_type' => $request->company_type,
           'company_field' => $request->company_field,
           'company_desc' => $request->company_desc,
           'company_address' => $request->company_address,
           'company_telpon_office' => $request->company_telpon_office,
           'company_faximile' => $request->company_faximile,
           'company_status' => $request->company_status
        ]);

        
        // $domain->update([
        //     'domain_name' => $request->domain_name,            
        // ]);

        
        // $email->update([
        //     'email_name' => $request->email_name,
        // ]);

        // dd();

        
        $pic->update([
           'pic_name' => $request->excel_pic_name,
           'pic_position' => $request->excel_pic_position,
           'pic_division' => $request->excel_pic_division,
           'pic_nohp' => $request->excel_pic_nohp,
           'pic_email' => $request->excel_pic_email,
           'pic_sosmed' => $request->excel_pic_sosmed,
           'pic_status' => $request->excel_pic_status,

        ]);
            
        return redirect(route('check.index'))->with(['success' => 'User berhasil diupdate']);
        
    }


  


    // public function show(Request $request){
    //     $showDetail = T_company::find($request->company_id);

    //     return response()->json($showDetail);
    // }


    // public function destroy($company_id){

    //     $company = T_company::findOrFail($company_id);
    //     $company->delete();
    //     return redirect()->back()->with(['success' => 'data berhasil dihapus!']);
    // }


    public function detail($company_id) {
        $companies = T_company::findOrFail($company_id);
        return view('checks.detail', compact('companies'));
    }


    public function checkExcel(Request $request){
      // ini_set('max_input_vars', 999999999);
      // print_r($request);
      // dd($request->request, count( $request->request));

        $this->validate($request, [
            'company_type' => 'nullable|in:tekno,nontekno',
            'company_telpon_office' => 'nullable|string:max50',        
            'company_status' => 'in:aktif,nonaktif',
        
        // //Company Contact
            'domain_name' => 'nullable|string:max:50|unique:t_company_domains',
            'email_name' => 'nullable|email:max50|unique:t_company_emails',
        
        // //PIC
            'pic_nohp' => 'nullable|numeric',
            'pic_email' => 'nullable|email',
            'pic_status' => 'nullable|in:aktif,nonaktif'
   
        ]);

      if (!$request->company_domain) {
        return redirect()->back()->with(['kosong' => 'data terdaftar']);
      }
      if (count( $request->request) < 18) {
        return redirect()->back()->with(['template' => 'data terdaftar']);
      }

      
      $company = $request->company_name;
      $company_type = $request->excel_type;
      $company_field = $request->excel_field;
      $company_desc = $request->excel_desc;
      $company_address = $request->excel_address;
      $company_telpon_office = $request->excel_telpon_office;
      $company_faximile = $request->excel_faximile;
      $company_status = $request->excel_status;
      $data = $request->company_domain;
      $company_email = $request->excel_email;
      //pic
      $pic_name = $request->excel_pic_name;
      $pic_position = $request->excel_pic_position;
      $pic_division = $request->excel_pic_division;
      $pic_nohp = $request->excel_pic_nohp;
      $pic_email = $request->excel_pic_email;
      $pic_sosmed = $request->excel_pic_sosmed;
      $pic_status = $request->excel_pic_status;


      foreach (array_combine($data, $company) as $url => $perusahaan) {
      

        $hostname = preg_replace('~^https?://(www\\.)?~i', null, $url); // shift scheme and www
        $hostname = preg_replace('~#.*$~', null, $hostname); // pop target
        $hostname = preg_replace('~\\?.*$~', null, $hostname); // pop query string
        $hostname = preg_replace('~/.*$~', null, $hostname); // pop uri

        $domains = DB::table('t_company_domains')->where('domain_name', '=', $hostname)->first();


        $companies = DB::table('t_companies')->where('company_name', '=', $perusahaan)->first();

        if($companies === null && $domains === null ){
          $excel[] = 1;
        }
        
        else{
          $excel[] = 0;
        }

        $dom[] = $hostname;
        $comp[] = $perusahaan;
      }

      // dd("berhasil lewat semua",$company, $company_type, $company_field, $company_desc, $company_address, $company_telpon_office, $company_faximile, $company_status, $data, $company_email);

  // dd($dom,$comp);    

      return redirect(route('check.index'))        
        ->with(['checked' => $excel])
        ->with(['data' => $dom])
        ->with(['company_type' => $company_type])
        ->with(['company_field' => $company_field])
        ->with(['company_desc' => $company_desc])
        ->with(['company_address' => $company_address])
        ->with(['company_telpon_office' => $company_telpon_office])
        ->with(['company_faximile' => $company_faximile])
        ->with(['company_status' => $company_status])
        ->with(['company' => $comp])
        ->with(['company_email' => $company_email])
        //pic
        ->with(['pic_name' => $pic_name])
        ->with(['pic_position' => $pic_position])
        ->with(['pic_division' => $pic_division])
        ->with(['pic_nohp' => $pic_nohp])
        ->with(['pic_email' => $pic_email])
        ->with(['pic_sosmed' => $pic_sosmed])
        ->with(['pic_status' => $pic_status]);
  }




  public function excel(Request $request){
        
        $user = Auth::user();

        $ambil = T_company::where('created_by', '=', $user->user_name)->first();

        $karyawan = T_user::select('user_id','user_name')->where('user_name', $user->user_name)->first();
        $t_salary = DB::table('t_salary')->select('*')->get();
        $id_salary = 0;

        foreach ($t_salary as $row) {
    
           $tgl_berlaku = $row->tgl_berlaku;
           $tgl_deadline = $row->tgl_deadline;
           $now = date('d-M-Y');

           if (strtotime($now) < strtotime($tgl_berlaku)) {
             echo "Tidak ada fee yang valid | ";
             // dd($id_salary);
           } elseif (strtotime($now) >= strtotime($tgl_berlaku) && strtotime($now) <= strtotime($tgl_deadline)) {
            $id_salary = $row->salary_id;
             echo "diantara  | ";
             // dd($id_salary);
           } elseif (strtotime($now) > strtotime($tgl_berlaku) && strtotime($now) > strtotime($tgl_deadline)) {
             echo "Semua fee kadaluarsa  | ";
             // dd($id_salary);
           } else {
             echo "Gak masuk kemna mana | ";
             // dd($id_salary);
           }
        }

        // dd($id_salary);

        // dd($id_salary, $now);

        $history_salary = History_salary::where('user_id', $user->user_id)
                    ->orderBy('history_id', 'DESC')
                    ->get();


        $count = count($history_salary);

        $harga_validasi = DB::table('t_salary')->where('salary_id', $id_salary)->first();
        // dd($harga_validasi, $id_salary);

        if (!$harga_validasi) {
          $harga_validasi = array(
            'salary_value' => 0
          );
          $harga_validasi = json_decode(json_encode($harga_validasi));
        }
        // dd($harga_validasi->salary_value);

        $gaji = 0;
        // dd($harga_validasi, $history_salary);

        // $nama = $ambil->company_name;
        
// ============================================================================================================================

// ============================================================================================================================
    

    if (!$request->excel_name) {
        return redirect()->back()->with(['kosong' => 'data terdaftar']);
      }

    $error_count = 0;
    $success_count = 0;
    
    for ($i=0; $i < count($request->excel_name); $i++) {     
      $domains = DB::table('t_company_domains')->where('domain_name', '=', $request->excel_domain[$i])->first();
      $companies = DB::table('t_companies')->where('company_name', '=', $request->excel_name[$i])->first();

      if($companies === null && $domains === null ){
         $excel[] = 1; 

         if ($request->excel_name[$i] && $request->excel_domain[$i]) {
             $company = T_company::create([   
                 'company_name' => $request->excel_name[$i],
                 'company_type' => $request->excel_type[$i],
                 'company_field' => $request->excel_field[$i],
                 'company_desc' => $request->excel_desc[$i],
                 'company_address' => $request->excel_address[$i],
                 'company_telpon_office' => $request->excel_telpon_office[$i],
                 'company_faximile' => $request->excel_faximile[$i],
                 'company_status' => $request->excel_status[$i]
                 
             ]);
            
            $email = T_company_email::create([
                 'email_name' => $request->excel_email[$i],
                 'company_id' => $company->company_id
             ]);

            $pic = T_company_pic::Create([           
                 'company_id' => $company->company_id,
                 'pic_name' => $request->excel_pic_name[$i],
                 'pic_position' => $request->excel_pic_position[$i],
                 'pic_division' => $request->excel_pic_division[$i],
                 'pic_nohp' => $request->excel_pic_nohp[$i],
                 'pic_email' => $request->excel_pic_email[$i],
                 'pic_sosmed' => $request->excel_pic_sosmed[$i],
                 'pic_status' => $request->excel_pic_status[$i]
             ]);

            $inputer = Inputer::Create([
                  'company_id' => $company->company_id
              ]);

            $domain = T_company_domain::create([
                 'domain_name' => $request->excel_domain[$i],
                 'company_id' => $company->company_id
                 
             ]);
        }
        
        $success_count++;
      }
      
      else{
        $excel[] = 0;
        $error_count++;
      }

      // $dom[] = $request->excel_domain[$i];
      // $comp[] = $request->excel_name[$i];


    }

    if ($harga_validasi->salary_value != 0) {

      // dd("Masuk ada salary value");
      

        $inputerValidasi = DB::table('t_companies')
            ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
            ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
            ->select('t_company_domains.domain_validation','t_company_emails.email_validation','t_companies.*')
            ->where('t_companies.company_validation', '>=', 24576)
            ->where('t_company_emails.email_validation', '=', true)
            ->where('t_company_domains.domain_validation', '=', true)
            ->where('t_companies.deleted_at', '=', null)
            ->where('t_companies.created_by', '=',$user->user_name)
            ->get();

        $input = DB::table('t_companies')
            ->select('*')
            ->where('t_companies.created_by', '=',$karyawan->user_name)
            ->get();
            

        if (count($history_salary) == 0) { //Buat baru history
            // dd($harga_validasi);
            $gaji = count($input) * $harga_validasi->salary_value;
            $create_history = History_salary::create([
                'user_id' => $karyawan->user_id,
                'valid_data' => count($inputerValidasi),
                'total_salary' => $gaji,
                'salary_id' => $harga_validasi->salary_id
            ]);

            // dd("Masuk Buat baru history");
            
        } elseif ($history_salary[0]->user_id == $karyawan->user_id) {


            if (count($history_salary) > 1 && $harga_validasi->salary_id == $history_salary[0]->salary_id) {  //cek kesalahan disini
                $update_history = $history_salary[0]->update([
                   'valid_data' => count($inputerValidasi),
                ]);
                                // Aktifkan ketika menambahkan bonus //
                // ========================================================================== //
                // $selisih = $history_salary[0]->valid_data - $history_salary[1]->valid_data; 

                $harga_before = DB::table('t_salary')->where('salary_id', $history_salary[1]->salary_id)->first();

                $selisih = (count($input) - ($history_salary[1]->total_salary / $harga_before->salary_value)); 
                $gaji = ($selisih * $harga_validasi->salary_value) + $history_salary[1]->total_salary; 

                $update_history = $history_salary[0]->update([
                   'total_salary' => $gaji,
                ]);

                // dd("Masuk update id salary sama", $harga_validasi, $history_salary);
            }

            elseif (count($history_salary) == 1 && $harga_validasi->salary_id == $history_salary[0]->salary_id) { //update history
                
                
                $update_history = $history_salary[0]->update([
                   'valid_data' => count($inputerValidasi),
                ]);

                // $gaji = count($inputerValidasi) * $harga_validasi->salary_value;

                $gaji = count($input) * $harga_validasi->salary_value;

                $update_history = $history_salary[0]->update([
                   'total_salary' => $gaji,
                ]);

                // dd("Masuk history salary cuma satu.", $gaji, $harga_validasi, $history_salary);
            
            }
            else{
                $create_history = History_salary::create([
                   'user_id' => $karyawan->user_id,
                   'valid_data' => count($inputerValidasi),
                   'salary_id' => $harga_validasi->salary_id
               ]);

                if (count($history_salary) == 1) {

                    // $selisih = $create_history->valid_data - $history_salary[0]->valid_data;
                    // $gaji = ($selisih * $harga_validasi->salary_value) + $history_salary[0]->total_salary;    // Create baru history

                    $harga_before = DB::table('t_salary')->where('salary_id', $create_history->salary_id)->first();

                    $selisih = (count($input) - ($history_salary[0]->total_salary / $harga_before->salary_value)); 
                    $gaji = ($selisih * $harga_validasi->salary_value) + $history_salary[0]->total_salary; 

                    $update_history = $create_history->update([
                       'total_salary' => $gaji,
                    ]);
                    
                    // dd("Masuk history salary cuma satu. terus bikin lagi", $selisih, $gaji,  $harga_validasi, $history_salary);

                }

                
                else{ // cek bagian inii ===============================================================================
                    // $selisih = $create_history->valid_data - $history_salary[0]->valid_data;
                    // $gaji = ($selisih * $harga_validasi->salary_value) + $history_salary[0]->total_salary;    // Create baru history

                    $harga_before = DB::table('t_salary')->where('salary_id', $create_history->salary_id)->first();

                    $selisih = (count($input) - ($history_salary[0]->total_salary / $harga_before->salary_value)); 
                    $gaji = ($selisih * $harga_validasi->salary_value) + $history_salary[0]->total_salary; 
            
                    $update_history = $create_history->update([
                        'total_salary' => $gaji,
                    ]);
                }
                
                // dd("Masuk last else.", $harga_validasi, $history_salary);


            }
            
        }

        // $gaji = count($inputerValidasi) * $harga_validasi->salary_value;

        if ($gaji == 0) {
            dd("Kok gajinya 0 ya??");
        }

      }


    // dd($excel, $dom, $comp);

    // if($error_count > 0){
    //   return redirect()->back()->with(['excel_error' => 'data terdaftar'])
    //   ->with(['checked' => $excel])
    //   ->with(['data' => $dom])
    //   ->with(['company' => $comp]);;
    // }


    // for ($i=0; $i < count($request->excel_name); $i++) { 

    //    $company = T_company::create([   
    //        'company_name' => $request->excel_name[$i],
           
    //    ]);

    //    $domain = T_company_domain::create([
    //        'domain_name' => $request->excel_domain[$i],
    //        'company_id' => $company->company_id
           
    //    ]);

    //   $email = T_company_email::create([
    //        'company_id' => $company->company_id
    //    ]);

    //   $pic = T_company_pic::Create([           
    //        'company_id' => $company->company_id
    //    ]);

    //   $inputer = Inputer::Create([
    //         'company_id' => $company->company_id
    //     ]);

      
    // }

    
      $salary_update = $user->update([
                'salary' => $gaji
            ]);
       return redirect(route('check.index'))
               ->with(['excel_success' => 'Data berhasil diinput'])
               ->with(['error_input' => $error_count])
               ->with(['success_input' => $success_count]);
        
    }
  

}


// =========================================
// Dipakai saat ada permintaan ketika company ada
                // else{
                //     $company_name = Temp_company::Create([
                //       'temp_company_name' => $row,
                //       'temp_company_time' => $times[$i]
                //     ]);

                //     $job_search = Job::where('temp_company_id',$company_name->id)->first();
                //     $job_check = Job::where('job_position','LIKE',$job[$i])
                //                 ->where('temp_company_id',$company_name->id)
                //                 ->first();

                //     if (!$job_check) {
                //         $job_search->update([
                //           // 'temp_company_id' => $company_name->id,
                //           'job_position' => $job_search->job_position .', '. $job[$i],
                //           // 'job_link' => $url[$i],
                //           'job_time' => $times[$i]
                //         ]);    
                //     }else{
                //         if (strtotime($job_check->job_time) < strtotime($times[$i])) {
                //             $company_name = Temp_company::Create([
                //               'temp_company_name' => $row,
                //               'temp_company_time' => $times[$i]
                //             ]);
                //             $job_position = Job::Create([
                //               'temp_company_id' => $company_name->id,
                //               'job_position' => $job[$i],
                //               'job_link' => $url[$i],
                //               'job_time' => $times[$i]
                //             ]);           
                //         }
                //     }
                    
                // }



// =======================================================
// Gatau bagian apa
// if (!$job_check || strtotime($job_check->job_time) < strtotime($data->times[$i])) {
            //     if (strtotime($job_check->job_time) < strtotime($data->times[$i])) {
            //         // dd("waktu masuk sini", $job_check);
            //         // dd("Ternyata kesini", $row);
            //         $company_name = Temp_company::Create([
            //           'temp_company_name' => $row,
            //           'temp_company_time' => $data->times[$i]
            //         ]);

            //         $job_position = Job::Create([
            //           'temp_company_id' => $company_name->id,
            //           'job_position' => $data->job[$i],
            //           'job_link' => $data->url[$i],
            //           'job_time' => $data->times[$i]
            //         ]);
            //     }
            //     // dd("Malah masuk sini", $job_check);
            //     $job_position = Job::Create([
            //       'temp_company_id' => $cek_temp->temp_company_id,
            //       'job_position' => $data->job[$i],
            //       'job_link' => $data->url[$i],
            //       'job_time' => $data->times[$i]
            //     ]);           
            // }
            // else {
            //     $job_kembar = Job::where('job_position','LIKE', '%'.$data->job[$i].'%')
            //                     ->where('temp_company_id',$cek_temp->temp_company_id)
            //                     ->first();
            //     if (!$job_kembar) {
            //         $job_check->update([
            //           'job_position' => $job_check->job_position .', '. $data->job[$i],                  
            //           'job_time' => $data->times[$i]
            //         ]);
            //     }
                
            // }