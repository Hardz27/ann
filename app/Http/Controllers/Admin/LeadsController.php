<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin\T_company;
use App\Admin\T_company_domain;
use App\Admin\T_company_email;
use App\Admin\T_company_pic;
use App\Inputer;
use App\History_salary;
use Illuminate\Support\Carbon;
use Excel;
use App\Exports\LeadsReport;
use App\Exports\exportNewInput;
use App\Exports\exportUnComplite;
use App\Exports\exportComplite;
use App\Exports\exportAllData;
use DB;
use App\T_user;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Auth;

class LeadsController extends Controller
{
    public function index(Request $request){
      

        $users = T_user::get();
        $user = Auth::user();
        $domain = array();
        $email = array();

    if (auth()->user()->can('leads admin')){

        // $newInput = T_company::join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        // ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        // ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        // ->where('t_companies.company_validation', '=', false)
        // ->where('t_companies.deleted_at', '=', null)
        // ->where('t_company_emails.email_validation', '=', false)
        // ->where('t_company_domains.domain_validation', '=', false)
        // ->orderBy('t_companies.created_at', 'DESC')
        // ->orderBy('t_companies.company_name', 'ASC')
        // ->get();
        
        $newInput = T_company::select('*')
            ->where('t_companies.company_validation', '=', false)
            ->where('t_companies.deleted_at', '=', null)
            ->orderBy('t_companies.created_at', 'DESC')
            ->orderBy('t_companies.company_name', 'ASC')
            ->get();

        foreach($newInput as $company) {
            $domain[$company->company_id] = T_company_domain::whereCompanyId($company->company_id)
                ->where('t_company_domains.domain_validation', '=', false)
                ->get();
            $email[$company->company_id] = T_company_email::whereCompanyId($company->company_id)
                ->where('t_company_emails.email_validation', '=', false)
                ->get();
        }

        // $unComplite = DB::table('t_companies')
        // ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        // ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        // ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        // ->where('t_companies.deleted_at', '=', null)

        // ->where(function ($query) {
        //         $query->where('t_companies.company_validation', '>', 0)
        //               ->where('t_companies.company_validation', '<', 24576)
        //               ->where(function ($query1) {
        //                         $query1->Where('t_company_emails.email_validation', '=', true)
        //                                ->orWhere('t_company_domains.domain_validation', '=', true);
        //               });
        //     })
        // ->orWhere(function ($query) {
        //         $query->where('t_companies.company_validation', '>', 0)
        //               ->where('t_companies.company_validation', '<=', 24576)
        //               ->where(function ($query1) {
        //                         $query1->where('t_company_emails.email_validation', '=', null)
        //                                ->orWhere('t_company_domains.domain_validation', '=', null);
        //               });
        //     })
        // ->orWhere(function ($query) {
        //         $query->where('t_companies.company_validation', '>', 0)
        //               ->where('t_companies.company_validation', '<=', 24576)
        //               ->where(function ($query1) {
        //                         $query1->where('t_company_domains.domain_validation', '=', null)
        //                                ->orWhere('t_company_emails.email_validation', '=', null);
        //               });
        //     })
        // ->orWhere(function ($query) {
        //         $query->where('t_companies.company_validation', '=', 0)
        //               ->where(function ($query1) {
        //                         $query1->Where('t_company_emails.email_validation', '=', true)
        //                                ->orWhere('t_company_domains.domain_validation', '=', true);
        //               });
        //     })

        // ->orWhere(function ($query) {
        //         $query->where('t_companies.company_validation', '=', 0)
        //               ->where(function ($query1) {
        //                         $query1->Where('t_company_emails.email_validation', '=', null)
        //                                ->orWhere('t_company_domains.domain_validation', '=', null);
        //               });
        //     })
        // ->orWhere(function ($query) {
        //         $query->where('t_companies.company_validation', '>', 0)
        //               ->where(function ($query1) {
        //                         $query1->where('t_company_emails.email_validation', '=', true)
        //                                ->where('t_company_domains.domain_validation', '=', null)
        //                                ->orWhere('t_company_emails.email_validation', '=', null)
        //                                ->where('t_company_domains.domain_validation', '=', true)
        //                                ->orWhere('t_company_emails.email_validation', '=', null)
        //                                ->where('t_company_domains.domain_validation', '=', null);
        //               });
        //     });
        
        // // ->where('t_companies.deleted_at', '=', null)
        // // ->orderBy('t_companies.created_at', 'DESC')
        // // ->orderBy('t_companies.company_name', 'ASC')
        // // ->get();

        $unComplite = T_company::select('*')
                    ->where('t_companies.company_validation', '>', 0)
                    ->where('t_companies.company_validation', '<', 24576)
                    ->where('t_companies.deleted_at', '=', null)
                    ->orderBy('t_companies.created_at', 'DESC')
                    ->orderBy('t_companies.company_name', 'ASC')
                    ->get();

        foreach($unComplite as $company) {
            $domain[$company->company_id] = T_company_domain::whereCompanyId($company->company_id)
            ->where(function ($query) {
                $query->where('t_company_domains.domain_validation', '>', 0)
                    ->where(function ($query1) {
                                $query1->Where('t_company_domains.domain_validation', '=', null)
                                    ->orWhere('t_company_domains.domain_validation', '=', true);
                    });
            })->get();

            $email[$company->company_id] = T_company_email::whereCompanyId($company->company_id)
            ->orWhere(function ($query) {
                $query->where('t_company_emails.email_validation', '=', true)
                    ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', null)
                                    ->orWhere('t_company_emails.email_validation', '=', true);
                    });
            })->get();
        }

            // ->orWhere(function ($query) {
            //         $query->where('t_companies.company_validation', '>', 0)
            //             ->where('t_companies.company_validation', '<', 24576)
            //             ->where(function ($query1) {
            //                         $query1->Where('t_company_emails.email_validation', '=', null)
            //                             ->orWhere('t_company_domains.domain_validation', '=', null);
            //             });
            //     })
            // ->orWhere(function ($query) {
            //         $query->where('t_companies.company_validation', '=', 0)
            //             ->where(function ($query1) {
            //                         $query1->Where('t_company_emails.email_validation', '=', true)
            //                             ->orWhere('t_company_domains.domain_validation', '=', true);
            //             });
            //     })
            // ->get();


        

    

        // $complite = DB::table('t_companies')
        // ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        // ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        // ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        // ->where('t_companies.company_validation', '>=', 24576)
        // ->where('t_company_emails.email_validation', '=', true)
        // ->where('t_company_domains.domain_validation', '=', true)
        // ->where('t_companies.deleted_at', '=', null)
        // ->orderBy('t_companies.created_at', 'DESC')
        // ->orderBy('t_companies.company_name', 'ASC')
        // ->get();

        
        $complites = T_company::select('*')
            ->where('t_companies.company_validation', '>=', 24576)
            ->where('t_companies.deleted_at', '=', null)
            ->orderBy('t_companies.created_at', 'DESC')
            ->orderBy('t_companies.company_name', 'ASC')
            ->get();

            // dd($complites);

        foreach($complites as $complite) {
            $domain[$complite->company_id] = T_company_domain::whereCompanyId($complite->company_id)
                ->where('t_company_domains.domain_validation', '=', true)
                ->get();
            $email[$complite->company_id] = T_company_email::whereCompanyId($complite->company_id)
                ->where('t_company_emails.email_validation', '=', true)
                ->get();
        }

        
        $allInput = T_company::orderBy('created_at' , 'DESC')
                    ->orderBy('t_companies.company_name', 'ASC')->get();
        
        $newInputCount = $newInput->count();
        $unCompliteCount = $unComplite->where('deleted_at', '=',null)->count();
        // $unCompliteCount -= $error;
        // dd($unCompliteCount);
        $compliteCount = $complites->count();
        $allinputCount =  $newInputCount + $unCompliteCount + $compliteCount;
        // dd($newInputCount, $unCompliteCount, $compliteCount, $allinputCount);
        $val = DB::table('t_companies')->select('company_validation')->get();
        
        $role = T_user::orderBy('user_name', 'ASC')->get();
        $inputer = DB::table('roles')->select('name')->where('name', '=', 'inputer')->get();
        $admin = DB::table('roles')->select('name')->where('name', '=', 'admin')->get();

        $note = DB::table('inputers')->select('inputer_note_company')->get();
        $note2 = DB::table('inputers')->select('inputer_name')->get();

        return view('leads.index', compact(
                'newInput','unComplite','complite','allInput', 'complites',
                'newInputCount', 'unCompliteCount', 'compliteCount',
                'allinputCount', 'note','note2', 'admin', 'inputer',
                'val', 'role', 'companies', 'domain', 'email'
            ))->with(['page' => 'Leads']);

    }
//==========================================================================//
//                                 SUPER ADMIN && INPUTER                   // 
//==========================================================================//
    else{

        // $inputerNewInput = DB::table('t_companies')
        // ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        // ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        // ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        // ->where('t_companies.company_validation', '=', false)
        // ->where('t_companies.deleted_at', '=', null)
        // ->where('t_company_emails.email_validation', '=', false)
        // ->where('t_company_domains.domain_validation', '=', false)
        // ->where('t_companies.user_id', '=', auth()->user()->user_id)
        // ->orderBy('t_companies.created_at', 'DESC')
        // ->orderBy('t_companies.company_name', 'ASC')
        // ->get();


        $inputerNewInput = T_company::select('*')
            ->where('t_companies.company_validation', '=', false)
            ->where('t_companies.deleted_at', '=', NULL)
            ->where('t_companies.user_id', '=', auth()->user()->user_id)
            ->orderBy('t_companies.created_at', 'DESC')
            ->orderBy('t_companies.company_name', 'ASC')
            ->get();

            foreach ($inputerNewInput as $input) {
                $domain[$input->company_id] = T_company_domain::whereCompanyId($input->company_id)
                ->where('t_company_domains.domain_validation', '=', false)
                ->get();
            }

            foreach ($inputerNewInput as $input) {
                $email[$input-> company_id] = T_company_email::whereCompanyId($input->company_id)
                ->where('t_company_emails.email_validation', '=', false)
                ->get();
            }

        // $inputerUnComplite = DB::table('t_companies')
        // ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        // ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        // ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        // ->where('t_companies.user_id', '=', auth()->user()->user_id)
        // ->where(function ($query) {
        //         $query->where('t_companies.user_id', '=', auth()->user()->user_id)
        //               ->where('t_companies.company_validation', '>', 0)
        //               ->where('t_companies.company_validation', '<', 24576);
        //     })
        // ->orWhere(function ($query) {
        //         $query->where('t_companies.user_id', '=', auth()->user()->user_id)
        //               ->Where('t_companies.company_validation', '==', 0)
        //               ->orWhere('t_company_emails.email_validation', '=', null)
        //               ->orWhere('t_company_domains.domain_validation', '=', null);
        //     })
        // ->orWhere(function ($query) {
        //         $query->where('t_companies.user_id', '=', auth()->user()->user_id)
        //               ->Where('t_companies.company_validation', '==', 0)
        //               ->Where('t_company_emails.email_validation', '!=', null)
        //               ->Where('t_company_domains.domain_validation', '!=', null);
        // })
        // ->orderBy('t_companies.created_at', 'DESC')
        // ->orderBy('t_companies.company_name', 'ASC')
        // ->get();


$inputerUnComplite = T_company::select('*')
->where('t_companies.user_id', '=', auth()->user()->user_id)
->where('t_companies.company_validation', '>', 0)
->where('t_companies.company_validation', '<', 24576)
->where('t_companies.deleted_at', '=', null)
->orderBy('t_companies.created_at', 'DESC')
->orderBy('t_companies.company_name', 'ASC')
->get();

foreach($inputerUnComplite as $company) {
    $domain[$company->company_id] = T_company_domain::whereCompanyId($company->company_id)
    ->where(function ($query) {
        $query->where('t_company_domains.domain_validation', '>', 0)
            ->where(function ($query1) {
                        $query1->Where('t_company_domains.domain_validation', '=', null)
                            ->orWhere('t_company_domains.domain_validation', '=', true);
            });
    })->get();

    $email[$company->company_id] = T_company_email::whereCompanyId($company->company_id)
    ->orWhere(function ($query) {
        $query->where('t_company_emails.email_validation', '=', true)
            ->where(function ($query1) {
                        $query1->Where('t_company_emails.email_validation', '=', null)
                            ->orWhere('t_company_emails.email_validation', '=', true);
            });
    })->get();
}




        $inputerComplite = T_company::select('*')
                    ->where('t_companies.company_validation', '>=', 24576)
                    ->where('t_companies.deleted_at', '=', null)
                    ->where('t_companies.user_id', '=', auth()->user()->user_id)
                    ->orderBy('t_companies.created_at', 'DESC')
                    ->orderBy('t_companies.company_name', 'ASC')
                    ->get();

        foreach ($inputerComplite as $inputCom) {
            $domain[$inputCom->company_id] = T_company_domain::whereCompanyId($inputCom->company_id)
            ->where('t_company_domains.domain_validation', '=', true)
            ->get();
        }

        foreach ($inputerComplite as $inputCom) {
            $email[$inputCom->company_id] = T_company_email::whereCompanyId($inputCom->company_id)
            ->where('t_company_emails.email_validation', '=', true)
            ->get();
        }

    //     $complites = T_company::select('*')
    //     ->where('t_companies.company_validation', '>=', 24576)
    //     ->where('t_companies.deleted_at', '=', null)
    //     ->orderBy('t_companies.created_at', 'DESC')
    //     ->orderBy('t_companies.company_name', 'ASC')
    //     ->get();

    // foreach($complites as $complite) {
    //     $domain[$company->company_id] = T_company_domain::whereCompanyId($company->company_id)
    //         ->where('t_company_domains.domain_validation', '=', true)
    //         ->get();
    //     $email[$company->company_id] = T_company_email::whereCompanyId($company->company_id)
    //         ->where('t_company_emails.email_validation', '=', true)
    //         ->get();
    // }

        // $inputerComplite = DB::table('t_companies')
        // ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        // ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        // ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        // ->where('t_companies.company_validation', '>=', 24576)
        // ->where('t_company_emails.email_validation', '=', true)
        // ->where('t_company_domains.domain_validation', '=', true)
        // ->where('t_companies.deleted_at', '=', null)
        // ->where('t_companies.user_id', '=', auth()->user()->user_id)
        // ->orderBy('t_companies.created_at', 'DESC')
        // ->orderBy('t_companies.company_name', 'ASC')
        // ->get();

        $inputerallInput = T_company::where('user_id', '=', auth()->user()->user_id)
                ->orderBy('created_at' , 'DESC')
                ->orderBy('t_companies.company_name', 'ASC')->get();
        
        $inputernewInputCount = $inputerNewInput->where('user_id', '=', auth()->user()->user_id)->count();
        $inputerunCompliteCount = $inputerUnComplite->where('user_id', '=', auth()->user()->user_id)->where('deleted_at', '=',null)->count();
        // $inputerunCompliteCount -= $error;
        $inputercompliteCount = $inputerComplite->where('user_id', '=', auth()->user()->user_id)->count();
        $inputerallinputCount = $inputerallInput->where('user_id', '=', auth()->user()->user_id)->count();

        // dd($inputerUnComplite, auth()->user()->user_id, $error, $inputerunCompliteCount);        

        $val = DB::table('t_companies')->select('company_validation')->get();
        
        $role = T_user::orderBy('user_name', 'ASC')->get();
        $inputer = DB::table('roles')->select('name')->where('name', '=', 'inputer')->get();
        $admin = DB::table('roles')->select('name')->where('name', '=', 'admin')->get();


        $note = DB::table('inputers')->select('inputer_note_company')->get();
        $note2 = DB::table('inputers')->select('inputer_name')->get();

        return view('leads.index', compact(
           'note','note2', 'admin', 'inputer',
            'val', 'role', 'companies', 'domain', 'email',
           'inputerNewInput', 'inputerUnComplite', 'inputerComplite',
            'inputerallInput', 'inputernewInputCount', 'inputerunCompliteCount',
            'inputercompliteCount', 'inputerallinputCount', 'users'
           ))->with(['page' => 'Leads']);
    }
}

    public function validation($company_id){

        $companies = T_company::findOrFail($company_id);
        $companies->orderBy('created_at', 'DESC')->get();
        $pic = T_company_pic::where('company_id', $company_id)->first();
        $note = Inputer::where('company_id', $company_id)->first();
        
        
        // $str_arr = explode (",", $note->inputer_note_company);
        // 
        // $val = $str_arr[0];
    // 
        // $data = $str_arr;
    // 
        // unset($data[0]);
        
        
        return view('leads.validation', compact('companies', 'pic', 'val', 'data', 'note', 'domain', 'email'));
        
    }


    public function validationData(Request $request, $company_id){

     //get data domain email dan company    

        $company = T_company::findOrFail($company_id);
        $ambil = T_company::where('company_id', '=', $company_id)->first();
        $domain =  T_company_domain::where('company_id', $company_id)->first();
        $email = T_company_email::where('company_id', $company_id)->first();
        $pic = T_company_pic::where('company_id', $company_id)->first();
        $inputer = Inputer::where('company_id', $company_id)->first();
        $user = T_user::where('user_name', $ambil->created_by)->first();

        // $salary = T_user::select('salary')->where('user_id', '=', $company_id)->first();
        $karyawan = T_user::select('user_id','user_name')->where('user_name', $ambil->created_by)->first();


        $nama = $ambil->company_name;
        

        if ($request->data) {
            $note_val = 0;
            $note_data = "";
        

            for ($i=0; $i < count($request->note); $i++) { 
                $note_val += $request->note[$i];
            }

            for ($i=0; $i < count($request->data); $i++) { 
                if ($i != 0) {
                    $note_data .= "," . $request->data[$i];
                }
                else{
                    $note_data .= $request->data[$i];
                }
            }

            $note_data = $note_val . ", " . $note_data;

            

            // dd($note_val, $note_data);
        }

           
        $n = ($request->valid_company_name +
             $request->valid_company_type +
             $request->valid_company_field +
             $request->valid_company_desc +
             $request->valid_company_telpon_office +
             $request->valid_company_faximile +
             $request->valid_company_address +
             $request->valid_company_status 
             
         );

         $p = (
             $request->valid_pic_name +
             $request->valid_pic_position +
             $request->valid_pic_division +
             $request->valid_pic_email +
             $request->valid_pic_nohp +
             $request->valid_pic_sosmed +
             $request->valid_pic_status
         );


         // dd($n, $p);

        $company_update = $company->update([
            
            'company_validation' => $n,
            'created_at' => Carbon::now()
        ]);
                    
        $domain_update = $domain->update([
            'domain_validation' => $request->valid_domain_name,
            'created_at' => Carbon::now()
        ]);
            
        $email_update = $email->update([
            'email_validation' => $request->valid_email_name,
            'created_at' => Carbon::now()
        ]);

        $pic_update = $pic->update([
            'pic_validation' => $p,
            'created_at' => Carbon::now()
        ]);


        if ($request->data) {
            $inputer->update([
                'inputer_note_company' => $note_data
             ]);
        }

        if ($request->email_note) {
            $inputer->update([
                'inputer_note_email' => $request->email_note
             ]);
        }

        if ($request->domain_note) {
            $inputer->update([
                'inputer_note_domain' => $request->domain_note
             ]);
        }


        if ($n >= 24576 && ($request->valid_domain_name == true && $request->valid_email_name == true)) {
            // dd("sukses", $request->valid_company_name ,$nama);
            // $salary_update = $user->update([
            //     'salary' => $gaji
            // ]);
            return redirect(route('leads.index'))->with(['complete' => $nama]);
        }
        else{
            // $salary_update = $user->update([
            //     'salary' => $gaji
            // ]);
            // dd("warning", $request->valid_company_name, $nama);
            return redirect(route('leads.index'))->with(['warning' => $nama]);
        }    
    
        // return view ('leads.index', compact('validated'))->with(['page' => 'Leads']);

  }

  public function editForm($company_id){

        $company = T_company::findOrFail($company_id);
        return view('leads.editForm', compact('company') )->with(['page' => 'Leads']);
  }

  public function update(Request $request , $company_id){
        $this->validate($request, [

             'company_name' => 'nullable|string|max:50',
            'company_type' => 'nullable|in:tekno,nontekno',
            'company_telpon_office' => 'string|max:50',

        
        // //Company Contact
            'domain_name[]' => 'nullable|string:max:50',
            'email_name[]' => 'nullable|email:max50',
      
            'pic_nohp' => 'nullable|numeric',
            'pic_email' => 'nullable|email',
        //     'pic_sosmed' => 'string',
            'pic_status' => 'nullable|in:aktif,nonaktif'

        ]);
    
        $company = T_company::findOrFail($company_id);
        $domain = T_company_domain::where('company_id', $company_id)->first();
        $email = T_company_email::where('company_id', $company_id)->first();
        $pic = T_company_pic::where('company_id', $company_id)->first();
        $nama = $request->company_name;

        $company->update([
            'company_name' => $request->company_name,
            'company_type' => $request->company_type,
            'company_field' => $request->company_field,
            'company_desc' => $request->company_desc,
            'company_address' => $request->company_address,
            'company_telpon_office' => $request->company_telpon_office,
            'company_faximile' => $request->company_faximile,
            'company_status' => $request->company_status
        ]);
foreach($request->domain_name as $domain_name){
    $company->domain()->save(new T_company_domain(['domain_name' => $domain_name]));
    // $company->domain()->sync(new T_company_domain(['domain_name' => $domain_name]));
    // $company->domain()->save(new T_company_domain(['domain_name' => $domain_name]));
    
    // $domain->update([
        // 'domain_name' => $request->domain_name
        // ]);
        // 
    }
    
    foreach ($request->email_name as $email_name) {
        
        
        $company->email()->save(new T_company_email(['email_name' => $email_name]));
        // $company->email()->save(new T_company_email(['email_name' => $email_name]));
        

        // $email->update([
            // 'email_name' => $request->email_name
        // ]);
    }


        $pic->update([
            'pic_name' => $request->pic_name,
            'pic_position' => $request->pic_position,
            'pic_division' => $request->pic_division,
            'pic_nohp' => $request->pic_nohp,
            'pic_email' => $request->pic_email,
            'pic_sosmed' => $request->pic_sosmed,
            'pic_status' => $request->pic_status,
        ]);
    return redirect(route('leads.index'))->with(['success' => $nama]);
  
    }

    public function delete_lead($company_id){
        $company = T_company::findOrFail($company_id);
        $role = Role::orderBy('name', 'ASC')->get();
        return view('leads.delete_lead', compact('company', 'role'));
    }
  
  public function destroy($company_id){
    $company = T_company::findOrFail($company_id);
    $company->delete();
    return redirect()->back()->with(['success' => 'Data ' . $company->company_name . 'Dihapus']);
  }

  public function detail($company_id){
      $companies = T_company::findOrFail($company_id);
      $companies->orderBy('created_at', 'DESC')->get();
      $pic = T_company_pic::where('company_id', $company_id)->first();
      $note = Inputer::where('company_id', $company_id)->first();

      $str_arr = explode(",", $note->inputer_note_company);
      $val = $str_arr[0];
      $data = $str_arr;
      unset($data[0]);

      return view('leads.detail', compact('companies', 'pic', 'note', 'val', 'data'));
  }

    public function laporanLeads(){

        // return Excel::download(new LeadsReport(), 'leads.xlsx');
        return (new LeadsReport)->download('leads.xlsx');
    
    }

    public function exportNewInput(){
        
        
        // return (new exportNewInput)->download('newinput.xlsx');

        return Excel::download(new exportNewInput, 'newinput.xlsx');
        
    
}

    public function export_unComplite(){


        return Excel::download(new exportUnComplite, 'unComplite.xlsx');

    }

    public function export_complite(){

        return Excel::download(new exportComplite, 'complite.xlsx');

    }

    public function export_allData(){
        return Excel::download(new exportAllData, 'allData.xlsx');
    }

}      
    



// =====================================================================================================================
        // $t_salary = DB::table('t_salary')->select('*')->get();

        // foreach ($t_salary as $row) {
    
        //    $tgl_berlaku = $row->tgl_berlaku;
        //    $tgl_deadline = $row->tgl_deadline;
        //    $now = date('d-M-Y');

        //    if ($now <= $tgl_berlaku) {
        //      echo "Tidak ada fee yang valid";
        //    } elseif ($now >= $tgl_berlaku && $now <= $tgl_deadline) {
        //     $id_salary = $row->salary_id;
        //      echo "diantara";
        //    } elseif ($now > $tgl_berlaku && $now > $tgl_deadline) {
        //      echo "Semua fee kadaluarsa";
        //    } else {
        //      echo "Gak masuk kemna mana";
        //      dd();
        //    }
        // }


        // $history_salary = History_salary::where('user_id', $karyawan->user_id)
        //             ->orderBy('history_id', 'DESC')
        //             ->get();


        // $count = count($history_salary);

        // $harga_validasi = DB::table('t_salary')->where('salary_id', $id_salary)->first();
        // $gaji = 0;
        // // dd($harga_validasi, $history_salary);


        // $inputerValidasi = DB::table('t_companies')
        //     ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        //     ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        //     ->select('t_company_domains.domain_validation','t_company_emails.email_validation','t_companies.*')
        //     ->where('t_companies.company_validation', '>=', 24576)
        //     ->where('t_company_emails.email_validation', '=', true)
        //     ->where('t_company_domains.domain_validation', '=', true)
        //     ->where('t_companies.deleted_at', '=', null)
        //     ->where('t_companies.created_by', '=',$karyawan->user_name)
        //     ->get();

        // $input = DB::table('t_companies')
        //     ->select('*')
        //     ->where('t_companies.created_by', '=',$karyawan->user_name)
        //     ->get();
            

        // if (count($history_salary) == 0) { //Buat baru history
        //     // dd($harga_validasi);
        //     $gaji = count($input) * $harga_validasi->salary_value;
        //     $create_history = History_salary::create([
        //         'user_id' => $karyawan->user_id,
        //         'valid_data' => count($inputerValidasi),
        //         'total_salary' => $gaji,
        //         'salary_id' => $harga_validasi->salary_id
        //     ]);

        //     // dd("Masuk Buat baru history");
            
        // }
        // elseif ($history_salary[0]->user_id == $karyawan->user_id) {


        //     if (count($history_salary) > 1 && $harga_validasi->salary_id == $history_salary[0]->salary_id) {  //cek kesalahan disini
        //         $update_history = $history_salary[0]->update([
        //            'valid_data' => count($inputerValidasi),
        //         ]);

        //         $selisih = $history_salary[0]->valid_data - $history_salary[1]->valid_data;
        //         $gaji = ($selisih * $harga_validasi->salary_value) + $history_salary[1]->total_salary;

        //         $update_history = $history_salary[0]->update([
        //            'total_salary' => $gaji,
        //         ]);

        //         // dd("Masuk update id salary sama", $harga_validasi, $history_salary);
        //     }

        //     elseif (count($history_salary) == 1 && $harga_validasi->salary_id == $history_salary[0]->salary_id) { //update history
                
        //         // if (count($history_salary) == 1) {
                    
        //         // }
        //         $update_history = $history_salary[0]->update([
        //            'valid_data' => count($inputerValidasi),
        //         ]);

        //         $gaji = count($inputerValidasi) * $harga_validasi->salary_value;

        //         $update_history = $history_salary[0]->update([
        //            'total_salary' => $gaji,
        //         ]);

        //         // dd("Masuk history salary cuma satu.", $gaji, $harga_validasi, $history_salary);
            
        //     }
        //     else{
        //         $create_history = History_salary::create([
        //            'user_id' => $karyawan->user_id,
        //            'valid_data' => count($inputerValidasi),
        //            'salary_id' => $harga_validasi->salary_id
        //        ]);

        //         if (count($history_salary) == 1) {

        //             $selisih = $create_history->valid_data - $history_salary[0]->valid_data;
        //             $gaji = ($selisih * $harga_validasi->salary_value) + $history_salary[0]->total_salary;    // Create baru history

        //             $update_history = $create_history->update([
        //                'total_salary' => $gaji,
        //             ]);
                    
        //             // dd("Masuk history salary cuma satu. terus bikin lagi", $selisih, $gaji,  $harga_validasi, $history_salary);

        //         }

                
        //         else{ // cek bagian inii ===============================================================================
        //             $selisih = $create_history->valid_data - $history_salary[0]->valid_data;
        //             $gaji = ($selisih * $harga_validasi->salary_value) + $history_salary[0]->total_salary;    // Create baru history
            
        //             $update_history = $create_history->update([
        //                 'total_salary' => $gaji,
        //             ]);
        //         }
                
        //         // dd("Masuk last else.", $harga_validasi, $history_salary);


        //     }
            
        // }

        // // $gaji = count($inputerValidasi) * $harga_validasi->salary_value;

        // if ($gaji == 0) {
        //     dd("Kok gajinya 0 ya??");
        // }
