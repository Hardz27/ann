<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin\T_company;
use App\Admin\T_company_domain;
use Illuminate\Support\Facades\DB;
use App\T_user;
use App\Salary;
use App\History_salary;
use App\Admin\T_company_pic;
use App\Admin\T_company_email;
use Illuminate\Support\Carbon;

class SalaryController extends Controller
{
    public function index(){

        $salary_value =  Salary::withTrashed()
                              // ->where('deleted_at', '=', null)
                              ->orderBy('salary_id', 'DESC')
                              ->paginate(5);
        // dd($salary_value);
        $allInput = History_salary::orderBy('created_at' , 'DESC')->get();

        // dd($allInput, $salary_value);

        foreach ($salary_value as $data) {
          $jumlah = 0;
          
          foreach ($allInput as $input) {
            $history = $input->updated_at->format("d-M-Y");

            if (strtotime($history) >= strtotime($data->tgl_berlaku) && strtotime($history) <= strtotime($data->tgl_deadline)) {
              $jumlah += $input->total_salary;
              // echo $input->total_salary. " | ";
            }
          }
          // if ($jumlah > 5000) {
          //   dd("Sok coba cek");
          // }

          // dd($jumlah);

          $report[] = array('salary_id' => $data->salary_id,'tanggal_berlaku' => $data->tgl_berlaku , 'tanggal_deadline' => $data->tgl_deadline, 'nilai' => $data->salary_value, 'total' => $jumlah, 'deleted_at' => $data->deleted_at);
        }

        // dd($report);
        

        for ($i=0; $i < count($report)-1; $i++) { 
          if ($report[$i]['total'] != 0) {
            $report[$i]['total'] -= $report[$i+1]['nilai'];
          } else{
            $report[$i]['total'] -= 0;
          }
          
        }

        // dd($report, $jumlah);

        // =================================================================================
        //                            Matikan saat kodingan akan diubah ASC                 
        // =================================================================================

        for ($i=count($report)-1; $i >=0 ; $i--) { 
          $report_fix[] = array(
                       'salary_id' => $report[$i]['salary_id'],
                       'tanggal_berlaku' => $report[$i]['tanggal_berlaku'],
                       'tanggal_deadline' => $report[$i]['tanggal_deadline'],
                       'nilai' => $report[$i]['nilai'],
                       'total' => $report[$i]['total'],
                       'deleted_at' => $report[$i]['deleted_at']
                     );
        }

        $report = $report_fix;

        // dd($report);

        // =================================================================================

        $report = json_decode(json_encode($report));

        // dd($report);



        
        // dd($salary_value);

         return view('salary.salary', [
             'salary_value' => $salary_value,
             'report' => $report
         ])->with(['page' => 'Salary']);
 
    }

    public function add(Request $request){

      $tgl = explode(" - ", $request->due_date);
      // $now = Carbon::now();
      // $now = $now->toDateString();
      $mulai = strtotime($tgl[0]);
      // $sekarang = strtotime($now);
      $deadline = strtotime($tgl[1]);

      $tgl_mulai = date("d-M-Y", $mulai);
      $tgl_deadline = date("d-M-Y", $deadline);

      $preg = str_replace("Rp. ","", $request->salary_value);
      $preg = str_replace(".","", $preg);
      $preg  = $preg + 1 - 1;

      $salary_value =  Salary::orderBy('salary_id', 'DESC')->get();
        
      $salary = Salary::create([
        'tgl_berlaku' => $tgl_mulai,
        'tgl_deadline' => $tgl_deadline,
        'salary_value' => $preg
      ]);

      return redirect(route('salary.index', [
        'salary_value' => $salary_value             
      ]))->with(['page' => 'Salary']);
 
    }



    public function detail(Request $request, $salary_id){

        $salary =  Salary::withTrashed()->where('salary_id', $salary_id)->first();
        $history = History_salary::orderBy('created_at' , 'DESC')->get();
        $user = T_user::orderBy('user_id' , 'ASC')->get();
        $company = T_company::orderBy('company_id' , 'ASC')->get();

        $jumlah = 0;        

        foreach ($user as $karyawan) {
          $input = 0;

          foreach ($company as $item) {
            $tgl_input = $item->updated_at->format("d-M-Y");
            if (strtotime($tgl_input) >= strtotime($salary->tgl_berlaku) && strtotime($tgl_input) <= strtotime($salary->tgl_deadline) && $item->created_by == $karyawan->user_name) {
                $input++;      
            }
          }
          $jumlah += $salary->salary_value*$input;
          $report[] = array('karyawan' => $karyawan->user_name,'total_input' => $input , 'total_salary' => $salary->salary_value*$input);
        }

        $report = json_decode(json_encode($report));

        // dd($report, $jumlah);

        return view('salary.detail', compact('salary', 'jumlah', 'report'))->with(['page' => 'Salary']);

    }

    public function edit($salary_id){

       $salary = Salary::findOrFail($salary_id);
        
        return view('salary.edit', compact('salary'))->with(['page' => 'Salary']);

    }

    public function update(Request $request, $salary_id)
    {
        $tgl = explode(" - ", $request->due_date);

      $mulai = strtotime($tgl[0]);
      $deadline = strtotime($tgl[1]);

      $tgl_mulai = date("d-M-Y", $mulai);
      $tgl_deadline = date("d-M-Y", $deadline);
   
      $preg = str_replace("Rp. ","", $request->salary_value);
      $preg = str_replace(".","", $preg);
      $preg  = $preg + 1 - 1;

        $salary = Salary::findOrFail($salary_id);
        $salary->update([
            'tgl_berlaku' => $tgl_mulai,
            'tgl_deadline' => $tgl_deadline,
            'salary_value' => $preg
        ]);

        return redirect(route('salary.index'))->with(['success' => 'Salary: ' . $salary->salary_value . ' Diperbaharui'])->with(['page' => 'Salary']);
    }

    public function delete($salary_id){

        $salary = Salary::findOrFail($salary_id);
        
        return view('salary.delete_form', compact('salary'))->with(['page' => 'Salary']);

    }

    public function destroy($salary_id)
    {
        $salary = Salary::findOrFail($salary_id);
        $salary->delete();
        return redirect()->back()->with(['success' => 'Salary: ' . $salary->salary_value . ' Dihapus']);
    }

    public function restore($salary_id){
        
        $salary = Salary::withTrashed()->find($salary_id);
        $salary->restore();
        return redirect()->back()->with(['success' => 'Data '. $salary->salary_value. ' Direstore']);
    }

}
// ======================================================================================================================

 // public function validationData(Request $request, $company_id){

 //     //get data domain email dan company    

 //        $company = T_company::findOrFail($company_id);
 //        $ambil = T_company::where('company_id', '=', $company_id)->first();
 //        $domain =  T_company_domain::where('company_id', $company_id)->first();
 //        $email = T_company_email::where('company_id', $company_id)->first();
 //        $pic = T_company_pic::where('company_id', $company_id)->first();
 //        $inputer = Inputer::where('company_id', $company_id)->first();
 //        $user = T_user::where('user_name', $ambil->created_by)->first();

 //        // $salary = T_user::select('salary')->where('user_id', '=', $company_id)->first();
 //        $karyawan = T_user::select('user_id','user_name')->where('user_name', $ambil->created_by)->first();
 //        $harga_validasi = DB::table('t_salary')->select('salary_id','salary_value')->latest('salary_id')->first();
 //        $history_salary = History_salary::where('user_id', $karyawan->user_id)
 //                        ->orderBy('history_id', 'DESC')
 //                        ->get();
 //        $count = count($history_salary);
 //        $gaji = 0;
 //        // dd($harga_validasi, $history_salary);

 //        $nama = $ambil->company_name;
        

 //        if ($request->data) {
 //            $note_val = 0;
 //            $note_data = "";
        

 //            for ($i=0; $i < count($request->note); $i++) { 
 //                $note_val += $request->note[$i];
 //            }

 //            for ($i=0; $i < count($request->data); $i++) { 
 //                if ($i != 0) {
 //                    $note_data .= "," . $request->data[$i];
 //                }
 //                else{
 //                    $note_data .= $request->data[$i];
 //                }
 //            }

 //            $note_data = $note_val . ", " . $note_data;

            

 //            // dd($note_val, $note_data);
 //        }

           
 //        $n = ($request->valid_company_name +
 //             $request->valid_company_type +
 //             $request->valid_company_field +
 //             $request->valid_company_desc +
 //             $request->valid_company_telpon_office +
 //             $request->valid_company_faximile +
 //             $request->valid_company_address +
 //             $request->valid_company_status 
             
 //         );

 //         $p = (
 //             $request->valid_pic_name +
 //             $request->valid_pic_position +
 //             $request->valid_pic_division +
 //             $request->valid_pic_email +
 //             $request->valid_pic_nohp +
 //             $request->valid_pic_sosmed +
 //             $request->valid_pic_status
 //         );


 //         // dd($n, $p);

 //        $company_update = $company->update([
            
 //            'company_validation' => $n,
 //            'created_at' => Carbon::now()
 //        ]);
                    
 //        $domain_update = $domain->update([
 //            'domain_validation' => $request->valid_domain_name,
 //            'created_at' => Carbon::now()
 //        ]);
            
 //        $email_update = $email->update([
 //            'email_validation' => $request->valid_email_name,
 //            'created_at' => Carbon::now()
 //        ]);

 //        $pic_update = $pic->update([
 //            'pic_validation' => $p,
 //            'created_at' => Carbon::now()
 //        ]);


 //        if ($request->data) {
 //            $inputer->update([
 //                'inputer_note_company' => $note_data
 //             ]);
 //        }

 //        if ($request->email_note) {
 //            $inputer->update([
 //                'inputer_note_email' => $request->email_note
 //             ]);
 //        }

 //        if ($request->domain_note) {
 //            $inputer->update([
 //                'inputer_note_domain' => $request->domain_note
 //             ]);
 //        }

 //        $inputerValidasi = DB::table('t_companies')
 //            ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
 //            ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
 //            ->select('t_company_domains.domain_validation','t_company_emails.email_validation','t_companies.*')
 //            ->where('t_companies.company_validation', '>=', 24576)
 //            ->where('t_company_emails.email_validation', '=', true)
 //            ->where('t_company_domains.domain_validation', '=', true)
 //            ->where('t_companies.deleted_at', '=', null)
 //            ->where('t_companies.created_by', '=',$karyawan->user_name)
 //            ->get();
            

 //        if (count($history_salary) == 0) { //Buat baru history
 //            // dd($harga_validasi);
 //            $gaji = count($inputerValidasi) * $harga_validasi->salary_value;
 //            $create_history = History_salary::create([
 //                'user_id' => $karyawan->user_id,
 //                'valid_data' => count($inputerValidasi),
 //                'total_salary' => $gaji,
 //                'salary_id' => $harga_validasi->salary_id
 //            ]);

 //            // dd("Masuk Buat baru history");
            
 //        }
 //        elseif ($history_salary[0]->user_id == $karyawan->user_id) {


 //            if (count($history_salary) > 1 && $harga_validasi->salary_id == $history_salary[0]->salary_id) {  //cek kesalahan disini
 //                $update_history = $history_salary[0]->update([
 //                   'valid_data' => count($inputerValidasi),
 //                ]);

 //                $selisih = $history_salary[0]->valid_data - $history_salary[1]->valid_data;
 //                $gaji = ($selisih * $harga_validasi->salary_value) + $history_salary[1]->total_salary;

 //                $update_history = $history_salary[0]->update([
 //                   'total_salary' => $gaji,
 //                ]);

 //                // dd("Masuk update id salary sama", $harga_validasi, $history_salary);
 //            }

 //            elseif (count($history_salary) == 1 && $harga_validasi->salary_id == $history_salary[0]->salary_id) { //update history
                
 //                // if (count($history_salary) == 1) {
                    
 //                // }
 //                $update_history = $history_salary[0]->update([
 //                   'valid_data' => count($inputerValidasi),
 //                ]);

 //                $gaji = count($inputerValidasi) * $harga_validasi->salary_value;

 //                $update_history = $history_salary[0]->update([
 //                   'total_salary' => $gaji,
 //                ]);

 //                // dd("Masuk history salary cuma satu.", $gaji, $harga_validasi, $history_salary);
            
 //            }
 //            else{
 //                $create_history = History_salary::create([
 //                   'user_id' => $karyawan->user_id,
 //                   'valid_data' => count($inputerValidasi),
 //                   'salary_id' => $harga_validasi->salary_id
 //               ]);

 //                if (count($history_salary) == 1) {

 //                    $selisih = $create_history->valid_data - $history_salary[0]->valid_data;
 //                    $gaji = ($selisih * $harga_validasi->salary_value) + $history_salary[0]->total_salary;    // Create baru history

 //                    $update_history = $create_history->update([
 //                       'total_salary' => $gaji,
 //                    ]);
                    
 //                    // dd("Masuk history salary cuma satu. terus bikin lagi", $selisih, $gaji,  $harga_validasi, $history_salary);

 //                }

                
 //                else{ // cek bagian inii ===============================================================================
 //                    $selisih = $create_history->valid_data - $history_salary[0]->valid_data;
 //                    $gaji = ($selisih * $harga_validasi->salary_value) + $history_salary[0]->total_salary;    // Create baru history
            
 //                    $update_history = $create_history->update([
 //                        'total_salary' => $gaji,
 //                    ]);
 //                }
                
 //                // dd("Masuk last else.", $harga_validasi, $history_salary);


 //            }
            
 //        }

 //        // $gaji = count($inputerValidasi) * $harga_validasi->salary_value;

 //        if ($gaji == 0) {
 //            dd("Kok gajinya 0 ya??");
 //        }

 //        if ($n >= 24576 && ($request->valid_domain_name == true && $request->valid_email_name == true)) {
 //            // dd("sukses", $request->valid_company_name ,$nama);
 //            $salary_update = $user->update([
 //                'salary' => $gaji
 //            ]);
 //            return redirect(route('leads.index'))->with(['complete' => $nama]);
 //        }
 //        else{
 //            $salary_update = $user->update([
 //                'salary' => $gaji
 //            ]);
 //            // dd("warning", $request->valid_company_name, $nama);
 //            return redirect(route('leads.index'))->with(['warning' => $nama]);
 //        }    
    
 //        // return view ('leads.index', compact('validated'))->with(['page' => 'Leads']);

 //  }