<?php

namespace App\Http\Controllers\Admin;

use App\Admin\T_company;
use App\Admin\T_company_pic;
use App\Admin\T_company_domain;
use App\Admin\T_company_email;
use App\T_user;
// use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Inputer;
use Illuminate\Support\Carbon;
use Excel;
use Request;
use App\Exports\LeadsReport;
use App\Exports\exportNewInput;
use App\Exports\exportUnComplite;
use App\Exports\exportComplite;
use App\Exports\exportAllData;

class ArchiveController extends Controller
{
    public function archive_company(){

        // $trashedAndNotTrashed = Model::withTrashed()->get();
        $archivePic = T_company_pic::onlyTrashed()->get();
        $archiveUser = T_user::onlyTrashed()->get();
        $archiveCompany = T_company::onlyTrashed()->get();
        $archiveLeads = T_company::onlyTrashed()->get();



        $archiveCompanyCount = $archiveCompany->count();
        $archivePicCount = $archivePic->count();
        $archiveUserCount = $archiveUser->count();
        $archiveLeadsCount = $archiveLeads->count();
        // $archiveLeadsCount =  $allinputCount;

        return view('archives.archive_company', compact('archiveLeads', 'archiveLeadsCount','archiveCompanyCount', 'archiveCompany',
        'archivePic', 'archivePicCount', 'archiveUser', 'archiveUserCount' ))->with(['page' => 'Archive']);

    }

    public function archive_pic(){
        $archivePic = T_company_pic::onlyTrashed()->get();
        $archiveUser = T_user::onlyTrashed()->get();
        $archiveCompany = T_company::onlyTrashed()->get();
        $archiveLeads = T_company::onlyTrashed()->get();

        $newInput = T_company::onlyTrashed()
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.company_validation', '=', false)
        // ->where('t_companies.deleted_at', '=', null)
        ->where('t_company_emails.email_validation', '=', false)
        ->where('t_company_domains.domain_validation', '=', false)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();

        $unComplite = T_company::onlyTrashed()
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where(function ($query) {
                $query->where('t_companies.company_validation', '>', 0)
                      ->where('t_companies.company_validation', '<', 24576);
            })
        ->orWhere(function ($query) {
                $query->Where('t_companies.company_validation', '==', 0)
                      ->Where('t_company_emails.email_validation', '=', null)
                      ->orWhere('t_company_domains.domain_validation', '=', null);
            })
        ->orWhere(function ($query) {
                $query->Where('t_companies.company_validation', '==', 0)
                      ->Where('t_company_emails.email_validation', '!=', null)
                      ->Where('t_company_domains.domain_validation', '!=', null);
            })

        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();

        $complite = T_company::onlyTrashed()
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.company_validation', '>=', 24576)
        ->where('t_company_emails.email_validation', '=', true)
        ->where('t_company_domains.domain_validation', '=', true)
        
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();

        
        $allInput = T_company::onlyTrashed()
                    ->orderBy('created_at' , 'DESC')
                    ->orderBy('t_companies.company_name', 'ASC')
                    ->get();
        
        $newInputCount = $newInput->count();
        $unCompliteCount = $unComplite->count();
        $compliteCount = $complite->count();
        $allinputCount = $allInput->count();

        $err = 0;
        

       
        foreach ($unComplite as $item) {
            if($item->company_validation == 0 && ($item->domain_validation != 1 && $item->email_validation != 1)){
                $err++;
            }
        }
        
        $unCompliteCount -= $err;
        


        $archiveCompanyCount = $archiveCompany->count();
        $archiveUserCount = $archiveUser->count();
        $archivePicCount = $archivePic->count();
        $archiveLeadsCount = $archiveLeads->count();
        $archiveLeadsCount =  $allinputCount;

        return view('archives.archive_pic', compact('archiveLeads', 'archiveLeadsCount','archiveCompanyCount', 'archiveCompany',
        'archivePic', 'archivePicCount', 'archiveUser', 'archiveUserCount'))->with(['page' => 'Archive']);
    }

    public function archive_user(){
        $archivePic = T_company_pic::onlyTrashed()->get();
        $archiveUser = T_user::onlyTrashed()->get();
        $archiveCompany = T_company::onlyTrashed()->get();
        $archiveLeads = T_company::onlyTrashed()->get();

        $newInput = T_company::onlyTrashed()
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.company_validation', '=', false)
        
        ->where('t_company_emails.email_validation', '=', false)
        ->where('t_company_domains.domain_validation', '=', false)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();

        $unComplite = T_company::onlyTrashed()
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where(function ($query) {
                $query->where('t_companies.company_validation', '>', 0)
                      ->where('t_companies.company_validation', '<', 24576);
            })
        ->orWhere(function ($query) {
                $query->Where('t_companies.company_validation', '==', 0)
                      ->Where('t_company_emails.email_validation', '=', null)
                      ->orWhere('t_company_domains.domain_validation', '=', null);
            })
        ->orWhere(function ($query) {
                $query->Where('t_companies.company_validation', '==', 0)
                      ->Where('t_company_emails.email_validation', '!=', null)
                      ->Where('t_company_domains.domain_validation', '!=', null);
            })

        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();

        $complite = T_company::onlyTrashed()
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.company_validation', '>=', 24576)
        ->where('t_company_emails.email_validation', '=', true)
        ->where('t_company_domains.domain_validation', '=', true)
        
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();

        
        $allInput = T_company::onlyTrashed()
                    ->orderBy('created_at' , 'DESC')
                    ->orderBy('t_companies.company_name', 'ASC')
                    ->get();
        
        $newInputCount = $newInput->count();
        $unCompliteCount = $unComplite->count();
        $compliteCount = $complite->count();
        $allinputCount = $allInput->count();

        $err = 0;
        

       
        foreach ($unComplite as $item) {
            if($item->company_validation == 0 && ($item->domain_validation != 1 && $item->email_validation != 1)){
                $err++;
            }
        }
        
        $unCompliteCount -= $err;
        

        
        $archiveCompanyCount = $archiveCompany->count();
        $archiveUserCount = $archiveUser->count();
        $archivePicCount = $archivePic->count();
        $archiveLeadsCount = $archiveLeads->count();

        $archiveLeadsCount =  $allinputCount;

        return view('archives.archive_user', compact('archiveLeads', 'archiveLeadsCount','archiveCompanyCount', 'archiveCompany',
        'archivePic', 'archivePicCount', 'archiveUser', 'archiveUserCount'))->with(['page' => 'Archive']);
    }

    public function archive_leads(){
        $domain = array();
        $email = array();
        // $archiveLeads = T_company::onlyTrashed()->get();
        $archivePic = T_company_pic::onlyTrashed()->get();
        $archiveUser = T_user::onlyTrashed()->get();
        $archiveCompany = T_company::onlyTrashed()->get();


        $archiveCompanyCount = $archiveCompany->count();
        $archiveUserCount = $archiveUser->count();
        $archivePicCount = $archivePic->count();
        


        $users = T_user::get();
        $user = Auth::user();
        // $newInput = T_company::onlyTrashed()
        // ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        // ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        // ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        // ->where('t_companies.company_validation', '=', false)
        // ->where('t_companies.company_validation', '<', 1)
        // ->where('t_companies.deleted_at', '!=', null)
        // ->where('t_company_emails.email_validation', '=', false)
        // ->where('t_company_domains.domain_validation', '=', false)
        // ->orderBy('t_companies.created_at', 'DESC')
        // ->orderBy('t_companies.company_name', 'ASC')
        // ->get();


        $newInput = T_company::onlyTrashed()->select('*')
        ->where('t_companies.company_validation', '=', false)
        
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();

    foreach($newInput as $company) {
        $domain[$company->company_id] = T_company_domain::whereCompanyId($company->company_id)
            ->where('t_company_domains.domain_validation', '=', false)
            ->get();
        $email[$company->company_id] = T_company_email::whereCompanyId($company->company_id)
            ->where('t_company_emails.email_validation', '=', false)
            ->get();
    }

        $unComplite = T_company::onlyTrashed()
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where(function ($query) {
                $query->where('t_companies.company_validation', '>', 0)
                      ->where('t_companies.deleted_at', '!=', null)
                      ->where('t_companies.company_validation', '<', 24576)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', true)
                                       ->orWhere('t_company_domains.domain_validation', '=', true);
                      });
            })
        ->orWhere(function ($query) {
                $query->where('t_companies.company_validation', '>', 0)
                      ->where('t_companies.company_validation', '<', 24576)
                      ->where('t_companies.deleted_at', '!=', null)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', null)
                                       ->orWhere('t_company_domains.domain_validation', '=', null);
                      });
            })
        ->orWhere(function ($query) {
                $query->where('t_companies.company_validation', '=', 0)
                      ->where('t_companies.deleted_at', '!=', null)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', true)
                                       ->orWhere('t_company_domains.domain_validation', '=', true);
                      });
            })
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();

        
        // $unComplite = T_company::select('*')
        //             ->where('t_companies.company_validation', '>', 0)
        //             ->where('t_companies.company_validation', '<', 24576)
        //             ->where('t_companies.deleted_at', '=', null)
        //             ->orderBy('t_companies.created_at', 'DESC')
        //             ->orderBy('t_companies.company_name', 'ASC')
        //             ->get();

        // foreach($unComplite as $company) {
        //     $domain[$company->company_id] = T_company_domain::whereCompanyId($company->company_id)
        //     ->where(function ($query) {
        //         $query->where('t_company_domains.domain_validation', '>', 0)
        //             ->where(function ($query1) {
        //                         $query1->Where('t_company_domains.domain_validation', '=', null)
        //                             ->orWhere('t_company_domains.domain_validation', '=', true);
        //             });
        //     })->get();

        //     $email[$company->company_id] = T_company_email::whereCompanyId($company->company_id)
        //     ->orWhere(function ($query) {
        //         $query->where('t_company_emails.email_validation', '=', true)
        //             ->where(function ($query1) {
        //                         $query1->Where('t_company_emails.email_validation', '=', null)
        //                             ->orWhere('t_company_emails.email_validation', '=', true);
        //             });
        //     })->get();
        // }


        $complite = T_company::onlyTrashed()
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.company_validation', '>=', 24576)
        ->where('t_company_emails.email_validation', '=', true)
        ->where('t_company_domains.domain_validation', '=', true)
        ->where('t_companies.deleted_at', '!=', null)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();

        
        $allInput = T_company::onlyTrashed()
                    ->where('t_companies.deleted_at', '!=', null)
                    ->orderBy('created_at' , 'DESC')
                    ->orderBy('t_companies.company_name', 'ASC')
                    ->get();
        
        $newInputCount = $newInput->count();
        $unCompliteCount = $unComplite->count();
        $compliteCount = $complite->count();
        $allinputCount = $allInput->count();

        // $err = 0;
        

       
        // foreach ($unComplite as $item) {
        //     if($item->company_validation == 0 && ($item->domain_validation != 1 && $item->email_validation != 1)){
        //         $err++;
        //     }
        // }
        
        // $unCompliteCount -= $err;

        $val = DB::table('t_companies')->select('company_validation')->get();
        
        $role = T_user::orderBy('user_name', 'ASC')->get();
        // $role = DB::table('roles')->select('name')->get();
        // $admin = Role::where('name', '=', 'admin')->get();
        $inputer = DB::table('roles')->select('name')->where('name', '=', 'inputer')->get();
        $admin = DB::table('roles')->select('name')->where('name', '=', 'admin')->get();


        $note = DB::table('inputers')->select('inputer_note_company')->get();
        $note2 = DB::table('inputers')->select('inputer_name')->get();

        $archiveLeadsCount =  $allinputCount;

        

        return view('archives.archive_leads', compact('archiveLeads', 'archiveLeadsCount','archiveCompanyCount', 'archiveCompany',
        'archivePic', 'archivePicCount', 'archiveUser', 'archiveUserCount',
        'newInput','unComplite','complite','allInput', 'domain', 'email'
        ,'newInputCount', 'unCompliteCount', 'compliteCount',
        'allinputCount', 'note','note2', 'admin', 'inputer',
        'company','companies', 'validated', 'value','val', 'role'))->with(['page' => 'Archive']);
    }

    public function form_restore_leads($company_id){
        $company = T_company::withTrashed()->find($company_id);
        $role = Role::orderBy('name', 'ASC')->get();
        return view('archives.form_restore_leads', compact('companies','role'));
    }

    public function restore_leads($company_id){
        $company = T_company::withTrashed()->find($company_id);
        $company->restore();
        return redirect()->back()->with(['success' => 'Data '. $company->company_name. ' Dihapus']);

    }

    public function form_restore($company_id){
        $companies = T_company::withTrashed()->find($company_id);
        $role = Role::orderBy('name', 'ASC')->get();

        return view('archives.restore_company', compact('companies','role'));
    }

    public function restore_all_company()
    {
        $checked = Request::input('checked', []);
        foreach($checked as $id) {
            T_company::withTrashed()->where('company_id', $id)->restore();
        }
        return redirect()->back();
    }

    public function restore($company_id){
        
        $companies = T_company::withTrashed()->find($company_id);
        $companies->restore();
        return redirect()->back()->with(['success' => 'Data '. $companies->company_name. ' Dihapus']);
    }

    public function form_restore_pic($pic_id){
        $pic = T_company_pic::withTrashed()->find($pic_id);
        $role = Role::orderBy('name', 'ASC')->get();


        return view('archives.restore_pic', compact('pic', 'role'));
    }

    public function restore_pic($pic_id){
        $pic = T_company_pic::withTrashed()->find($pic_id);
        $pic->restore();
        return redirect()->back()->with(['success' => 'Data '. $pic->pic_name. ' Dihapus']);

    }

    public function form_restore_user($user_id){
        $user = T_user::withTrashed()->find($user_id);
        $role = Role::orderBy('name', 'ASC')->get();

        return view('archives.restore_user', compact('user', 'role'));
    }

    public function restore_user($user_id){
        $user = T_user::withTrashed()->find($user_id);
        $user->restore();
        return redirect()->back()->with(['success' => 'Data' . $user->user_name. 'Direstore']);
    }

    public function detail_company($company_id){
        $companies = T_company::withTrashed()->find($company_id);

        return view('archives.detail_company', compact('companies'));
    }

    public function detail_pic($pic_id){
        $pics = T_company_pic::withTrashed()->find($pic_id);

        return view('archives.detail_pic', compact('pics'));
    }


    public function detail_user(Request $request, $user_id){
        $user = T_user::withTrashed()->find($user_id);

        $inputerNewInput =  T_company::onlyTrashed()
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.company_validation', '=', false)
        
        ->where('t_company_emails.email_validation', '=', false)
        ->where('t_company_domains.domain_validation', '=', false)
        ->where('t_companies.user_id', '=', $user_id)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();
        
       
        $inputerUnComplite =  T_company::onlyTrashed()
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.user_id', '=', $user_id)
        ->where('t_companies.company_validation', '<', 24576)
        ->orWhere('t_company_emails.email_validation', '=', null)->orWhere('t_company_emails.email_validation', '=', true)
        ->where('t_company_domains.domain_validation', '=', null)->orWhere('t_company_domains.domain_validation', '=', true)
        //and
        ->where('t_companies.company_validation', '<', 24576)
        ->where('t_company_domains.domain_validation', '=', null)
        ->where('t_company_domains.domain_validation', '=', null)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();

        $inputerComplite =  T_company::onlyTrashed()
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.company_validation', '>=', 24576)
        ->where('t_company_emails.email_validation', '=', true)
        ->where('t_company_domains.domain_validation', '=', true)
        
        ->where('t_companies.user_id', '=', $user_id)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();

        $inputerallInput = T_company::onlyTrashed()
        ->where('user_id', '=', $user_id)
        ->orderBy('created_at' , 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')->get();

        
        $inputernewInputCount = $inputerNewInput->where('user_id', $user_id)->count();
        $inputerunCompliteCount = $inputerUnComplite->where('user_id', $user_id)->count();
        $inputercompliteCount = $inputerComplite->where('user_id', $user_id)->count();
        $inputerallinputCount = $inputerallInput->where('user_id',$user_id)->count();
        
        

        $err = 0;
        // $err2 = 0;

        foreach ($inputerUnComplite as $item) {
            if($item->company_validation == 0 && ($item->domain_validation != 1 && $item->email_validation != 1)){
                $err++;
            }
        }

        $inputerunCompliteCount -= $err;
        $inputerTotalInput = T_company::where('created_by', '=', auth()->user()->user_name)->count();

        

        $salary = $inputerComplite->count() * 1000;
        return view('archives.detail_user', compact('user', 'salary', 'inputernewInputCount', 'inputercompliteCount', 'inputerunCompliteCount', 'inputerallinputCount'))->with(['page' => 'Archive']);

    }

    public function detail_leads($company_id){
        $companies = T_company::withTrashed()->find($company_id);
        // $companies = T_company::findOrFail($company_id);
        $pic = T_company_pic::withTrashed()->where('company_id', $company_id)->first();
        $note = Inputer::where('company_id', $company_id)->first();
  
        $str_arr = explode(",", $note->inputer_note_company);
        $val = $str_arr[0];
        $data = $str_arr;
        unset($data[0]);

        return view('archives.detail_leads', compact('companies', 'pic', 'note', 'val', 'data'));
    }

    
}
