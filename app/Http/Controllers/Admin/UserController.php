<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\T_user;
use App\Admin\T_company;
use DB;

class UserController extends Controller
{
    public function index()
    {
        $users = T_user::orderBy('created_at', 'DESC')->paginate(10);
        return view('users.index', compact('users'))->with(['page' => 'User']);
    }

    public function create()
    {
        $role = Role::orderBy('name', 'ASC')->get();
        // dd($role);
        return view('users.create', compact('role'))->with(['page' => 'User']);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'user_name' => 'required|string|max:100',
            'user_email' => 'required|email|unique:t_users',
            'user_password' => 'required|min:6',
            'user_nohp' => 'required|max:15',
            'role' => 'required|string|exists:roles,name'
        ]);

        $user = T_user::firstOrCreate([
            'user_email' => $request->user_email
        ], [
            'user_name' => $request->user_name,
            'user_password' => bcrypt($request->user_password),
            'user_nohp' => $request->user_nohp,
            'user_status' => true
        ]);

        $user->assignRole($request->role);
        return redirect(route('users.index'))->with(['success' => 'User: ' . $user->user_name . ' Ditambahkan']);
    }

    public function edit($user_id)
    {
        $user = T_user::findOrFail($user_id);
        
        return view('users.edit', compact('user'))->with(['page' => 'Check']);
    }

    public function update(Request $request, $user_id)
    {
        $this->validate($request, [
            'user_name' => 'required|string|max:100',
            'user_email' => 'email|exists:t_users,user_email',
            'user_password' => 'nullable|min:6',
        ]);

        $user = T_user::findOrFail($user_id);
        $user_password = !empty($request->user_password) ? bcrypt($request->user_password):$user->user_password;
        $user->update([
            'user_name' => $request->user_name,
            'user_password' => $user_password
        ]);
        return redirect(route('users.index'))->with(['success' => 'User: ' . $user->user_name . ' Diperbaharui'])->with(['page' => 'User']);
    }

    public function delete_form($user_id){
        $user = T_user::findOrFail($user_id);

        return view('users.delete_form', compact('user'))->with(['page' => 'User']);
    }

    public function destroy($user_id)
    {
        $user = T_user::findOrFail($user_id);
        $user->delete();
        return redirect()->back()->with(['success' => 'User: ' . $user->user_name . ' Dihapus']);
    }

    public function detail(Request $request, $user_id){

        $inputerNewInput = DB::table('t_companies')
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.company_validation', '=', false)
        ->where('t_companies.deleted_at', '=', null)
        ->where('t_company_emails.email_validation', '=', false)
        ->where('t_company_domains.domain_validation', '=', false)
        ->where('t_companies.user_id', '=', $user_id)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();
        
       
        $inputerUnComplite = DB::table('t_companies')
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.user_id', '=', $user_id)
        ->where('t_companies.company_validation', '<', 24576)
        ->orWhere('t_company_emails.email_validation', '=', null)->orWhere('t_company_emails.email_validation', '=', true)
        ->where('t_company_domains.domain_validation', '=', null)->orWhere('t_company_domains.domain_validation', '=', true)
        //and
        ->where('t_companies.company_validation', '<', 24576)
        ->where('t_company_domains.domain_validation', '=', null)
        ->where('t_company_domains.domain_validation', '=', null)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();

        $inputerComplite = DB::table('t_companies')
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.company_validation', '>=', 24576)
        ->where('t_company_emails.email_validation', '=', true)
        ->where('t_company_domains.domain_validation', '=', true)
        ->where('t_companies.deleted_at', '=', null)
        ->where('t_companies.user_id', '=', $user_id)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();

        $inputerallInput = T_company::where('user_id', '=', $user_id)
        ->orderBy('created_at' , 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')->get();

        
        $inputernewInputCount = $inputerNewInput->where('user_id', $user_id)->count();
        $inputerunCompliteCount = $inputerUnComplite->where('user_id', $user_id)->where('deleted_at', '=',null)->count();
        $inputercompliteCount = $inputerComplite->where('user_id', $user_id)->count();
        $inputerallinputCount = $inputerallInput->where('user_id',$user_id)->count();
        
        

        $err = 0;
        // $err2 = 0;

        foreach ($inputerUnComplite as $item) {
            if($item->company_validation == 0 && ($item->domain_validation != 1 && $item->email_validation != 1)){
                $err++;
            }
        }

        $inputerunCompliteCount -= $err;
        $inputerTotalInput = T_company::where('created_by', '=', auth()->user()->user_name)->count();

        $user = T_user::findOrFail($user_id);

        $salary = $inputerComplite->count() * 1000;
        return view('users.detail', compact('user', 'salary', 'inputernewInputCount', 'inputercompliteCount', 'inputerunCompliteCount', 'inputerallinputCount'))->with(['page' => 'User']);

    }

    public function roles(Request $request, $user_id)
    {
        $user = T_user::findOrFail($user_id);
        
        $roles = Role::all()->pluck('name');
        return view('users.roles', compact('user', 'roles'))->with(['page' => 'Role']);
    }

    public function setRole(Request $request, $user_id)
    {
        $this->validate($request, [
            'role' => 'required'
        ]);

        $user = T_user::findOrFail($user_id);
        $user->syncRoles($request->role);
        return redirect()->back()->with(['success' => 'Role Sudah Di Set'])->with(['page' => 'Role']);
    }

    public function rolePermission(Request $request)
    {
        $role = $request->get('role');
        // $user = $request->get('user_email');

        // dd($user);

        //default permission adalah kosong
        $permissions = null;
        $hasPermission = null;

        //ambil data-data nama role
        $roles = Role::all()->pluck('name');
        // $users = T_user::all()->pluck('user_name');

        //jika parameter terpenuhi
        if (!empty($role)) {
            //select role berdasarkan NAMA
            $getRole = Role::findByName($role);
            // $getUser = T_user::findByName($user);

            $hasPermission = DB::table('role_has_permissions')
            //select nama-nama permission
                ->select('permissions.name')
            //join table permission dengan ROLE HAS PERMISSION

                ->join('permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
                ->where('role_id', $getRole->id)->get()->pluck('name')->all();
                
                //AMBIL DATA PERMISSION
            $permissions = Permission::all()->pluck('name');
        }
        return view('users.role_permission', compact('roles', 'permissions', 'hasPermission'))->with(['page' => 'Role']);
    }

    public function addPermission(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:permissions'
        ]);

        $permission = Permission::firstOrCreate([
            'name' => $request->name
        ]);
        return redirect()->back();
    }

    public function setRolePermission(Request $request, $role)
    {
        //select ROLE berdasarkan nama
        $role = Role::findByName($role);
        // $user = T_user::findByName($user);
        // dd($role);
        // fungsi syncPermissions() untuk menghapus semua permission yang dimiliki role tersebut
        //assign kembali menghindari duplikasi data
        $role->syncPermissions($request->permission);
        
        return redirect()->back()->with(['success' => 'Permission to Role Saved!']);
    }
}
