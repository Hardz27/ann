<?php

namespace App\Http\Controllers;

use App\Admin\T_company;
use App\Admin\T_company_pic;
use Illuminate\Http\Request;
use App\T_user;
use App\History_salary;
use Carbon\Carbon;
use DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB as IlluminateDB;

class HomeController extends Controller
{
    public function index()
    {
    //==========================================================================//
  //                                 DASHBOARD ADMIN                           //          
 //==========================================================================//
        $user = T_user::count();
        $karyawan = T_user::orderBy('user_name', 'ASC')->get();
        $history_salary = History_salary::orderBy('user_id', 'ASC')
                    ->get();


        if(auth()->user()->can('leads inputer')){
            $users = Auth::user();
            $history_salary_inputer = History_salary::where('user_id', $users->user_id)->orderBy('history_id', 'DESC')
                    ->first();
            if ($history_salary_inputer) {
                $gaji = $history_salary_inputer->total_salary;
            }
            else{
                $gaji = 0;
            }
            // dd($history_salary, $users->user_id);
        }
        
       



        // dd($karyawan, $history_salary);
        for ($i=0; $i < count($karyawan); $i++) { 
            if (count($history_salary) == 0) {
                for ($x=0; $x < count($karyawan); $x++) { 
                    $salary_update = $karyawan[$x]->update([
                        'salary' => 0
                    ]);            
                }
            }
            else{
                $cek = History_salary::where('user_id', $karyawan[$i]->user_id)->orderBy('history_id', 'DESC')->first();
                if ($cek) {
                    // dd($cek->total_salary);
                    $salary_update = $karyawan[$i]->update([
                        'salary' => $cek->total_salary
                    ]);
                }
                else{
                    
                }
            }
        }

        $NewInput = DB::table('t_companies')
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.company_validation', '=', false)
        ->where('t_companies.deleted_at', '=', null)
        ->where('t_company_emails.email_validation', '=', false)
        ->where('t_company_domains.domain_validation', '=', false)
        
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get()->count();

        $validasi= DB::table('t_companies')
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.company_validation', '>=', 24576)
        ->where('t_company_emails.email_validation', '=', true)
        ->where('t_company_domains.domain_validation', '=', true)
        ->where('t_companies.deleted_at', '=', null)
        ->orderBy('t_companies.company_name', 'ASC')
        ->orderBy('t_companies.created_at', 'DESC')
        ->get()->count();

        $picCount= T_company_pic::orderBy('pic_id', 'ASC')
        ->where('pic_name', '!=', null)
        ->where('pic_name', '!=', '-')
        ->where('deleted_at', '=', null)
        ->count();
        

        $unComplite = DB::table('t_companies')
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.deleted_at', '=', null)
        ->where(function ($query) {
                $query->where('t_companies.company_validation', '>', 0)
                      ->where('t_companies.company_validation', '<', 24576)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', true)
                                       ->orWhere('t_company_domains.domain_validation', '=', true);
                      });
            })
        ->orWhere(function ($query) {
                $query->where('t_companies.company_validation', '>', 0)
                      ->where('t_companies.company_validation', '<', 24576)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', null)
                                       ->orWhere('t_company_domains.domain_validation', '=', null);
                      });
            })
        ->orWhere(function ($query) {
                $query->where('t_companies.company_validation', '=', 0)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', true)
                                       ->orWhere('t_company_domains.domain_validation', '=', true);
                      });
            })
        
        ->where('t_companies.deleted_at', '=', null)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();

        

           //==========================================================================//
         //                                DASHBOARD INPUTER                          //          
        //==========================================================================//

        $inputerNewInput = DB::table('t_companies')
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.company_validation', '=', false)
        ->where('t_companies.deleted_at', '=', null)
        ->where('t_company_emails.email_validation', '=', false)
        ->where('t_company_domains.domain_validation', '=', false)
        ->where('t_companies.user_id', '=', auth()->user()->user_id)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get()->count();
        
        $inputerValidasi = DB::table('t_companies')
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.company_validation', '>=', 24576)
        ->where('t_company_emails.email_validation', '=', true)
        ->where('t_company_domains.domain_validation', '=', true)
        ->where('t_companies.deleted_at', '=', null)
        ->where('t_companies.user_id', '=', auth()->user()->user_id)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get()->count();


        $inputerunComplite = DB::table('t_companies')
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.deleted_at', '=', null)
        ->where('t_companies.created_by', '=', auth()->user()->user_name)
        ->where(function ($query) {
                $query->where('t_companies.created_by', '=', auth()->user()->user_name)
                      ->where('t_companies.company_validation', '>', 0)
                      ->where('t_companies.company_validation', '<', 24576)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', true)
                                       ->orWhere('t_company_domains.domain_validation', '=', true);
                      });
            })
        ->orWhere(function ($query) {
                $query->where('t_companies.created_by', '=', auth()->user()->user_name)
                      ->where('t_companies.company_validation', '>', 0)
                      ->where('t_companies.company_validation', '<', 24576)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', null)
                                       ->orWhere('t_company_domains.domain_validation', '=', null);
                      });
            })
        ->orWhere(function ($query) {
                $query->where('t_companies.created_by', '=', auth()->user()->user_name)
                      ->where('t_companies.company_validation', '=', 0)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', true)
                                       ->orWhere('t_company_domains.domain_validation', '=', true);
                      });
            })
        ->where('t_companies.user_id', '=', auth()->user()->user_id)
        ->where('t_companies.deleted_at', '=', null)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();
        $inputerunCompliteCount = $inputerunComplite->where('user_id', '=', auth()->user()->user_id)->where('deleted_at', '=',null)->count();
        $unCompliteCount = $unComplite->where('deleted_at', '=',null)->count();

        $TotalInput = $NewInput + $unCompliteCount + $validasi;

        // $err = 0;
        // $err2 = 0;

        // foreach ($inputerunComplite as $item) {
        //     if($item->company_validation == 0 && ($item->domain_validation != 1 && $item->email_validation != 1)){
        //         $err++;
        //     }
        // }

        // foreach ($unComplite as $item) {
        //     if($item->company_validation == 0 && ($item->domain_validation != 1 && $item->email_validation != 1)){
        //         $err2++;
        //     }
        // }

        // $inputerunCompliteCount -= $err;
        // $unCompliteCount -= $err2;

        $inputerTotalInput = T_company::where('created_by', '=', auth()->user()->user_name)->count();
        $total_salary = 0;
        
        foreach ($karyawan as $item) {
            $total_salary += $item->salary;
        }

        // dd($gaji);

        // $rupiah = $inputerValidasi * 1000;
        // $salaryKaryawan = $validasi * 1000;

        

        return view('home', compact('picCount','NewInput','user','role', 'TotalInput', 'validasi','inputerValidasi', 'total_salary', 'gaji',
                    'unComplite', 'unCompliteCount', 'inputerunComplite', 'inputerunCompliteCount', 'inputerTotalInput', 'inputerNewInput'))->with(['page' => 'Home']);
    }

    public function valid_admin(){
        $user = T_user::orderBy('created_at', 'DESC');
        $adminComplite = DB::table('t_companies')
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.company_validation', '>=', 24576)
        ->where('t_company_emails.email_validation', '=', true)
        ->where('t_company_domains.domain_validation', '=', true)
        ->where('t_companies.deleted_at', '=', null)
        // ->where('t_companies.user_id', '=', auth()->user()->user_id)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->paginate(5);

        return view('home_valid_admin', compact('user','adminComplite'));
    }

    public function unvalid_admin(){
        $user = T_user::orderBy('created_at', 'DESC');
        $adminUncomplite = DB::table('t_companies')
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.deleted_at', '=', null)
        ->where(function ($query) {
                $query->where('t_companies.company_validation', '>', 0)
                      ->where('t_companies.company_validation', '<', 24576)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', true)
                                       ->orWhere('t_company_domains.domain_validation', '=', true);
                      });
            })
        ->orWhere(function ($query) {
                $query->where('t_companies.company_validation', '>', 0)
                      ->where('t_companies.company_validation', '<', 24576)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', null)
                                       ->orWhere('t_company_domains.domain_validation', '=', null);
                      });
            })
        ->orWhere(function ($query) {
                $query->where('t_companies.company_validation', '=', 0)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', true)
                                       ->orWhere('t_company_domains.domain_validation', '=', true);
                      });
            })
        
        ->where('t_companies.deleted_at', '=', null)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->paginate(5);

        return view('home_unvalid_admin', compact('user','adminUncomplite'));
    }

    public function valid(){
        $inputerComplite = DB::table('t_companies')
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.company_validation', '>=', 24576)
        ->where('t_company_emails.email_validation', '=', true)
        ->where('t_company_domains.domain_validation', '=', true)
        ->where('t_companies.deleted_at', '=', null)
        ->where('t_companies.user_id', '=', auth()->user()->user_id)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->paginate(5);

        return view('home_valid', compact('inputerComplite'));
    }

    public function unvalid(){
        $inputerUnComplite = DB::table('t_companies')
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.deleted_at', '=', null)
        ->where('t_companies.created_by', '=', auth()->user()->user_name)
        ->where(function ($query) {
                $query->where('t_companies.created_by', '=', auth()->user()->user_name)
                      ->where('t_companies.company_validation', '>', 0)
                      ->where('t_companies.company_validation', '<', 24576)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', true)
                                       ->orWhere('t_company_domains.domain_validation', '=', true);
                      });
            })
        ->orWhere(function ($query) {
                $query->where('t_companies.created_by', '=', auth()->user()->user_name)
                      ->where('t_companies.company_validation', '>', 0)
                      ->where('t_companies.company_validation', '<', 24576)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', null)
                                       ->orWhere('t_company_domains.domain_validation', '=', null);
                      });
            })
        ->orWhere(function ($query) {
                $query->where('t_companies.created_by', '=', auth()->user()->user_name)
                      ->where('t_companies.company_validation', '=', 0)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', true)
                                       ->orWhere('t_company_domains.domain_validation', '=', true);
                      });
            })
        
        ->where('t_companies.deleted_at', '=', null)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->paginate(5);

        return view('home_unvalid', compact('inputerUnComplite'));
    }

    public function karyawan(Request $request){

        $roles = Role::get();
        $user = T_company::select('created_by')->get();
        // $karyawan = DB::table('t_companies')->get(['created_by']);
        $karyawan = T_user::orderBy('user_name', 'ASC')->get();

        return view('home_karyawan', compact('karyawan', 'role', 'rupiah', 'user', 'role'));

    }
    
//Method untuk generate Data 1 minggu terakhir
    public function getChart()
    {
        $start = Carbon::now()->subWeek()->addDay()->format('Y-m-d') . ' 00:00:01';
        $end = Carbon::now()->format('Y-m-d') . ' 23:59:00';

        
        $all = T_company::get('created_by');
        $admin = T_company::where('created_by', '=', 'Fahrur')->get();
        
        $inputer = T_company::where('created_by', '=', 'Sofyan')->get();
        //SELECT DATA KAPAN RECORDS DIBUAT DAN JUGA TOTAL COMPANY
        $company = T_company::select(DB::raw('date(created_at) as company_date'), DB::raw('count(created_by) as total_company'))
         
                    
                    ->whereBetween('created_at', [$start, $end])
                   
                    ->groupBy('created_at')->get()->pluck('total_company', 'company_date')->all();

        // $companyInputer = T_company::select(DB::raw('date(created_at) as company_date'), DB::raw('count(*) as total_company'))
        //             ->whereBetween('created_at', [$start, $end])
        //             ->where('created_by', $inputer)
        //             ->groupBy('created_at')->get()->pluck('total_company', 'company_date')->all();
                    
        
                    //LOOPING TANGGAL dengan interval Semiggu terakhir
                    for($i = Carbon::now()->subWeek()->addDay(); $i <= Carbon::now(); $i->addDay()){
                        //jika ada Data
                        if (array_key_exists($i->format('Y-m-d'), $company)) {
                            //Maka Total Company push ke key tanggal
                            $data[$i->format('Y-m-d')] = $company[$i->format('Y-m-d')];
                        // }if (array_key_exists($i->format('Y-m-d'), $companyInputer)) {
                        //     //Maka Total Company push ke key tanggal
                        //     $data2[$i->format('Y-m-d')] = $companyInputer[$i->format('Y-m-d')];
                        
                        // 
                        }else {
                            //jika Tidak
                            $data[$i->format('Y-m-d')] = 0;
                        }
                    }
            // return response()->json($data, $data2);
            return response()->json($data);
    }

}