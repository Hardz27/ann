<?php

namespace App\Http\Controllers\Inputer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\T_user;
use DB;

class UserController extends Controller
{
    public function index()
    {
        $users = T_user::orderBy('created_at', 'DESC')->paginate(10);
        return view('users.index', compact('users'));
    }

    public function create()
    {
        $role = Role::orderBy('name', 'ASC')->get();
        return view('users.create', compact('role'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'user_name' => 'required|string|max:100',
            'user_email' => 'required|email|unique:t_users',
            'user_password' => 'required|min:6',
            'user_nohp' => 'required|max:15',
            'role' => 'required|string|exists:roles,name'
        ]);

        $user = T_user::firstOrCreate([
            'user_email' => $request->user_email
        ], [
            'user_name' => $request->user_name,
            'user_password' => $request->user_password,
            'user_nohp' => $request->user_nohp,
            'user_status' => true
        ]);

        $user->assignRole($request->role);
        return redirect(route('users.index'))->with(['success' => 'User: ' . $user->user_name . ' Ditambahkan']);
    }

    public function edit($user_id)
    {
        $user = T_user::findOrFail($user_id);
        return view('users.edit', compact('user'));
    }

    public function update(Request $request, $user_id)
    {
        $this->validate($request, [
            'user_name' => 'required|string|max:100',
            'user_email' => 'required|email|exists:t_users,user_email',
            'user_password' => 'nullable|min:6',
        ]);

        $user = T_user::findOrFail($user_id);
        $user_password = !empty($request->user_password) ? bcrypt($request->user_password):$user->user_password;
        $user->update([
            'user_name' => $request->user_name,
            'user_password' => $user_password
        ]);
        return redirect(route('users.index'))->with(['success' => 'User: ' . $user->user_name . ' Diperbaharui']);
    }

    public function destroy($user_id)
    {
        $user = T_user::findOrFail($user_id);
        $user->delete();
        return redirect()->back()->with(['success' => 'User: ' . $user->user_name . ' Dihapus']);
    }

    public function roles(Request $request, $user_id)
    {
        $user = T_user::findOrFail($user_id);
        $roles = Role::all()->pluck('name');
        return view('users.roles', compact('user', 'roles'));
    }

    public function setRole(Request $request, $user_id)
    {
        $this->validate($request, [
            'role' => 'required'
        ]);

        $user = T_user::findOrFail($user_id);
        $user->syncRoles($request->role);
        return redirect()->back()->with(['success' => 'Role Sudah Di Set']);
    }

    public function rolePermission(Request $request)
    {
        $role = $request->get('role');
        $permissions = null;
        $hasPermission = null;

        $roles = Role::all()->pluck('user_name');

        if (!empty($role)) {
            $getRole = Role::findByName($role);
            $hasPermission = DB::table('role_has_permissions')
                ->select('permissions.user_name')
                ->join('permissions', 'role_has_permissions.permission_id', '=', 'permissions.user_id')
                ->where('role_id', $getRole->
                _id)->get()->pluck('user_name')->all();
            $permissions = Permission::all()->pluck('user_name');
        }
        return view('users.role_permission', compact('roles', 'permissions', 'hasPermission'));
    }

    public function addPermission(Request $request)
    {
        $this->validate($request, [
            'user_name' => 'required|string|unique:permissions'
        ]);

        $permission = Permission::firstOrCreate([
            'user_name' => $request->user_name
        ]);
        return redirect()->back();
    }

    public function setRolePermission(Request $request, $role)
    {
        $role = Role::findByName($role);
        $role->syncPermissions($request->permission);
        return redirect()->back()->with(['success' => 'Permission to Role Saved!']);
    }
}
