<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use App\Admin\T_company;
use App\Admin\T_company_domain;


class CheckController extends Controller
{
    public function input(){

            $company = T_company::orderBy('company_id', 'ASC')->get();
            $domain =  T_company_domain::orderBy('domain_id', 'ASC')->get();
            return view('checks.index', compact('company', 'domain'));
        
    }


    public function checkInput(Request $request)
    {    
        // dd($request->headers->all());

        
    $this->validate($request, [
        'company_name' => 'required|unique:t_companies|string',
        'domain_name' => 'required|unique:t_company_domains|string',
    ]);

       
        $company = T_company::where('company_name', '=', $request->company_name)->get();
        $domain = T_company_domain::where('domain_name', '=', $request->domain_name)->get();
       
            if ($company and $domain === NULL) {
                return redirect(route('check.input'))->with(['error' => 'data tidak terdaftar!']);
                
          }
          else{
                return redirect(route('check.input'))->with(['error' => 'data terdaftar']);
          }
       
 
    }
}


           
    

    // $this->validate($request, [
    //     'company_name' => 'required|string',
    //     'company_address' => 'required|string',
    // ]);

    //  if(T_company::where('company_name', '=', $request->input('company_name'))
    //         ->where('company_address', '=', $request->input('company_address'))->exists()){
    //             return "exist";
    //         } else{
    //             return "not exist";
    //         }
    //   }


    // LOGIK 2
//     public function input(){
//         $company = T_company::orderBy('company_id', 'ASC')->get();
//         $domain =  T_company_domain::orderBy('domain_id', 'ASC')->get();
//         return view('checks.index', compact('company', 'domain'));
//     }

//     public function checkInput(Request $request)
//     {    
//         $this->validate($request, [
//             'company_name' => 'required|string',
//             'domain_name' => 'required|string'
//         ]);
// //    LOGIK 1 
//         $company = T_company::where('company_name', '=', $request->company_name);
//         $domain = T_company_domain::where('domain_name', '=', $request->domain_name);

//             if ($company && $domain === null) {
//                 return redirect(route('checks.index'))->with(['error' => 'data tidak terdaftar!']);
                
//           }




