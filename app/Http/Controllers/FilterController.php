<?php

namespace App\Http\Controllers;

use App\Admin\T_company;
use App\Admin\T_company_pic;
use App\T_user;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FilterController extends Controller
{

    public function index(Request $request)
    {
        $companies = T_company::orderBy('company_name', 'desc')->with('user');
        $pics = T_company_pic::where('pic_name', '!=', null)->orderBy('pic_name', 'ASC')->with('company', 'company.user');
        $users = T_user::orderBy('user_name', 'ASC')->get();
        $company = T_company::all();
        
#Filter Category#
        if (!empty($request->category == 'company')) { 
            // dd($request->category);   
            $companies = $companies;
            // $companies->orderBy('company_name', 'ASC')->get();
        }
        if(!empty($request->category == 'pic')) {
            // dd($request->category);   
            $pics = $pics; 
        }

#Filter User#
        if(!empty($request->user_id) && $request->category == 'company')
        {
            // dd("company");
            $companies->where('user_id', $request->user_id);

        }if(!empty($request->user_id) && $request->category == 'pic') {
            // dd("pic");

            // $pics->where('user_id', $request->user_id);

            $pics->whereHas('company', function($q) use ($request){
                $q->where('user_id', $request->user_id);
            });
           
        }

        #Filter Date#        
        if (!empty($request->start_date) && !empty($request->end_date)) {
            
            $this->validate($request,[
                'start_date' => 'nullable|date',
                'end_date' => 'nullable|date'
            ]);

            $start_date = Carbon::parse($request->start_date)->format('Y-m-d') . ' 00:00:01';
            $end_date = Carbon::parse($request->end_date)->format('Y-m-d') . ' 23:59:59';
            
            if($request->category == 'company') {
                $companies = $companies->whereBetween('created_at', [$start_date, $end_date])->get();
            } else {
                $pics = $pics->whereBetween('created_at', [$start_date, $end_date])->get();
            }
        } else {
            $companies = $companies->get();
            $pics = $pics->get();
        }

        #End filter date

        return view('filters.index', [
            'companies' => $companies,
            'pics' => $pics,
            'users' => $users,
            'company' => $company
        ])->with(['page' => 'Filter']);

    }
    
}
