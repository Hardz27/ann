<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/check';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'user_email' => 'required|email',
            'user_password' => 'required|string'
        ]);

        //  $qwe = auth()->attempt(['user_email' => $request->user_email, 'user_password' => $request->user_password , 'user_status' => true]);
        //  dd(auth()->check());

        if (auth()->attempt(['user_email' => $request->user_email, 'user_password' => $request->user_password, 'user_status' => 1])) {
            return redirect()->intended('check');
        }
        return redirect()->back()->with(['error' => 'Password Invalid / Inactive Users']);
    }
}
