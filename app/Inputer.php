<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Inputer extends Model
{


     protected static function boot(){
          parent::boot();
          static::creating(function($model){
               $user = Auth::user();
               // $model->created_by = $user->user_name;
               // $model->user_id = $user->user_id;
               $model->inputer_name = $user->user_name;
          });

          static::updating(function($model){
               $user = Auth::user();
               // $model->updated_by = $user->user_name;
          });
     }
     
    protected $fillable = [
        'inputer_id', 'user_id', 'company_id',
        'inputer_name', 'inputer_note_company', 
        'inputer_note_domain','inputer_note_email',
        'inputer_note_pic','created_at','updated_at'

    ];

    protected $table = 'inputers';
    protected $primaryKey = 'inputer_id';

    public function note_company(){
        return $this->hasOne('App\Admin\T_company', 'inputer_id');
    }

    public function user()
    {
         return $this->belongsTo('App\Inputer', 'user_id');
    }

}
