<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temp_company extends Model
{
    protected $table = 'temp_companies';

    protected $fillable = [
        'temp_company_id', 
        'temp_company_name', 
        'temp_company_time',
        'temp_company_code',
        'temp_company_link' 
    ];

    

    public $timestamps = true;

    public function job()
    {
        return $this->hasMany('App\Job');
    }
}
