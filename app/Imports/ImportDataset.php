<?php

namespace App\Imports;

use App\Admin\M_dataset;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Illuminate\Support\Facades\DB;

class ImportDataset implements ToModel, WithBatchInserts, WithChunkReading, ShouldQueue
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        $data_conf = DB::table('tb_fuzzy_conf')->where('status_conf', 1)->first();
        $fuzzy_conf = $data_conf->json_conf;
        $fuzzy_conf = json_decode($fuzzy_conf);
        $fuzzy_conf = [
            'soil_moisture' => [
                'soil_satruated' => [
                    '0' => $fuzzy_conf->soil_moisture->soil_satruated[0],
                    '1' => $fuzzy_conf->soil_moisture->soil_satruated[1],
                    '2' => $fuzzy_conf->soil_moisture->soil_satruated[2],
                    '3' => $fuzzy_conf->soil_moisture->soil_satruated[3],
                ],
                'soil_adq' => [
                    '0' => $fuzzy_conf->soil_moisture->soil_adq[0],
                    '1' => $fuzzy_conf->soil_moisture->soil_adq[1],
                    '2' => $fuzzy_conf->soil_moisture->soil_adq[2],
                    '3' => $fuzzy_conf->soil_moisture->soil_adq[3],
                ],
                'soil_normal' => [
                    '0' => $fuzzy_conf->soil_moisture->soil_normal[0],
                    '1' => $fuzzy_conf->soil_moisture->soil_normal[1],
                    '2' => $fuzzy_conf->soil_moisture->soil_normal[2],
                    '3' => $fuzzy_conf->soil_moisture->soil_normal[3],
                ],
                'soil_dry' => [
                    '0' => $fuzzy_conf->soil_moisture->soil_dry[0],
                    '1' => $fuzzy_conf->soil_moisture->soil_dry[1],
                    '2' => $fuzzy_conf->soil_moisture->soil_dry[2],
                    '3' => $fuzzy_conf->soil_moisture->soil_dry[3],
                ]
            ],
            'air_temprature' => [
                'temp_vcold' => [
                    '0' => $fuzzy_conf->air_temprature->temp_vcold[0],
                    '1' => $fuzzy_conf->air_temprature->temp_vcold[1],
                    '2' => $fuzzy_conf->air_temprature->temp_vcold[2],
                    '3' => $fuzzy_conf->air_temprature->temp_vcold[3],
                ],
                'temp_cold' => [
                    '0' => $fuzzy_conf->air_temprature->temp_cold[0],
                    '1' => $fuzzy_conf->air_temprature->temp_cold[1],
                    '2' => $fuzzy_conf->air_temprature->temp_cold[2],
                    '3' => $fuzzy_conf->air_temprature->temp_cold[3],
                ],
                'temp_normal' => [
                    '0' => $fuzzy_conf->air_temprature->temp_normal[0],
                    '1' => $fuzzy_conf->air_temprature->temp_normal[1],
                    '2' => $fuzzy_conf->air_temprature->temp_normal[2],
                    '3' => $fuzzy_conf->air_temprature->temp_normal[3],
                ],
                'temp_hot' => [
                    '0' => $fuzzy_conf->air_temprature->temp_hot[0],
                    '1' => $fuzzy_conf->air_temprature->temp_hot[1],
                    '2' => $fuzzy_conf->air_temprature->temp_hot[2],
                    '3' => $fuzzy_conf->air_temprature->temp_hot[3],
                ],
                'temp_vhot' => [
                    '0' => $fuzzy_conf->air_temprature->temp_vhot[0],
                    '1' => $fuzzy_conf->air_temprature->temp_vhot[1],
                    '2' => $fuzzy_conf->air_temprature->temp_vhot[2],
                    '3' => $fuzzy_conf->air_temprature->temp_vhot[3],
                ]
            ],
            'air_humidity' => [
                'humid_low' => [
                    '0' => $fuzzy_conf->air_humidity->humid_low[0],
                    '1' => $fuzzy_conf->air_humidity->humid_low[1],
                    '2' => $fuzzy_conf->air_humidity->humid_low[2],
                    '3' => $fuzzy_conf->air_humidity->humid_low[3],
                ],
                'humid_normal' => [
                    '0' => $fuzzy_conf->air_humidity->humid_normal[0],
                    '1' => $fuzzy_conf->air_humidity->humid_normal[1],
                    '2' => $fuzzy_conf->air_humidity->humid_normal[2],
                    '3' => $fuzzy_conf->air_humidity->humid_normal[3],
                ],
                'humid_high' => [
                    '0' => $fuzzy_conf->air_humidity->humid_high[0],
                    '1' => $fuzzy_conf->air_humidity->humid_high[1],
                    '2' => $fuzzy_conf->air_humidity->humid_high[2],
                    '3' => $fuzzy_conf->air_humidity->humid_high[3],
                ],
                'humid_exthigh' => [
                    '0' => $fuzzy_conf->air_humidity->humid_exthigh[0],
                    '1' => $fuzzy_conf->air_humidity->humid_exthigh[1],
                    '2' => $fuzzy_conf->air_humidity->humid_exthigh[2],
                    '3' => $fuzzy_conf->air_humidity->humid_exthigh[3],
                ]
            ]
        ];

        $data = [
            'value_soil_moisture'   => $row[0],
            'value_air_temprature'  => $row[1],
            'value_air_humidity'    => $row[2]
        ];

        $label = $this->fuzzy($data, $fuzzy_conf);

        return new M_dataset([
            'value_soil_moisture' => $row[0],
            'value_air_temprature' => $row[1], 
            'value_air_humidity' => $row[2], 
            'label' => $label, 
        ]);
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    }


    // ======================================== Fuzzy label ==============================================

    // ================ Soil Moisture ==========================

    public function generateFuzzy($fuzzy_conf, $x){
        if ($x <= $fuzzy_conf[0] || $x >= $fuzzy_conf[3]) {
          return 0;  
        }else if($x > $fuzzy_conf[0] && $x < $fuzzy_conf[1]){
          return ($x-$fuzzy_conf[0])/($fuzzy_conf[1]-$fuzzy_conf[0]);
        }else if($x >= $fuzzy_conf[1] && $x <= $fuzzy_conf[2]){
          return 1;
        }else if($x > $fuzzy_conf[2] && $x < $fuzzy_conf[3]){
          return ($fuzzy_conf[3]-$x)/($fuzzy_conf[3]-$fuzzy_conf[2]);
        }
    }

    public function dry($fuzzy_conf, $x)
    {
        return $this->generateFuzzy($fuzzy_conf, $x);
    }

    public function normal($fuzzy_conf, $x)
    {
        return $this->generateFuzzy($fuzzy_conf, $x);
    }

    public function adq_wet($fuzzy_conf, $x)
    {
        return $this->generateFuzzy($fuzzy_conf, $x);
    }

    public function satruated($fuzzy_conf, $x)
    {
        return $this->generateFuzzy($fuzzy_conf, $x);
    }

    // ====================== Air Temprature ==========================

    public function v_cold($fuzzy_conf, $x)
    {
        return $this->generateFuzzy($fuzzy_conf, $x);
    }

    public function cold($fuzzy_conf, $x)
    {
        return $this->generateFuzzy($fuzzy_conf, $x);
    }

    public function normal_temp($fuzzy_conf, $x)
    {
        return $this->generateFuzzy($fuzzy_conf, $x);
    }

    public function hot($fuzzy_conf, $x)
    {
        return $this->generateFuzzy($fuzzy_conf, $x);
    }

    public function v_hot($fuzzy_conf, $x)
    {
        return $this->generateFuzzy($fuzzy_conf, $x);
    }

        // ====================== Air Humidity ==========================

    public function low($fuzzy_conf, $x)
    {
        return $this->generateFuzzy($fuzzy_conf, $x);
    }

    public function medium($fuzzy_conf, $x)
    {
        return $this->generateFuzzy($fuzzy_conf, $x);
    }

    public function high($fuzzy_conf, $x)
    {
        return $this->generateFuzzy($fuzzy_conf, $x);
    }

    public function ext_high($fuzzy_conf, $x)
    {
        return $this->generateFuzzy($fuzzy_conf, $x);
    }

        // ====================== Output ==========================

    public function onebyfive($alpha)
    {
        $z[] = 30 - ($alpha * 10);

        return $z[0];
    }

    public function twobyfive($alpha)
    {
        $z[] = ($alpha * 10) + 20;
        $z[] = 50 - ($alpha * 10);
        sort($z);

        return $z[0];
    }

    public function threebyfive($alpha)
    {
        $z[] = ($alpha * 10) + 40;
        $z[] = 70 - ($alpha * 10);
        sort($z);

        return $z[0];
    }

    public function fourbyfive($alpha)
    {
        $z[] = ($alpha * 10) + 60;
        $z[] = 80 - ($alpha * 10);
        sort($z);

        return $z[0];
    }

    public function fivebyfive($alpha)
    {
        $z[] = ($alpha * 10) + 80;

        return $z[0];
    }

    public function ten($alpha)
    {
        $z[] = ($alpha * 10) + 0;
        $z[] = 20 - ($alpha * 10);
        sort($z);

        return $z[0];
    }

    public function twentyfive($alpha)
    {
        $z[] = ($alpha * 15) + 15;
        $z[] = 45 - ($alpha * 15);
        sort($z);

        return $z[0];
    }

    public function fifty($alpha)
    {
        $z[] = ($alpha * 15) + 40;
        $z[] = 70 - ($alpha * 15);
        sort($z);

        return $z[0];
    }

    public function seventyfive($alpha)
    {
        $z[] = ($alpha * 10) + 65;
        $z[] = 85 - ($alpha * 10);
        sort($z);

        return $z[0];
    }

    public function openfull($alpha)
    {
        $z[] = ($alpha * 10) + 80;
        $z[] = 100 - ($alpha * 10);
        sort($z);

        return $z[0];
    }

    // ==========================================================================================
    // =                                                                                        =
    // =                                    Function Fuzzy                                      =
    // =                                                                                        =
    // ==========================================================================================

    public function fuzzy($data, $fuzzy_conf)
    {

        // dd($fuzzy_conf);

        $value_soil_moisture    = $data['value_soil_moisture'];
        $value_air_temprature   = $data['value_air_temprature'];
        $value_air_humidity     = $data['value_air_humidity'];

        $soil_moisture = $value_soil_moisture;
        $air_temprature = $value_air_temprature;
        $air_humidity = $value_air_humidity;
                // echo $soil_moisture .', '. $air_temprature .', '. $air_humidity . '<br>';


                // =========================== Evaluation Rule ===============================

              // R1 Soil Moist Dry dan Air Temprature V.Hot and Air Humid Low Then openfull (fivebyfive) (5)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->v_hot($fuzzy_conf['air_temprature']['temp_vhot'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 1 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[0] = $miu[0];
        $z[0] = $this->fivebyfive($alpha[0]);

              // R2 Soil Moist Dry dan Air Temprature V.Hot and Air Humid Medium Then openfull (fivebyfive) (4)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->v_hot($fuzzy_conf['air_temprature']['temp_vhot'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 2 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[1] = $miu[0];
        $z[1] = $this->fivebyfive($alpha[1]);

              // R3 Soil Moist Dry dan Air Temprature V.Hot and Air Humid High Then seperempat(fourbyfive) (3)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->v_hot($fuzzy_conf['air_temprature']['temp_vhot'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 3 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[2] = $miu[0];
        $z[2] = $this->fourbyfive($alpha[2]);

              // R4 Soil Moist Dry dan Air Temprature V.Hot and Air Humid Extreme cool Then seperempat(fourbyfive) (2)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->v_hot($fuzzy_conf['air_temprature']['temp_vhot'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 4 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[3] = $miu[0];
        $z[3] = $this->fourbyfive($alpha[3]);

    // ==============================================================================

              // R5 Soil Moist Dry dan Air Temprature Hot and Air Humid Low Then openfull(fivebyfive) (5)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->hot($fuzzy_conf['air_temprature']['temp_hot'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 5 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[4] = $miu[0];
        $z[4] = $this->fivebyfive($alpha[4]);

              // R6 Soil Moist Dry dan Air Temprature Hot and Air Humid Medium Then openfull(fivebyfive) (4)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->hot($fuzzy_conf['air_temprature']['temp_hot'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 6 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[5] = $miu[0];
        $z[5] = $this->fivebyfive($alpha[5]);

              // R7 Soil Moist Dry dan Air Temprature Hot and Air Humid High Then (fourbyfive) (3)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->hot($fuzzy_conf['air_temprature']['temp_hot'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 7 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[6] = $miu[0];
        $z[6] = $this->fourbyfive($alpha[6]);

              // R8 Soil Moist Dry dan Air Temprature Hot and Air Humid Extreme Then (fourbyfive) (2)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->hot($fuzzy_conf['air_temprature']['temp_hot'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 8 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[7] = $miu[0];
        $z[7] = $this->fourbyfive($alpha[7]);

    // ==============================================================================

              // R9 Soil Moist Dry dan Air Temprature Normal_temp and Air Humid Low kering Then (fourbyfive) (4)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->normal_temp($fuzzy_conf['air_temprature']['temp_normal'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 9 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[8] = $miu[0];
        $z[8] = $this->fourbyfive($alpha[8]);

              // R10 Soil Moist Dry dan Air Temprature Normal_temp and Air Humid Medium Then (fourbyfive) (3)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->normal_temp($fuzzy_conf['air_temprature']['temp_normal'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 10 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[9] = $miu[0];
        $z[9] = $this->fourbyfive($alpha[9]);

              // R11 Soil Moist Dry dan Air Temprature Normal_temp and Air Humid High Then (fourbyfive) (2)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->normal_temp($fuzzy_conf['air_temprature']['temp_normal'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 11 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[10] = $miu[0];
        $z[10] = $this->fourbyfive($alpha[10]);

              // R12 Soil Moist Dry dan Air Temprature Normal_temp and Air Humid Extreme Then (threebyfive) (2)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->normal_temp($fuzzy_conf['air_temprature']['temp_normal'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 12 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[11] = $miu[0];
        $z[11] = $this->threebyfive($alpha[11]);

    // ==============================================================================

              // R13 Soil Moist Dry dan Air Temprature Cold and Air Humid Low Then (fourbyfive) (3)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->cold($fuzzy_conf['air_temprature']['temp_cold'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 13 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[12] = $miu[0];
        $z[12] = $this->fourbyfive($alpha[12]);

              // R14 Soil Moist Dry dan Air Temprature Cold and Air Humid Medium Then (fourbyfive) (2)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->cold($fuzzy_conf['air_temprature']['temp_cold'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 14 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[13] = $miu[0];
        $z[13] = $this->fourbyfive($alpha[13]);

              // R15 Soil Moist Dry dan Air Temprature Cold and Air Humid High Then (threebyfive) (2)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->cold($fuzzy_conf['air_temprature']['temp_cold'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 15 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[14] = $miu[0];
        $z[14] = $this->threebyfive($alpha[14]);

              // R16 Soil Moist Dry dan Air Temprature Cold and Air Humid Extreme Then (threebyfive) (1)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->cold($fuzzy_conf['air_temprature']['temp_cold'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 16 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[15] = $miu[0];
        $z[15] = $this->threebyfive($alpha[15]);

    // ==============================================================================

              // R17 Soil Moist Dry dan Air Temprature v_cold and Air Humid Low Then (threebyfive) (3)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->v_cold($fuzzy_conf['air_temprature']['temp_vcold'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 17 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[16] = $miu[0];
        $z[16] = $this->threebyfive($alpha[16]);

              // R18 Soil Moist Dry dan Air Temprature v_cold and Air Humid Medium Then (threebyfive) (3)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->v_cold($fuzzy_conf['air_temprature']['temp_vcold'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 18 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[17] = $miu[0];
        $z[17] = $this->threebyfive($alpha[17]);

              // R19 Soil Moist Dry dan Air Temprature v_cold and Air Humid High Then (twobyfive) (2)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->v_cold($fuzzy_conf['air_temprature']['temp_vcold'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 19 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[18] = $miu[0];
        $z[18] = $this->twobyfive($alpha[18]);

              // R20 Soil Moist Dry dan Air Temprature v_cold and Air Humid Extreme Then (twobyfive) (1)
        $miuSoilMoist = $this->dry($fuzzy_conf['soil_moisture']['soil_dry'],$soil_moisture);
        $miuAirTemp = $this->v_cold($fuzzy_conf['air_temprature']['temp_vcold'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 20 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[19] = $miu[0];
        $z[19] = $this->twobyfive($alpha[19]);


    // ==============================================================================

    // ##############################################################################

    // ==============================================================================

              // R21 Soil Moist normal dan Air Temprature V.Hot and Air Humid Low Then (fourbyfive) (4)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->v_hot($fuzzy_conf['air_temprature']['temp_vhot'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 21 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[20] = $miu[0];
        $z[20] = $this->fourbyfive($alpha[20]);

              // R22 Soil Moist normal dan Air Temprature V.Hot and Air Humid Medium Then (fourbyfive) (4)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->v_hot($fuzzy_conf['air_temprature']['temp_vhot'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);          
                // echo "Rule 22 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';  
        $alpha[21] = $miu[0];
        $z[21] = $this->fourbyfive($alpha[21]);

              // R23 Soil Moist normal dan Air Temprature V.Hot and Air Humid High Then (threebyfive) (3)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->v_hot($fuzzy_conf['air_temprature']['temp_vhot'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 23 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[22] = $miu[0];
        $z[22] = $this->threebyfive($alpha[22]);

              // R24 Soil Moist normal dan Air Temprature V.Hot and Air Humid Extreme Then (threebyfive) (3)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->v_hot($fuzzy_conf['air_temprature']['temp_vhot'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 24 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[23] = $miu[0];
        $z[23] = $this->threebyfive($alpha[23]);

    // ==============================================================================

              // R25 Soil Moist normal dan Air Temprature Hot and Air Humid Low Then (fourbyfive) (4)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->hot($fuzzy_conf['air_temprature']['temp_hot'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 25 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[24] = $miu[0];
        $z[24] = $this->fourbyfive($alpha[24]);

              // R26 Soil Moist normal dan Air Temprature Hot and Air Humid Medium Then (fourbyfive) (4)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->hot($fuzzy_conf['air_temprature']['temp_hot'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 26 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[25] = $miu[0];
        $z[25] = $this->fourbyfive($alpha[25]);

              // R27 Soil Moist normal dan Air Temprature Hot and Air Humid High Then (threebyfive) (3)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->hot($fuzzy_conf['air_temprature']['temp_hot'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 27 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[26] = $miu[0];
        $z[26] = $this->threebyfive($alpha[26]);

              // R28 Soil Moist normal dan Air Temprature Hot and Air Humid Extreme Then (threebyfive) (3)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->hot($fuzzy_conf['air_temprature']['temp_hot'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 28 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[27] = $miu[0];
        $z[27] = $this->threebyfive($alpha[27]);

    // ==============================================================================

              // R29 Soil Moist normal dan Air Temprature Normal_temp and Air Humid Low Then (threebyfive) (4)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->normal_temp($fuzzy_conf['air_temprature']['temp_normal'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 29 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[28] = $miu[0];
        $z[28] = $this->threebyfive($alpha[28]);

              // R30 Soil Moist normal dan Air Temprature Normal_temp and Air Humid Medium Then (threebyfive) (4)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->normal_temp($fuzzy_conf['air_temprature']['temp_normal'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);           
                // echo "Rule 30 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>'; 
        $alpha[29] = $miu[0];
        $z[29] = $this->threebyfive($alpha[29]);

              // R31 Soil Moist normal dan Air Temprature Normal_temp and Air Humid High Then (twobyfive) (3)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->normal_temp($fuzzy_conf['air_temprature']['temp_normal'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
                // echo "Rule 31 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[30] = $miu[0];
        $z[30] = $this->twobyfive($alpha[30]);

              // R32 Soil Moist normal dan Air Temprature Normal_temp and Air Humid Extreme Then (twobyfive) (2)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->normal_temp($fuzzy_conf['air_temprature']['temp_normal'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
                // echo "Rule 32 : ". $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $alpha[31] = $miu[0];
        $z[31] = $this->twobyfive($alpha[31]);


    // ==============================================================================

        $n = 32;

              // R33 Soil Moist normal dan Air Temprature Cold and Air Humid Low Then (threebyfive) (3)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->cold($fuzzy_conf['air_temprature']['temp_cold'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[$n] = $miu[0];
                // echo "Rule ". (($n+1)) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->threebyfive($alpha[$n]);

              // R34 Soil Moist normal dan Air Temprature Cold and Air Humid Medium Then (twobyfive) (2)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->cold($fuzzy_conf['air_temprature']['temp_cold'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->twobyfive($alpha[$n]);

              // R35 Soil Moist normal dan Air Temprature Cold and Air Humid High Then (twobyfive) (2)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->cold($fuzzy_conf['air_temprature']['temp_cold'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->twobyfive($alpha[$n]);

              // R36 Soil Moist normal dan Air Temprature Cold and Air Humid Extreme Then (onebyfive) (2)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->cold($fuzzy_conf['air_temprature']['temp_cold'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

    // ==============================================================================

              // R37 Soil Moist normal dan Air Temprature v_cold and Air Humid Low Then (twobyfive) (4)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->v_cold($fuzzy_conf['air_temprature']['temp_vcold'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->twobyfive($alpha[$n]);

              // R38 Soil Moist normal dan Air Temprature v_cold and Air Humid Medium Then (twobyfive) (3)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->v_cold($fuzzy_conf['air_temprature']['temp_vcold'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->twobyfive($alpha[$n]);

              // R39 Soil Moist normal dan Air Temprature v_cold and Air Humid High Then ten (onebyfive) (2)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->v_cold($fuzzy_conf['air_temprature']['temp_vcold'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R40 Soil Moist normal dan Air Temprature v_cold and Air Humid Extreme Then ten (onebyfive) (2)
        $miuSoilMoist = $this->normal($fuzzy_conf['soil_moisture']['soil_normal'],$soil_moisture);
        $miuAirTemp = $this->v_cold($fuzzy_conf['air_temprature']['temp_vcold'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

    // ==============================================================================

    // ##############################################################################

    // ==============================================================================

              // R41 Soil Moist Adq_wet dan Air Temprature V.Hot and Air Humid Low Then openfull (twobyfive) (3)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->v_hot($fuzzy_conf['air_temprature']['temp_vhot'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->twobyfive($alpha[$n]);

              // R42 Soil Moist Adq_wet dan Air Temprature V.Hot and Air Humid Medium Then openfull (twobyfive) (3)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->v_hot($fuzzy_conf['air_temprature']['temp_vhot'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->twobyfive($alpha[$n]);

              // R43 Soil Moist Adq_wet dan Air Temprature V.Hot and Air Humid High Then seventyfive (onebyfive) (2)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->v_hot($fuzzy_conf['air_temprature']['temp_vhot'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R44 Soil Moist Adq_wet dan Air Temprature V.Hot and Air Humid Extreme Then seventyfive (onebyfive) (1)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->v_hot($fuzzy_conf['air_temprature']['temp_vhot'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);


    // ==============================================================================

              // R45 Soil Moist Adq_wet dan Air Temprature Hot and Air Humid Low Then seventyfive (twobyfive) (3)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->hot($fuzzy_conf['air_temprature']['temp_hot'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->twobyfive($alpha[$n]);

              // R46 Soil Moist Adq_wet dan Air Temprature Hot and Air Humid Medium Then seventyfive (twobyfive) (3)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->hot($fuzzy_conf['air_temprature']['temp_hot'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->twobyfive($alpha[$n]);

              // R47 Soil Moist Adq_wet dan Air Temprature Hot and Air Humid High Then fifty (onebyfive) (3)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->hot($fuzzy_conf['air_temprature']['temp_hot'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R48 Soil Moist Adq_wet dan Air Temprature Hot and Air Humid Extreme Then fifty (onebyfive) (2)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->hot($fuzzy_conf['air_temprature']['temp_hot'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

    // ==============================================================================

              // R49 Soil Moist Adq_wet dan Air Temprature Normal_temp and Air Humid Low Then (onebyfive)  (3)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->normal_temp($fuzzy_conf['air_temprature']['temp_normal'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R50 Soil Moist Adq_wet dan Air Temprature Normal_temp and Air Humid Medium Then (onebyfive)  (2)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->normal_temp($fuzzy_conf['air_temprature']['temp_normal'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);     
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';       
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R51 Soil Moist Adq_wet dan Air Temprature Normal_temp and Air Humid High Then ten (onebyfive) (1)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->normal_temp($fuzzy_conf['air_temprature']['temp_normal'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R52 Soil Moist Adq_wet dan Air Temprature Normal_temp and Air Humid Extreme ten (onebyfive) (1)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->normal_temp($fuzzy_conf['air_temprature']['temp_normal'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);


    // ==============================================================================

              // R53 Soil Moist Adq_wet dan Air Temprature Cold and Air Humid Low Then twentyfive (onebyfive) (3)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->cold($fuzzy_conf['air_temprature']['temp_cold'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R54 Soil Moist Adq_wet dan Air Temprature Cold and Air Humid Medium Then ten (onebyfive) (3)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->cold($fuzzy_conf['air_temprature']['temp_cold'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R55 Soil Moist Adq_wet dan Air Temprature Cold and Air Humid High Then ten (onebyfive) (2)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->cold($fuzzy_conf['air_temprature']['temp_cold'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R56 Soil Moist Adq_wet dan Air Temprature Cold and Air Humid Extreme Then ten (onebyfive) (1)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->cold($fuzzy_conf['air_temprature']['temp_cold'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);


    // ==============================================================================

              // R57 Soil Moist Adq_wet dan Air Temprature v_cold and Air Humid Low Then ten (onebyfive) (3)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->v_cold($fuzzy_conf['air_temprature']['temp_vcold'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R58 Soil Moist Adq_wet dan Air Temprature v_cold and Air Humid Medium Then ten (onebyfive) (2)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->v_cold($fuzzy_conf['air_temprature']['temp_vcold'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R59 Soil Moist Adq_wet dan Air Temprature v_cold and Air Humid High Then ten (onebyfive) (1)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->v_cold($fuzzy_conf['air_temprature']['temp_vcold'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);     
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R60 Soil Moist Adq_wet dan Air Temprature v_cold and Air Humid Extreme Then ten (onebyfive) (1)
        $miuSoilMoist = $this->adq_wet($fuzzy_conf['soil_moisture']['soil_adq'],$soil_moisture);
        $miuAirTemp = $this->v_cold($fuzzy_conf['air_temprature']['temp_vcold'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);


    // ==============================================================================

    // ##############################################################################

    // ==============================================================================

              // R61 Soil Moist Satruated dan Air Temprature V.Hot and Air Humid Low Then fifty (onebyfive) (2)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->v_hot($fuzzy_conf['air_temprature']['temp_vhot'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R62 Soil Moist Satruated dan Air Temprature V.Hot and Air Humid Medium Then fifty (onebyfive) (2)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->v_hot($fuzzy_conf['air_temprature']['temp_vhot'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R63 Soil Moist Satruated dan Air Temprature V.Hot and Air Humid High Then twentyfive (onebyfive) (1)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->v_hot($fuzzy_conf['air_temprature']['temp_vhot'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R64 Soil Moist Satruated dan Air Temprature V.Hot and Air Humid Extreme Then twentyfive (onebyfive) (1)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->v_hot($fuzzy_conf['air_temprature']['temp_vhot'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

    // ==============================================================================

              // R65 Soil Moist Satruated dan Air Temprature Hot and Air Humid Low Then fifty (onebyfive) (2)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->hot($fuzzy_conf['air_temprature']['temp_hot'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R66 Soil Moist Satruated dan Air Temprature Hot and Air Humid Medium Then twentyfive (onebyfive) (2)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->hot($fuzzy_conf['air_temprature']['temp_hot'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R67 Soil Moist Satruated dan Air Temprature Hot and Air Humid High Then twentyfive (onebyfive) (1)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->hot($fuzzy_conf['air_temprature']['temp_hot'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R68 Soil Moist Satruated dan Air Temprature Hot and Air Humid Extreme Then twentyfive (onebyfive) (1)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->hot($fuzzy_conf['air_temprature']['temp_hot'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

    // ==============================================================================

              // R69 Soil Moist Satruated dan Air Temprature Normal_temp and Air Humid Low Then twentyfive (onebyfive) (3)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->normal_temp($fuzzy_conf['air_temprature']['temp_normal'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R70 Soil Moist Satruated dan Air Temprature Normal_temp and Air Humid Medium Then twentyfive (onebyfive) (2)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->normal_temp($fuzzy_conf['air_temprature']['temp_normal'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R71 Soil Moist Satruated dan Air Temprature Normal_temp and Air Humid High Then ten (onebyfive) (2)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->normal_temp($fuzzy_conf['air_temprature']['temp_normal'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R72 Soil Moist Satruated dan Air Temprature Normal_temp and Air Humid Extreme Then ten (onebyfive) (1)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->normal_temp($fuzzy_conf['air_temprature']['temp_normal'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);


    // ==============================================================================

              // R73 Soil Moist Satruated dan Air Temprature Cold and Air Humid Low Then ten (onebyfive) (2)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->cold($fuzzy_conf['air_temprature']['temp_cold'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R74 Soil Moist Satruated dan Air Temprature Cold and Air Humid Medium Then ten (onebyfive) (2)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->cold($fuzzy_conf['air_temprature']['temp_cold'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R75 Soil Moist Satruated dan Air Temprature Cold and Air Humid High Then ten (onebyfive) (1)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->cold($fuzzy_conf['air_temprature']['temp_cold'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R76 Soil Moist Satruated dan Air Temprature Cold and Air Humid Extreme Then ten (onebyfive) (1)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->cold($fuzzy_conf['air_temprature']['temp_cold'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

    // ==============================================================================

              // R77 Soil Moist Satruated dan Air Temprature v_cold and Air Humid Low Then ten (onebyfive) (2)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->v_cold($fuzzy_conf['air_temprature']['temp_vcold'],$air_temprature);
        $miuAirHumid = $this->low($fuzzy_conf['air_humidity']['humid_low'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R78 Soil Moist Satruated dan Air Temprature v_cold and Air Humid Medium Then ten (onebyfive) (2)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->v_cold($fuzzy_conf['air_temprature']['temp_vcold'],$air_temprature);
        $miuAirHumid = $this->medium($fuzzy_conf['air_humidity']['humid_normal'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R79 Soil Moist Satruated dan Air Temprature v_cold and Air Humid High Then ten (onebyfive) (1)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->v_cold($fuzzy_conf['air_temprature']['temp_vcold'],$air_temprature);
        $miuAirHumid = $this->high($fuzzy_conf['air_humidity']['humid_high'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);

              // R80 Soil Moist Satruated dan Air Temprature v_cold and Air Humid Extreme Then ten (onebyfive) (1)
        $miuSoilMoist = $this->satruated($fuzzy_conf['soil_moisture']['soil_satruated'],$soil_moisture);
        $miuAirTemp = $this->v_cold($fuzzy_conf['air_temprature']['temp_vcold'],$air_temprature);
        $miuAirHumid = $this->ext_high($fuzzy_conf['air_humidity']['humid_exthigh'],$air_humidity);
        $miu = array($miuSoilMoist, $miuAirTemp, $miuAirHumid);
        sort($miu);            
        $alpha[++$n] = $miu[0];
                // echo "Rule ". ($n+1) .' : '. $miu[0].', '. $miu[1].', '. $miu[2].', '.'<br>';
        $z[$n] = $this->onebyfive($alpha[$n]);


    // ==============================================================================
                // dd('Nilai Z :', $z,' Alpha : ', $alpha);



              //  -------------------------------- Defuzzyfication ------------------------------
        $result = 0;
        $pembagi = 0;
        for ($j=0; $j < count($alpha); $j++) { 
            $result += $z[$j] * $alpha[$j];
            $pembagi += $alpha[$j];
        }
                // dd($result, $pembagi);
        if ($pembagi == 0) {
            $result = 0;
        }else{
            $result = $result / $pembagi;   
        }
        number_format($result, 1);
                // DB::table('normalized')->where('id_data', $i)->update([
                //     'label' => number_format($result, 1).'%'
                // ]);

        return number_format($result, 1).'%';
            // dd($hasil);
    }
}
