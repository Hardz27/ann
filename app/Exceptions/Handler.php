<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {


        if ($exception instanceof \Spatie\Permission\Exceptions\UnauthorizedException) {
            return redirect('login');
        }
    
    
        return parent::render($request, $exception);
    }

}


//     public function render($request, Exception $e)
// {
//     if ($e->getStatusCode() == 403) {
//         return redirect('yourpage'); // this will be on a 403 exception
//     }

//     return parent::render($request, $e); // all the other exceptions
// }

// public function render($request, Exception $e)
// {
//     //if $e is an HttpException
//     if ($e instanceof HttpException ) {

//         //get the status code 
//         $status = $e->getStatusCode() ;

//         //if status code is 501 redirect to custom view
//         if( $status == 501 )
//             return response()->view('my.custom.view', [], 501);
//     }

//     return parent::render($request, $e);
// }



//example

// {
//     if ($exception instanceof \Illuminate\Session\TokenMismatchException) {

//         return redirect('/login');

//     }

//     return parent::render($request, $exception);
// }


// public function render($request, Exception $e)
//     {
//         if ($e instanceof \Illuminate\Session\TokenMismatchException) {
            
//             return redirect('/login');
 
//         }

//         return parent::render($request, $e);
//     }




//     protected function prepareException(Exception $e)
//     {
//         if ($e instanceof ModelNotFoundException) {
//             $e = new NotFoundHttpException($e->getMessage(), $e);
//         } elseif ($e instanceof AuthorizationException) {
//             $e = new AccessDeniedHttpException($e->getMessage(), $e);
//         } elseif ($e instanceof TokenMismatchException) {
//               return redirect()->route('login');
//         }

//         return $e;
//     }