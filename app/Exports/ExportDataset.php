<?php

namespace App\Exports;

use App\Admin\M_dataset;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMappedCells;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\BeforeSheet;
use Illuminate\Support\Facades\DB;

// class ExportDataset implements FromQuery, WithHeadings, WithEvents, WithMappedCells, ShouldAutoSize{
class ExportDataset implements FromQuery, ShouldAutoSize{

    public function query()
    {
        // return M_dataset::query()->select('normalized.id_data', 'normalized.date', 'normalized.time',
        //     'normalized.device','normalized.value_soil_moisture', 'normalized.value_leaf_wetness',
        //     'normalized.value_air_temprature', 'normalized.value_air_humidity', 'normalized.value_wind_speed',
        //     'normalized.value_wind_direction', 'normalized.value_precipitation', 'normalized.value_solar_radiation','normalized.label');

        return M_dataset::query()->select('normalized.value_soil_moisture', 'normalized.value_air_temprature', 'normalized.value_air_humidity','normalized.label');
    }

    public function headings(): array
    {
        return [
            'ID Data', 'Date',
            'Time', 'Devices',
            'Soil Moisture', 'Leaf Wetness',
            'Air Temprature', 'Air Humidity', 'Wind Speed', 'Wind Direction',
            'Precipitation','Solar Radiation', 'label'
        ];
    }

    public function mapping(): array
    {
        return [
            'id_data' => 'B2',
            'date' => 'C2'

        ];
    }
    
    public function registerEvents(): array{

        //BORDER
         $styleBorder = [
                // 'borders' => [
                //     'outline' => [
                //         'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                //         'color' => ['argb' => '000000'],
                //     ],
                // ],
                // ''
                
        ];
        
        //BOLD
        $styleBold = [
                       //      'font' => [
                       // 'bold' => true,
                      // ]
        ];
                         
        // Fungsi membuat background
                        //  $newStyle= [
                        //     'fill' => [
                        //         'type'  => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        //         'color' => ['rgb' => 'FF0000']
                        //     ]
                        // ];
        
                        
                return [
        
                    BeforeSheet::class => function(BeforeSheet $event)use ($styleBold){
         
                        // $event->sheet->setCellValue('D1', ' DATA PIC')->getStyle('A1:J1')->getFont()->setSize(14)->applyFromArray($styleBold);
                        $event->sheet->setCellValue('F2', '')->getStyle('A1:J1')->applyFromArray($styleBold);
                        
                    },
        
                    AfterSheet::class => function(AfterSheet $event)  use ($styleBorder, $styleBold){
                        $cellRange = 'A1:L1'; // All headers
                        $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                        $event->sheet->getStyle('A2:M100')->applyFromArray($styleBorder);
                        $event->sheet->getStyle('A2:M2')->applyFromArray($styleBorder);
                        $event->sheet->getStyle('A2:A100')->applyFromArray($styleBorder);
                        $event->sheet->getStyle('B2:B100')->applyFromArray($styleBorder);
                        $event->sheet->getStyle('C2:C100')->applyFromArray($styleBorder);
                        // $event->sheet->getStyle('D2:D10')->applyFromArray($styleArray);
                        $event->sheet->getStyle('E2:M100')->applyFromArray($styleBorder);
                        $event->sheet->getStyle('A2:M2')->applyFromArray($styleBold);
                        // $event->sheet->getStyle('B2:G1')->applyFromArray($newStyle);
                   
                    },
                ];        
        
                
            }
}