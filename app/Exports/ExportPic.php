<?php

namespace App\Exports;

use App\Admin\T_company_pic;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMappedCells;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\BeforeSheet;

class ExportPic implements FromQuery, WithHeadings, WithEvents, WithMappedCells, ShouldAutoSize{

    public function query()
    {
        return T_company_pic::query()->select('t_company_pics.pic_id','t_company_pics.pic_name',
                't_company_pics.pic_position', 't_company_pics.pic_division',
                't_company_pics.pic_nohp', 't_company_pics.pic_email',
                't_company_pics.pic_sosmed', 't_company_pics.pic_status'
        );
    }

    public function headings(): array
    {
        return [
            'PIC id', 'PIC Name',
            'PIC position', 'PIC Division',
            'PIC Nohp', 'PIC Email',
            'PIC sosmed','PIC Status'
        ];
    }

    public function mapping(): array
    {
        return [
            't_companies.company_id' => 'B2',
            'Company Name' => 'C2'

        ];
    }
    
    public function registerEvents(): array{

        //BORDER
         $styleBorder = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '000000'],
                    ],
                ],
                ''
                
        ];
        
        //BOLD
                $styleBold = [
                            'font' => [
                       'bold' => true,
                       ]
                ];
                         
        // Fungsi membuat background
                        //  $newStyle= [
                        //     'fill' => [
                        //         'type'  => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        //         'color' => ['rgb' => 'FF0000']
                        //     ]
                        // ];
        
                        
                return [
        
                    BeforeSheet::class => function(BeforeSheet $event)use ($styleBold){
         
                        $event->sheet->setCellValue('D1', ' DATA PIC')->getStyle('A1:J1')->getFont()->setSize(14)->applyFromArray($styleBold);
                        $event->sheet->setCellValue('F2', '')->getStyle('A1:J1')->applyFromArray($styleBold);
                        
                    },
        
                    AfterSheet::class => function(AfterSheet $event)  use ($styleBorder, $styleBold){
                        $cellRange = 'A3:H3'; // All headers
                        $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                        $event->sheet->getStyle('A3:H100')->applyFromArray($styleBorder);
                        $event->sheet->getStyle('A3:H3')->applyFromArray($styleBorder);
                        $event->sheet->getStyle('A3:A100')->applyFromArray($styleBorder);
                        // $event->sheet->getStyle('B2:B100')->applyFromArray($styleBorder);
                        $event->sheet->getStyle('C3:C100')->applyFromArray($styleBorder);
                        // $event->sheet->getStyle('D2:D10')->applyFromArray($styleArray);
                        $event->sheet->getStyle('E3:E100')->applyFromArray($styleBorder);
                        $event->sheet->getStyle('G3:G100')->applyFromArray($styleBorder);
                        $event->sheet->getStyle('A3:H3')->applyFromArray($styleBold);
                        
                   
                    },
                ];        
        
                
            }
}