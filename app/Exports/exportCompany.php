<?php

namespace App\Exports;

use App\Admin\T_company;
use App\Admin\T_company_pic;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMappedCells;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\BeforeSheet;

class exportCompany implements FromQuery, WithHeadings, WithEvents, WithMappedCells, ShouldAutoSize{
    
    public function query()
    {
        return T_company::query()
                ->select('t_companies.company_id','t_companies.company_name', 
                        't_companies.company_type', 't_companies.company_field',
                        't_companies.company_desc', 't_companies.company_address',
                        't_companies.company_telpon_office', 't_companies.company_faximile',
                        't_companies.company_status', 't_companies.created_by'
                )->orderBy('created_at', 'DESC');
        
    }

    public function headings(): array
    {
        return [
            'Company id', 'Company Name', 'Company Type',
            'Company Field', 'Company Description', 'Company Address',
            'Company Telpon', 'Company Faximile', 'Company Status',
            'Dibuat Oleh'

        ];
    }

    public function mapping(): array
    {
        return [
            't_companies.company_id' => 'B2',
            'Company Name' => 'C2'

        ];
    }
    
    public function registerEvents(): array{

        //BORDER
         $styleBorder = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '000000'],
                    ],
                ],
                ''
                
        ];
        
        //BOLD
                $styleBold = [
                            'font' => [
                       'bold' => true,
                       ]
                ];
                         
        // Fungsi membuat background
                        //  $newStyle= [
                        //     'fill' => [
                        //         'type'  => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        //         'color' => ['rgb' => 'FF0000']
                        //     ]
                        // ];
        
                        
                return [
        
                    BeforeSheet::class => function(BeforeSheet $event)use ($styleBold){
         
                        $event->sheet->setCellValue('F1', ' DATA COMPANY')->getStyle('A1:J1')->getFont()->setSize(14)->applyFromArray($styleBold);
                        $event->sheet->setCellValue('F2', '')->getStyle('A1:J1')->applyFromArray($styleBold);
                        
                    },
        
                    AfterSheet::class => function(AfterSheet $event)  use ($styleBorder, $styleBold){
                        $cellRange = 'A3:J3'; // All headers
                        $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                        $event->sheet->getStyle('A3:J100')->applyFromArray($styleBorder);
                        $event->sheet->getStyle('A3:J3')->applyFromArray($styleBorder);
                        $event->sheet->getStyle('A3:A100')->applyFromArray($styleBorder);
                        // $event->sheet->getStyle('B2:B100')->applyFromArray($styleBorder);
                        $event->sheet->getStyle('C3:C100')->applyFromArray($styleBorder);
                        // $event->sheet->getStyle('D2:D10')->applyFromArray($styleArray);
                        $event->sheet->getStyle('E3:E100')->applyFromArray($styleBorder);
                        $event->sheet->getStyle('G3:G100')->applyFromArray($styleBorder);
                        $event->sheet->getStyle('I3:I100')->applyFromArray($styleBorder);
                        $event->sheet->getStyle('A3:J3')->applyFromArray($styleBold);
                        
                   
                    },
                ];        
        
                
            }

}