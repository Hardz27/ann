<?php

namespace App\Exports;

use App\Admin\T_company;
use Maatwebsite\Excel\Concerns\FromQuery;
// use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMappedCells;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\BeforeSheet;

class exportUnComplite implements FromQuery, WithHeadings,WithEvents,WithMappedCells,ShouldAutoSize{

    public function query()
    {
        return T_company::query()->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.deleted_at', '=', null)
        ->where(function ($query) {
                $query->where('t_companies.company_validation', '>', 0)
                      ->where('t_companies.company_validation', '<', 24576)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', true)
                                       ->orWhere('t_company_domains.domain_validation', '=', true);
                      });
            })
        ->orWhere(function ($query) {
                $query->where('t_companies.company_validation', '>', 0)
                      ->where('t_companies.company_validation', '<', 24576)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', null)
                                       ->orWhere('t_company_domains.domain_validation', '=', null);
                      });
            })
        ->orWhere(function ($query) {
                $query->where('t_companies.company_validation', '=', 0)
                      ->where(function ($query1) {
                                $query1->Where('t_company_emails.email_validation', '=', true)
                                       ->orWhere('t_company_domains.domain_validation', '=', true);
                      });
            })
        
        ->where('t_companies.deleted_at', '=', null)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC');

    }



   
    

    public function headings(): array
    {
        return [
            'Company id', 'Company Name',
             'Company Telpon', 'Domain Name', 'Email Name'
        ];
    }

    public function mapping(): array
    {
        return [
            't_companies.company_id' => 'B2',
            'Company Name' => 'C2'

        ];
    }


    public function registerEvents(): array{

//BORDER
 $styleArray = [
        'borders' => [
            'outline' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '000000'],
            ],
        ],
        ''
        
];

//BOLD
        $styleBold = [
                    'font' => [
               'bold' => true,
               ]
        ];
                 
// Fungsi membuat background
                 $newStyle= [
                    'fill' => [
                        'type'  => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'color' => ['rgb' => 'FF0000']
                    ]
                ];

                
        return [

            BeforeSheet::class => function(BeforeSheet $event)use ($styleBold){
 
                $event->sheet->setCellValue('A1', ' DATA UNCOMPLITE')->getStyle('A1:E1')->applyFromArray($styleBold);
                
            },

            AfterSheet::class => function(AfterSheet $event)  use ($styleArray, $styleBold,$newStyle){
                $cellRange = 'A1:E1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                $event->sheet->getStyle('A2:E100')->applyFromArray($styleArray);
                $event->sheet->getStyle('A2:E2')->applyFromArray($styleArray);
                $event->sheet->getStyle('A2:A100')->applyFromArray($styleArray);
                $event->sheet->getStyle('B2:B100')->applyFromArray($styleArray);
                $event->sheet->getStyle('C2:C100')->applyFromArray($styleArray);
                // $event->sheet->getStyle('D2:D10')->applyFromArray($styleArray);
                $event->sheet->getStyle('E2:E100')->applyFromArray($styleArray);
                $event->sheet->getStyle('A2:E2')->applyFromArray($styleBold);
                $event->sheet->getStyle('B2:E1')->applyFromArray($newStyle);
           
            },
        ];        

        
    }

   

    
}