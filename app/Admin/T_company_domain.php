<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class T_company_domain extends Model
{
    protected $table = 't_company_domains';

    protected $fillable = [
        'domain_name', 'company_id', 'domain_validation'
    ];

    protected $primaryKey = 'domain_id';

    public function company(){
        return $this->belongsTo('App\Admin', 'company_id');
    }
}
