<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class T_company_pic extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 't_company_pics';

    protected $fillable = [
        'company_id','pic_name', 'pic_position', 'pic_division', 
        'pic_nohp', 'pic_email', 'pic_sosmed', 'pic_status', 'pic_validation'
    
    ];

    protected $primaryKey = 'pic_id';

    public function company(){
        return $this->belongsTo('App\Admin\T_company', 'company_id');
    }
    
}
