<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class M_dataset extends Model
{
    

    // protected $dates = ['deleted_at'];

    public $timestamps = false;

    protected $table = 'normalized';

    protected $fillable = [
        'id_data',   'date', 'time',   'device', 'value_soil_moisture',	'value_leaf_wetness',
        'value_air_temprature',	'value_air_humidity', 'value_wind_speed', 'value_wind_direction',
        'value_precipitation','value_solar_radiation', 'label'
    ];

    protected $primaryKey = 'id_data';
    
}
