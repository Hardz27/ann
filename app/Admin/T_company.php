<?php

namespace App\Admin;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\T_user as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use App\T_user;
use App\Temp_company;

class T_company extends Model
{
     
     use SoftDeletes, Notifiable, HasRoles;

    

     protected $dates = ['deleted_at'];

     protected static function boot(){
          parent::boot();
          static::creating(function($model){
               $user = Auth::user();
               $model->created_by = $user->user_name;
               $model->user_id = $user->user_id;
               // $model->updated_by = $user->user_name;
          });

          static::updating(function($model){
               $user = Auth::user();
               $model->updated_by = $user->user_name;
          });

          static::creating(function($model){
               
          });
     }

    protected $table = 't_companies';

    protected $fillable =[
        'company_id', 'company_name', 'company_type', 'company_field',
        'company_desc', 'company_address', 'company_telpon_office',
        'company_faximile', 'company_status', 'company_validation', 
        'created_at', 'created_by','updated_by', 'user_id'
    ];

//     protected $appends = ['domain_name'];

//     public function setDomain(){
//          return $this->domains->domain_name;
//     }

    protected $primaryKey = 'company_id';

    public $timestamps = true;

    public function domain(){
        return $this->hasMany('App\Admin\T_company_domain', 'company_id', 'company_id');
 
   }

   public function email(){
        return $this->hasMany('App\Admin\T_company_email', 'company_id', 'company_id');
   }

   public function pic(){
        return $this->hasOne('App\Admin\T_company_pic', 'company_id', 'company_id');
   }

   public function company_note(){
        return $this->belongsTo('App\Inputer', 'company_id');
   }

   public function user()
    {
        return $this->belongsTo(T_user::class);
    }

    public function jobs()
    {
          return $this->hasMany('App\Job', 'company_id');
    }

  
//    view('view.name', $data);
   

}
