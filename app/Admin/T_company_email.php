<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class T_company_email extends Model
{
    protected $table = 't_company_emails';

    protected $fillable = ['company_id','email_name', 'email_validation'];

    protected $primaryKey = 'email_id';

    public function company(){
        return $this->belongsTo('App\Admin', 'company_id');
    }   
}
