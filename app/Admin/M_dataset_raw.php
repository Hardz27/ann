<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class M_dataset_raw extends Model
{
    

    // protected $dates = ['deleted_at'];

    public $timestamps = false;

    protected $table = 'dataset';

    protected $fillable = [
        'id_data',   'tanggal', 'waktu',   'devices', 'sensor_id',   'sensor_type', 'value'
    
    ];

    protected $primaryKey = 'id_data';
    
}
