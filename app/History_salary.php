<?php

namespace App;

// use Illuminate\Notifications\Notifiable;
// use Illuminate\Foundation\Auth\T_user as Authenticatable;
// use Spatie\Permission\Traits\HasRoles;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class History_salary extends Model
{

     // protected static function boot(){
     //      parent::boot();
     //      static::creating(function($model){
     //           $user = Auth::user();
     //           $model->created_by = $user->user_name;
     //           $model->user_id = $user->user_id;
     //           // $model->updated_by = $user->user_name;
     //      });

     //      static::updating(function($model){
     //           $user = Auth::user();
     //           $model->updated_by = $user->user_name;
     //      });
     // }

    protected $table = 't_history_salary';

    protected $fillable =[
        'history_id','user_id', 'valid_data', 'total_salary', 'tanggal', 'salary_id'
    ];

//     protected $appends = ['domain_name'];

//     public function setDomain(){
//          return $this->domains->domain_name;
//     }

    protected $primaryKey = 'history_id';

    public $timestamps = true;

   //  public function domain(){
   //      return $this->hasOne('App\Admin\T_company_domain', 'company_id', 'company_id');
 
   // }

   // public function email(){
   //      return $this->hasOne('App\Admin\T_company_email', 'company_id', 'company_id');
   // }

   // public function pic(){
   //      return $this->hasOne('App\Admin\T_company_pic', 'company_id', 'company_id');
   // }

   // public function company_note(){
   //      return $this->belongsTo('App\Inputer', 'company_id');
   // }

  
//    view('view.name', $data);
   

}
