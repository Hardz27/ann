
@if (!isset($page))
    @php $page = ""; @endphp
@endif
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="{{ asset('dist/img/erporate1.png') }}" alt="POS" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">ERPORATE-LEADS</span>
    </a>

    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                 <a href="#" class="d-block">{{auth()->user()->user_name}}</a>
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                <a href="{{ route('home') }}" class='nav-link
                            @if ($page == "Home")
                                @php echo "active" @endphp
                            @endif
                '">
                        <i class="nav-icon fa fa-dashboard"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>

                @if (auth()->user()->can('show products') || auth()->user()->can('delete products') || auth()->user()->can('create products'))
              
                @endif      
                
                
                @role('admin')
                <li class="nav-item">
                    <a href="{{ route('salary.index') }}" class='nav-link
                            @if ($page == "Salary")
                                @php echo "active" @endphp
                            @endif
                            '>
                        <i class="nav-icon fa fa-toggle-on"></i>
                        <p>
                            Manage Salary
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('check.index') }}" class='nav-link
                            @if ($page == "Check")
                                @php echo "active" @endphp
                            @endif
                            '>
                        <i class="nav-icon fa fa-toggle-on"></i>
                        <p>
                            Manage Cek
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ route('leads.index') }}" class='nav-link
                            @if ($page == "Leads")
                                @php echo "active" @endphp
                            @endif
                            '>
                        <i class="nav-icon fa fa-history"></i>
                        <p>
                            Manage Leads
                            {{-- <i class="right fa fa-angle-left"></i> --}}
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('company.index') }}" class='nav-link
                            @if ($page == "Company")
                                @php echo "active" @endphp
                            @endif
                    '">
                        <i class="nav-icon fa fa-building"></i>
                        <p>
                            Manage Company
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('pic.index') }}" class='nav-link
                            @if ($page == "Pic")
                                @php echo "active" @endphp
                            @endif
                    '">
                        <i class="nav-icon fa fa-address-book"></i>
                        <p>
                            Manage PIC
                        </p>
                    </a>
                </li>

     {{--@if (auth()->user()->can('show users') || auth()->user()->can('create users') || auth()->user()->can('delete users') || auth()->user()->can('delete users')|| auth()->user()->can('make roles'))   --}}
                <li class="nav-item has-treeview">
                    <a href="#" class='nav-link
                            @if ($page == "User" || $page == "Role")
                                @php echo "active" @endphp
                            @endif
                    '">
                        <i class="nav-icon fa fa-users"></i>
                        <p>
                            Manage User
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('role.index') }}" class="nav-link">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Role</p>
                            </a>
                        </li>
                        {{--
                        <li class="nav-item">
                            <a href="{{ route('users.roles_permission') }}" class="nav-link">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Role Permission</p>
                            </a>
                        </li>
                        --}}

                        <li class="nav-item">
                            <a href="{{ route('users.index') }}" class="nav-link">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Users</p>
                            </a>
                        </li>
                    </ul>
                </li>
    {{--@endif   --}}
                @endrole

                
                @role('inputer')
                {{-- <li class="nav-item">
                    <a href="{{ route('order.transaksi') }}" class="nav-link">
                        <i class="nav-icon fa fa-shopping-cart"></i>
                        <p>
                            Transaksi
                        </p>
                    </a>
                </li> --}}
               
                <li class="nav-item">
                        <a href="" class="nav-link">
                            <i class="nav-icon fa fa-server"></i>
                            <p>
                                Manage Produk
                            </p>
                        </a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Kategori</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                        <a href="{{ route('manageinput.index') }}" class="nav-link">
                            <i class="nav-icon fa fa-history"></i>
                            <p>
                                Manage Leads
                                {{-- <i class="right fa fa-angle-left"></i> --}}
                            </p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                    <a href="{{route('users.index')}}" class="nav-link">
                                <i class="fa fa-user-circle-o"></i>
                                <p>Profile</p>
                            </a>
                    </li>
                    {{-- <li class="nav-item">
                        <a href="{{ route('user.edit') }}" class="nav-link">
                            <i class="fa fa-circle-o nav-icon"></i>
                            <p>Users</p>
                        </a>
                    </li> --}}
                @endrole

                <li class="nav-item has-treeview">
                    <a class="nav-link" href="#" data-toggle="modal" data-target="#logout">
                    <!-- <a class="nav-link" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();b();"> -->
                        <i class="nav-icon fa fa-sign-out"></i>
                        <p>
                            {{ __('Logout') }}
                        </p>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
    </div>
</aside>

@include('layouts.module.logout')