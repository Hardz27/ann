<?php


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function() {

    return redirect(route('login'));
});

Route::get('/check', 'Admin\CheckerController@index')->name('check.index');
Route::get('/check/website', 'Admin\CheckerController@website')->name('check.website');

Route::post('/check/import', 'Admin\CheckerController@import')->name('check.import');
Route::post('/check/train', 'Admin\CheckerController@train')->name('check.train');
Route::get('/check/log', 'Admin\CheckerController@log')->name('check.log');

Route::get('/check/website/normalizing', 'Admin\CheckerController@normalizing')->name('check.normalizing');
Route::post('/check/generate', 'Admin\CheckerController@generate')->name('check.generate');
Route::post('/check/generateNormalized', 'Admin\CheckerController@generate_normalized')->name('check.generateNormalized');

Route::get('/check/normalized', 'Admin\CheckerController@normalized')->name('check.normalized');
Route::get('/check/raw', 'Admin\CheckerController@rawData')->name('check.raw');

Route::get('/check/fuzzyfication', 'Admin\CheckerController@fuzzy')->name('check.fuzzy');




















































































Route::group(['middleware' => 'auth'], function() {

//     Route::group(['middleware' => ['role:admin']], function () {

//       //==========================================================================//
//      //                                 SUPER ADMIN                              //          
//     //==========================================================================//

// //Fitur Role & User
//         Route::resource('/role', 'Admin\RoleController')->except([
//             'create', 'show', 'edit', 'update'
//             ]);
//         Route::resource('/users', 'Admin\UserController')->except([
//             'show', 'destroy'
//         ]);
//         Route::get('/users/delete_form/{user_id}', 'Admin\UserController@delete_form')->name('user.delete_form');
//         Route::post('/users/delete/{user_id}', 'Admin\UserController@destroy')->name('users.destroy');
//         Route::get('/users/roles/{user_id}', 'Admin\UserController@roles')->name('users.roles');
//         Route::put('/users/roles/{id}', 'Admin\UserController@setRole')->name('users.set_role');
//         Route::post('/users/permission', 'Admin\UserController@addPermission')->name('users.add_permission');
        Route::get('/users/role-permission', 'Admin\UserController@rolePermission')->name('users.roles_permission');
        Route::put('/users/permission/{user}', 'Admin\UserController@setRolePermission')->name('users.setRolePermission');
//         Route::get('/users/detail/{user_id}', 'Admin\UserController@detail')->name('users.detail');
        
// //Fitur COMPANY
        Route::get('/company', 'Admin\CompanyController@index')->name('company.index');
//         Route::get('/company/create', 'Admin\CompanyController@create')->name('company.create');
//         Route::post('/company/data', 'Admin\CompanyController@store')->name('company.store');
//         Route::get('/company/edit/{company_id}', 'Admin\CompanyController@edit')->name('company.edit');
//         Route::post('/company/update/{company_id}', 'Admin\CompanyController@update')->name('company.update');
//         Route::get('/company/detail/{company_id}', 'Admin\CompanyController@detail')->name('company.detail');
//         Route::get('/company/export_excel', 'Admin\CompanyController@export_excel')->name('company.export_excel');       
//         Route::get('/company/delete_form/{company_id}', 'Admin\CompanyController@delete_form')->name('company.delete_form');
//         Route::post('/company/destory/{company_id}', 'Admin\CompanyController@destroy')->name('company.destroy');
        
//         Route::get('/company/delete_check', 'Admin\CompanyController@delete_check')->name('company.delete_check');
        
// //Fitur PIC
        Route::get('/pic', 'Admin\PicController@index')->name('pic.index');
//         Route::get('/pic/edit/{pic_id}', 'Admin\PicController@edit')->name('pic.edit');
//         Route::get('/pic/view/{pic_id}', 'Admin\PicController@view')->name('pic.view');
//         Route::post('/pic/update/{pic_id}', 'Admin\PicController@update')->name('pic.update');
//         Route::get('/pic/detail/{pic_id}', 'Admin\PicController@detail')->name('pic.detail');
//         Route::get('/pic/delete_pic/{pic_id}', 'Admin\PicController@delete_pic')->name('pic.delete_pic');
//         Route::post('/pic/destroy/{pic_id}', 'Admin\PicController@destroy')->name('pic.destroy');
//         Route::get('/pic/export-excel', 'Admin\PicController@export_pic')->name('pic.export_excel');

// //Manage Salary
        Route::get('/salary', 'Admin\SalaryController@index')->name('salary.index');    
//         Route::post('/salary/store', 'Admin\SalaryController@add')->name('salary.add');    
//         Route::get('/salary/detail/{salary_id}', 'Admin\SalaryController@detail')->name('salary.detail');
//         // Route::post('/salary/history', 'Admin\SalaryController@updateHistorySalary')->name('salary.update.history');
//         Route::get('/salary/edit/{salary_id}', 'Admin\SalaryController@edit')->name('salary.edit');
//         Route::get('/salary/update/{salary_id}', 'Admin\SalaryController@update')->name('salary.update');
        
//         Route::get('/salary/delete/{salary_id}', 'Admin\SalaryController@delete')->name('salary.delete');
//         Route::get('/salary/destroy/{salary_id}', 'Admin\SalaryController@destroy')->name('salary.destroy');

//         Route::get('/salary/restore/{salary_id}', 'Admin\SalaryController@restore')->name('salary.restore');
//     });
//         Route::get('/pic/email/{email}', 'Admin\PicController@email')->name('pic.email');
        
// //Fitur Archieve
//         Route::get('/archive/company', 'Admin\ArchiveController@archive_company')->name('archive.company');
//         Route::get('/archive/pic', 'Admin\ArchiveController@archive_pic')->name('archive.pic');
//         Route::get('/archive/user', 'Admin\ArchiveController@archive_user')->name('archive.user');
        Route::get('/archive/leads', 'Admin\ArchiveController@archive_leads')->name('archive.leads');
//         #RESTORE
//         Route::get('/archive/form_restore_leads/{company_id}', 'Admin\LeadsController@form_restore_leads')->name('archive.form_restore_leads');
//         Route::post('/archive/restore_leads/{company_id}', 'Admin\LeadsController@restore_leads')->name('archive.restore_leads');
//         Route::get('/archive/form_restore/{company_id}', 'Admin\ArchiveController@form_restore')->name('archive.form_restore');
//         Route::post('/archive/restore/{company_id}', 'Admin\ArchiveController@restore')->name('archive.restore');
//         Route::get('/archive/form_restore_pic/{company_id}', 'Admin\ArchiveController@form_restore_pic')->name('archive.form_restore_pic');
//         Route::post('/archive/restore_pic/{company_id}', 'Admin\ArchiveController@restore_pic')->name('archive.restore_pic');
//         Route::get('/archive/form_restore_user/{user_id}', 'Admin\ArchiveController@form_restore_user')->name('archive.form_restore_user');
//         Route::post('/archive/restore_user/{user_id}', 'Admin\ArchiveController@restore_user')->name('archive.restore_user');

//         Route::get('/archive/restore_all', 'Admin\ArchiveController@restore_all_company')->name('archive.check_all');
        
//         #DETAIL
//         Route::get('/archive/detail_company/{company_id}', 'Admin\ArchiveController@detail_company')->name('archive.detail_company');
//         Route::get('archive/detail_pic/{pic_id}', 'Admin\ArchiveController@detail_pic')->name('archive.detail_pic');
//         Route::get('/archive/detail_user/{user_id}', 'Admin\ArchiveController@detail_user')->name('archive.detail_user');
//         Route::get('/archive/detail_leads/{company_id}', 'Admin\ArchiveController@detail_leads')->name('archive.detail_leads');

// //Fitur Filter
    Route::get('/filter', 'FilterController@index')->name('filter.index');

});


      //==========================================================================//
     //                                 SUPER ADMIN && INPUTER                   //          
    //==========================================================================//

    Route::group(['middleware' => ['permission:manage check|leads inputer|leads admin']], function() {
        
//FITUR CHECK

        Route::get('/check', 'Admin\CheckerController@index')->name('check.index');
//         Route::get('/check/website', 'Admin\CheckerController@website')->name('check.website');
//         Route::get('/check/website/normalized', 'Admin\CheckerController@normalized')->name('check.normalized');
//         Route::post('/check/generate', 'Admin\CheckerController@generate')->name('check.generate');
//         Route::post('/check/domain', 'Admin\CheckerController@domain')->name('check.web.domain');
//         Route::post('/check/store', 'Admin\CheckerController@excel')->name('check.excel');
//         Route::post('/check', 'Admin\CheckerController@checkInput');
//         Route::post('/check_excel', 'Admin\CheckerController@checkExcel')->name('check.checkExcel');
//         Route::get('/check/{data1}/form/{data2}', 'Admin\CheckerController@getForm')->name('getForm');
//         Route::post('/check/data', 'Admin\CheckerController@store')->name('check.store');
//         Route::get('/check/edit/{company_id}', 'Admin\CheckerController@edit')->name('check.edit');
//         Route::post('/check/update/{company_id}', 'Admin\CheckerController@update')->name('check.update');
//         Route::post('/check/destroy/{company_id}', 'Admin\CheckerController@destroy')->name('check.destroy');
//         Route::get('/check/detail/{company_id}', 'Admin\CheckerController@detail')->name('check.detail');

// // FITUR MANAGE-LEADS
       
        Route::get('/leads', 'Admin\LeadsController@index')->name('leads.index');
//         Route::get('/leads/validation/{company_id}', 'Admin\LeadsController@validation')->name('lead.validation');
//         Route::post('/leads/validationData/{company_id}', 'Admin\LeadsController@validationData')->name('lead.validationData'); 
//         Route::get('/leads/editForm/{company_id}', 'Admin\LeadsController@editForm')->name('lead.editForm');
//         Route::post('/leads/update/{company_id}', 'Admin\LeadsController@update')->name('lead.update');
//         Route::get('/leads/delete_lead/{company_id}', 'Admin\LeadsController@delete_lead')->name('lead.delete_lead');
//         Route::post('/leads/delete/{company_id}', 'Admin\LeadsController@destroy')->name('lead.delete');
//         Route::get('/leads/detail/{company_id}', 'Admin\LeadsController@detail')->name('lead.detail');

//         // ----EXPORT----//
//         Route::get('/leads/export-excel', 'Admin\LeadsController@laporanLeads')->name('leads.export_excel');
//         Route::get('/leads/export-newinput', 'Admin\LeadsController@exportNewInput')->name('leads.export_newInput');
//         Route::get('/leads/export-uncomplite', 'Admin\LeadsController@export_unComplite')->name('leads.export_unComplite');
//         Route::get('/leads/export-complite', 'Admin\LeadsController@export_complite')->name('leads.export_complite');
//         Route::get('/leads/export-alldata', 'Admin\LeadsController@export_allData')->name('leads.export_allData');
//         // ----END EXPORT----//

//     //LEADS DETAIL
//         Route::get('/pic/detail/{pic_id}', 'Admin\PicController@detail')->name('pic.detail');


        
    });




        // =======================================================================//
      // =                               INPUTER                               =//
     // =======================================================================//

    Route::group(['middleware' => ['role:inputer']], function() {

// FITUR MANAGE-Input

        Route::get('/manage-input', 'Inputer\ManageInputController@index')->name('manageinput.index');
        

    });

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/home/valid_admin', 'HomeController@valid_admin')->name('home.valid_admin');
    Route::get('/home/unvalid_admin', 'HomeController@unvalid_admin')->name('home.unvalid_admin');
    Route::get('/home/valid', 'HomeController@valid')->name('home.valid');
    Route::get('/home/unvalid', 'HomeController@unvalid')->name('home.unvalid');
    Route::get('/home/karyawan', 'HomeController@karyawan')->name('home.karyawan');
    
