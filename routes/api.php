<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/company/{id}', 'CheckController@getProduct');
Route::post('/cart', 'CheckController@addToCart');
Route::get('/cart', 'CheckController@getCart');
Route::delete('/cart/{id}', 'OrderController@removeCart');
Route::post('/customer/search', 'CustomerController@search');

Route::get('/chart', 'HomeController@getChart');
// Route::get('/chartInputer', 'HomeController@getChartInputer');