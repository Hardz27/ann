<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::dropIfExists('t_companies');
        Schema::create('t_companies', function (Blueprint $table) {
            $table->bigIncrements('company_id');
            $table->string('company_name');
            $table->enum('company_type', ['tekno','nontekno']);
            $table->string('company_field');
            $table->string('company_desc');
            $table->string('company_address');
            $table->string('company_telpon_office');
            $table->string('company_faximile');
            $table->enum('company_status',['aktif','nonaktif']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_companies');
    }
}
