<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoleIdTUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::table('t_users', function (Blueprint $table) {
         $table->unsignedInteger('role_id')->nullable()->after('user_id');
     });

     Schema::table('t_users', function (Blueprint $table) {
         $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_users', function (Blueprint $table) {
            $table->dropForeign('t_users_role_id_foreign');
            $table->dropColumn('role_id');
        });
    }
}
