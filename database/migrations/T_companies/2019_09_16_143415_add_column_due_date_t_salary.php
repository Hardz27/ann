<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDueDateTSalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_salary', function (Blueprint $table) {
            $table->string('tgl_berlaku')->after('salary_value');
            $table->string('tgl_deadline')->after('tgl_berlaku');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('t_salary', function (Blueprint $table) {
            $table->dropColumn('tgl_berlaku');
            $table->dropColumn('tgl_deadline');
        });
    }
}
