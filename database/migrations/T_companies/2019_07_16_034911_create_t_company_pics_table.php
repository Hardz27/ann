<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTCompanyPicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_company_pics', function (Blueprint $table) {
            $table->bigIncrements('pic_id');
            $table->integer('company_id')->unsigned();
            $table->string('pic_name');
            $table->string('pic_position');
            $table->string('pic_nohp');
            $table->string('pic_email');
            $table->string('pic_sosmed');
            $table->enum('pic_status',['aktif', 'nonaktif']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_company_pics');
    }
}
