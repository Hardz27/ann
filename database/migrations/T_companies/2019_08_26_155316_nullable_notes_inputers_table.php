<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NullableNotesInputersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inputers', function (Blueprint $table) {
            $table->string('inputer_note_company')->nullable()->change();
            $table->string('inputer_note_domain')->nullable()->change();
            $table->string('inputer_note_email')->nullable()->change();
            $table->string('inputer_note_pic')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inputers', function (Blueprint $table) {
            $table->string('inputer_note_company')->change();
            $table->string('inputer_note_domain')->change();
            $table->string('inputer_note_email')->change();
            $table->string('inputer_note_pic')->change();
        });
    }
}
