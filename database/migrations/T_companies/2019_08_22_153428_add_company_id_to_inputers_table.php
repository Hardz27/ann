<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyIdToInputersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inputers', function (Blueprint $table) {
            $table->unsignedInteger('company_id')->nullable()->after('user_id');
        });

        Schema::table('inputers', function (Blueprint $table) {
            $table->foreign('company_id')->references('company_id')->on('t_companies')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inputers', function (Blueprint $table) {
            $table->dropForeign('inputers_company_id_foreign');
            $table->dropIndex('inputers_company_id_foreign');
            $table->dropColumn('company_id');
        });
    }
}
