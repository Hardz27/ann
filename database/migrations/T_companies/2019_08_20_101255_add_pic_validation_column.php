<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPicValidationColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_company_pics', function (Blueprint $table) {
            $table->integer('company_validation')->nullable()->default(0)->after('pic_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_company_pics', function (Blueprint $table) {
            $table->dropColumn('company_validation');
        });
    }
}
