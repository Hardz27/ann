<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyInputersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inputers', function (Blueprint $table) {
            // $table->renameColumn('id', 'inputer_id');
            $table->integer('user_id')->nullable()->after('id');
            $table->string('inputer_name')->after('user_id');
            $table->string('inputer_note_company')->after('inputer_name');
            $table->string('inputer_note_domain')->after('inputer_note_company');
            $table->string('inputer_note_email')->after('inputer_note_domain');
            $table->string('inputer_note_pic')->after('inputer_note_email');
        });

        Schema::table('inputers', function (Blueprint $table) {

            $table->foreign('user_id')->references('user_id')->on('t_users')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inputers', function (Blueprint $table) {
            // $table->renameColumn('inputer_id', 'id');
            $table->dropForeign('inputers_user_id_foreign');
            $table->dropIndex('inputers_user_id_foreign');
            $table->dropColumn('user_id');
            $table->dropColumn('inputer_name');
            $table->dropColumn('inputer_note_company');
            $table->dropColumn('inputer_note_domain');
            $table->dropColumn('inputer_note_email');
            $table->dropColumn('inputer_note_pic');
        });
    }
}
