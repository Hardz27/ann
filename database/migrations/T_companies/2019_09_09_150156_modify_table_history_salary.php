<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTableHistorySalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_history_salary', function (Blueprint $table) {

            $table->foreign('user_id')->references('user_id')->on('t_users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('salary_id')->references('salary_id')->on('t_salary')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_history_salary');
    }
}
