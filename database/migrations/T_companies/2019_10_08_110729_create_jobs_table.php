<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('job_id');
            $table->unsignedInteger('company_id')->nullable();
            $table->unsignedInteger('temp_company_id');
            $table->string('job_position');
            $table->string('job_link');
            $table->string('job_time');
            $table->timestamps();
        });

        Schema::table('jobs', function (Blueprint $table) {

            $table->foreign('company_id')->references('company_id')->on('t_companies')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('temp_company_id')->references('temp_company_id')->on('temp_companies')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
