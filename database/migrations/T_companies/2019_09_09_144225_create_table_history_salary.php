<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHistorySalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_history_salary', function (Blueprint $table) {
            $table->increments('history_id');
            $table->integer('user_id');
            $table->integer('valid_data');
            $table->integer('total_salary')->nullable();
            $table->timestamps();
            $table->unsignedInteger('salary_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_history_salary');
    }
}
