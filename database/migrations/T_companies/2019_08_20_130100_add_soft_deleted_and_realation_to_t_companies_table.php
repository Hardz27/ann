<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeletedAndRealationToTCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_companies', function (Blueprint $table) {
            $table->softDeletes()->after('updated_by');
            $table->integer('user_id')->nullable()->after('company_id');

        });

        Schema::table('t_companies', function (Blueprint $table) {
            
            $table->foreign('user_id')->references('user_id')
                    ->on('t_users')->onDelete('cascade')->onUpdate('cascade');

        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_companies', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
            $table->dropForeign('t_companies_user_id_foreign');
            $table->dropIndex('t_companies_user_id_foreign');
            $table->dropColumn('user_id');
        });
    }
}
