<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('role_has_permissions', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->after('role_id');

        });

        Schema::table('role_has_permissions', function (Blueprint $table) {
            
            $table->foreign('user_id')->references('user_id')
                    ->on('t_users')->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['user_id']);
        });
        

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('role_has_permissions', function(Blueprint $table){
        //     $table->dropForeign('role_has_permissions_user_id_foreign');
        // });

        // Schema::table('role_has_permissions', function(Blueprint $table){
        //     $table->dropIndex('role_has_permissions_user_id_foreign');
        // });

        // Schema::table('role_has_permissions', function($table){
        //     $table->$table->dropColumn('user_id');
        // });

        // app('cache')->forget('spatie.permission.cache');
    }
}
