<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdUserOnPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('role_has_permissions', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->after('role_id');

            $table->foreign('user_id')->references('user_id')
                    ->on('t_users')->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['user_id']);  
        });

        
        
        

        
            
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
