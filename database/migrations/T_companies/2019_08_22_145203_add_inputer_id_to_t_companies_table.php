<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInputerIdToTCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_companies', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->nullable()->after('user_id');
        });

        Schema::table('t_companies', function (Blueprint $table) {
            $table->foreign('id')->references('id')->on('inputers')
                    ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_companies', function (Blueprint $table) {
            $table->dropForeign('t_companies_inputer_id_foreign');
            $table->dropIndex('t_companies_inputer_id_foreign');
            $table->dropColumn('id');
        });
    }
}
