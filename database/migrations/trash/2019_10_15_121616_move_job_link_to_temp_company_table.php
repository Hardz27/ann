<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class MoveJobLinkToTempCompanyTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::table('temp_companies', function (Blueprint $table) {
            $table->string('temp_company_link')->after('temp_company_name');
        });

        DB::table('temp_companies')->join('jobs', 'jobs.temp_company_id', '=', 'temp_companies.temp_company_id')
            ->update([ ('temp_company_link') => DB::raw('job_link') 
        ]);

        Schema::table('jobs', function (Blueprint $table) {
            $table->dropColumn('job_link');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('temp_company', function (Blueprint $table) {
            //
        });
    }
}
