<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationshipsToTCompanyDomains extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('t_company_domains', function (Blueprint $table) {
             $table->integer('company_id')->change();
            $table->foreign('company_id')->references('company_id')->on('t_companies')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('t_company_domains', function (Blueprint $table) {
        //     $table->dropForeign('t_company_domains_id_company_foreign');
        // });
    }
}
