<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTCompanyDomainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('t_company_domains');
        Schema::create('t_company_domains', function (Blueprint $table) {
            $table->increments('domain_id');
            $table->unsignedInteger('company_id');
            $table->string('domain_name');
            $table->string('domain_desc');
            $table->timestamps();

            //Relationship
            $table->foreign('company_id')->references('company_id')->on('t_companies')
            ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_company_domains');
    }
}
