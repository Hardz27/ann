<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNameVaidationCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
{
    Schema::table('t_companies', function(Blueprint $table)
    {
           DB::statement('ALTER TABLE t_companies CHANGE validation company_validation');
    });
}

public function down()
{
    Schema::table('t_companies', function(Blueprint $table)
    {
           DB::statement('ALTER TABLE t_companies CHANGE company_validation validation');
    });
}

}
