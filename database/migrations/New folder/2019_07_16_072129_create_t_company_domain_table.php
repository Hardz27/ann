<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTCompanyDomainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('t_company_domains');
        Schema::create('t_company_domains', function (Blueprint $table) {
            //id
            $table->increments('domain_id');
            $table->unsignedInteger('company_id');
            $table->string('domain_name',200);
            $table->string('domain_desc');

            //Relationship

            
            $table->foreign('company_id')->references('company_id')->on('t_companies')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_company_domains');
    }
}
