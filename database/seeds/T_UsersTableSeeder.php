<?php

use Illuminate\Database\Seeder;
use App\T_user;

class T_UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        T_user::create([
            'user_name' => 'king',
            'user_email' => 'king@gmail.com',
            'user_password' => bcrypt('123456'),
            'user_nohp' => '0000000',            
            'user_level' => 'admin',
            'user_status' => true
        ]);

        

        
        // T_user::create([
        //     'user_name' => 'Sofyan',
        //     'user_password' => bcrypt('123456'),
        //     'user_email' => 'Sofyan@gmail.com',
        //     'user_nohp' => '0000000',
        //     'user_level' => 'inputer',            
        //     'user_status' => true
        // ]);
    }
}
