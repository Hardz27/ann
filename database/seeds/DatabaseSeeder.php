<?php

use Illuminate\Database\Seeder;
use App\Admin\T_company;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(DomainsTableSeeder::class);
        // $this->call(CompaniesTableSeeder::class);
        $this->call(T_UsersTableSeeder::class);
        // $this->call(TusersTableSeeder::class);
        
    }
}
