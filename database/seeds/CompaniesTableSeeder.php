<?php

use Illuminate\Database\Seeder;
use Doctrine\DBAL\Schema\Schema;
use App\Admin\T_company;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        T_company::create([
            'company_name' => 'erporate',
            'company_type' => 'tekno',
            'company_field' => 'Software House',
            'company_desc' => 'Software house',
            'company_address' => 'Yogyakarta',
            'company_telpon_office' => '000000',
            'company_faximile' => 'erporate.faximile',
            'company_status' => 'aktif'
        ]);

        T_company::create([
            'company_name' => 'google',
            'company_type' => 'tekno',
            'company_field' => 'Software Industri',
            'company_desc' => 'Software Industri',
            'company_address' => 'Amerika',
            'company_telpon_office' => '1111111',
            'company_faximile' => 'google.faximile',
            'company_status' => 'aktif'
        ]);
    }
}
