<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // User::create([
            // 'name' => 'Daengweb',
            // 'email' => 'admin@daengweb.id',
            // 'password' => bcrypt('secret'),
            // 'status' => true
        // ]);

        User::create([
            'name' => 'fahrur',
            'email' => 'fahrur@gmail.com',
            'password' => bcrypt('123456'),
            'status' => true
        ]);

        
        User::create([
            'name' => 'Shofyan',
            'email' => 'Shofyan@gmail.com',
            'password' => bcrypt('123456'),
            'status' => true
        ]);
    }
}
