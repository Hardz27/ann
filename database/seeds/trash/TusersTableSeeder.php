<?php

use Illuminate\Database\Seeder;
use App\T_user;
use DB;
class TusersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        T_user::create([
            'user_name' => 'Fahrur',
            'user_password' => bcrypt('123456'),
            'user_email' => 'Fahrur@gmail.com',
            'user_nohp' => '0000000',            
            'user_status' => true
        ]); 
    }
}
