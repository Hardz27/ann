<?php

use Illuminate\Database\Seeder;
use App\Admin\T_company_domain;

class DomainsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        T_company_domain::create([
            'domain_name' => 'google.com',
            'domain_desc' => 'Software Industri Amerika'
        ]);   
        
        
    }
}
