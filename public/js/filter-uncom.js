$(document).ready(function() {
    $('#uncom').DataTable( {
      "pagingType": "full_numbers"
     });
});

 var filtersConfig = {
  base_path: 'tablefilter/',
  auto_filter: {
                    delay: 110 //milliseconds
              },
              filters_row_index: 1,
              state: true,
              alternate_rows: true,
              rows_counter: true,
              btn_reset: true,
              status_bar: true,
              msg_filter: 'Filtering...'
            };
            var tf = new TableFilter('uncom', filtersConfig);
            tf.init();
          