        $(document).ready(function () {
            // DataTable
            // var table = $('#all').DataTable();
            $('#new-inp_filter').find("input[type=search]").daterangepicker({
                // dateFormat: 'yy-mm-dd',
                singleDatePicker: true,
                showDropdowns: true,
                locale: { format: 'YYYY-MM-DD' }
            }).on("input change", function () {
                filterNew();
            });                               
        });

        $(document).ready(function () {
            // DataTable
            // var table = $('#all').DataTable();
            $('#uncom_filter').find("input[type=search]").daterangepicker({
                // dateFormat: 'yy-mm-dd',
                singleDatePicker: true,
                showDropdowns: true,
                locale: { format: 'YYYY-MM-DD' }
            }).on("input change", function () {
                filterUncomplete();
            });                               
        });

        $(document).ready(function () {
            // DataTable
            // var table = $('#all').DataTable();
            $('#com_filter').find("input[type=search]").daterangepicker({
                // dateFormat: 'yy-mm-dd',
                singleDatePicker: true,
                showDropdowns: true,
                locale: { format: 'YYYY-MM-DD' }
            }).on("input change", function () {
                filterComplete();
            });                               
        });

        $(document).ready(function () {
            // DataTable
            // var table = $('#all').DataTable();
            $('#all_filter').find("input[type=search]").daterangepicker({
                // dateFormat: 'yy-mm-dd',
                singleDatePicker: true,
                showDropdowns: true,
                locale: { format: 'YYYY-MM-DD' }
            }).on("input change", function () {
                filterAll();
            });                               
        });


        function filterNew() {
            $('#new-inp').DataTable().search(
                $('#new-inp_filter').find("input[type=search]").val()
            ).draw();
        }
        function filterUncomplete() {
            $('#uncom').DataTable().search(
                $('#uncom_filter').find("input[type=search]").val()
            ).draw();
        }
        function filterComplete() {
            $('#com').DataTable().search(
                $('#com_filter').find("input[type=search]").val()
            ).draw();
        }     
        function filterAll() {
            $('#all').DataTable().search(
                $('#all_filter').find("input[type=search]").val()
            ).draw();
        }