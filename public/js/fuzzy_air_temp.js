var temp_vcold_max, temp_cold_max, temp_normal_max, temp_hot_max, temp_vhot_max = 0
var data_temp_vcold, data_temp_cold, data_temp_normal, data_temp_hot, data_temp_vhot = [0, 0, 0, 0]

$('#generate_temp').on('click', function(){
  data_temp_vcold = generate_fuzzy_temp('temp_vcold')
  temp_vcold_max = data_temp_vcold[3]
  data_temp_cold = generate_fuzzy_temp('temp_cold')
  temp_cold_max = data_temp_cold[3]
  data_temp_normal = generate_fuzzy_temp('temp_normal')
  temp_normal_max = data_temp_normal[3]
  data_temp_hot = generate_fuzzy_temp('temp_hot')
  temp_hot_max = data_temp_hot[3]
  data_temp_vhot = generate_fuzzy_temp('temp_vhot')
  temp_vhot_max = data_temp_vhot[3]

  grafik_temp()
})

function generate_fuzzy_temp(id){
  data = $('#'+id).val()
  data = data.split(' ')

  if (parseInt(data[1]) < parseInt(data[0])) {
    data[1] = parseInt(data[0])
  }
  if (parseInt(data[2]) < parseInt(data[1])) {
    data[2] = parseInt(data[1])
  }
  if (parseInt(data[3]) < parseInt(data[2])) {
    data[3] = parseInt(data[2])
  }

  return data

}

$('#temp_vcold').on('change', function(){
  data_temp_vcold = generate_fuzzy_temp('temp_vcold')
  temp_vcold_max = data_temp_vcold[3]

  $('#temp_vcold').val(data_temp_vcold[0] +" "+ data_temp_vcold[1] +" "+ data_temp_vcold[2] +" "+ data_temp_vcold[3])

  grafik_temp()

})

$('#temp_cold').on('change', function(){
  data_temp_cold = generate_fuzzy_temp('temp_cold')
  temp_cold_max = data_temp_cold[3]

  $('#temp_cold').val(data_temp_cold[0] +" "+ data_temp_cold[1] +" "+ data_temp_cold[2] +" "+ data_temp_cold[3])

  grafik_temp()

})

$('#temp_normal').change(function(){
  data_temp_normal = generate_fuzzy_temp('temp_normal')
  temp_normal_max = data_temp_normal[3]

  $('#temp_normal').val(data_temp_normal[0] +" "+ data_temp_normal[1] +" "+ data_temp_normal[2] +" "+ data_temp_normal[3])

  grafik_temp()

})

$('#temp_hot').on('change', function(){
  data_temp_hot = generate_fuzzy_temp('temp_hot')
  temp_hot_max = data_temp_hot[3]

  $('#temp_hot').val(data_temp_hot[0] +" "+ data_temp_hot[1] +" "+ data_temp_hot[2] +" "+ data_temp_hot[3])

  grafik_temp()

})

$('#temp_vhot').on('change', function(){
  data_temp_vhot = generate_fuzzy_temp('temp_vhot')
  temp_vhot_max = data_temp_vhot[3]

  $('#temp_vhot').val(data_temp_vhot[0] +" "+ data_temp_vhot[1] +" "+ data_temp_vhot[2] +" "+ data_temp_vhot[3])

  grafik_temp()

})

grafik_temp()

function grafik_temp() 
{
        // Start Chart total Pasien
        ////////////////////////////////
        var lineChartTemp = document.getElementById("lineChartTemp");
        function objectLabel() {this.lineTension= 0.0, this.fill = false, borderColor = ['#2bc0d9'], this.data = []};

        var dataLine = {
          labels: [],
          datasets: []
        };

        for (var i = 0; i < 100; i++) {
          dataLine.labels.push(i);
        }

        for (var i = 1; i <= 5; i++) {
          dataLine.datasets.push(new objectLabel())
        }

        for (var i = 1; i <= temp_vcold_max; i++) {
          if (i <= data_temp_vcold[0] || i >= data_temp_vcold[3]) {
            dataLine.datasets[0].data.push(0);  
          }else if(i > data_temp_vcold[0] && i < data_temp_vcold[1]){
            dataLine.datasets[0].data.push((i-data_temp_vcold[0])/(data_temp_vcold[1]-data_temp_vcold[0]));
          }else if(i >= data_temp_vcold[1] && i <= data_temp_vcold[2]){
            dataLine.datasets[0].data.push(1);
          }else if(i > data_temp_vcold[2] && i < data_temp_vcold[3]){
            dataLine.datasets[0].data.push((data_temp_vcold[3]-i)/(data_temp_vcold[3]-data_temp_vcold[2]));
          }
        }

        for (var i = 1; i <= temp_cold_max; i++) {
          if (i <= data_temp_cold[0] || i >= data_temp_cold[3]) {
            dataLine.datasets[1].data.push(0);  
          }else if(i > data_temp_cold[0] && i < data_temp_cold[1]){
            dataLine.datasets[1].data.push((i-data_temp_cold[0])/(data_temp_cold[1]-data_temp_cold[0]));
          }else if(i >= data_temp_cold[1] && i <= data_temp_cold[2]){
            dataLine.datasets[1].data.push(1);
          }else if(i > data_temp_cold[2] && i < data_temp_cold[3]){
            dataLine.datasets[1].data.push((data_temp_cold[3]-i)/(data_temp_cold[3]-data_temp_cold[2]));
          }
        }

        for (var i = 1; i <= temp_normal_max; i++) {
          if (i <= data_temp_normal[0] || i >= data_temp_normal[3]) {
            dataLine.datasets[2].data.push(0);  
          }else if(i > data_temp_normal[0] && i < data_temp_normal[1]){
            dataLine.datasets[2].data.push((i-data_temp_normal[0])/(data_temp_normal[1]-data_temp_normal[0]));
          }else if(i >= data_temp_normal[1] && i <= data_temp_normal[2]){
            dataLine.datasets[2].data.push(1);
          }else if(i > data_temp_normal[2] && i < data_temp_normal[3]){
            dataLine.datasets[2].data.push((data_temp_normal[3]-i)/(data_temp_normal[3]-data_temp_normal[2]));
          }
        }

        for (var i = 1; i <= temp_hot_max; i++) {
          if (i <= data_temp_hot[0] || i >= data_temp_hot[3]) {
            dataLine.datasets[3].data.push(0);  
          }else if(i > data_temp_hot[0] && i < data_temp_hot[1]){
            dataLine.datasets[3].data.push((i-data_temp_hot[0])/(data_temp_hot[1]-data_temp_hot[0]));
          }else if(i >= data_temp_hot[1] && i <= data_temp_hot[2]){
            dataLine.datasets[3].data.push(1);
          }else if(i > data_temp_hot[2] && i < data_temp_hot[3]){
            dataLine.datasets[3].data.push((data_temp_hot[3]-i)/(data_temp_hot[3]-data_temp_hot[2]));
          }
        }

        for (var i = 1; i <= temp_vhot_max; i++) {
          if (i <= data_temp_vhot[0] || i >= data_temp_vhot[3]) {
            dataLine.datasets[4].data.push(0);  
          }else if(i > data_temp_vhot[0] && i < data_temp_vhot[1]){
            dataLine.datasets[4].data.push((i-data_temp_vhot[0])/(data_temp_vhot[1]-data_temp_vhot[0]));
          }else if(i >= data_temp_vhot[1] && i <= data_temp_vhot[2]){
            dataLine.datasets[4].data.push(1);
          }else if(i > data_temp_vhot[2] && i < data_temp_vhot[3]){
            dataLine.datasets[4].data.push((data_temp_vhot[3]-i)/(data_temp_vhot[3]-data_temp_vhot[2]));
          }
        }

        var fuzzyChartTemp = new Chart(lineChartTemp, {
          type: 'line',
          data: dataLine,
          options: {
            title: {
              display: true,                
              text: 'Fuzzy Graph'
            },
            pieceLabel: {
              render: 'value'
            },
            legend: {
              display: false,
            },
            elements: {
              point:{
                radius: 0
              }
            },
              // scales: {
              //     xAxes: [{
              //         ticks: {
              //             autoSkip : true,
              //             callback: function(value, index, values) {
              //                 return value;
              //             }
              //         },
              //         gridLines : {
              //             display : false,
              //         }
              //     }]
              // },
          }
        })
    }