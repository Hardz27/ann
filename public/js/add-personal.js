$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    var counter = 1;
    var actions = $("table td:last-child").html();
    // Append table with add row form on add new button click
    $(".add-new").click(function(){
        
        var index = $("table tbody tr:last-child").index();
        var row =
          '<div class="panel panel-default">' +
            '<div class="panel-heading">' +
                '<h4 class="panel-title">' +
          '<a data-toggle="collapse" data-parent="#accordion" href="#collapse' + counter + '">Personal data information ' + (counter + 1) +'</a>' +
          '<a data-toggle="collapse" data-parent="#accordion" href="#collapse' + counter + '" style="float: right; font-size: 20px;">-</a>' +
        '</h4>' +
      '</div>' +
      '<div id="collapse' + counter + '" class="panel-collapse collapse in">' +
        ' <div class="panel-body" style="padding: 10px;">' +
            
            '<thead><tr><td></td></tr></thead>' +
                '<tr>' +
                    '<td><p id="kolom">Personal name</td>' + 
                    '<td><input type="text" class="form-control" name="pic_name" placeholder="Personal name"></td>' +
                '</tr>' +
                '<tr>' +
                    '<td><p id="kolom">Position</td>' + 
                    '<td><input type="text" class="form-control" name="pic_position" placeholder="Postion"></td>' +
                '</tr>' +
                '<tr>' +
                    '<td><p id="kolom">Divisi</td>' + 
                    '<td><input type="text" class="form-control" name="pic_divisi" placeholder="Division"></td>' +
                '</tr>' +
                '<tr>' +
                    '<td><p id="kolom">Email</td>' + 
                    '<td><input type="text" class="form-control" name="pic_email" placeholder="Email Address"></td>' +
                '</tr>' + 
                '<tr>' +
                    '<td><p id="kolom">Phone number</td>' + 
                    '<td><input type="text" class="form-control" name="pic_phone" placeholder="Phone number"></td>' +
                '</tr>' +
                '<tr>' +
                    '<td><p id="kolom">Social media URL/LinkedIn</td>' + 
                    '<td><input type="text" class="form-control" name="pic_url" placeholder="Social Media URL"></td>' +
                '</tr>' +
                '<tr>' +
                    '<td><p id="kolom">Staus</td>' + 
                    '<td><select name="pic_status" class="form-control">' +
                            '<option value="pilihan1">Pilihan 1</option>' +
                            '<option value="pilihan2">Pilihan 2</option>' +
                            '<option value="pilihan3">Pilihan 3</option>' +
                            '<option value="pilihan4">Pilihan 4</option>' +
                        '</select></td>' +
                '</tr></div>' +
             '</div>' +
            '</div>';
        $("#person").append(row);     
        $("table tbody tr").eq(index + 1).find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();
        counter++;
    });
    // Add row on add button click
    $(document).on("click", ".add", function(){
        var empty = false;
        var input = $(this).parents("tr").find('input[type="text"]');
        input.each(function(){
            if(!$(this).val()){
                $(this).addClass("error");
                empty = true;
            } else{
                $(this).removeClass("error");
            }
        });
        $(this).parents("tr").find(".error").first().focus();
        if(!empty){
            input.each(function(){
                $(this).parent("td").html($(this).val());
            });         
            $(this).parents("tr").find(".add, .edit").toggle();
            $(".add-new").removeAttr("disabled");
        }       
    });
    // Edit row on edit button click
    $(document).on("click", ".edit", function(){        
        $(this).parents("tr").find("td:not(:last-child)").each(function(){
            $(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">');
        });     
        $(this).parents("tr").find(".add, .edit").toggle();
        $(".add-new").attr("disabled", "disabled");
    });
    // Delete row on delete button click
    $(document).on("click", ".delete", function(){
        $(this).parents("tr").remove();
        $(this).parents("tr").remove();
        $(".add-new").removeAttr("disabled");
    });

    $(document).on("click", ".tabledel", function(){
        document.excel_form.action = "http://leads-project_v2.test/check_excel";
        $(this).parents("table").remove();
        $("#check3").remove();
        $("#excelmain").append('<table id="exceltable" class="table table-striped projects">');
        $("#excelmain").append('</table>');
        $("#excelmain").append('<button id="check2" type="submit" class="btn btn-success btn-md null">Check</button>');
        
    });
});