// BUTTON DETAIL 
            
$("body").on("click", ".btn-show-detail", function(event){
    event.preventDefault();

    var me = $(this),
        url = me.attr("href"),
        title = me.attr("title");

    $("#modal-title-detail").text(title);

    $.ajax({
        url: url,
        dataType: "html",
        success: function (response) {
            $("#modal-body-detail").html(response);
        }
    });
    $("#modal-detail").modal("show");

});

// BUTTON VALIDATION 

$("body").on("click", ".btn-show-validation", function(event){
    event.preventDefault();

    var me = $(this),
    url = me.attr("href"),
    title = me.attr("title");

    $("#modal-title-validation").text(title);

    $.ajax({
        url:url,
        dataType: "html",
        success:function(response) {
            $("#modal-body-validation").html(response);
        }
    });
    $("#modal-validation").modal("show");

    });

    // MODAL Delete
    $("body").on("click", ".btn-show-delete", function(event){
        event.preventDefault();
    
        var me = $(this),
        url = me.attr("href"),
        title = me.attr("title");
    
        $("#modal-title-delete").text(title);
    
        $.ajax({
            url:url,
            dataType: "html",
            success:function(response) {
                $("#modal-body-delete").html(response);
            }
        });
        $("#modal-delete").modal("show");
    
        });

          //MODAL RESTORE
    $("body").on("click", ".btn-show-restore", function (event){
    event.preventDefault();
    
        var me = $(this),
        url = me.attr("href"),
        title = me.attr("title");

        $("#modal-title-restore").text(title);

        $.ajax({
            url:url,
            dataType: "html",
            success:function (response) {
                $("#modal-body-restore").html(response);
                
            }
        });
        $("#modal-restore").modal("show");
    });


          // MODAL Valid
    $("body").on("click", ".btn-home-valid", function(event){
        event.preventDefault();
    
        var me = $(this),
        url = me.attr("href"),
        title = me.attr("title");
    
        $("#modal-title-valid").text(title);
    
        $.ajax({
            url:url,
            dataType: "html",
            success:function(response) {
                $("#modal-body-valid").html(response);
            }
        });
        $("#modal-valid").modal("show");
    
        });

        // Modal Unvalid

        $("body").on("click", ".btn-home-unvalid", function(event){
            event.preventDefault();
        
            var me = $(this),
            url = me.attr("href"),
            title = me.attr("title");
        
            $("#modal-title-unvalid").text(title);
        
            $.ajax({
                url:url,
                dataType: "html",
                success:function(response) {
                    $("#modal-body-unvalid").html(response);
                }
            });
            $("#modal-unvalid").modal("show");
        
            });

            // Modal Unvalid

        $("body").on("click", ".btn-home-karyawan", function(event){
            event.preventDefault();
        
            var me = $(this),
            url = me.attr("href"),
            title = me.attr("title");
        
            $("#modal-title-karyawan").text(title);
        
            $.ajax({
                url:url,
                dataType: "html",
                success:function(response) {
                    $("#modal-body-karyawan").html(response);
                }
            });
            $("#modal-karyawan").modal("show");
        
            });


           //Modal HOME-UNVALID
    $(document).on('ajaxComplete ajaxReady ready', function () {
        $('.unvalid ul.pagination-unvalid li a').off('click').on('click', function (e) {
            $('#modal-unvalid').modal('show');
            $('#modal-body-unvalid').load($(this).attr('href'));
            $('#modal-title-unvalid').html($(this).attr('title'));
            e.preventDefault();
        });
    });

    //Modal HOME-VALID
    $(document).on('ajaxComplete ajaxReady ready', function () {
        $('.valid ul.pagination-valid li a').off('click').on('click', function (e) {
            $('#modal-valid').modal('show');
            $('#modal-body-valid').load($(this).attr('href'));
            $('#modal-title-valid').html($(this).attr('title'));
            e.preventDefault();
        });
    });

     //Modal HOME-Karyawan
     $(document).on('ajaxComplete ajaxReady ready', function () {
        $('.karyawan ul.pagination-karyawan li a').off('click').on('click', function (e) {
            $('#modal-karyawan').modal('show');
            $('#modal-body-karyawan').load($(this).attr('href'));
            $('#modal-title-karyawan').html($(this).attr('title'));
            e.preventDefault();
        });
    });

  
