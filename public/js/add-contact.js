$(document).ready(function () {
        var domain = 0;
        var email = 0;

         // ========================================================================
        // =                               Add Domian                         =
        // ======================================================================


        $("#addDomain").on("click", function () {
            var newRow = $('<tr>');
            var cols = "";

            cols += '<td style="border-top: none; width: 30%;"></td>';
            cols += '<td style="border-top: none; width: 60%;"><div class="col-sm-10"><input type="text" style="width: 110%;" class="form-control" name="domain_name[]" placeholder="Your Company Domain" required="required" /></div></td>';
            cols += '<td style="border-top: none; width: 10%;"><a class="delete domainBtnDel" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a></td>';
            newRow.append(cols);
            $("#sub_domain").append(newRow);
            domain++;
            
        });

{/* <td style="border-top: none;"><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td> */}

        $("#sub_domain").on("click", ".domainBtnDel", function (event) {
            $(this).closest("tr").remove();       
            domain -= 1;
        });

        // ========================================================================
        // =                               Add E-Mail                             =
        // ======================================================================

        $("#addEmail").on("click", function () {
            var newRow = $('<tr>');
            var cols = "";

            cols += '<td style="border-top: none; width: 30%;"></td>';
            cols += '<td style="border-top: none; width: 60%;"><div class="col-sm-10"><input type="text" style="width: 110%;" class="form-control" name="email_name[]" placeholder="Your Company E-Mail" required="required" /></div></td>';
            cols += '<td style="border-top: none; width: 10%;"><a class="delete emailBtnDel" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a></td>';
            newRow.append(cols);
            $("#sub_email").append(newRow);
            email++;
        });

{/* <td style="border-top: none;"><input type="button" class="ibtnDel btn btn-md btn-danger"  value="Delete"></td> */}

        $("#sub_email").on("click", ".emailBtnDel", function (event) {
            $(this).closest("tr").remove();       
            email -= 1;
        });


    });
