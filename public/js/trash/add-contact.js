$(document).ready(function () {
        var domain = 0;
        var email = 0;
        dynamic_field(count);


    function dynamic_field(number){

     
        $("#addDomain").on("click", function () {
            var newRow = $('<tr>');
            var cols = "";

            cols += '<td style="border-top: none; width: 30%;"></td>';
            cols += '<td style="border-top: none; width: 60%;"><div class="col-sm-10"><input type="text" style="width: 110%;" class="form-control" name="domain_name[]'+ domain +'" placeholder="Your Company Domain" required="required" /></div></td>';
            cols += '<td style="border-top: none; width: 10%;"><a class="delete domainBtnDel" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a></td>';
            newRow.append(cols);
            $("#sub_domain").append(newRow);
            domain++;
        });

// <td style="border-top: none;"><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>

        // $("#sub_domain").on("click", ".domainBtnDel", function (event) {
        //     $(this).closest("tr").remove();       
        //     domain -= 1;
        // });

        // ========================================================================
        // =                               Add E-Mail                             =
        // ========================================================================

        $("#addEmail").on("click", function () {
            var newRow = $('<tr>');
            var cols = "";

            cols += '<td style="border-top: none; width: 30%;"></td>';
            cols += '<td style="border-top: none; width: 60%;"><div class="col-sm-10"><input type="text" style="width: 110%;" class="form-control" name="email_name[]'+ email +'" placeholder="Your Company E-Mail" required="required" /></div></td>';
            cols += '<td style="border-top: none; width: 10%;"><a class="delete emailBtnDel" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a></td>';
            newRow.append(cols);
            $("#sub_email").append(newRow);
            email++;
        });

// <td style="border-top: none;"><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>

        // $("#sub_email").on("click", ".emailBtnDel", function (event) {
        //     $(this).closest("tr").remove();       
        //     email -= 1;
        // });

    }

    //add & remove input field

    $("#sub_domain").on("click", ".domainBtnDel", function (event) {
        $(this).closest("tr").remove();       
        domain -= 1;
    });

    $("#sub_email").on("click", ".emailBtnDel", function (event) {
        $(this).closest("tr").remove();       
        email -= 1;
    });

        // $('#add').click(function(){
        //     count++;
        //     dynamic_field(count);
        // });

        // $(document).on('click', '#remove', function(){
        //     count--;
        //     dynamic_field(count);
        // });

        //submit for Store

    $('#input_form').on('submit', function(event){
        event.preventDefault();
        $.ajax({
            url:'{{ route("check.store") }}',
            method:'post',
            data:$(this).serialize(),
            dataType:'json',
            beforeSend:function(){
                $('#save').attr('disableb','disableb');
            },
            success:function(data)
            {
                if (data.error) {
                    var error_html = '';
                    for(var count = 0; count < data.error.length; count++)
                    {
                        error_html += '<p>'+data.error[count]+'</p>';
                    }
                        $('#result').html('<div class="alert alert-danger">'+error_html+'</div>');
                } 
                
                else {
                        dynamic_field(1);
                        $('#result').html('<div class="alert alert-success">'+data.success+'</div>');
                }
                $('#save').attr('disabled', false);
            }
        })
    });


    });
