require('./bootstrap');
window.$ = window.jQuery = require('jquery');

var no = 1;
$('#tambah_bahan').click(function(){
  no += 1;
  var appendthis = '<tr>' + $('#input_bahan').html() + '</tr>';
  var id = 'bahan_id_'+no;
  var kuantiti_id = 'bahan_kuantiti_'+no;
  var row_bahan = 'row_bahan_'+no;
  var html = ' <div class="field-group" id="'+row_bahan+'"> \
              <div class="field-row"> \
                <div class="form-group"> \
                  <select class="form-control select2" id="'+id+'" name="'+id+'"></select> \
                </div> \
              </div> \
              <div class="field-row"> \
                <div class="form-group"> \
                  <input class="form-control" name="bahan_kuantiti_'+no+'" type="number" id="bahan_kuantiti_'+no+'" /> \
                </div> \
              </div> \
              <div class="field-row close"> \
                <a id="delete_tambah_bahan" href="#" class="btn btn-danger">Hapus</a> \
              </div> \
            </div> ';
    // var html = '<tr id="'+row_bahan+'"> \
    //               <td> \
    //                 <div class="form-group row"> \
    //                    \
    //                   <div class="col-md-6"> \
    //                     <select class="form-control select2 select2bahan" id="'+id+'" name="'+id+'" style="width:100%;"> \</select> \
    //                   </div> \
    //                 </div> \
    //               </td> \
    //               <td> \
    //                 <div class="form-group row"> \
    //                   <div class="col-md-2"><label></label></div> \
    //                   <div class="col-md-6"> \
    //                     <input class="form-control" name="bahan_kuantiti_'+no+'" type="number" id="bahan_kuantiti_'+no+'"> \
    //                   </div> \
    //               </td> \
    //             </tr>';
    //               // <td><a id="'+no+'" class="btn btn-xs btn-outline-danger delete-bahan"><i class="fa fa-trash"></i></a></td> \
  $(html).insertBefore('#firstField');
  // recall select2
  var el = '#bahan_id_'+no;
  $('.select2').select2();
  $('#bahan_id_1 option').each(function(){
    // Add $(this).val() to your list
    var text = $(this).text();
    var value = $(this).val();
    var newOption = new Option(text,value, false, false);
    $(el).append(newOption).trigger('change');
  });
    // scroll ke appended element
  $([document.documentElement, document.body]).stop().animate({
      scrollTop: $("#dinamicPenerimaan").offset().top
  }, 500);
});

$("#delete_tambah_bahan").live('click', function (ev) {
  if (ev.type == 'click') {
    $(this).parents(".field-group").fadeOut();
    $(this).parents(".field-group").remove();
});

var no_metode = 1;
$('#tambah_metode').click(function(){
    no_metode += 1;
    var id = 'metode_id_'+no_metode;
    var row_metode = 'row_metode_'+no_metode;
    var html = '<div class="form-group row" id="'+row_metode+'"> \
                    <div class="col-md-2"><label></label></div> \
                    <div class="col-md-6"> \
                    <select class="form-control select2" id="metode_id_'+no_metode+'" name="'+id+'" class="" style="width:100%;"> \</select> \
                    </div> \
                </div> \
                ';
    $(html).insertBefore('#tambah_metode');
    var el_metode = '#metode_id_'+no_metode;
    $('.select2').select2();
    $('#metode_id_1 option').each(function(){
      // Add $(this).val() to your list
      var text = $(this).text();
      var value = $(this).val();
      var newOption_metode = new Option(text,value, false, false);
      $(el_metode).append(newOption_metode).trigger('change');
    });
    // scroll ke appended element
    $([document.documentElement, document.body]).stop().animate({
        scrollTop: $("#input_metode").offset().top
    }, 500);
});

$('.delete-bahan').click(function(){
  var id_row = $(this).id();
});
