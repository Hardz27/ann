    function ExportToTable() {
        $('#dummy').addClass('null');
        $('#check2').removeClass('null'); 
        $('#viewfile').attr("disabled", "disabled");

        // $("#exceltable").on("click", ".tableBtnDel", function (event) {
        //     $(this).parent("table").remove();       
        // });

         var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx|.xls)$/;  
         /*Checks whether the file is a valid excel file*/  
         if (regex.test($("#excelfile").val().toLowerCase())) {  
             var xlsxflag = false; /*Flag for checking whether excel is .xls format or .xlsx format*/  
             if ($("#excelfile").val().toLowerCase().indexOf(".xlsx") > 0) {  
                 xlsxflag = true;  
             }  
             /*Checks whether the browser supports HTML5*/  
             if (typeof (FileReader) != "undefined") {  
                 var reader = new FileReader();  
                 reader.onload = function (e) {  
                     var data = e.target.result;  
                     /*Converts the excel data in to object*/  
                     if (xlsxflag) {  
                         var workbook = XLSX.read(data, { type: 'binary' });  
                     }  
                     else {  
                         var workbook = XLS.read(data, { type: 'binary' });  
                     }  
                     /*Gets all the sheetnames of excel in to a variable*/  
                     var sheet_name_list = workbook.SheetNames;  
      
                     var cnt = 0; /*This is used for restricting the script to consider only first sheet of excel*/  
                     sheet_name_list.forEach(function (y) { /*Iterate through all sheets*/  
                         /*Convert the cell value to Json*/  
                         console.log(y);
                         if (xlsxflag) {  
                             var exceljson = XLSX.utils.sheet_to_json(workbook.Sheets[y]);  
                         }  
                         else {  
                             var exceljson = XLS.utils.sheet_to_row_object_array(workbook.Sheets[y]);  
                         }  
                         if (exceljson.length > 0 && cnt == 0) {  
                             BindTable(exceljson, '#exceltable');  
                             cnt++;  
                         }  
                     });  
                     $('#exceltable').show();  
                 }  
                 if (xlsxflag) {/*If excel file is .xlsx extension than creates a Array Buffer from excel*/  
                     reader.readAsArrayBuffer($("#excelfile")[0].files[0]);  
                 }  
                 else {  
                     reader.readAsBinaryString($("#excelfile")[0].files[0]);  
                 }  
             }  
             else {  
                 alert("Sorry! Your browser does not support HTML5!");  
             }  
         }  
         else {  
          $(document).ready(function() {
              alert('Success upload CSV Filesed!');
              // The event listener for the file upload
              // var coba = document.getElementById('excelfile');
              // console.log(coba);
              // document.getElementById('excelfile').addEventListener('change', upload, false);

              // // Method that checks that the browser supports the HTML5 File API
              function browserSupportFileUpload() {
                  var isCompatible = false;
                  if (window.File && window.FileReader && window.FileList && window.Blob) {
                  isCompatible = true;
                  }
                  return isCompatible;
              }

              // // Method that reads and processes the selected file
              function upload(evt) {
                alert('Masuk upload');
                  if (!browserSupportFileUpload()) {
                      alert('The File APIs are not fully supported in this browser!');
                      } else {
                          var data = null;
                          let val;
                          var file = evt.target.files[0];
                          var reader = new FileReader();
                          reader.readAsText(file);

                          reader.onload = function(event) {
                              var csvData = event.target.result;
                              data = $.csv.toArrays(csvData);
                              $("#dummy").find("tr").append("asdasdasd");
                              if (data && data.length > 0) {
                                alert('Imported -' + data.length + '- rows successfully!');
                              } else {
                                alert('No data to import!');
                              }
                              $.each(data, function(i, row) {
                                  console.log(row[0].split(';'));
                              })
                          };
                          
                          reader.onerror = function() {
                              // alert('Unable to read ' + file.fileName);
                              alert("Please upload a valid Excel file or CSV filess! "); 
                              $('#dummy').removeClass('null'); 
                              $('#check2').addClass('null'); 
                              $('#viewfile').removeAttr("disabled", "disabled");
                          };
                      }
                  }

                  upload();
              });
         }  
     }  
    
    function BindTable(jsondata, tableid) {/*Function used to convert the JSON array to Html Table*/  
         var columns = BindTableHeader(jsondata, tableid); /*Gets all the column headings of Excel*/  
         console.log(columns);
         // for (var i = 1; i < jsondata.length; i++) {  
          for (var i = 1; i < jsondata.length; i++) {  
            console.log(i);
             var row$ = $('<tr/>');  
             for (var colIndex = 0; colIndex < columns.length; colIndex++) {  

                 var cellValue = jsondata[i][columns[colIndex]];  
                 if (cellValue == null)  {
                     cellValue = "";  
                 }
                 if(colIndex==0){
                    row$.append($('<td>').html('<input type="hidden" name="company_name[]" value="' + cellValue + '" class="hidden" style="display: none;" />' + cellValue));
                  }
                  else if(colIndex==1){
                    row$.append($('<td>').html('<input type="hidden" name="company_domain[]" value="' + cellValue + '" class="hidden" style="display: none;" />' + cellValue));
                  }
                  else if(colIndex==2){
                    row$.append('<input type="hidden" name="excel_email[]" value="' + cellValue + '" class="hidden" style="display: none;" />');
                  }
                  else if(colIndex==3){
                    row$.append('<input type="hidden" name="excel_field[]" value="' + cellValue + '" class="hidden" style="display: none;" />');
                  }   
                  else if(colIndex==4){
                    row$.append('<input type="hidden" name="excel_type[]" value="' + cellValue + '" class="hidden" style="display: none;" />');
                  }   
                  else if(colIndex==5){
                    row$.append('<input type="hidden" name="excel_desc[]" value="' + cellValue + '" class="hidden" style="display: none;" />');
                  }   
                  else if(colIndex==6){
                    row$.append('<input type="hidden" name="excel_address[]" value="' + cellValue + '" class="hidden" style="display: none;" />');
                  }   
                  else if(colIndex==7){
                    row$.append('<input type="hidden" name="excel_telpon_office[]" value="' + cellValue + '" class="hidden" style="display: none;" />');
                  }   
                  else if(colIndex==8){
                    row$.append('<input type="hidden" name="excel_faximile[]" value="' + cellValue + '" class="hidden" style="display: none;" />');
                  }   
                  else if(colIndex==9){
                    row$.append('<input type="hidden" name="excel_status[]" value="' + cellValue + '" class="hidden" style="display: none;" />');
                  }   
                  else if(colIndex==10){
                    row$.append('<input type="hidden" name="excel_pic_name[]" value="' + cellValue + '" class="hidden" style="display: none;" />');
                  }
                  else if(colIndex==11){
                    row$.append('<input type="hidden" name="excel_pic_position[]" value="' + cellValue + '" class="hidden" style="display: none;" />');
                  }
                  else if(colIndex==12){
                    row$.append('<input type="hidden" name="excel_pic_division[]" value="' + cellValue + '" class="hidden" style="display: none;" />');
                  }
                  else if(colIndex==13){
                    row$.append('<input type="hidden" name="excel_pic_nohp[]" value="' + cellValue + '" class="hidden" style="display: none;" />');
                  }
                  else if(colIndex==14){
                    row$.append('<input type="hidden" name="excel_pic_email[]" value="' + cellValue + '" class="hidden" style="display: none;" />');
                  }
                  else if(colIndex==15){
                    row$.append('<input type="hidden" name="excel_pic_sosmed[]" value="' + cellValue + '" class="hidden" style="display: none;" />');
                  }
                  else if(colIndex==16){
                    row$.append('<input type="hidden" name="excel_pic_status[]" value="' + cellValue + '" class="hidden" style="display: none;" />');
                  }                 

             }


                 // 'pic_name' => $request->excel_pic_name[$i],
                 // 'pic_position' => $request->excel_pic_position[$i],
                 // 'pic_division' => $request->excel_pic_division[$i],
                 // 'pic_nohp' => $request->excel_pic_nohp[$i],
                 // 'pic_email' => $request->excel_pic_email[$i],
                 // 'pic_sosmed' => $request->excel_pic_sosmed[$i],
                 // 'pic_status' => $request->excel_pic_status[$i],


            row$.append($('<td><a class="delete domainBtnDel" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a></td>')); 
             $(tableid).append(row$);  
         }  
     }
     function BindTableHeader(jsondata, tableid) {/*Function used to get all column names from JSON and bind the html table header*/  
         var columnSet = [];  
         var headerTr$ = $('<tr/>'); 
         var head = 0; 
         for (var i = 0; i < jsondata.length; i++) {  
             var rowHash = jsondata[i];  
             for (var key in rowHash) {  
                 if (rowHash.hasOwnProperty(key)) {  
                     if ($.inArray(key, columnSet) == -1) {/*Adding each unique column names to a variable array*/  
                         columnSet.push(key);
                         if (head < 2) {
                            // headerTr$.append($('<th/>').html(key));  
                         }
                     }  
                 }
                 head++;  
             }  
         }
         headerTr$.append($('<th/>').html("Company Name"));  
         headerTr$.append($('<th/>').html("Domain"));  
         headerTr$.append($('<th/>').html("action"));  
         $(tableid).append(headerTr$);  
         return columnSet;  
     }  


// ==================================================================
// Menghilangkan tabel dummy
// ==================================================================

$(document).ready(function() {
        
        setTimeout(function(){
            $('body').addClass('loaded');
            $('#title-opening').css('color','#FFFFFF');
        }, 100);
        
    });


     