var soil_dry_max, soil_normal_max, soil_adq_max, soil_satruated_max = 0
var data_soil_dry, data_soil_normal, data_soil_adq, data_soil_satruated = [0, 0, 0, 0]

$('#generate_soil').on('click', function(){
  data_soil_dry = generate_fuzzy_soil('soil_dry')
  soil_dry_max = data_soil_dry[3]
  data_soil_normal = generate_fuzzy_soil('soil_normal')
  soil_normal_max = data_soil_normal[3]
  data_soil_adq = generate_fuzzy_soil('soil_adq')
  soil_adq_max = data_soil_adq[3]
  data_soil_satruated = generate_fuzzy_soil('soil_satruated')
  soil_satruated_max = data_soil_satruated[3]

  grafik()
})

function generate_fuzzy_soil(id){
  data = $('#'+id).val()
  data = data.split(' ')

  if (parseInt(data[1]) < parseInt(data[0])) {
    data[1] = parseInt(data[0])
  }
  if (parseInt(data[2]) < parseInt(data[1])) {
    data[2] = parseInt(data[1])
  }
  if (parseInt(data[3]) < parseInt(data[2])) {
    data[3] = parseInt(data[2])
  }

  return data

}

$('#soil_dry').on('change', function(){
  data_soil_dry = generate_fuzzy_soil('soil_dry')
  soil_dry_max = data_soil_dry[3]

  $('#soil_dry').val(data_soil_dry[0] +" "+ data_soil_dry[1] +" "+ data_soil_dry[2] +" "+ data_soil_dry[3])

  grafik()

})

$('#soil_normal').change(function(){
  data_soil_normal = generate_fuzzy_soil('soil_normal')
  soil_normal_max = data_soil_normal[3]

  $('#soil_normal').val(data_soil_normal[0] +" "+ data_soil_normal[1] +" "+ data_soil_normal[2] +" "+ data_soil_normal[3])

  grafik()

})

$('#soil_adq').on('change', function(){
  data_soil_adq = generate_fuzzy_soil('soil_adq')
  soil_adq_max = data_soil_adq[3]

  $('#soil_adq').val(data_soil_adq[0] +" "+ data_soil_adq[1] +" "+ data_soil_adq[2] +" "+ data_soil_adq[3])

  grafik()

})

$('#soil_satruated').on('change', function(){
  data_soil_satruated = generate_fuzzy_soil('soil_satruated')
  soil_satruated_max = data_soil_satruated[3]

  $('#soil_satruated').val(data_soil_satruated[0] +" "+ data_soil_satruated[1] +" "+ data_soil_satruated[2] +" "+ data_soil_satruated[3])

  grafik()

})

grafik()

function grafik() 
{
        // Start Chart total Pasien
        ////////////////////////////////
        var lineChart = document.getElementById("lineChart");
        function objectLabel() {this.lineTension= 0.0, this.fill = false, borderColor = ['#2bc0d9'], this.data = []};

        var dataLine = {
          labels: [],
          datasets: []
        };

        for (var i = 0; i < 100; i++) {
          dataLine.labels.push(i);
        }

        for (var i = 1; i <= 4; i++) {
          dataLine.datasets.push(new objectLabel())
        }

        for (var i = 1; i <= soil_dry_max; i++) {
          if (i <= data_soil_dry[0] || i >= data_soil_dry[3]) {
            dataLine.datasets[0].data.push(0);  
          }else if(i > data_soil_dry[0] && i < data_soil_dry[1]){
            dataLine.datasets[0].data.push((i-data_soil_dry[0])/(data_soil_dry[1]-data_soil_dry[0]));
          }else if(i >= data_soil_dry[1] && i <= data_soil_dry[2]){
            dataLine.datasets[0].data.push(1);
          }else if(i > data_soil_dry[2] && i < data_soil_dry[3]){
            dataLine.datasets[0].data.push((data_soil_dry[3]-i)/(data_soil_dry[3]-data_soil_dry[2]));
          }
        }

        for (var i = 1; i <= soil_normal_max; i++) {
          if (i <= data_soil_normal[0] || i >= data_soil_normal[3]) {
            dataLine.datasets[1].data.push(0);  
          }else if(i > data_soil_normal[0] && i < data_soil_normal[1]){
            dataLine.datasets[1].data.push((i-data_soil_normal[0])/(data_soil_normal[1]-data_soil_normal[0]));
          }else if(i >= data_soil_normal[1] && i <= data_soil_normal[2]){
            dataLine.datasets[1].data.push(1);
          }else if(i > data_soil_normal[2] && i < data_soil_normal[3]){
            dataLine.datasets[1].data.push((data_soil_normal[3]-i)/(data_soil_normal[3]-data_soil_normal[2]));
          }
        }

        for (var i = 1; i <= soil_adq_max; i++) {
          if (i <= data_soil_adq[0] || i >= data_soil_adq[3]) {
            dataLine.datasets[2].data.push(0);  
          }else if(i > data_soil_adq[0] && i < data_soil_adq[1]){
            dataLine.datasets[2].data.push((i-data_soil_adq[0])/(data_soil_adq[1]-data_soil_adq[0]));
          }else if(i >= data_soil_adq[1] && i <= data_soil_adq[2]){
            dataLine.datasets[2].data.push(1);
          }else if(i > data_soil_adq[2] && i < data_soil_adq[3]){
            dataLine.datasets[2].data.push((data_soil_adq[3]-i)/(data_soil_adq[3]-data_soil_adq[2]));
          }
        }

        for (var i = 1; i <= soil_satruated_max; i++) {
          if (i <= data_soil_satruated[0] || i >= data_soil_satruated[3]) {
            dataLine.datasets[3].data.push(0);  
          }else if(i > data_soil_satruated[0] && i < data_soil_satruated[1]){
            dataLine.datasets[3].data.push((i-data_soil_satruated[0])/(data_soil_satruated[1]-data_soil_satruated[0]));
          }else if(i >= data_soil_satruated[1] && i <= data_soil_satruated[2]){
            dataLine.datasets[3].data.push(1);
          }else if(i > data_soil_satruated[2] && i < data_soil_satruated[3]){
            dataLine.datasets[3].data.push((data_soil_satruated[3]-i)/(data_soil_satruated[3]-data_soil_satruated[2]));
          }
        }

        var fuzzyChart = new Chart(lineChart, {
          type: 'line',
          data: dataLine,
          options: {
            title: {
              display: true,                
              text: 'Fuzzy Graph'
            },
            pieceLabel: {
              render: 'value'
            },
            legend: {
              display: false,
            },
            elements: {
              point:{
                radius: 0
              }
            },
              // scales: {
              //     xAxes: [{
              //         ticks: {
              //             autoSkip : true,
              //             callback: function(value, index, values) {
              //                 return value;
              //             }
              //         },
              //         gridLines : {
              //             display : false,
              //         }
              //     }]
              // },
          }
        })
    }