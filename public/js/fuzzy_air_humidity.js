var humidity_low_max, humidity_normal_max, humidity_high_max, humidity_ext_high_max = 0
var data_humidity_low, data_humidity_normal, data_humidity_high, data_humidity_ext_high = [0, 0, 0, 0]

$('#generate_humidity').on('click', function(){
  data_humidity_low = generate_fuzzy_humidity('humidity_low')
  humidity_low_max = data_humidity_low[3]
  data_humidity_normal = generate_fuzzy_humidity('humidity_normal')
  humidity_normal_max = data_humidity_normal[3]
  data_humidity_high = generate_fuzzy_humidity('humidity_high')
  humidity_high_max = data_humidity_high[3]
  data_humidity_ext_high = generate_fuzzy_humidity('humidity_ext_high')
  humidity_ext_high_max = data_humidity_ext_high[3]

  grafik_humid()
})

function generate_fuzzy_humidity(id){
  data = $('#'+id).val()
  data = data.split(' ')

  if (parseInt(data[1]) < parseInt(data[0])) {
    data[1] = parseInt(data[0])
  }
  if (parseInt(data[2]) < parseInt(data[1])) {
    data[2] = parseInt(data[1])
  }
  if (parseInt(data[3]) < parseInt(data[2])) {
    data[3] = parseInt(data[2])
  }

  return data

}

$('#humidity_low').on('change', function(){
  data_humidity_low = generate_fuzzy_humidity('humidity_low')
  humidity_low_max = data_humidity_low[3]

  $('#humidity_low').val(data_humidity_low[0] +" "+ data_humidity_low[1] +" "+ data_humidity_low[2] +" "+ data_humidity_low[3])

  grafik_humid()

})

$('#humidity_normal').change(function(){
  data_humidity_normal = generate_fuzzy_humidity('humidity_normal')
  humidity_normal_max = data_humidity_normal[3]

  $('#humidity_normal').val(data_humidity_normal[0] +" "+ data_humidity_normal[1] +" "+ data_humidity_normal[2] +" "+ data_humidity_normal[3])

  grafik_humid()

})

$('#humidity_high').on('change', function(){
  data_humidity_high = generate_fuzzy_humidity('humidity_high')
  humidity_high_max = data_humidity_high[3]

  $('#humidity_high').val(data_humidity_high[0] +" "+ data_humidity_high[1] +" "+ data_humidity_high[2] +" "+ data_humidity_high[3])

  grafik_humid()

})

$('#humidity_ext_high').on('change', function(){
  data_humidity_ext_high = generate_fuzzy_humidity('humidity_ext_high')
  humidity_ext_high_max = data_humidity_ext_high[3]

  $('#humidity_ext_high').val(data_humidity_ext_high[0] +" "+ data_humidity_ext_high[1] +" "+ data_humidity_ext_high[2] +" "+ data_humidity_ext_high[3])

  grafik_humid()

})

grafik_humid()

function grafik_humid() 
{
        // Start Chart total Pasien
        ////////////////////////////////
        var lineChartHumid = document.getElementById("lineChartHumid");
        function objectLabel() {this.lineTension= 0.0, this.fill = false, borderColor = ['#2bc0d9'], this.data = []};

        var dataLine = {
          labels: [],
          datasets: []
        };

        for (var i = 0; i < 100; i++) {
          dataLine.labels.push(i);
        }

        for (var i = 1; i <= 4; i++) {
          dataLine.datasets.push(new objectLabel())
        }

        for (var i = 1; i <= humidity_low_max; i++) {
          if (i <= data_humidity_low[0] || i >= data_humidity_low[3]) {
            dataLine.datasets[0].data.push(0);  
          }else if(i > data_humidity_low[0] && i < data_humidity_low[1]){
            dataLine.datasets[0].data.push((i-data_humidity_low[0])/(data_humidity_low[1]-data_humidity_low[0]));
          }else if(i >= data_humidity_low[1] && i <= data_humidity_low[2]){
            dataLine.datasets[0].data.push(1);
          }else if(i > data_humidity_low[2] && i < data_humidity_low[3]){
            dataLine.datasets[0].data.push((data_humidity_low[3]-i)/(data_humidity_low[3]-data_humidity_low[2]));
          }
        }

        for (var i = 1; i <= humidity_normal_max; i++) {
          if (i <= data_humidity_normal[0] || i >= data_humidity_normal[3]) {
            dataLine.datasets[1].data.push(0);  
          }else if(i > data_humidity_normal[0] && i < data_humidity_normal[1]){
            dataLine.datasets[1].data.push((i-data_humidity_normal[0])/(data_humidity_normal[1]-data_humidity_normal[0]));
          }else if(i >= data_humidity_normal[1] && i <= data_humidity_normal[2]){
            dataLine.datasets[1].data.push(1);
          }else if(i > data_humidity_normal[2] && i < data_humidity_normal[3]){
            dataLine.datasets[1].data.push((data_humidity_normal[3]-i)/(data_humidity_normal[3]-data_humidity_normal[2]));
          }
        }

        for (var i = 1; i <= humidity_high_max; i++) {
          if (i <= data_humidity_high[0] || i >= data_humidity_high[3]) {
            dataLine.datasets[2].data.push(0);  
          }else if(i > data_humidity_high[0] && i < data_humidity_high[1]){
            dataLine.datasets[2].data.push((i-data_humidity_high[0])/(data_humidity_high[1]-data_humidity_high[0]));
          }else if(i >= data_humidity_high[1] && i <= data_humidity_high[2]){
            dataLine.datasets[2].data.push(1);
          }else if(i > data_humidity_high[2] && i < data_humidity_high[3]){
            dataLine.datasets[2].data.push((data_humidity_high[3]-i)/(data_humidity_high[3]-data_humidity_high[2]));
          }
        }

        for (var i = 1; i <= humidity_ext_high_max; i++) {
          if (i <= data_humidity_ext_high[0] || i >= data_humidity_ext_high[3]) {
            dataLine.datasets[3].data.push(0);  
          }else if(i > data_humidity_ext_high[0] && i < data_humidity_ext_high[1]){
            dataLine.datasets[3].data.push((i-data_humidity_ext_high[0])/(data_humidity_ext_high[1]-data_humidity_ext_high[0]));
          }else if(i >= data_humidity_ext_high[1] && i <= data_humidity_ext_high[2]){
            dataLine.datasets[3].data.push(1);
          }else if(i > data_humidity_ext_high[2] && i < data_humidity_ext_high[3]){
            dataLine.datasets[3].data.push((data_humidity_ext_high[3]-i)/(data_humidity_ext_high[3]-data_humidity_ext_high[2]));
          }
        }

        var fuzzyChartHumid = new Chart(lineChartHumid, {
          type: 'line',
          data: dataLine,
          options: {
            title: {
              display: true,                
              text: 'Fuzzy Graph'
            },
            pieceLabel: {
              render: 'value'
            },
            legend: {
              display: false,
            },
            elements: {
              point:{
                radius: 0
              }
            },
              // scales: {
              //     xAxes: [{
              //         ticks: {
              //             autoSkip : true,
              //             callback: function(value, index, values) {
              //                 return value;
              //             }
              //         },
              //         gridLines : {
              //             display : false,
              //         }
              //     }]
              // },
          }
        })
    }