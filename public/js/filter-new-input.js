$(document).ready(function() {
    $('#new-inp').DataTable( {
      "pagingType": "full_numbers"
     });
});

 var filtersConfig = {
        base_path: 'tablefilter/',
        auto_filter: {
           delay: 110 //milliseconds
        },
       paging: {
           results_per_page: ['Records: ', [10, 25, 50, 100]]
        },
        filters_row_index: 1,
        state: true,
        alternate_rows: true,
        rows_counter: true,
        btn_reset: true,
        status_bar: true,
        msg_filter: 'Filtering...'
      };
var tf = new TableFilter('new-inp', filtersConfig);
tf.init();