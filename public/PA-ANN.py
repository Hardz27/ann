#Train model and make predictions
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import json
import numpy
import pandas
import database
import datetime
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
from keras.callbacks import ModelCheckpoint
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from keras.models import load_model

db = database.Database()

conf = db.fetchWhere(1)

conf = conf[0]

# fix random seed for reproducibility
seed = 3
numpy.random.seed(seed)

# load dataset
dataframe = pandas.read_csv("../storage/app/exports/dataset_v4.csv", header=None)
dataset = dataframe.values
X = dataset[:,0:3].astype(float)
Y = dataset[:,3]

# encode class values as integers
encoder = LabelEncoder()
encoder.fit(Y)
encoded_Y = encoder.transform(Y)

# convert integers to dummy variables (i.e. one hot encoded)
dummy_y = np_utils.to_categorical(encoded_Y)


################## Hanya digunakan pertamakali bae / mencari model baru untuk akurasi yang baru ####################

# create model
def baseline_model():
	model = Sequential()
	is_init = 0
	for node_layer, kernel_init, activation_node in zip (conf.node_layer_conf, conf.kernel_init_node_layer_conf, conf.activation_node_layer_conf):
		if is_init == 0:
			model.add(Dense(int(node_layer), input_dim=3, kernel_initializer=str(kernel_init.replace("'", "")), activation=str(activation_node.replace("'", ""))))
		else:
			model.add(Dense(int(node_layer), kernel_initializer=str(kernel_init.replace("'", "")), activation=str(activation_node.replace("'", ""))))
		is_init = 1
    #model.add(Dense(12, input_dim=3, kernel_initializer='uniform', activation='relu'))
    #model.add(Dense(9, kernel_initializer='uniform', activation='relu'))
    #model.add(Dense(3, kernel_initializer='uniform', activation = 'sigmoid'))
    
    # Compile model
    #model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
	model.compile(loss=conf.loss_compile_conf, optimizer=conf.optimizer_compile_conf, metrics=['accuracy'])
	return model

#################### Pelatihan Model
#X_train, X_test, Y_train, Y_test = train_test_split(X, dummy_y, test_size=0.1, random_state=7)
X_train, X_test, Y_train, Y_test = train_test_split(X, dummy_y, test_size=float(conf.test_size_train_conf), random_state=7)

#estimator = KerasClassifier(build_fn=baseline_model, epochs=1, batch_size=5)
if conf.is_train_conf == 0:

	estimator = KerasClassifier(build_fn=baseline_model, epochs=int(conf.epoch_size_train_conf), batch_size=int(conf.batch_size_train_conf))
	filepath="weights.best.hdf5"
	checkpoint = ModelCheckpoint(filepath, monitor='accuracy', verbose=1, save_best_only=True, mode='max')
	callbacks_list = [checkpoint]

	estimator.fit(X_train, Y_train, callbacks=callbacks_list, verbose = 1)
	estimator.model.load_weights("weights.best.hdf5")
	estimator.model.save("best_model_v4.h5")

	print("Saved model to disk")
else:
	estimator = KerasClassifier(build_fn=baseline_model, epochs=1, batch_size=5)
	estimator.fit(X_train, Y_train, verbose = 0)
	print("Hmmm.. It seems we have trained..")
	print("")
	print("Done :) We have Loaded last model!")
	print("")
	estimator.model.load_weights("weights.best.hdf5")
	# estimator.model = load_model(conf.model_name_conf)

#estimator.fit(X_train, Y_train, verbose = 1)
################## End pelatihan model  ##################

#estimator.model = load_model("best_model_v2.h5")

predictions = estimator.predict(X_test)

Y_data = []
for i in range (len(Y_test)):
    a = Y_test[i]
    for j in range (len(a)):
        if(a[j] == 1.0):
            Y_data.append(encoder.inverse_transform([[j]])[0]),

misclassified = numpy.where(Y_data != encoder.inverse_transform(predictions))
print('Total Missclassified/error data: ')
print(len(misclassified[0]))
print('started')

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
	print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
	client.subscribe("mqtt-test")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
	data = str(msg.payload.decode("utf-8"))
	data = data.split(';')
	deviceId = data[0]
	status = data[1]
	soilMoist = float(data[2])
	if(soilMoist >= 1000):
		soilMoist = 1000
	airTemp = float(data[3])
	airHumid = float(data[4])
	print('Soil Moist :'+ str(100-(soilMoist/10)) +', Air Temp :'+ str(airTemp) +', Air Humidity : '+ str(airHumid) +" | "+ datetime.datetime.today().strftime("%d-%m-%Y %H:%M:%S")) 
	predictions = estimator.predict([[[100.0-(soilMoist/10.0),airTemp,airHumid]]])
	print('Action : ' + encoder.inverse_transform(predictions))
	if predictions == 0:
		waterTime = 5
		pupukTime = 2
	elif predictions == 1:
		waterTime = 10
		pupukTime = 4
	elif predictions == 2:
		waterTime = 15
		pupukTime = 6
	if status == '1' :
		print('status : Verified')
		publish.single("inTopic", str(deviceId)+";1;"+str(waterTime)+";"+str(pupukTime), hostname="172.31.9.9")
	else :
		print("Status : Error!")
	print("")

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("172.31.9.9", 1883, 60)

client.loop_forever()


