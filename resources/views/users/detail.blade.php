<form action="/pic"  method="post" id="detailform">
           
    <div class="modal-body">

            <div class="">
                    <!-- Widget: user widget style 1 -->
                    <div class="card card-widget widget-user">
                      <!-- Add the bg color to the header using any of the bg-* classes -->
                    @foreach ($user->getRoleNames() as $role)
                        @if ($role == 'admin')
                        <div class="widget-user-header text-black"
                        style="background: url('../dist/img/photo4.png') center center;">
                              <h3 class="widget-user-username" style="text-align:center">{{$user->user_name}}</h3>
                        </div>
                        <div class="widget-user-image">
                                <img class="img-circle elevation-2" src="../dist/img/user1-128x128.jpg" alt="User Avatar">
                              </div>
                        @else
                        <div class="widget-user-header text-black"
                        style="background: url('../dist/img/boxed-bg.png') center center;">
                            <h3 class="widget-user-username" style="text-align:center">{{$user->user_name}}</h3>
                        </div>
                        <div class="widget-user-image">
                                <img class="img-circle elevation-2" src="../dist/img/user3-128x128.jpg" alt="User Avatar">
                              </div>
                        @endif
                      @endforeach
                     
                      <div class="card-footer">
                        <div class="row">
                          <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <span class="description-text">Email</span>
                                <h5 class="description-header">{{$user->user_email}}</h5>


                              
                              
                            </div>
                            <!-- /.description-block -->
                          </div>
                          <!-- /.col -->
                          <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <span class="description-text">Telpon</span>
                                <h5 class="description-header">{{$user->user_nohp}}</h5>
                              
                            </div>
                            <!-- /.description-block -->
                          </div>
                          <!-- /.col -->
                          
                      
                        @foreach ($user->getRoleNames() as $role)
                            @if ($role == 'admin')
                            <div class="col-sm-4">
                                <div class="description-block">
                                  <span class="description-text">Role</span><br/>
                                  <span class="badge badge-secondary">{{$role}}</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            @else
                            <div class="col-sm-4">
                                <div class="description-block">
                                    <span class="description-text">ROLE</span><br/>
                                      <span class="badge badge-success">{{$role}}</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                            @endif
                        @endforeach
                        


                          <!-- /.col -->
                        </div>
                        <!-- /.row -->
                      </div>
                    </div>
                    <!-- /.widget-user -->
                  </div>

           
      <div class="card">
    <div class="card-header">
      <h3 class="card-title">User Iputers</h3>
      
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    
    <div class="card-body p-0">
            
      <table class="table table-striped projects">
          <thead>
              <tr>
                  <th style="width: 2%">
                      No
                  </th>
                  <th style="width: 30%">
                      Data
                  </th>
                  <th style="width: 50%">
                      Value
                  </th>
                  
              </tr>
          </thead>
          <tbody>
                <tr>
                    <td>
                        #
                    </td>
                    <a>
                        <td>
                            New Input
                    </td>
                    </a>
                    
                    <td>
                        <ul class="list-inline">
                          <li class="list-inline-item">
                               <mark class="badge badge-danger">{{$inputernewInputCount}}</mark>
                          </li>                 
                        </ul>
                    </td>
                   
                </tr>

                
                <tr>
                    <td>
                        #
                    </td>
                    <a>
                        <td>
                            Unvalid Input
                    </td>
                    </a>
                    
                    <td>
                        <ul class="list-inline">
                          <li class="list-inline-item">
                            <mark class="badge badge-warning">{{$inputerunCompliteCount}}</mark>
                          </li>                 
                        </ul>
                    </td>
                   
                </tr>

               <tr>
                  <td>
                      #
                  </td>
                  <td>
                      <a>
                        Valid Input
                      </a>
                  </td>
                  <td>
                      <ul class="list-inline">
                        <li class="list-inline-item">
                            <mark class="badge badge-success">{{$inputercompliteCount}}</mark>
                        </li>                 
                      </ul>
                  </td>
              </tr>
              <tr>
                  <td>
                      #
                  </td>
                  <td>
                      <a>
                          Total Input
                      </a>
                  </td>
                  <td>
                      <ul class="list-inline">
                        <li class="list-inline-item">
                            <mark class="badge badge-info">{{$inputerallinputCount}}</mark>
                        </li>                 
                      </ul>
                  </td>
              </tr>
              <tr>
                    <td>
                        #
                    </td>
                    <td>
                        <a>
                            Salary
                        </a>
                    </td>
                    <td>
                        <ul class="list-inline">
                          <li class="list-inline-item">
                              @if ( $salary != null)
                                <mark class="badge badge-dark">{{formatRupiah($salary).',00'}}</mark>
                              @else
                                <mark class="badge badge-dark">0</mark>
                              @endif
                              
                          </li>                 
                        </ul>
                    </td>
                </tr>

          </tbody>
      </table>
    </div>
</div>
</div>