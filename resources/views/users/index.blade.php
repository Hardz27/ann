@extends('layouts.master')

@section('title')
    <title>Manajemen User</title>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Manajemen User</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">User</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        @card
                            @slot('title')
                            <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus mR-5"></i>Tambah Baru</a>
                            @endslot
                            
                            @if (session('success'))
                                @alert(['type' => 'success'])
                                    {!! session('success') !!}
                                @endalert
                            @endif
                            
                            <div class="table-responsive">
                                <table id="demo" class="table table-hover">
                                    <thead style="text-align: center;">
                                        <tr>
                                            <td style="width: 60px;">No</td>
                                            <td>Nama</td>
                                            <td>Email</td>
                                            <td>No HP</td>
                                            <td>Role</td>
                                            <td>Aksi</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @php $no = 1; @endphp

                                        @forelse ($users as $row)
                                        <tr>
                                            <td style="text-align: center;">{{ $no++ }}</td>
                                            <td>{{ $row->user_name }}</td>
                                            <td>{{ $row->user_email }}</td>
                                            <td>{{ $row->user_nohp }}</td>
                                            <!-- <td></td> -->

                                            <td style="text-align: center;">

                                                @foreach ($row->getRoleNames() as $role)
                                                @php
                                                    // dd($row->getRoleNames());
                                                @endphp
                                                    @if ($role == 'admin')
                                                        <label for="" class="badge badge-secondary">{{ $role }}</label>        
                                                    @else
                                                        <label for="" class="badge badge-success">{{ $role }}</label>        
                                                    @endif
                                                @endforeach

                                            </td>
                                            <td style="text-align: center;">
                                                 {{-- <form action="{{ route('users.destroy', $row->user_id) }}" method="POST">  --}}
                                                    
                                                    {{-- <input type="hidden" name="_method" value="DELETE"> --}}
                                                    <a href="{{ route('users.roles', $row->user_id) }}" class="btn btn-success btn-sm" style="padding: 4px 6px;"><i class="fa fa-toggle-on"></i></a>
                                                    <a href="{{ route('users.edit', $row->user_id) }}" class="btn btn-warning btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a>
                                                    <a href="{{route('users.detail', $row->user_id)}}" class="btn btn-info btn-sm btn-show-detail" style="padding: 4px 11px;"><i class="fa fa-info"></i></a>
                                                    @if ($row->user_id != auth()->user()->user_id)    
                                                        <a href="{{route('user.delete_form', $row->user_id)}}" class="btn btn-danger btn-sm btn-show-delete"><i class="fa fa-archive"></i></a>
                                                    @endif
                                                {{-- </form> --}}
                                            </td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="4" class="text-center">Tidak ada data</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            @slot('footer')

                            @endslot
                        @endcard
                    </div>
                </div>
            </div>
        </section>
    </div>

    {{-- @include('users.delete') --}}

  
@endsection



@section('js')


    
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/tablefilter/tablefilter.js') }}"></script>
    <script src="{{ asset('js/pop-up.js') }}"></script>
    <script src="{{ asset('js/validation.js') }}"></script>
@endsection