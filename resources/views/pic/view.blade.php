@extends('layouts.master')

@section('title')
    <title>Form Input</title>
@endsection

@section('css')
    <style type="text/css">
        .tab-pane{
            height:150px;
            overflow-y:scroll;
        }
    </style>
@endsection



@section('content')
            <!--Login modal-->
            @php
            function validation($n){
                 $i = 0; 
                 $count = $n;
                 while ($n > 0) 
                 { 
                     $binaryNum; 
                     $binaryNum[$i] = $n % 2; 
                     $n = (int)($n / 2); 
                     $i++; 
                 }
                 for ($j = $i - 1; $j >= 0; $j--){ 
                     $bin_val[] = $binaryNum[$j];
                 }
    
                 while(count($bin_val)< 8 ){
                    array_unshift($bin_val, 0);
               }
               
                 return $bin_val;
            }
            
            if($pic->company_validation != 0){
              $arr = validation($pic->company_validation);
            }
            else{
            $arr = array(0,0,0,0,0,0,0,0);
          }
            
         @endphp


    <div class="content-wrapper" >
        <div class="content" style="background: white;">
            <div class="header">
                <br>
                <h4 class="modal-title">
                    <br><br>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-1 extra-w3layouts"> </div>
                    <div class="col-md-10 extra-w3layouts" style="border: 1px dotted #C2C2C2;padding-right: 30px; margin-bottom: 100px;padding-bottom: 30px;">
                @if (session('error'))
                    @alert(['type' => 'danger'])
                        {!! session('error') !!}
                    @endalert
                @endif

                @if (session('success'))
                    @alert(['type' => 'success'])
                        {!! session('success') !!}
                    @endalert
                @endif
                        <!-- Nav tabs -->
                        <!-- Tab panes -->
                        <button onclick="goBack()" class="btn btn-primary" style="margin:20px;"><i class="fa fa-angle-double-left"></i>Go Back</button>
                        <div class="tab-content" style="padding-left: 25px;">
                            <div class="active" id="Login">

    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Personal Information Company</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
            @csrf
            
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%">
                          #
                      </th>
                      <th style="width: 30%">
                          Field
                      </th>
                      <th style="width: 50%">
                          Value
                      </th>
                      <th style="width: 18%" class="text-center">
                          Status
                      </th>
                  </tr>
              </thead>
              <tbody>
                  
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Personal Name
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$pic->pic_name}}
                            </li>                 
                          </ul>
                      </td>
                      {{-- <td class="project-state" style="text-align: center;">
                          <span class="badge badge-success">Success</span>
                      </td> --}}
                <td class="project-state" style="text-align: center;">
                    @if ($arr[0] == 1)
                        <span class="badge badge-success">Verified</span>
                    @elseif($arr[0] == 0)
                      <span class="badge badge-danger">Unverified</span>
                    @endif
                </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Position
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$pic->pic_position}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                      @if ($arr[1] == 1)
                        <span class="badge badge-success">Verified</span>
                    @elseif($arr[1] == 0)
                        <span class="badge badge-danger">Unverified</span>
                    @endif
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Divisi
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$pic->pic_division}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                        @if ($arr[2] == 1)
                          <span class="badge badge-success">Verified</span>
                      @elseif($arr[2] == 0)
                          <span class="badge badge-danger">Unverified</span>
                      @endif
                        </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              E-Mail
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$pic->pic_email}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-success">Success</span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Phone Number
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$pic->pic_nohp}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-success">Success</span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Social Media URL/LinkedIn
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$pic->pic_sosmed}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-success">Success</span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Status
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$pic->pic_status}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-success">Success</span>
                      </td>
                  </tr>
              
              </tbody>
          </table>          
        </div>
        </div>  
                            </div>
                            
                        </div>
                        <div id="OR"  ><img src="{{ asset('images/icon.png') }}" style="max-width: 100%;"></div>
                    </div>
                    <div class="col-md-1 extra-w3layouts"> </div>
                </div>
                
            </div>
        </div>
</div>



    @endsection

@section('js')


<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
