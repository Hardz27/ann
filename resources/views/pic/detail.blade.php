
        @php
        
        function validation($n){
             $i = 0; 
             $count = $n;
             while ($n > 0) 
             { 
                 $binaryNum; 
                 $binaryNum[$i] = $n % 2; 
                 $n = (int)($n / 2); 
                 $i++; 
             }
             for ($j = $i - 1; $j >= 0; $j--){ 
                 $bin_val[] = $binaryNum[$j];
             }


             while(count($bin_val)< 7){
                array_unshift($bin_val, 0);
           }
           
             return $bin_val;
        }
        
    //     if($pic->pic_validation != 0){
    //       $arr = validation($pic->pic_validation);
    //     }
    //     else{
    //     $arr = array(0,0,0,0,0,0,0);

    //   }

      if ($pic->pic_validation != 0) {
          $arr = validation($pic->pic_validation);
        //   dd($arr);
      } else {
        $arr = array(0,0,0,0,0,0,0);
      }
      
      
        
     @endphp


     @php

/**
* Create a Message from an email formatted string.
*
* @param  string $email Email formatted string.
* @return Google_Service_Gmail_Message Message containing email.
*/
function createMessage($email) {
 $message = new Google_Service_Gmail_Message();
 $email = strtr(base64_encode($email), array('+' => '-', '/' => '_'));
 $message->setRaw($email);
 return $message;
}

@endphp 
        
        <form action="/pic"  method="post" id="detailform">
           
        <div class="modal-body">
                <section class="content" >
                        <div class="container-fluid">
                          <h5 class="mb-2"></h5>
                          <div class="row">
                            <div class="col-md-6 col-sm-3 col-12">
                              <div class="info-box">
                         
                                <span class="info-box-icon bg-warning"><i class="fa fa-building-o"></i></span>
                    
                                <div class="info-box-content">
                                        
                                  <span class="info-box-text">Company Name</span>
                                  <span class="info-box-number">{{$pic->company['company_name']}}</span>
                                  
                        
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6 col-sm-3 col-12">
                              <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-phone"></i></span>
                    
                                <div class="info-box-content">
                                  <span class="info-box-text">Company Telpon</span>
                                  <span class="info-box-number">{{$pic->company['company_telpon_office']}}</span>
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                            </div>
                    
                          </div>
                          <!-- /.row -->
                        </div>
                    </section>
                
          <div class="card">
        <div class="card-header">
          <h3 class="card-title">PIC Information</h3>
          
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        
        <div class="card-body p-0">
                
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%">
                          No
                      </th>
                      <th style="width: 30%">
                          Data
                      </th>
                      <th style="width: 50%">
                          Isi
                      </th>
                      <th style="width: 18%" class="text-center">
                          Status
                      </th>
                  </tr>
              </thead>
              <tbody>

                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                        
                          <a>
                              PIC Name
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                    {{$pic->pic_name}}
                            </li>                 
                          </ul>
                      </td>

                    {{-- <td class="project-state" style="text-align: center;">
                        @if ($companies->company_validation == true)
                            <span class="badge badge-success">Verivied</span>
                        @else
                            <span class="badge badge-danger">Unverified</span>
                        @endif
                    </td> --}}

                    <td class="project-state" style="text-align: center;">
                        @if ($arr[0] == 1)
                            <span class="badge badge-success">Verivied</span>
                        @elseif($arr[0] == 0)
                            <span class="badge badge-danger">Unverified</span>
                        @endif
                    </td>

                    </tr>

                    
                    <tr>
                        <td>
                            #
                        </td>
                        <a>
                            <td>
                                PIC posotion
                        </td>
                        </a>
                        
                        <td>
                            <ul class="list-inline">
                              <li class="list-inline-item">
                                    {{$pic->pic_position}}
                              </li>                 
                            </ul>
                        </td>
  
                        <td class="project-state" style="text-align: center;">
                            @if ($arr[1] == 1)
                                <span class="badge badge-success">Verivied</span>
                            @elseif($arr[1] == 0)
                                <span class="badge badge-danger">Unverified</span>
                            @endif
                      
                        </td>
  
                    </tr>

                   <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              PIC Division
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$pic->pic_division}}
                            </li>                 
                          </ul>
                      </td>
                    
                        
                      <td class="project-state" style="text-align: center;">
                        @if ($arr[2] == 1)
                            <span class="badge badge-success">Verivied</span>
                        @elseif($arr[2] == 0)
                            <span class="badge badge-danger">Unverified</span>
                        @endif
                        
                    </td>
                      {{-- <td class="project-state" style="text-align: center;">
                            <span class="badge badge-warning">Success</span>
                      </td> --}}

                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              PIC Phone-number
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$pic->pic_nohp}}
                            </li>                 
                          </ul>
                      </td>

                      <td class="project-state" style="text-align: center;">
                        @if ($arr[3] == 1)
                            <span class="badge badge-success">Verivied</span>
                        @elseif($arr[3] == 0)
                            <span class="badge badge-danger">Unverified</span>
                        @endif

                    </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              PIC email
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$pic->pic_email}}
                            </li>                 
                          </ul>
                      </td>

                      <td class="project-state" style="text-align: center;">
                        @if ($arr[4] == 1)
                        <a class="btn btn-danger btn-sm" target="_blank" href="https://mail.google.com/mail/u/0/#inbox?compose=new?authuser=user@gmail.com" style="margin-top: 5px;">
                            <i class="fa fa-envelope" style="padding-right: 5px;"></i>
                        </a>
                        {{-- <a class="btn btn-primary btn-sm btn-show-detail" href="{{route('pic.email')}}" style="margin-top: 5px;">
                            <i class="fa fa-send" style="padding-right: 5px;"></i>
                        </a> --}}
                        
                            {{-- <span class="badge badge-success">Verivied</span> --}}
                        @elseif($arr[4] == 0)
                        {{-- <a class="btn btn-primary btn-sm " href="{{route('pic.email')}}" style="margin-top: 5px;">
                            <i class="fa fa-send" style="padding-right: 5px;"></i>
                        </a> --}}
                    <a class="btn btn-danger btn-sm" target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to={{$pic->pic_email}}&su=HRD-Erporate" style="margin-top: 5px;">
                        {{-- <a class="btn btn-primary btn-sm" target="_blank" href=https://mail.google.com/mail/?view=cm&fs=1&to=fahrurozi@gmail.com&su=SUBJECT&body=BODY&bcc=someone.else@example.com" style="margin-top: 5px;"> --}}
                            <i class="fa fa-envelope" style="padding-right: 5px;"></i>
                        </a>
                        
                            {{-- <span class="badge badge-danger">Unverified</span> --}}
                        @endif
                       
                    </td>

                  </tr>

                 

                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              PIC Media
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$pic->pic_sosmed}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                        @if ($arr[5] == 1)
                            <span class="badge badge-success">Verivied</span>
                        @elseif($arr[5] == 0)
                            <span class="badge badge-danger">Unverified</span>
                        @endif
                    </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Pic Status
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$pic->pic_status}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                        @if ($arr[6] == 1)
                            <span class="badge badge-success">Verivied</span>
                        @elseif($arr[6] == 0)
                            <span class="badge badge-danger">Unverified</span>
                        @endif
                        
                    </td>
              
                  </tr>
              </tbody>
          </table>
        </div>
    </div>
</div>
{{-- @php
                        dd("apa")   
                       @endphp
 --}}


        <!-- /.card-body -->

