
@php $data = 0; @endphp
@foreach($pics as $key => $item)
<div class="modal fade" id="delete@php echo $data;@endphp" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="text-align: center; background: #ec4141; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Are you sure want to delete this data?</h4>
        </div>
        <form action="#"  method="post">
        <div class="modal-body">

      <!-- ============================================================================= -->

      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Personal Information Data</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
            
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%">
                          #
                      </th>
                      <th style="width: 30%">
                          Field
                      </th>
                      <th style="width: 50%">
                          Value
                      </th>
                      <th style="width: 18%" class="text-center">
                          Status
                      </th>
                  </tr>
              </thead>
              <tbody>
                  
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Personal Name
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->pic_name}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-success">Success</span>
                      </td>
                  </tr>
              
              </tbody>
          </table>          
        </div>
        </div>  
        </div>  
        </form>
        <div class="modal-footer" style="text-align: center;">
          <form action="{{ route('pic.destroy', $item->pic_id) }}" method="POST">
              @csrf
              <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
              <button class="btn btn-danger">Send to Archive <i class="fa fa-archive"></i></button>
           </form>
        </div>
      </div>
      </div>
    </div>
  </div>
  @php $data++;@endphp
@endforeach           