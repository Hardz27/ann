@extends('layouts.master')

@section('title')
    <title>Manage PIC</title>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endsection

@section('content')

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Manajemen PIC</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">{{ $page }}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        @card
                            @slot('title')
                            <!-- <p id="time"></p> -->
                                @php
                                    date_default_timezone_set("Asia/Jakarta");
                                @endphp
                                <div class="row">
                                    <div class="col-sm-6">@php echo date("l d/m/Y ");@endphp</div>
                                    <div class="col-sm-6" style='text-align: right;' id="time"> @php echo date("h:i:sa")  @endphp</div>
                                </div>
                            @endslot
                            
                            @if (session('success'))
                                @alert(['type' => 'success'])
                                    {!! session('success') !!}
                                @endalert
                            @endif
                            
                            <div class="table-responsive">
                                <table id="demo" class="table table-hover">
                                    <thead style="text-align: center;">
                                        <tr>
                                            <td style="width: 60px;">No</td>
                                            <td>Nama</td>
                                            <td>Email</td>
                                            <td>No HP</td>
                                            <td>Social Media</td>
                                            <td>Aksi</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @php $no = 1; 
                                            // dd($pics);
                                        @endphp
                                        @forelse ($pics as $row)
                                        
                                        @if($row->pic_name)
                                        
                                        <tr>
                                            <td style="text-align: center; padding-left: 18px;">{{ $no++ }}</td>
                                            <td style="padding-left: 18px">{{ $row->pic_name }}</td>
                                            <td style="padding-left: 18px">{{ $row->pic_email }}</td>
                                            <td style="padding-left: 18px">{{ $row->pic_nohp }}</td>
                                            <!-- <td></td> -->

                                            <td style="text-align: center;">{{ $row->pic_sosmed }}</td>
                                            <td style="text-align: center;">
                                                
                                                    
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    {{-- <a class="btn btn-info btn-sm" href="#" data-toggle="modal" data-target="#myModal@php echo ($no);@endphp">
                                                        <i class="fa fa-info" style="padding-right: 5px;"></i>
                                                    </a> --}}
                                                    <a href="{{ route('pic.detail', $row->pic_id )}}" class="btn btn-info btn-sm btn-show-detail" style="padding: 4px 11px;"><i class="fa fa-info"></i></a>
                                                    <a href="{{ route('pic.edit',  $row->pic_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a>
                                                    <a href="{{ route('pic.delete_pic', $row->pic_id)}}" class="btn btn-danger btn-sm btn-show-delete"><i class="fa fa-archive"></i></a>
                                                    {{-- <i class="btn btn-danger btn-sm" href="#" data-toggle="modal" data-target="#delete@php echo ($no-2) @endphp"><i class="fa fa-archive"></i></i> --}}

                                                <!-- </form> -->
                                            </td>
                                        </tr>
                                        @endif
                                        @empty
                                        <tr>
                                            <td colspan="4" class="text-center">Tidak ada data</td>
                                        </tr>
                                        @endforelse
                                        
                                    </tbody>
                                    
                                </table>
                            </div>
                            @slot('footer')

                            @endslot
                        @endcard
                    </div>
                </div>
            </div>
        </section>
    </div>

    {{-- @include('pic.modal')

    @include('pic.delete') --}}
    

    
@endsection

@section('js')
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <!-- <script src="{{ asset('plugins/tablefilter/tablefilter.js') }}"></script> -->
    <script src="{{ asset('js/filter-demo.js') }}"></script>
    <script src="{{ asset('js/pop-up.js') }}"></script>
    <script src="{{ asset('js/validation.js') }}"></script>

    <script language='javascript'>
        function happycode(){
           $('#demo').find('.rdiv .row').append('<a href="/pic/export-excel" class="export btn btn-success btn-sm"></i>Export to excel<i class="fa fa-file-excel-o" style="padding-left: 10px;"></a>');
        }
    </script>
    <script>
        //call after page loaded
        window.onload=happycode; 
    </script>

@endsection