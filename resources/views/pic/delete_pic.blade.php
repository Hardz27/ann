<form action="/pic"  method="post">
    {{-- <div class="modal-body"> --}}

  <!-- ============================================================================= -->
  <section class="content" >
      <div class="container-fluid">
        <h5 class="mb-2"></h5>
        <div class="row">
          <div class="col-md-6 col-sm-3 col-12">
            <div class="info-box">
       
              <span class="info-box-icon bg-warning"><i class="fa fa-building-o"></i></span>
  
              <div class="info-box-content">
                      
                <span class="info-box-text">Company Name</span>
                <span class="info-box-number">{{$pic->company['company_name']}}</span>
                
      
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-6 col-sm-3 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-success"><i class="fa fa-phone"></i></span>
  
              <div class="info-box-content">
                <span class="info-box-text">Company Telpon</span>
                <span class="info-box-number">{{$pic->company['company_telpon_office']}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
  
        </div>
        <!-- /.row -->
      </div>
  </section>
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Company Data</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="card-body p-0">
        
      <table class="table table-striped projects">
          <thead>
              <tr>
                  <th style="width: 2%">
                      #
                  </th>
                  <th style="width: 30%">
                      Nama PIC
                  </th>
                  <th style="width: 50%">
                      PIC Phone-number
                  </th>
                  <th style="width: 18%" class="text-center">
                      Dibuat Pada
                  </th>
              </tr>
          </thead>
          <tbody>
              
              <tr>
                  <td>
                      #
                  </td>
                  <td>
                      <a>
                        {{$pic->pic_name}}
                      </a>
                  </td>
                  <td>
                      <ul class="list-inline">
                        <li class="list-inline-item">
                              {{$pic->pic_nohp}}
                        </li>                 
                      </ul>
                  </td>
                  <td class="project-state" style="text-align: center;">
                    <label for="" class="badge badge-secondary">{{$pic->created_at}}</label>    
                  </td>
              </tr>
          
          </tbody>
      </table>          
    </div>
    </div> 

    {{-- </div>   --}}
    </form>

    <div class="modal-footer" style="text-align: center;">
      <form action="{{ route('pic.destroy', $pic->pic_id) }}" method="POST">
          @csrf
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          <button class="btn btn-danger">Send to archive <i class="fa fa-archive"></i></button>
       </form>
    </div>
