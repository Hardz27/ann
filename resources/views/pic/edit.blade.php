@extends('layouts.master')

@section('title')
    <title>Form Input</title>
@endsection

@section('css')
    <style type="text/css">
        .tab-pane{
            height:150px;
            overflow-y:scroll;
        }
    </style>
@endsection



@section('content')


            <!--Login modal-->

{{-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog" 
    aria-hidden="true" style="z-index: 9999;"> --}}
    <div class="content-wrapper" >
        <div class="content" style="background: white;">
                <section class="content" style="padding-left:8%;">
                        <div class="container-fluid">
                          <h5 class="mb-2"></h5>
                          <div class="row">
                            <div class="col-md-5.5 col-sm-6 col-12">
                              <div class="info-box">
                         
                                <span class="info-box-icon bg-info"><i class="fa fa-building-o"></i></span>
                  
                                <div class="info-box-content">
                                        
                                  <span class="info-box-text">Company Name</span>
                                  <span class="info-box-number">{{$pic->company['company_name']}}</span>
                                  
                        
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-5 col-sm-3 col-12">
                              <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-phone"></i></span>
                  
                                <div class="info-box-content">
                                  <span class="info-box-text">Company Telpon</span>
                                  <span class="info-box-number">{{$pic->company['company_telpon_office']}}</span>
                                </div>
                                <!-- /.info-box-content -->
                              </div>
                              <!-- /.info-box -->
                            </div>
                
                          </div>
                          <!-- /.row -->
                        </div>
                </section>

                          
          
            <div class="body">
                
                <div class="row">

                    <div class="col-md-1 extra-w3layouts"> </div>
                    <div class="col-md-10 extra-w3layouts" style="border: 1px dotted #C2C2C2;padding-right: 30px; margin-bottom: 100px;padding-bottom: 30px;">
                @if (session('error'))
                    @alert(['type' => 'danger'])
                        {!! session('error') !!}
                    @endalert
                @endif

                @if (session('success'))
                    @alert(['type' => 'success'])
                        {!! session('success') !!}
                    @endalert
                @endif
                        <!-- Nav tabs -->
                        <!-- Tab panes -->

                        




                        <div class="tab-content" style="padding-left: 15px;">
                            <div class="active" id="Login">

                        
                                
                                <!-- End of contact -->
                            <table id="person" class="table table-hover">

                                <ul class="nav nav-tabs" style="padding-bottom: 0px;margin-bottom: 20px; margin-top: 40px">
                                    <p id="header-name">Personal Information Company</p>
                                    <button onclick="goBack()" class="btn btn-primary" id="goback"><i class="fa fa-angle-double-left"></i>Go Back</button>
                                </ul>
                                <form action="{{route('pic.update', $pic->pic_id)}}"  class="form-horizontal" method="post">
                                @csrf

                                <div class="panel panel-default" style="margin-top: 10px;">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse">Personal data information 1</a>
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse" style="float: right; font-size: 20px;" aria-expanded="true" class="">-</a>
                                        </h4>
                                    </div>
                                    <div id="collapse" class="panel-collapse collapse show">
                                        <div class="panel-body" style="padding: 10px;">
                                            <div id="pic_form" class="row">
                                                <div class="col-sm-6">
                                                    Personal name
                                                </div>
                                                <div class="col-sm-6">
                                                    Position
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control {{$errors->has('pic_name') ? 'is-invalid': ''}}" value="{{$pic->pic_name}}" name="pic_name" placeholder="Personal name" required/>
                                                    <p class="text-danger">{{$errors->first('pic_name')}}</p>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control {{$errors->has('pic_position') ? 'is-invalid': ''}}" name="pic_position" value="{{$pic->pic_position}}" placeholder="Postion" required/>
                                                    <p class="text-danger">{{$errors->first('pic_position')}}</p>
                                                </div>
                                            </div>
                                            <div id="pic_form" class="row">
                                                <div class="col-sm-6">
                                                    Divisi
                                                </div>
                                                <div class="col-sm-6">
                                                    Email
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input type="text" value="{{$pic->pic_division}}" class="form-control {{$errors->has('pic_division') ? 'is-invalid': ''}}" name="pic_division" placeholder="Division">
                                                    <p class="text-danger">{{$errors->first('pic_division')}}</p>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" value="{{$pic->pic_email}}" class="form-control {{$errors->has('pic_email') ? 'is-invalid': ''}}" name="pic_email" placeholder="Email Address">
                                                    <p class='text-danger'>{{$errors->first('pic_email')}}</p>
                                                </div>
                                            </div>
                                            <div id="pic_form" class="row">
                                                <div class="col-sm-6">
                                                    Phone number
                                                </div>
                                                <div class="col-sm-6">
                                                    Social media URL/LinkedIn
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input type="text" value="{{$pic->pic_nohp}}" class="form-control {{$errors->has('pic_nohp') ? 'is-invalid': ''}}" name="pic_nohp" placeholder="Phone number">
                                                    <p class='text-danger'>{{$errors->first('pic_nohp')}}</p>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" value="{{$pic->pic_sosmed}}" class="form-control {{$errors->has('pic_sosmed') ? 'is-invalid': ''}}" name="pic_sosmed" placeholder="Social Media URL">
                                                </div>
                                            </div>                                            
                                                <p id="kolom">
                                                    Status
                                                    <select name="pic_status" class="form-control {{$errors->has('pic_status') ? 'is-invalid': ''}}">
                                                        <option value="{{$pic->pic_status}}">{{$pic->pic_status}}</option>  
                                                        <option value="aktif">Aktif</option>
                                                        <option value="nonaktif">Nonaktif</option>                        
                                                    </select>
                                                <p class="text-danger">{{$errors->first('pic_status')}}</p>
                                                </p>
                                        </div>
                                    </div>
                                

                                

                                <!-- <div class="row">
                                    
                                </div> -->

                             </table>
                            <div style="float: right;">
                            <br>
                            <button type="submit" class="btn btn-success btn-md" style="background: #28a745;">Submit</button>
                            <button type="reset" class="btn btn-danger btn-md" value="Reset">Reset</button>
                            
                        </div>
                                </form>
                            </div>
                            
                        </div>
                        <div id="OR"  ><img src="{{ asset('images/icon.png') }}" style="max-width: 100%;"></div>
                    </div>
                    <div class="col-md-1 extra-w3layouts"> </div>
                </div>
                
            </div>
        </div>
    {{-- </div> --}}



    @endsection

@section('js')


<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
