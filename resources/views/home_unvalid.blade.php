
@extends('layouts.app')
@section('css2')
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endsection
@section('content2')



<form action="/company"  method="post" id="detailform">
<div class="modal-body">
  <div class="card">
<div class="card-header">
  <h3 class="card-title">Company Information</h3>

  <div class="card-tools">
    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
      <i class="fa fa-minus"></i></button>
  </div>
</div>

<div class="card-body p-0">
  <table class="table table-striped projects">
      <thead>
          <tr>
              <th style="width: 2%">
                  No
              </th>
              <th style="width: 30%">
                  Company
              </th>
              <th style="width: 50%">
                  Domain
              </th>
              <th style="width: 18%" class="text-center">
                  Status
              </th>
          </tr>
      </thead>
      <tbody>
        @php
            $no = 1;
        @endphp
        @if(auth()->user()->can('leads inputer'))
        @forelse ($inputerUnComplite as $item)
        @if ($item->created_by == auth()->user()->user_name)
    @php
        $company = DB::table('t_companies')->where('t_companies.company_id', $item->company_id)
                  ->where('t_companies.company_validation', '<', 24576)->count();
        $domain = DB::table('t_company_domains')->where('t_company_domains.company_id', $item->company_id)
                   ->where('t_company_domains.domain_validation', '=', null)->count();
        $notes = DB::table('inputers')->select('inputer_note_company')->where('company_id', $item->company_id)
                ->where('inputer_note_company', '!=', null)->first();
        $email = DB::table('t_company_emails')->where('t_company_emails.company_id', $item->company_id)
              ->where('t_company_emails.email_validation', '=', null)->count();   

    @endphp
        
        @if (!$item->deleted_at)
            @if($item->company_validation == 0 && ($item->domain_validation != 1 && $item->email_validation != 1))
            
        @else
          <tr>
              <td>
                  {{$no++}}
              </td>
              <td>
                
                  <a>
                    {{$item->company_name}}
                  </a>
              </td>
              <td>
                  <ul class="list-inline">
                    <li class="list-inline-item">
                            {{$item->domain_name}}
                    </li>                 
                  </ul>
              </td>
            <td class="project-state" style="text-align: center;">
                
                    {{-- <span class="badge badge-warning">Unvalid</span> --}}

                    @php                                                        
                                                
                    if($notes){

                        echo "<label for='' class='badge badge-danger'>*</label> ";
                    }
                    else{
                     
                    }
                    
                    if($item->company_validation >= 16384 && $item->company_validation < 24576){
                        echo '<label for="" class="badge badge-warning">telpon</label> ';
                    }
                    else if($item->company_validation < 16384 && $item->company_validation >= 8192){
                        echo '<label for="" class="badge badge-warning">name</label> ';
                    }
                    else if($item->company_validation < 8192){
                        echo '<label for="" class="badge badge-warning">name</label> ';
                        echo '<label for="" class="badge badge-warning">telpon</label> ';
                    }
                      
                    if ($domain > 0 ) {
                           echo "<label for='' class='badge badge-warning'>domain</label> ";
                    } else {
                          
                    }
            
                     if($email > 0 ){
                        echo "<label for='' class='badge badge-warning'>email</label> ";
                     }

                     @endphp
               
            </td>

            </tr>
            
            @endif
                @endif
            @endif
            
            @empty
            <tr>
                <td colspan="4" class="text-center">Tidak ada data</td>
            </tr> 
          
            
            @endforelse
           
            
            @endif
            
      </tbody>
  </table>
  
</div>

</div>

<div class="card-tools unvalid">
        <ul class="pagination-unvalid pagination-sm m-0 float-right">            
                {{$inputerUnComplite->links()}}
        </ul>
</div>



</div>
@endsection

<!-- /.card-body -->

