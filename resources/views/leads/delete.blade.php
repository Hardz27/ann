@php $data = 0; @endphp
@foreach($allInput as $key => $item)
<div class="modal fade" id="delete@php echo $data;@endphp" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="text-align: center; background: #FFC107; color: white;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Are you sure want to delete this data?</h4>
        </div>
        <form action="#"  method="post">
        <div class="modal-body">

      <!-- ============================================================================= -->

      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Company Data</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
            
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%">
                          #
                      </th>
                      <th style="width: 30%">
                          Nama Perusahaan
                      </th>
                      <th style="width: 50%">
                          Dibuat Pada
                      </th>
                      <th style="width: 18%" class="text-center">
                          Dibuat Oleh
                      </th>
                  </tr>
              </thead>
              <tbody>
                  
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                            {{$item->company_name}}
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->created_at}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                        
                        @foreach ($role as $row)
                                                    
                            @if ($row->name == 'admin')
                                <label for="" class="badge badge-danger">{{$item->created_by}}</label>    
                            @else
                                <label for="" class="badge badge-success">{{$item->created_by}}</label>
                            @endif
                            @break
                            
                        @endforeach
                        
                      </td>
                  </tr>
              
              </tbody>
          </table>          
        </div>
        </div>  
        </div>  
        </form>
        <div class="modal-footer" style="text-align: center;">
          <form action="{{ route('lead.delete', $item->company_id) }}" method="POST">
              @csrf
              <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
              <button class="btn btn-danger">Yes delete it!<i class="fa fa-trash"></i></button>
           </form>
        </div>
      </div>
      </div>
    </div>
  </div>
  @php $data++;@endphp
@endforeach           