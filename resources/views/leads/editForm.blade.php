@extends('layouts.master')

@section('title')
    <title>Form Edit</title>
@endsection

@section('css')
    <style type="text/css">
        .tab-pane{
            height:150px;
            overflow-y:scroll;
        }
    </style>
@endsection



@section('content')
            <!--Login modal-->

{{-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog" 
    aria-hidden="true" style="z-index: 9999;"> --}}
    <div class="content-wrapper" >
        <div class="content" style="background: white;">

            <div class="header">
                <br>
                <h4 class="modal-title"> 
                    <br><br>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-1 extra-w3layouts"> </div>
                    <div class="col-md-10 extra-w3layouts" style="border: 1px dotted #C2C2C2;padding-right: 30px; margin-bottom: 100px;padding-bottom: 30px;">
                @if (session('error'))
                    @alert(['type' => 'danger'])
                        {!! session('error') !!}
                    @endalert
                @endif

                @if (session('success'))
                    @alert(['type' => 'success'])
                        {!! session('success') !!}
                    @endalert
                @endif
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" style="padding: 10px 20px;margin-top: 40px;">
                            <p id="header-name">Company Information</p>
                            <button onclick="goBack()" class="btn btn-primary" id="goback"><i class="fa fa-angle-double-left"></i>Go Back</button>
                           
                           
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content" style="padding-left: 15px;">
                            <div class="active" id="Login" style="height: 600px;">

                        <form action="{{route('lead.update', $company->company_id)}}"  class="form-horizontal" method="post">
                                @csrf
                                
                                
                                <div class="panel-group" id="accordion">
                                <table id="company" class="table table-hover">
                                
                                <div class="form-group colinp" style="margin-bottom: 0px;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p id="kolom">Company names</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p id="kolom">Company Address</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group colinp" style="margin-bottom: -10px;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" name="company_name" value="{{old('company_name', $company->company_name )}}" class="form-control {{ $errors->has('company_name') ? 'is-invalid': '' }}" id="company_name" placeholder="Company name"  />
                                            <p class="text-danger">{{ $errors->first('company_name') }}</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" value="{{old('company_address', $company->company_address)}}" class="form-control {{$errors->has('company_address') ? 'is-invalid': '' }}" name="company_address" placeholder="Company Address"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group colinp" style="margin-bottom: 0px;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p id="kolom">Company Business</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p id="kolom">General Information/Description</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group colinp" style="margin-bottom: -10px;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="company_field" type="text" value="{{old('company_field',$company->company_field)}}" class="form-control {{$errors->first('company_field') ? 'is-invalid': ''}}" placeholder="Company Business" />
                                            <p class="text-danger">{{$errors->first('company_field')}}</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" value="{{old('company_desc',$company->company_desc)}}" class="form-control {{$errors->has('company_desc') ? 'is-invalid':  '' }}" name="company_desc" placeholder="Company Short Description"  />
                                            <p class="{{$errors->first('company_desc')}}"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group colinp" style="margin-bottom: 0px;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p id="kolom">Office Number</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p id="kolom">Faximile</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group colinp" style="margin-bottom: -10px;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" name="company_telpon_office" value="{{old('company_telpon_office', $company->company_telpon_office)}}" class="form-control {{$errors->has('company_telpon_office') ? 'is-invalid': ''}}" placeholder="Company Office Number"  />
                                            <p class="text-danger">{{$errors->first('company_telpon_office')}}</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" name="company_faximile" value="{{old('company_faximile',$company->company_faximile)}}" class="form-control {{$errors->has('company_faximile') ? 'is-invalid': '' }}" placeholder="Company Faximile" />
                                            <p class='text-danger'>{{$errors->first('company_faximile')}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group colinp" style="margin-bottom: 0px;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p id="kolom">Company Type</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p id="kolom">Company Status</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group colinp" style="margin-bottom: -10px;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                        <select name="company_type" value="{{old('company_type')}}" class="form-control {{$errors->has('company_type') ? 'is-invalid': ''}}" >
                                                <option>{{old('company_type', $company->company_type)}}</option>
                                                <option value="tekno">Tekno</option>
                                                <option value="nontekno">Nontekno</option>      
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <select name="company_status" value="{{old('company_status')}}" class="form-control {{$errors->has('company_status') ? 'is-invalid': ''}}">
                                                  <option>{{old('company_status', $company->company_status)}}</option>
                                                    <option value="aktif">Aktif</option>      
                                                    <option value="nonaktif">Nonaktif</option>      
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </table>
                            <table id="sub_domain" class="table table-hover">

                                <ul class="nav nav-tabs" style="margin-top: 50px;"">
                                    <p id="header-name">Company Contact</p>
                                </ul><div class="form-group" style="margin-bottom: 0px;">
                                <tr>
                                    <td style="width: 30%;"><p id="kolom">Domain/Web Company</td>
                                    <td style="width: 60%;">
                                        <div class="col-sm-10">
                                            @foreach ($company->domain as $key => $domain)
                                            
                                            
                                                <input type="text" style="width: 110%;" value="{{old('domain_name',$domain['domain_name'])}}" class="form-control {{$errors->has('domain_name') ? 'is-invalid': ''}}" name="domain_name[]" placeholder="Company Domain"/>
                                            
                                            
                                                
                                            @endforeach
                                        <p class="text-danger">{{$errors->first('domain_name')}}</p>
                                        </div>
                                    </td>
                                    <td style="width: 10%;">
                                        <input type="button" class="btn btn-primary" id="addDomain" value="+" />
                                        <a class="delete domainBtnDel" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
                                    </td>
                                    <td style="width: 10%;">
                                        <a class="delete domainBtnDel" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
                                    </td>
                                </tr>
                                </div>
                                

                            </table>

                            <table id="sub_email" class="table table-hover">

                                <tr>
                                    <td style="width: 30%;"><p id="kolom">E-Mail Companys</td>
                                    <td style="width: 60%;">
                                        <div class="col-sm-10">
                                            @foreach ($company->email as $key => $email)
                                            
                                            
                                                <input type="email" style="width: 110%;" value="{{old('email_name',$email['email_name'])}}" class="form-control {{$errors->has('email_name')}}" name="email_name[]" placeholder="Your Company E-Mail" />        
                                            
                                            
                                            @endforeach
                                        
                                        <p class="text-danger">{{$errors->first('email_name')}}</p>
                                        </div>
                                    </td>
                                    <td style="width: 10%;">
                                        <input type="button" class="btn btn-primary" id="addEmail" value="+" />
                                        <input type="button" class="btn btn-primary" id="addEmail" value="+" />
                                        <a class="delete emailBtnDel" title="Delete" data-toggle="tooltip">btn<i class="material-icons">&#xE872;</i></a>
                                    </td>
                                    <td style="width: 10%;">
                                        
                                    </td>
                                
                                </tr>
                                </div>

                            </table>

                                <!-- End of contact -->
                            <table id="person" class="table table-hover">

                                <ul class="nav nav-tabs" style="padding-bottom: 0px;margin-bottom: 0px;">
                                    <p id="header-name">Personal Information Company</p>
                                </ul>

                                <div class="panel panel-default" style="margin-top: 10px;">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse">Personal data information 1</a>
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse" style="float: right; font-size: 20px;" aria-expanded="true" class="">-</a>
                                        </h4>
                                    </div>
                                    <div id="collapse" class="panel-collapse collapse show">
                                        <div class="panel-body" style="padding: 10px;">
                                            <div id="pic_form" class="row">
                                                <div class="col-sm-6">
                                                    Personal name
                                                </div>
                                                <div class="col-sm-6">
                                                    Position
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    
                                                    
                                                    
                                                    <input type="text" class="form-control {{$errors->has('pic_name') ? 'is-invalid': ''}}" value="{{old('pic_name', $company->pic_name)}}" name="pic_name" placeholder="Personal name" />
                                                    <p class="text-danger">{{$errors->first('pic_name')}}</p>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control {{$errors->has('pic_position') ? 'is-invalid': ''}}" name="pic_position" value="{{old('pic_position', $company->pic_position)}}" placeholder="Postion" />
                                                    <p class="text-danger">{{$errors->first('pic_position')}}</p>
                                                </div>
                                            </div>
                                            <div id="pic_form" class="row">
                                                <div class="col-sm-6">
                                                    Divisi
                                                </div>
                                                <div class="col-sm-6">
                                                    Email
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input type="text" value="{{old('pic_division', $company->pic_division)}}" class="form-control {{$errors->has('pic_division') ? 'is-invalid': ''}}" name="pic_division" placeholder="Division">
                                                    <p class="text-danger">{{$errors->first('pic_division')}}</p>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" value="{{old('pic_email', $company->pic_email)}}" class="form-control {{$errors->has('pic_email') ? 'is-invalid': ''}}" name="pic_email" placeholder="Email Address">
                                                    <p class='text-danger'>{{$errors->first('pic_email')}}</p>
                                                </div>
                                            </div>
                                            <div id="pic_form" class="row">
                                                <div class="col-sm-6">
                                                    Phone number
                                                </div>
                                                <div class="col-sm-6">
                                                    Social media URL/LinkedIn
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input type="text" value="{{old('pic_nohp', $company->pic_nohp)}}" class="form-control {{$errors->has('pic_nohp') ? 'is-invalid': ''}}" name="pic_nohp" placeholder="Phone number">
                                                    <p class='text-danger'>{{$errors->first('pic_nohp')}}</p>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" value="{{old('pic_sosmed', $company->pic_sosmed)}}" class="form-control {{$errors->has('pic_sosmed') ? 'is-invalid': ''}}" name="pic_sosmed" placeholder="Social Media URL">
                                                </div>
                                            </div>                                            
                                                <p id="kolom">
                                                    Status
                                                    <select name="pic_status" value="{{old('pic_status', $company->pic_status)}}" class="form-control {{$errors->has('pic_status') ? 'is-invalid': ''}}">
                                                        <option>{{old('pic_status', $company->pic_status)}}</option>  
                                                        <option value="aktif">Aktif</option>
                                                        <option value="nonaktif">Nonaktif</option>                        
                                                    </select>
                                                <p class="text-danger">{{$errors->first('pic_status')}}</p>
                                                </p>
                                        </div>
                                    </div>
                                

                                

                                <!-- <div class="row">
                                    
                                </div> -->

                             </table>
                                <div>
                                <button type="button" class="btn btn-primary add-new"><i class="fa fa-plus"></i> Add Personal</button>

                                </div>
                            </div>
                            <div style="float: right;">
                            <br>
                            <button type="submit" class="btn btn-success btn-md" style="background: #28a745;">Submit</button>
                            <button type="reset" class="btn btn-danger btn-md" value="Reset">Reset</button>
                            
                        </div>
                                </form>
                            </div>
                            
                        </div>
                        <div id="OR"  ><img src="{{ asset('images/icon.png') }}" style="max-width: 100%;"></div>
                    </div>
                    <div class="col-md-1 extra-w3layouts"> </div>
                </div>
                
            </div>
        </div>
    {{-- </div> --}}



    @endsection

@section('js')


<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
