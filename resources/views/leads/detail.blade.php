@extends('layouts.app')
        @section('css2')
            <link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
        @endsection
 @section('content2')

     @php
        function validation($n){
             $i = 0; 
             $count = $n;
             while ($n > 0) 
             { 
                 $binaryNum; 
                 $binaryNum[$i] = $n % 2; 
                 $n = (int)($n / 2); 
                 $i++; 
             }
             for ($j = $i - 1; $j >= 0; $j--){ 
                 $bin_val[] = $binaryNum[$j];
             }

             if($count >= 128){
                while(count($bin_val)< 15 ){
                  array_unshift($bin_val, 0);
                }
             }
             else{
               while(count($bin_val)< 7 ){
                  array_unshift($bin_val, 0);
                }
              }
           
             return $bin_val;
        }

        function validation_note($n){
             $i = 0; 
             $count = $n;
             while ($n > 0) 
             { 
                 $binaryNum; 
                 $binaryNum[$i] = $n % 2; 
                 $n = (int)($n / 2); 
                 $i++; 
             }
             for ($j = $i - 1; $j >= 0; $j--){ 
                 $bin_val[] = $binaryNum[$j];
             }

             while(count($bin_val)< 15 ){
                array_unshift($bin_val, 0);
           }
           
             return $bin_val;
        }        
        
        if($companies->company_validation != 0){
          $arr = validation($companies->company_validation);
        }
        else{
        $arr = array(0,0,0,0,0,0,0,0);
      }
      
      if($pic->pic_validation != 0){
          $ay = validation($pic->pic_validation);
        }
        else{
        $ay = array(0,0,0,0,0,0,0);
      }

// Bagian explode note

      if($val != 0){
          $note_val = validation_note($val);
          $dat_avai = 1;
      
          for($ind = 0; $ind < 15; $ind++){
            if($note_val[$ind] == 0){
              $notes[$ind] = "";
            }
            else{
             $notes[$ind] = "$data[$dat_avai]";
             $dat_avai++; 
            }
          }
      }
        else{
        $note_val = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        $notes = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
      }

      // Yang dipake itu note_val dan notes
      //dd($note_val, $notes);

        
     @endphp
                    <form action="/detail"  method="post">
                            {{-- @csrf    
                            {{ csrf_field() }} --}}

                            {{-- <div class="modal-body"> --}}
                              <div class="card">

                            <a class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse" id="val_head">
                            <div class="card-header">
{{-- COMPANY VALIDATION --}}                         
                              <h3 class="card-title">Company Information</h3>
                    
                              <div class="card-tools">
                                <!-- <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"> -->
                                  <i class="fa fa-minus"></i>
                                <!-- </button> -->
                              </div>
                            </div></a>
                            <div class="card-body p-0">
                              <table class="table table-striped projects">
                                  <thead>
                                      <tr>
                                          <th style="width: 2%">
                                              
                                          </th>
                                          <th style="width: 30%">
                                              Field
                                          </th>
                                          <th style="width: 50%">
                                              Value
                                          </th>
                                          <th style="width: 18%" class="text-center">
                                              Status
                                          </th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      
                                      <tr>
                                          <td>
                                              #`1
                                          </td>
                                         <td>
                                            <a>
                                              Company Name<span id="req">(&nbsp;<span id="star">*</span>&nbsp;)</span>
                                            </a>
                                          </td>
                                          <td class="here">
                                                <a>
                                                   {{$companies->company_name}}
                                                </a>
                                                <br>
                                                <small>
                                                  @if($note_val[0] == 1)
                                                    {{$notes[0]}}
                                                  @endif
                                                    <!-- Created 01.01.2019 -->
                                                </small>
                                          </td>
                                          <td class="project-state" style="text-align: center;">
                                              <span class="button-checkbox">
                                                    @if ($arr[0] == 1)
                                                        <button type="button" class="coba btn" data-color="success" disabled>Validated</button>
                                                        <input id="company_valid" type="checkbox" name="valid_company_name" value="16384" class="hidden data" style="display: none;" checked="" />
                                                    @elseif($arr[0] == 0)
                                                        <button type="button" class="btn" data-color="danger" disabled>Unvalidate</button>
                                                        <input id="company_valid" type="checkbox" name="valid_company_name" value="16384" class="hidden data" style="display: none;" />
                                                    @endif
                                                        
                                              </span>
                                          </td>

                                        </tr>
                                        <tr>
                                          <td>
                                              #
                                          </td>
                                          <td>
                                              <a>
                                                  Office Number<span id="req">(&nbsp;<span id="star">*</span>&nbsp;)</span>
                                              </a>
                                          </td>
                                          <td class="here">
                                            <a>
                                                {{$companies->company_telpon_office}}
                                            </a>
                                            <br>
                                            <small>
                                              @if($note_val[1] == 1)
                                                {{$notes[1]}}
                                              @endif
                                            </small>
                                          </td>
                                          <td class="project-state" style="text-align: center;">
                                            <span class="button-checkbox">
                                                @if ($arr[1] == 1)
                                                <button type="button"  id="valid" class="coba btn" data-color="success" disabled>Validated</button>
                                                <input type="checkbox" name="valid_company_telpon_office" value="8192" class="hidden data" style="display: none;" checked="" />
                                                @elseif ($arr[1] == 0)
                                                <button type="button" class="btn" data-color="success" disabled>Unvalidate</button>
                                                <input type="checkbox" name="valid_company_telpon_office" value="8192" class="hidden data" style="display: none;" />
                                                @endif
                                                
                                              </span>
                                          </td>
                                      </tr>
                                          <td>
                                              #
                                          </td>
                                          <td>
                                              <a>
                                                  Company type
                                              </a>
                                          </td>
                                          <td  class="here">
                                            <a>
                                              {{$companies->company_type}}
                                            </a>
                                              <br>
                                              <small>
                                                @if($note_val[2] == 1)
                                                  {{$notes[2]}}
                                                @endif
                                              </small>
                                          </td>
                                          <td class="project-state" style="text-align: center;">
                                              <span class="button-checkbox">
                                                @if ($arr[2] == 1)
                                                <button type="button" id="valid" class="coba btn" data-color="success" disabled>Validated</button>
                                                <input id="company_valid" type="checkbox" name="valid_company_type" value="4096" class="hidden data" style="display: none;" checked="" />
                                                @elseif ($arr[2] == 0)
                                                <button type="button" class="btn" data-color="success" disabled>Unvalidate</button>
                                                <input type="checkbox" name="valid_company_type" value="4096" class="hidden data" style="display: none;" />
                                                @endif
                                                
                                              </span>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              #
                                          </td>
                                          <td>
                                              <a>
                                                  Company Business
                                              </a>
                                          </td>
                                          <td class="here">
                                              <a>
                                                      {{$companies->company_field}}
                                                <br>                 
                                              <small>
                                                @if($note_val[3] == 1)
                                                  {{$notes[3]}}
                                                @endif
                                              </small>
                                          </td>
                                          <td class="project-state" style="text-align: center;">
                                              <span class="button-checkbox">
                                                @if ($arr[3] == 1)
                                                <button type="button"  id="valid" class="coba btn" data-color="success" disabled>Validated</button>
                                                <input type="checkbox" name="valid_company_field" value="2048" class="hidden data" style="display: none;" checked="" />
                                                @elseif ($arr[3] == 0)
                                                <button type="button" class="btn" data-color="success" disabled>Unvalidate</button>
                                                <input type="checkbox" name="valid_company_field" value="2048" class="hidden data" style="display: none;" />
                                                @endif
                                                
                                              </span>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              #
                                          </td>
                                          <td>
                                              <a>
                                                  General Information/Description
                                              </a>
                                          </td>
                                          <td class="here">
                                              <a>
                                                      {{$companies->company_desc}}
                                                <br>                 
                                              <small>
                                                @if($note_val[4] == 1)
                                                  {{$notes[4]}}
                                                @endif
                                              </small>
                                          </td>
                                          <td class="project-state" style="text-align: center;">
                                              <span class="button-checkbox">
                                                @if ($arr[4] == 1)
                                                <button type="button"  id="valid" class="coba btn" data-color="success" disabled>Validated</button>
                                                <input type="checkbox" name="valid_company_desc" value="1024" class="hidden data" style="display: none;" checked="" />
                                                @elseif ($arr[4] == 0)
                                                <button type="button" class="btn" data-color="success" disabled>Unvalidate</button>
                                                <input type="checkbox" name="valid_company_desc" value="1024" class="hidden data" style="display: none;" />
                                                @endif
                                                  
                                              </span>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              #
                                          </td>
                                          <td>
                                              <a>
                                                  Faximile
                                              </a>
                                          </td>
                                          <td class="here">
                                              <a>
                                                      {{$companies->company_faximile}}
                                                <br>                 
                                              <small>
                                                @if($note_val[5] == 1)
                                                  {{$notes[5]}}
                                                @endif
                                              </small>
                                          </td>
                                          <td class="project-state" style="text-align: center;">
                                              <span class="button-checkbox">
                                                @if ($arr[5] == 1)
                                                <button type="button"  id="valid" class="coba btn" data-color="success" disabled>Validated</button>
                                                <input type="checkbox" name="valid_company_faximile" value="512" class="hidden data" style="display: none;" checked="" />
                                                @elseif ($arr[5] == 0)
                                                <button type="button" class="btn" data-color="success" disabled>Unvalidate</button>
                                                <input type="checkbox" name="valid_company_faximile" value="512" class="hidden data" style="display: none;" />
                                                @endif
                                              </span>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              #
                                          </td>
                                          <td>
                                              <a>
                                                  Address
                                              </a>
                                          </td>
                                          <td class="here">
                                              <a>
                                                      {{$companies->company_address}}
                                                <br>                 
                                              <small>
                                                @if($note_val[6] == 1)
                                                  {{$notes[6]}}
                                                @endif
                                              </small>
                                          </td>
                                          <td class="project-state" style="text-align: center;">
                                              <span class="button-checkbox">
                                                @if ($arr[6] == 1)
                                                <button type="button" id="valid" class="coba btn" data-color="success" disabled>Validated</button>
                                                <input type="checkbox" name="valid_company_address" value="256" class="hidden data" style="display: none;" checked="" />
                                                @elseif ($arr[6] == 0)
                                                <button type="button" class="btn" data-color="success" disabled>Unvalidate</button>
                                                <input type="checkbox" name="valid_company_address" value="256" class="hidden data" style="display: none;" />
                                                @endif
                                                  
                                              </span>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              #
                                          </td>
                                          <td>
                                              <a>
                                                  Company status
                                              </a>
                                          </td>
                                          <td class="here">
                                              <a>
                                                    {{$companies->company_status}}
                                                <br>                 
                                              <small>
                                                @if($note_val[7] == 1)
                                                  {{$notes[7]}}
                                                @endif
                                              </small>
                                          </td>
                                          <td class="project-state" style="text-align: center;">
                                              <span class="button-checkbox">
                                                @if ($arr[7] == 1)
                                                <button type="button"  id="valid" class="coba btn" data-color="success" disabled>Validated</button>
                                                <input type="checkbox" name="valid_company_status" value="128" class="hidden data" style="display: none;" checked="" />
                                                @elseif ($arr[7] == 0)
                                                <button type="button" class="btn" data-color="success" disabled>Unvalidate</button>
                                                @endif
                                                  <input type="checkbox" name="valid_company_status" value="128" class="hidden data" style="display: none;" />
                                              </span>
                                          </td>
                                      </tr>
                                      
                                  </tbody>
                              </table>
                            </div>
                            <!-- /.card-body -->
                          </div>
                          
<!-- =========================================================================== -->
{{-- COMPANY- EMAIL & DOMAIN VALIDATION --}}
                    
                          <div class="card collapsed-card">
                            <a class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse" id="val_head">
                            <div class="card-header">
                              <h3 class="card-title">Company Contacts</h3>
                    
                              <div class="card-tools">
                                <!-- <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"> -->
                                  <i class="fa fa-minus"></i>
                                <!-- </button> -->
                              </div>
                            </div></a>
                            <div class="card-body p-0">
                              <table class="table table-striped projects">
                                  <thead>
                                      <tr>
                                          <th style="width: 2%">
                                              #
                                          </th>
                                          <th style="width: 30%">
                                              Project Name
                                          </th>
                                          <th style="width: 50%">
                                              Team Members
                                          </th>
                                          <th style="width: 18%" class="text-center">
                                              Status
                                          </th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>
                                              #
                                          </td>
                                          <td>
                                              <a>
                                                  Domain/Web Company<span id="req">(&nbsp;<span id="star">*</span>&nbsp;)</span>
                                              </a>
                                          </td>
                                          <td class="here">
                                              <a>
                                              @foreach ($companies->domain as $domain)
                                                {{$domain['domain_name']}} <br><br>
                                                
                                              @endforeach
                                                      
                                                <br>                 
                                              <small></small>
                                          </td>
                                          <td class="project-state" style="text-align: center;">
                                              <span class="button-checkbox">
                                              @foreach ($companies->domain as $domain)
                                                  
                                              
                                                @if ($domain['domain_validation'] == true)
                                                    <button type="button" class="btn button-success btn-success active" data-color="success" disabled>Validated</button>
                                                    <input type="checkbox" name="valid_domain_name" value="1" class="hidden data" style="display: none;" checked="" />
                                                @else
                                                    <button type="button" class="btn button-success btn-success active" data-color="success" disabled>Unvalidated</button>
                                                    <input type="checkbox" name="valid_domain_name" value="1" class="hidden data" style="display: none;"/>
                                                @endif
                                                @break
                                              @endforeach
                                              </span>
                                        
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              #
                                          </td>
                                          <td>
                                              <a>
                                                  E-Mail Company<span id="req">(&nbsp;<span id="star">*</span>&nbsp;)</span>
                                              </a>
                                          </td>
                                          <td class="here">
                                              <a>
                                                @foreach ($companies->email as $email)
                                                  {{$email['email_name']}} <br><br>
                                                  
                                                @endforeach
                                                      
                                                <br>                 
                                              <small></small>
                                          </td>
                                          <td class="project-state" style="text-align: center;">
                                              <span class="button-checkbox">
                                                @foreach ($companies->email as $email)
                                                    
                                                
                                                @if ($email['email_validation'] == true)
                                                  <button type="button" class="btn" data-color="success" disabled>Validated</button>
                                                  <input type="checkbox" name="valid_email_name" value="1" class="hidden data" style="display: none;" checked="" />
                                                @else
                                                <button type="button" class="btn" data-color="success" disabled>Unvalidated</button>
                                                <input type="checkbox" name="valid_email_name" value="1" class="hidden data" style="display: none;" />
                                                @endif
                                                @break
                                              @endforeach
                                              </span>
                                          </td>
                                      </tr>
                                      
                                  </tbody>
                              </table>
                            </div>
                            <!-- /.card-body  -->
                          </div>
                    
<!-- ============================================================================= -->
{{-- PIC VALIDATION --}}
                          <div class="card collapsed-card">
                            <a class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse" id="val_head">
                            <div class="card-header">
                              <h3 class="card-title">Personal Information Company</h3>
                    
                              <div class="card-tools">
                                <!-- <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"> -->
                                  <i class="fa fa-minus"></i>
                                <!-- </button> -->
                              </div>
                            </div></a>
                            <div class="card-body p-0">
                                
                              <table class="table table-striped projects">
                                  <thead>
                                      <tr>
                                          <th style="width: 2%">
                                              #
                                          </th>
                                          <th style="width: 30%">
                                              Company Domain
                                          </th>
                                          <th style="width: 50%">
                                              Team Members
                                          </th>
                                          <th style="width: 18%" class="text-center">
                                              Status
                                          </th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>
                                              #
                                          </td>
                                          <td>
                                              <a>
                                                  Personal Name
                                              </a>
                                          </td>
                                          <td class="here">
                                              <a>
                                                      {{$companies->pic['pic_name']}}
                                                <br>                 
                                              <small>
                                                @if($note_val[8] == 1)
                                                  {{$notes[8]}}
                                                @endif
                                              </small>
                                          </td>
                                          <td class="project-state" style="text-align: center;">
                                              {{-- <span class="button-checkbox">
                                                  <button type="button" class="btn" data-color="success">Validasi</button>
                                                  <input type="checkbox" name="valid_pic_name" value="0" class="hidden data" style="display: none;" />
                                              </span> --}}
                                              <span class="button-checkbox">
                                                @if ($ay[0] == 1)
                                                <button type="button"  id="valid" class="coba btn" data-color="success" disabled>Validated</button>
                                                <input type="checkbox" name="valid_pic_name" value="64" class="hidden data" style="display: none;" checked="" />
                                                @elseif ($ay[0] == 0)
                                                <button type="button" class="btn" data-color="success" disabled>Unvalidate</button>
                                                @endif
                                                  <input type="checkbox" name="valid_pic_name" value="64" class="hidden data" style="display: none;" />
                                              </span>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              #
                                          </td>
                                          <td>
                                              <a>
                                                  Position
                                              </a>
                                          </td>
                                          <td class="here">
                                              <a>
                                                      {{$companies->pic['pic_position']}}
                                                <br>                 
                                              <small>
                                                @if($note_val[9] == 1)
                                                  {{$notes[9]}}
                                                @endif
                                              </small>
                                          </td>
                                          <td class="project-state" style="text-align: center;">
                                            <span class="button-checkbox">
                                                @if ($ay[1] == 1)
                                                <button type="button"  id="valid" class="coba btn" data-color="success" disabled>Validated</button>
                                                <input type="checkbox" name="valid_pic_position" value="32" class="hidden data" style="display: none;" checked="" />
                                                @elseif ($ay[1] == 0)
                                                <button type="button" class="btn" data-color="danger" disabled>Unvalidate</button>
                                                @endif
                                                  <input type="checkbox" name="valid_pic_position" value="32" class="hidden data" style="display: none;" />
                                              </span>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              #
                                          </td>
                                          <td>
                                              <a>
                                                  Divisi
                                              </a>
                                          </td>
                                          <td class="here">
                                              <a>
                                                      {{$companies->pic['pic_division']}}
                                                <br>                 
                                              <small>
                                                @if($note_val[10] == 1)
                                                  {{$notes[10]}}
                                                @endif
                                              </small>
                                          </td>
                                          <td class="project-state" style="text-align: center;">
                                            <span class="button-checkbox">
                                                @if ($ay[2] == 1)
                                                <button type="button"  id="valid" class="coba btn" data-color="success" disabled>Validated</button>
                                                <input type="checkbox" name="valid_pic_division" value="16" class="hidden data" style="display: none;" checked="" />
                                                @elseif ($ay[2] == 0)
                                                <button type="button" class="btn" data-color="success" disabled>Unvalidate</button>
                                                @endif
                                                  <input type="checkbox" name="valid_pic_division" value="16" class="hidden data" style="display: none;" />
                                              </span>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              #
                                          </td>
                                          <td>
                                              <a>
                                                  E-Mail
                                              </a>
                                          </td>
                                          <td class="here">
                                              <a>
                                                      {{$companies->pic['pic_email']}}
                                                <br>                 
                                              <small>
                                                @if($note_val[11] == 1)
                                                  {{$notes[11]}}
                                                @endif
                                              </small>
                                          </td>
                                          <td class="project-state" style="text-align: center;">
                                            <span class="button-checkbox">
                                                @if ($ay[3] == 1)
                                                <button type="button"  id="valid" class="coba btn" data-color="success" disabled>Validated</button>
                                                <input type="checkbox" name="valid_pic_email" value="8" class="hidden data" style="display: none;" checked="" />
                                                @elseif ($ay[3] == 0)
                                                <button type="button" class="btn" data-color="danger" disabled>Unvalidate</button>
                                                @endif
                                                  <input type="checkbox" name="valid_pic_email" value="8" class="hidden data" style="display: none;" />
                                              </span>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              #
                                          </td>
                                          <td>
                                              <a>
                                                  Phone Number
                                              </a>
                                          </td>
                                          <td class="here">
                                              <a>
                                                      {{$companies->pic['pic_nohp']}}
                                                <br>                 
                                              <small>
                                                @if($note_val[12] == 1)
                                                  {{$notes[12]}}
                                                @endif
                                              </small>
                                          </td>
                                          <td class="project-state" style="text-align: center;">
                                            <span class="button-checkbox">
                                                @if ($ay[4] == 1)
                                                <button type="button"  id="valid" class="coba btn" data-color="success" disabled>Validated</button>
                                                <input type="checkbox" name="valid_pic_nohp" value="4" class="hidden data" style="display: none;" checked="" />
                                                @elseif ($ay[4] == 0)
                                                <button type="button" class="btn" data-color="danger" disabled>Unvalidate</button>
                                                @endif
                                                  <input type="checkbox" name="valid_pic_nohp" value="4" class="hidden data" style="display: none;" />
                                              </span>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              #
                                          </td>
                                          <td>
                                              <a>
                                                  Social Media URL/LinkedIn
                                              </a>
                                          </td>
                                          <td class="here">
                                              <a>
                                                      {{$companies->pic['pic_sosmed']}}
                                                <br>                 
                                              <small>
                                                @if($note_val[13] == 1)
                                                  {{$notes[13]}}
                                                @endif
                                              </small>
                                          </td>
                                          <td class="project-state" style="text-align: center;">
                                            <span class="button-checkbox">
                                                @if ($ay[5] == 1)
                                                <button type="button"  id="valid" class="coba btn" data-color="success" disabled>Validated</button>
                                                <input type="checkbox" name="valid_pic_sosmed" value="2" class="hidden data" style="display: none;" checked="" />
                                                @elseif ($ay[5] == 0)
                                                <button type="button" class="btn" data-color="danger" disabled>Unvalidate</button>
                                                @endif
                                                  <input type="checkbox" name="valid_pic_sosmed" value="2" class="hidden data" style="display: none;" />
                                              </span>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              #
                                          </td>
                                          <td>
                                              <a>
                                                  Status
                                              </a>
                                          </td>
                                          <td class="here">
                                              <a>
                                                      {{$companies->pic['pic_status']}}
                                                <br>                 
                                              <small>
                                                @if($note_val[14] == 1)
                                                  {{$notes[14]}}
                                                @endif
                                              </small>
                                          </td>
                                          <td class="project-state" style="text-align: center;">
                                            <span class="button-checkbox">
                                                @if ($ay[6] == 1)
                                                <button type="button"  id="valid" class="coba btn" data-color="success" disabled>Validated</button>
                                                <input type="checkbox" name="valid_pic_status" value="1" class="hidden data" style="display: none;" checked="" />
                                                @elseif ($ay[6] == 0)
                                                <button type="button" class="btn" data-color="danger" disabled>Unvalidate</button>
                                                @endif
                                                  <input type="checkbox" name="valid_pic_status" value="1" class="hidden data" style="display: none;" />
                                              </span>
                                          </td>
                                      </tr>
                                      
                                  </tbody>
                              </table>
{{-- END OF VALIDATION ==================================================== --}}
                            </div>
                            </div>
                            <!-- /.card-body -->

                          {{-- </div> --}}

                          <span id="note">#Note : <span id="star_note">*</span> Required to complete</span>
                         
                    </form>
                    @endsection
