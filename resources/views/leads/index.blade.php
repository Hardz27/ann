@extends('layouts.master')

@section('title')
    <title>Manage Leads</title>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/tab/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker.css') }}" />
@endsection

@section('content')

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Manage Leads</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Manage Leads</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>


        @if (session('complete'))
        @php
            $nama = session('complete');
        @endphp
        <div class="col-sm-12">
            <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data {{$nama}} lengkap! Data telah berpindah ke tab Complete!</div>
        </div>

        @elseif (session('warning'))
        @php
            $nama = session('warning');
        @endphp
        <div class="col-sm-12">
            <div class="alert alert-warning"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data {{$nama}} tidak lengkap! Data telah berpindah ke tab uncomplete!</div>
        </div>

        @elseif  (session('success'))
        @php
            $nama = session('success');
        @endphp
        <div class="col-sm-12">
            <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data {{$nama}} berhasil diupdate!</div>
        </div>
        @endif
        <!-- Main content -->
        <div class="container">
            <section id="fancyTabWidget" class="tabs t-tabs" style="margin: 10px;">
        <ul class="nav nav-tabs fancyTabs dots" role="tablist">
                    <li class="tab fancyTab active" style="border-top-left-radius: 15px;">
                        <a id="tab0" href="#tabBody0" role="tab" aria-controls="tabBody0" aria-selected="true" data-toggle="tab" tabindex="0">
                            @if (auth()->user()->can('leads admin'))
                                <mark>{{$newInputCount}}</mark>    
                            @else
                                <mark>{{$inputernewInputCount}}</mark>
                            @endif
                            
                            <span class="hidden-xs">New Input</span><br>
                            <!-- <span class="hidden-xs" style="text-align: center;width: 63%;"></span> -->
                        <div class="whiteBlock"></div></a>
                    </li>
                    
                    <li class="tab fancyTab">
                    <!-- <div class="arrow-down"><div class="arrow-down-inner"></div></div> -->
                    <!-- menggunakan tab-l -->
                        <a id="tab1" href="#tabBody1" role="tab" aria-controls="tabBody1" aria-selected="true" data-toggle="tab" tabindex="0">
                            @if (auth()->user()->can('leads admin'))
                                <mark class="orange">{{$unCompliteCount}}</mark>    
                            @else
                                <mark class="orange">{{$inputerunCompliteCount}}</mark>
                            @endif
                            
                            <span class="hidden-xs">Uncomplete</span> 
                        <br>
                        <!-- <span style="text-align: center;width: 60%;"></span> -->
                        <div class="whiteBlock"></div></a>
                    </li>
                    
                    <li class="tab fancyTab">
                    <!-- <div class="arrow-down"><div class="arrow-down-inner"></div></div> -->
                        <a id="tab2" href="#tabBody2" role="tab" aria-controls="tabBody2" aria-selected="true" data-toggle="tab" tabindex="0">
                                @if (auth()->user()->can('leads admin'))
                                <mark class="green">{{$compliteCount}}</mark><span class="hidden-xs">Complete</span> 
                            @else
                                <mark class="green">{{$inputercompliteCount}}</mark><span class="hidden-xs">Complete</span>
                            @endif
                            
                        <br>
                        <!-- <span style="text-align: center;width: 60%;"></span> -->
                        <div class="whiteBlock"></div></a>
                    </li>
                    
                    <li class="tab fancyTab" style="border-top-right-radius: 15px; height: 95px">
                    <!-- <div class="arrow-down"><div class="arrow-down-inner"></div></div> -->
                        <a id="tab3" href="#tabBody3" role="tab" aria-controls="tabBody3" aria-selected="true" data-toggle="tab" tabindex="0">
                            @if (auth()->user()->can('leads admin'))
                                <mark class="blue">{{$allinputCount}}</mark><span class="hidden-xs">All Data</span>
                            @else
                                <mark class="blue">{{$inputerallinputCount}}</mark><span class="hidden-xs">All Data</span>
                            @endif
                            
                        <br>
                        <!-- <span style="text-align: center;width: 60%;"></span> -->
                        <div class="whiteBlock"></div></a>
                    </li> 
        </ul>
        
            <div id="myTabContent" class="tab-content fancyTabContent" aria-live="polite" 
                style="padding-bottom: 14px;margin-bottom: -10px;bottom: 19px;">
                    <div class="tab-pane  fade active in active show" id="tabBody0" role="tabpanel" aria-labelledby="tab0" aria-hidden="false" tabindex="0">
                        <div>
                            <div class="row">
                                
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                    <table id="new-inp" class="table table-hover">
                                    <thead style="text-align: center;">
                                        <tr>
                                            <td style="width: 60px;">No</td>
                                            <td>Inputer</td>
                                            <td>Company Name</td>
                                            <td>Domain</td>
                                            <td>Office Number</td>
                                            <td>Office Email</td>
                                            <td>Validation</td>

                                            <td>Date & Time created</td>
                                            <td>Action</td>
                                        </tr>
                                    </thead>
                                    <tbody>       
{{-- TABEL NEW INPUT  --}}
                                 
                                         @php $no = 1; @endphp
                                        @if ( auth()->user()->can('leads admin'))    
                                            @forelse ($newInput as $item)
                                               @if ($item->deleted_at == null)
                                      <tr>
                                            <td style="text-align: center;">{{ $no++ }}
                                                @if(session('success'))
                                                    @if($nama == $item->company_name)
                                                    <mark class="oranges">edited</mark>
                                                    @endif
                                                @endif
                                            </td>
                                            <td style="text-align: center;">
                                            @if ($item->created_by == 'Fahrur' ) 
                                                <label for="" class="badge badge-secondary">{{$item->created_by}}</label>                                                
                                                @else
                                                <label for="" class="badge badge-success">{{$item->created_by}}</label>    
                                            @endif
             
                                            </td> 
                                            <td>{{$item->company_name}}</td>
                                            <td>
                                                @foreach($domain[$item->company_id] as $dom)
                                                    {{$dom->domain_name}}
                                                @break
                                                @endforeach
                         
                                            </td>
                                            <td>{{$item->company_telpon_office}}</td>
                                            <td>

                                                    @foreach($email[$item->company_id] as $ema)
                                                        {{$ema->email_name}}
                                                        @break
                                                    @endforeach
                                            </td>
                                            <!-- <td></td> -->

                                            <td style="text-align: center;">

                                                <label for="" class="badge badge-danger">unvalidate</label>
                                                
                                            </td>
                                            <td>{{$item->created_at}}</td>
                                            <td style="text-align: center;">
                                                @if (auth()->user()->can('leads admin'))   
                                                <a href="{{route('lead.validation', $item->company_id)}}" class="btn btn-success btn-sm btn-show-validation" style="padding: 4px 7px;"><i class="fa fa-check-square-o"></i></a>
                                                    {{-- <i href="{{route('lead.validation', $item->company_id)}}" class="btn btn-success btn-sm btn-show-validation"><i class="fa fa-check-square-o"></i></i> --}}
                                                    {{-- <a href="{{route('lead.validation', $item->company_id)}}" class="btn btn-success btn-sm show-validation" style="padding: 4px 7px;"><i class="fa fa-check-square-o"></i></a> --}}
                                                    <a href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a>
                                                    <a href="{{route('lead.delete_lead', $item->company_id)}}" class="btn btn-danger btn-sm btn-show-delete"><i class="fa fa-archive"></i></a>
                                                @endif
                                                {{-- @if (auth()->user()->can('leads inputer'))    --}}
                                                    {{-- <a href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a> --}}
                                                {{-- @endif --}}
                                            </td>
                                            @endif 
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="4" class="text-center">Tidak ada data</td>
                                        </tr> 
                  
                                    @endforelse
                                    @elseif(auth()->user()->can('leads inputer'))
                                    @forelse ($inputerNewInput as $item)
                                    @if ($item->deleted_at == null)
                           <tr>
                                 <td style="text-align: center;">{{ $no++ }}
                                     @if(session('success'))
                                         @if($nama == $item->company_name)
                                         <mark class="oranges">edited</mark>
                                         @endif
                                     @endif
                                 </td>
                                 <td style="text-align: center;">
{{-- 
                                    @foreach ($users->getRoleNames() as $role)
                            @if ($role == 'inputer')
                                <label for="" class="badge badge-secondary">{{ $item->created_by }}</label>
                            @else
                                <label for="" class="badge badge-success">{{ $item->created_by }}</label>
                            @endif
                            
                            @endforeach --}}

                                 @if ($item->created_by == 'Fahrur' ) 
                                     <label for="" class="badge badge-secondary">{{$item->created_by}}</label>                                                
                                     @else
                                     <label for="" class="badge badge-success">{{$item->created_by}}</label>    
                                 @endif
  
                                 </td> 
                                 <td>{{$item->company_name}}</td>
                                 <td>

                                        @foreach($item->domain as $domain)
                                            {{$domain['domain_name']}}
                                            @break
                                        @endforeach
                                </td>
                                 <td>{{$item->company_telpon_office}}</td>
                                 <td>
                                        @foreach($item->email as $key => $email)
                                            {{$email['email_name']}}
                                            @break
                                        @endforeach
                                </td>
                                 <!-- <td></td> -->

                                 <td style="text-align: center;">

                                     <label for="" class="badge badge-danger">unvalidate</label>
                                     
                                 </td>
                                 <td>{{$item->created_at}}</td>
                                 <td style="text-align: center;">
                                     @if (auth()->user()->can('leads admin'))   
                                         <a href="{{route('lead.validation', $item->company_id)}}" class="btn btn-success btn-sm btn-show-validation" style="padding: 4px 7px;"><i class="fa fa-check-square-o"></i></a>
                                         <a href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a>
                                         
                                         
                                     @endif
                                     @if (auth()->user()->can('leads inputer'))   
                                         <a href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a>
                                     @endif
                                 </td>
                                 @endif 
                             </tr>
                             @empty
                             <tr>
                                 <td colspan="4" class="text-center">Tidak ada data</td>
                             </tr> 
                             @endforelse
                             @endif
                                </tbody>
                                </table>
                            </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    @php $modal = 0;     @endphp
 
{{-- TABLE UNCOMPLITE --}}
                
                    <div class="tab-pane  fade" id="tabBody1" role="tabpanel" aria-labelledby="tab1" aria-hidden="true" tabindex="0">
                        <div class="row">
                                
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                    <table id="uncom" class="table table-hover">
                                    <thead style="text-align: center;">
                                        <tr>
                                            <td style="width: 60px;">No</td>
                                            <td>Inputer</td>
                                            <td>Company Name</td>
                                            <td>Domain</td>
                                            <td>Office Number</td>
                                            <td>Office Email</td>
                                            <td>Validation</td>
                                            <td>Date & Time Validated</td>
                                            <td>Action</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @php $no = 1;@endphp
                                        @if (auth()->user()->can('leads admin'))
                                            
                                        @forelse ($unComplite as $item)
                                        @php
                                                    $company = DB::table('t_companies')->where('t_companies.company_id', $item->company_id)
                                                              ->where('t_companies.company_validation', '<', 24576)->count();
                                                    $domain = DB::table('t_company_domains')->where('t_company_domains.company_id', $item->company_id)
                                                               ->where('t_company_domains.domain_validation', '=', null)->count();
                                                    $notes = DB::table('inputers')->select('inputer_note_company')->where('company_id', $item->company_id)
                                                            ->where('inputer_note_company', '!=', null)->first();
                                                    $email = DB::table('t_company_emails')->where('t_company_emails.company_id', $item->company_id)
                                                          ->where('t_company_emails.email_validation', '=', null)->count();   

                                        @endphp
        
                                        {{-- @forelse (T_company::instance('unComplite')->content() as $item) CARA INSTANCE MODEL --}}

                                        
                                        
                                        @if (!$item->deleted_at)
                                            

                                        <tr>
                                            
                                            <td style="text-align: center;">{{ $no++ }}
                                            @if  (session('success'))
                                                @if($nama == $item->company_name)
                                                    <mark class="oranges">edited</mark>
                                                @endif
                                            @endif
                                            </td>
                                            <td style="text-align: center;">
                                            
                                                    

                                                 
                                                    @if ($item->created_by == 'Fahrur' )   
                                                        <label for="" class="badge badge-secondary">{{$item->created_by}}</label>
                                                    @else 
                                                    
                                                        <label for="" class="badge badge-success">{{$item->created_by}}</label>    
                                                    @endif
                                                    
                                                </td>
                                            <td >{{$item->company_name}}</td>
                                            <td>

                                                    {{-- @foreach($item->domain as $key => $domain) --}}
                                                        {{$item->domain_name}}
                                                    {{-- @endforeach --}}
                                            </td>
                                            <td >{{$item->company_telpon_office}}</td>
                                            <td>

                                                    {{-- @foreach($item->email as $key => $email) --}}
                                                        {{$item->email_name}}
                                                    {{-- @endforeach --}}
                                            </td>
                                            <!-- <td></td> -->
                                            <td style="text-align: center;">
                                            {{-- @if ($validation = true) --}}

                                                @php                                                        
                                                
                                                // if($notes){

                                                    // echo "<label for='' class='badge badge-danger'>*</label> ";
                                                // }
                                                // else{
                                                //  
                                                // }
                                                
                                                if($item->company_validation >= 16384 && $item->company_validation < 24576){
                                                    echo '<label for="" class="badge badge-warning">telpon</label> ';
                                                }
                                                else if($item->company_validation < 16384 && $item->company_validation >= 8192){
                                                    echo '<label for="" class="badge badge-warning">name</label> ';
                                                }
                                                else if($item->company_validation < 8192){
                                                    echo '<label for="" class="badge badge-warning">name</label> ';
                                                    echo '<label for="" class="badge badge-warning">telpon</label> ';
                                                }
                                                  
                                                if ($domain > 0 ) {
                                                       echo "<label for='' class='badge badge-warning'>domain</label> ";
                                                } else {
                                                      
                                                }
                                        
                                                 if($email > 0 ){
                                                    echo "<label for='' class='badge badge-warning'>email</label> ";
                                                 }
 
                                                 @endphp

                                            </td>
                                            <td>{{$item->created_at}}</td>
                                            <td style="text-align: center;">
                                                
                                                
                                                    <input type="hidden" name="_method" value="DELETE">
                 
                                                    
                                                @if (auth()->user()->can('leads admin'))   
                                                        <a href="{{route('lead.validation', $item->company_id)}}" class="btn btn-success btn-sm btn-show-validation" style="padding: 4px 7px;"><i class="fa fa-check-square-o"></i></a>
                                                        <a href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a>
                                                        <a href="{{route('lead.delete_lead', $item->company_id)}}" class="btn btn-danger btn-sm btn-show-delete"><i class="fa fa-archive"></i></a>
                                                @endif
                                                {{-- @if (auth()->user()->can('leads inputer'))  --}}
                                                    {{-- <a href="{{route('lead.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a>   --}}
                                                    {{-- <a href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a> --}}
                                                {{-- @endif --}}
                                         
                                            </td>
                                        </tr>
                                            
                                        @endif
                                        @empty
                                        
                                        <tr>
                                            <td colspan="4" class="text-center">Tidak ada data</td>
                                        </tr>
                                         
                                        @endforelse
                                        @else
                                        @forelse ($inputerUnComplite as $item)
                                            
                                        @if ($item->created_by == auth()->user()->user_name)
                                            
                                        
                                        @if (!$item->deleted_at )
                                            @if($item->company_validation == 0 && ($item->domain_validation != 1 && $item->email_validation != 1))

                                            @else
                                        <tr>
                                            
                                            <td style="text-align: center;">{{ $no++ }}
                                            @if  (session('success'))
                                                @if($nama == $item->company_name)
                                                    <mark class="oranges">edited</mark>
                                                @endif
                                            @endif
                                            </td>
                                            <td style="text-align: center;">
                                                    @if ($item->created_by == 'Fahrur' )   
                                                        <label for="" class="badge badge-secondary">{{$item->created_by}}</label>
                                                    @else 
                                                    
                                                        <label for="" class="badge badge-success">{{$item->created_by}}</label>    
                                                    @endif
                                                </td>
                                            <td >{{$item->company_name}}</td>
                                            <td>

                                                    @foreach($item->domain as $key => $domain)
                                                        {{$domain['domain_name']}}
                                                        @break
                                                    @endforeach
                                            </td>
                                            <td >{{$item->company_telpon_office}}</td>
                                            <td>

                                                    @foreach($item->email as $key => $email)
                                                        {{$email['email_name']}}
                                                        @break
                                                    @endforeach
                                            </td>
                                            <!-- <td></td> -->
                                            <td style="text-align: center;">
                                            {{-- @if ($validation = true) --}}

                                                @php
                                                    $company = DB::table('t_companies')->where('t_companies.company_id', $item->company_id)
                                                              ->where('t_companies.company_validation', '<', 24576)->count();
                                                    $domain = DB::table('t_company_domains')->where('t_company_domains.company_id', $item->company_id)
                                                               ->where('t_company_domains.domain_validation', '=', null)->count();
                                                    $notes = DB::table('inputers')->select('inputer_note_company')->where('company_id', $item->company_id)
                                                            ->where('inputer_note_company', '!=', null)->first();
                                                    $email = DB::table('t_company_emails')->where('t_company_emails.company_id', $item->company_id)
                                                          ->where('t_company_emails.email_validation', '=', null)->count();   

                                                        
                                                
                                                // if($notes){
// 
                                                    // echo "<label for='' class='badge badge-danger'>*</label> ";
                                                // }
                                                // else{
                                                //  
                                                // }
                                                
                                                if($item->company_validation >= 16384 && $item->company_validation < 24576){
                                                    echo '<label for="" class="badge badge-warning">telpon</label> ';
                                                }
                                                else if($item->company_validation < 16384 && $item->company_validation >= 8192){
                                                    echo '<label for="" class="badge badge-warning">name</label> ';
                                                }
                                                else if($item->company_validation < 8192){
                                                    echo '<label for="" class="badge badge-warning">name</label> ';
                                                    echo '<label for="" class="badge badge-warning">telpon</label> ';
                                                }
                                                  
                                                if ($domain > 0 ) {
                                                       echo "<label for='' class='badge badge-warning'>domain</label> ";
                                                } else {
                                                      
                                                }
                                        
                                                 if($email > 0 ){
                                                    echo "<label for='' class='badge badge-warning'>email</label> ";
                                                 }
 
                                                 @endphp

                                                    
                                                
                                            </td>
                                            <td>{{$item->created_at}}</td>
                                            <td style="text-align: center;">
                                                
                                                
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    

                                                    {{-- @foreach ($role as $row)
                                                        
                                                    @if ($row->name == 'admin')
                                                        <label for="" class="badge badge-secondary">{{$item->created_by}}</label>    
                                                    @else
                                                        <label for="" class="badge badge-success">{{$item->created_by}}</label>
                                                    @endif
                                                    @break
                                                    
                                                @endforeach --}}

                                                
                                                    
                                                @if (auth()->user()->can('leads admin'))   
                                                        <a href="{{route('lead.validation', $item->company_id)}}" class="btn btn-success btn-sm btn-show-validation"><i class="fa fa-check-square-o"></i></a>
                                                        <a href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a>
                                                        <a href="{{route('lead.delete_lead', $item->company_id)}}" class="btn btn-danger btn-sm btn-show-delete"><i class="fa fa-archive"></i></a>
                                                @endif
                                                @if (auth()->user()->can('leads inputer')) 
                                                    <a href="{{route('lead.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a>  
                                                    <a href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a>
                                                @endif
                                                
                                                    
                                                    
                                                
                                            </td>
                                        </tr>
                                        @endif
                                        @endif
                                        @endif
                                        @empty
                                        
                                        <tr>
                                            <td colspan="4" class="text-center">Tidak ada data</td>
                                        </tr>
                                        @endforelse
                                        
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                                    
                                   
                                </div>
                            </div>
                    </div>


{{-- TABLE COMPLITE --}}

                    <div class="tab-pane  fade" id="tabBody2" role="tabpanel" aria-labelledby="tab2" aria-hidden="true" tabindex="0">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                    <table id="com" class="table table-hover">
                                    <thead style="text-align: center;">
                                        <tr>
                                            <td style="width: 60px;">No</td>
                                            <td>Inputer</td>
                                            <td>Company Name</td>
                                            <td>Domain</td>
                                            <td>Office Number</td>
                                            <td>Office Email</td>
                                            <td>Validation</td>
                                            <td>Date & Time Validated</td>
                                            <td>Action</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                       

                                        @php $no = 1; @endphp
                                        @if (auth()->user()->can('leads admin'))
                                            
                                        
                                        

                                        @forelse ($complites as $item)
                     
                                        <tr>
                                            <td style="text-align: center;">{{ $no++ }}
                                                @if  (session('success'))
                                                    @if($nama == $item->company_name)
                                                        <mark class="oranges">edited</mark>
                                                    @endif
                                                @endif
                                            </td>
                                            <td style="text-align: center;">
                                                    @if ($item->created_by == 'Fahrur' )   
                                                        <label for="" class="badge badge-secondary">{{$item->created_by}}</label>
                                                    @else 
                                                    
                                                        <label for="" class="badge badge-success">{{$item->created_by}}</label>    
                                                    @endif
                                                </td>
                                            <td>{{$item->company_name}}</td>
                                            <td>
                                                
                                                {{-- @foreach ($domain[$item->company_id] as $domain)
                                                    {{dump($domain->domain_name)}}
                                                @endforeach
                                                @php
                                                    die;
                                                @endphp --}}



                                                    @foreach($item->domain as $key => $domain)
                                                        {{$domain['domain_name']}}
                                                        @break
                                                    @endforeach
                                            </td>
                                            <td>{{$item->company_telpon_office}}</td>
                                            <td>

                                                    @foreach($item->email as $key => $email)
                                                        {{$email['email_name']}}
                                                        @break
                                                    @endforeach
                                            </td>
                                            <!-- <td></td> -->

                                            <td style="text-align:center;">

                                                @if ($item->validation = true)
                                                
                                                <label for="" class="badge badge-success">name</label>
                                                <label for="" class="badge badge-success">telpon</label>    
                                                <label for="" class="badge badge-success">domain</label>
                                                <label for="" class="badge badge-success">email</label>
                                                @endif 
                                                
                                                
                                            </td>
                                            <td>{{$item->created_at}}</td>
                                            <td style="text-align: center;">
                                                
                                                    
                                            @if (auth()->user()->can('leads admin'))   
                                                    <a href="{{route('lead.validation', $item->company_id)}}" class="btn btn-success btn-sm btn-show-validation" style="padding: 4px 7px;"><i class="fa fa-check-square-o"></i></a>
                                                    <a href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm"  style="padding: 4px 6px;"><i class="fa fa-edit"></i></a>
                                                    <a href="{{route('lead.delete_lead', $item->company_id)}}" class="btn btn-danger btn-sm btn-show-delete"><i class="fa fa-archive"></i></a>
                                            @endif
                                            {{-- @if (auth()->user()->can('leads inputer')) --}}
                                                    {{-- <a href="{{route('lead.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a>    --}}
                                                    {{-- <a href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a> --}}
                                            {{-- @endif --}}
                                            </td>
                                        </tr>
                                        @empty 
                                        <tr>
                                            <td colspan="4" class="text-center">Tidak ada data</td>
                                        </tr> 
                                        @endforelse
                                        @else
                                        @forelse ($inputerComplite as $item)
                     
                                        <tr>
                                            <td style="text-align: center;">{{ $no++ }}
                                                @if  (session('success'))
                                                    @if($nama == $item->company_name)
                                                        <mark class="oranges">edited</mark>
                                                    @endif
                                                @endif
                                            </td>
                                            <td style="text-align: center;">
                                                    @if ($item->created_by == 'Fahrur' )   
                                                        <label for="" class="badge badge-secondary">{{$item->created_by}}</label>
                                                    @else 
                                                    
                                                        <label for="" class="badge badge-success">{{$item->created_by}}</label>    
                                                    @endif
                                                </td>
                                            <td>{{$item->company_name}}</td>
                                            <td>

                                                    @foreach($item->domain as $key => $domain)
                                                        {{$domain['domain_name']}}
                                                        @break
                                                    @endforeach
                                            </td>
                                            <td>{{$item->company_telpon_office}}</td>
                                            <td>

                                                    @foreach($item->email as $key => $email)
                                                        {{$email['email_name']}}
                                                        @break
                                                    @endforeach
                                            </td>
                                            <!-- <td></td> -->

                                            <td style="text-align:center;">

                                                @if ($item->validation = true)
                                                
                                                <label for="" class="badge badge-success">name</label>
                                                <label for="" class="badge badge-success">telpon</label>    
                                                <label for="" class="badge badge-success">domain</label>
                                                <label for="" class="badge badge-success">email</label>
                                                @endif 
                                                
                                                
                                            </td>
                                            <td>{{$item->created_at}}</td>
                                            <td style="text-align: center;">
                                                
                                                    
                                            @if (auth()->user()->can('leads admin'))   
                                                    <a href="{{route('lead.validation', $item->company_id)}}" class="btn btn-success btn-sm btn-show-validation" style="padding: 4px 7px;"><i class="fa fa-check-square-o"></i></a>
                                                    <a href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a>
                                                    <a href="{{route('lead.delete_lead', $item->company_id)}}" class="btn btn-danger btn-sm btn-show-delete"><i class="fa fa-archive"></i></a>
                                            @endif
                                            @if (auth()->user()->can('leads inputer'))
                                                    <a href="{{route('lead.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a>   
                                                    <a href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a>
                                            @endif
                                            </td>
                                        </tr>
                                        @empty 
                                        <tr>
                                            <td colspan="4" class="text-center">Tidak ada data</td>
                                        </tr> 
                                        
                                        @endforelse    
                                        @endif
                                    </tbody>
                                </table>
                            </div>                                    
                                  
                                </div>
                            </div>
                    </div>

{{-- TABLE ALLDATA --}}
                    <div class="tab-pane  fade" id="tabBody3" role="tabpanel" aria-labelledby="tab3" aria-hidden="true" tabindex="0">
                        <div class="row">
                            <div class="table-responsive">
                                    <table id="all" class="table table-hover">
                                    <thead style="text-align: center;">
                                        <tr>
                                            <td style="width: 60px;">No</td>
                                            <td>Inputer</td>
                                            <td>Company Name</td>
                                            <td>Domain</td>
                                            <td>Office Number</td>
                                            <td>Office Email</td>
                                            <td>Date & Time Validated</td>
                                            <td>Action</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @php $no = 1; @endphp
                                        @if (auth()->user()->can('leads admin'))
                                            
                                        
                                        @forelse ($allInput as $item)
                                        
                                        <tr>
                                            <td style="text-align: center;">{{ $no++ }}
                                                @if  (session('success'))
                                                    @if($nama == $item->company_name)
                                                        <mark class="oranges">edited</mark>
                                                    @endif
                                                @endif
                                            </td>
                                            <td style="text-align: center;">
                                                
                                                    {{-- @foreach ($role as $row)
                                                        
                                                        @if ($row->name == 'admin')
                                                            <label for="" class="badge badge-secondary">{{$item->created_by}}</label>    
                                                        @else
                                                            <label for="" class="badge badge-success">{{$item->created_by}}</label>
                                                        @endif
                                                        @break
                                                        
                                                    @endforeach --}}
                                                    @if ($item->created_by == 'Fahrur' )   
                                                    <label for="" class="badge badge-secondary">{{$item->created_by}}</label>
                                                @else 
                                                                                             
                                                    <label for="" class="badge badge-success">{{$item->created_by}}</label>    
                                                @endif
                                                        
                                                </td> 
                                            <td>{{$item->company_name}}</td>
                                            <td>

                                                    @foreach($item->domain as $key => $domain)
                                                        {{$domain['domain_name']}}
                                                        @break
                                                    @endforeach
                                            </td>
                                            <td>{{$item->company_telpon_office}}</td>
                                            <td>

                                                    @foreach($item->email as $key => $email)
                                                        {{$email['email_name']}}
                                                        @break
                                                    @endforeach
                                            </td>
                                            

                                            <td>{{$item->created_at}}</td>
                                            <td style="text-align: center;">
                                                @if (auth()->user()->can('leads admin'))   
                                                    {{-- <i href="{{route('lead.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail" style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></i> --}}
                                                    <a href="{{route('check.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a>
                                                    <a href="{{route('lead.delete_lead', $item->company_id)}}" class="btn btn-danger btn-sm btn-show-delete" style="margin-top: 5px;"><i class="fa fa-archive"></i></a>        
                                                @endif
                                                {{-- @if (auth()->user()->can('leads inputer'))    --}}
                                                    {{-- <i href="{{route('lead.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"><i class="fa fa-info"></i></i> --}}
                                                    {{-- <a href="{{route('check.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail" style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a> --}}
                                                {{-- @endif --}}
                                                
                                                
                                            </td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="4" class="text-center">Tidak ada data</td>
                                        </tr>
                                        @endforelse
                                        @else
                                        @forelse ($inputerallInput as $item)
                                        
                                        <tr>
                                            <td style="text-align: center;">{{ $no++ }}
                                                @if  (session('success'))
                                                    @if($nama == $item->company_name)
                                                        <mark class="oranges">edited</mark>
                                                    @endif
                                                @endif
                                            </td>
                                            <td style="text-align: center;">
                                                
                                                    {{-- @foreach ($role as $row)
                                                        
                                                        @if ($row->name == 'admin')
                                                            <label for="" class="badge badge-secondary">{{$item->created_by}}</label>    
                                                        @else
                                                            <label for="" class="badge badge-success">{{$item->created_by}}</label>
                                                        @endif
                                                        @break
                                                        
                                                    @endforeach --}}
                                                    @if ($item->created_by == 'Fahrur' )   
                                                    <label for="" class="badge badge-secondary">{{$item->created_by}}</label>
                                                @else 
                                                                                             
                                                    <label for="" class="badge badge-success">{{$item->created_by}}</label>    
                                                @endif
                                                        
                                                </td> 
                                            <td>{{$item->company_name}}</td>
                                            <td>

                                                    @foreach($item->domain as $key => $domain)
                                                        {{$domain['domain_name']}}
                                                        @break
                                                    @endforeach
                                            </td>
                                            <td>{{$item->company_telpon_office}}</td>
                                            {{-- <td>{{$item->email['email_name']}}</td> --}}
                                            <td>

                                                    @foreach($item->email as $key => $email)
                                                        {{$email['email_name']}}
                                                        @break
                                                    @endforeach
                                            </td>
                                            

                                            <td>{{$item->created_at}}</td>
                                            <td style="text-align: center;">
                                                @if (auth()->user()->can('leads admin'))   
                                                    {{-- <i href="{{route('lead.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"><i class="fa fa-info"></i></i> --}}
                                                    <a href="{{route('check.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail" style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a>
                                                    <a href="{{route('lead.delete_lead', $item->company_id)}}" class="btn btn-danger btn-sm btn-show-delete" style="margin-top: 5px;"><i class="fa fa-archive"></i></a>        
                                                @endif
                                                @if (auth()->user()->can('leads inputer'))   
                                                    {{-- <a href="{{route('lead.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"><i class="fa fa-info"></i></a> --}}
                                                    <a href="{{route('check.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a>
                                                @endif
                                                
                                                
                                            </td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="4" class="text-center">Tidak ada data</td>
                                        </tr>
                                        @endforelse
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        
        </div>
     </div>

 </div>


@endsection

@section('js')
    <script src="{{ asset('plugins/tablefilter/tablefilter.js') }}"></script>
    <script src="{{ asset('js/filter-new-input.js') }}"></script> <!-- ini dibutuhin di leads -->
    <script src="{{ asset('js/filter-uncom.js') }}"></script> <!-- ini dibutuhin di leads -->
    <script src="{{ asset('js/filter-com.js') }}"></script> <!-- ini dibutuhin di leads -->
    <script src="{{ asset('js/filter-all.js') }}"></script> <!-- ini dibutuhin di leads -->
    <script src="{{ asset('js/pop-up.js') }}"></script> 
    <script src="{{ asset('js/validation.js') }}"></script>    
    <script src="{{ asset('plugins/tab/js/index.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/daterangepicker.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>

    <script>
        var x = document.getElementsByClassName("nextPage");
        x[0].value = "Next";
        var y = document.getElementsByClassName("lastPage");
        y[0].value = "Last";

        var a = document.getElementsByClassName("previousPage");
        a[0].value = "Prev";
        var b = document.getElementsByClassName("firstPage");
        b[0].value = "First";
        

        $('.nextPage, .lastPage, .previousPage, .firstPage').addClass("btn btn-default");
        $('.mdiv').addClass("float-right");
        // $('.lastPage').addClass("btn btn-default");


    </script>
    
@if (auth()->user()->can('leads admin'))
        <script src="{{ asset('js/append_export_button.js') }}"></script>
@else
        
@endif
    
    <script src="{{ asset('js/filter_date.js') }}"></script>
@endsection