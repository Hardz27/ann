
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Information Details</h4>
        </div>
        <form action="#"  method="post">
        <div class="modal-body">
          <div class="card">
        <div class="card-header">
          <h3 class="card-title">Company Information</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%">
                          #
                      </th>
                      <th style="width: 30%">
                          Field
                      </th>
                      <th style="width: 50%">
                          Value
                      </th>
                      <th style="width: 18%" class="text-center">
                          Status
                      </th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                        <!--   <a>
                              AdminLTE v3
                          </a>
                          <br/>
                          <small>
                              Created 01.01.2019
                          </small> -->
                          <a>
                              Company Name
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  Isi Company name
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="button-checkbox">
                            <button type="button" class="btn" data-color="success">Validasi</button>
                            <input type="checkbox" class="hidden" style="display: none;" />
                          </span>
                      </td>

                   <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Company type
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  Isi Company name
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="button-checkbox">
                            <button type="button" class="btn" data-color="success">Validasi</button>
                            <input type="checkbox" class="hidden" style="display: none;" />
                          </span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Company Business
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  Isi Company name
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="button-checkbox">
                            <button type="button" class="btn" data-color="success">Validasi</button>
                            <input type="checkbox" class="hidden" style="display: none;" />
                          </span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              General Information/Description
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  Isi Company name
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="button-checkbox">
                            <button type="button" class="btn" data-color="success">Validasi</button>
                            <input type="checkbox" class="hidden" style="display: none;" />
                          </span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Office Number
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  Isi Company name
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="button-checkbox">
                            <button type="button" class="btn" data-color="success">Validasi</button>
                            <input type="checkbox" class="hidden" style="display: none;" />
                          </span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Faximile
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  Isi Company name
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="button-checkbox">
                            <button type="button" class="btn" data-color="success">Validasi</button>
                            <input type="checkbox" class="hidden" style="display: none;" />
                          </span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Address
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  Isi Company name
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="button-checkbox">
                            <button type="button" class="btn" data-color="success">Validasi</button>
                            <input type="checkbox" class="hidden" style="display: none;" />
                          </span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Company status
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  Isi Company name
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="button-checkbox">
                            <button type="button" class="btn" data-color="success">Validasi</button>
                            <input type="checkbox" class="hidden" style="display: none;" />
                          </span>
                      </td>
                  </tr>
                  
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>

      <!-- =========================================================================== -->

      <div class="card collapsed-card">
        <div class="card-header">
          <h3 class="card-title">Company Contacts</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%">
                          #
                      </th>
                      <th style="width: 30%">
                          Project Name
                      </th>
                      <th style="width: 50%">
                          Team Members
                      </th>
                      <th style="width: 18%" class="text-center">
                          Status
                      </th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Domain/Web Company
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  Isi Company name
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="button-checkbox">
                            <button type="button" class="btn" data-color="success">Validasi</button>
                            <input type="checkbox" class="hidden" style="display: none;" />
                          </span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              E-Mail Company
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  Isi Company name
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="button-checkbox">
                            <button type="button" class="btn" data-color="success">Validasi</button>
                            <input type="checkbox" class="hidden" style="display: none;" />
                          </span>
                      </td>
                  </tr>
                  
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>

      <!-- ============================================================================= -->

      <div class="card collapsed-card">
        <div class="card-header">
          <h3 class="card-title">Personal Information Company</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
            
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%">
                          #
                      </th>
                      <th style="width: 30%">
                          Company Domain
                      </th>
                      <th style="width: 50%">
                          Team Members
                      </th>
                      <th style="width: 18%" class="text-center">
                          Status
                      </th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Personal Name
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  Isi Company name
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="button-checkbox">
                            <button type="button" class="btn" data-color="success">Validasi</button>
                            <input type="checkbox" class="hidden" style="display: none;" />
                          </span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Position
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  Isi Company name
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="button-checkbox">
                            <button type="button" class="btn" data-color="success">Validasi</button>
                            <input type="checkbox" class="hidden" style="display: none;" />
                          </span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Divisi
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  Isi Company name
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="button-checkbox">
                            <button type="button" class="btn" data-color="success">Validasi</button>
                            <input type="checkbox" class="hidden" style="display: none;" />
                          </span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              E-Mail
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  Isi Company name
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="button-checkbox">
                            <button type="button" class="btn" data-color="success">Validasi</button>
                            <input type="checkbox" class="hidden" style="display: none;" />
                          </span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Phone Number
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  Isi Company name
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="button-checkbox">
                            <button type="button" class="btn" data-color="success">Validasi</button>
                            <input type="checkbox" class="hidden" style="display: none;" />
                          </span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Social Media URL/LinkedIn
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  Isi Company name
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="button-checkbox">
                            <button type="button" class="btn" data-color="success">Validasi</button>
                            <input type="checkbox" class="hidden" style="display: none;" />
                          </span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Status
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  Isi Company name
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="button-checkbox">
                            <button type="button" class="btn" data-color="success">Validasi</button>
                            <input type="checkbox" class="hidden" style="display: none;" />
                          </span>
                      </td>
                  </tr>
                  
              </tbody>
          </table>          
        </div>
        </div>
        <!-- /.card-body -->
      </div>
      <div style="text-align: center;padding: 25px;padding-top: 0px;">
        <button id="validate" type="submit" class="btn btn-success btn-lg">Submit</button>
      </div>
        </form>
        <div class="modal-footer">
            
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </div>
      </div>
    </div>
  </div>
