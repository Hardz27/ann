@extends('layouts.master')
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="{{ asset('dist/img/erporate1.png') }}" alt="POS" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">ERPORATE-LEADS</span>
    </a>

    <div class="sidebar" id="myDIV">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                 <a href="#" class="d-block">{{auth()->user()->name}}</a>
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                <a href="{{ route('home') }}" class="nav-link">
                        <i class="nav-icon fa fa-dashboard"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>

                @if (auth()->user()->can('show products') || auth()->user()->can('delete products') || auth()->user()->can('create products'))
              
                @endif                
                @role('admin')
                <li class="nav-item">
                    <a href="{{ route('check.index') }}" class="nav-link">
                        <i class="nav-icon fa fa-toggle-on"></i>
                        <p>
                            Manage Cek
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{ route('order.index') }}" class="nav-link">
                        <i class="nav-icon fa fa-history"></i>
                        <p>
                            Manage Leads
                            {{-- <i class="right fa fa-angle-left"></i> --}}
                        </p>
                    </a>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa fa-users"></i>
                        <p>
                            Manage User
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('role.index') }}" class="nav-link">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Role</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('users.roles_permission') }}" class="nav-link">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Role Permission</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('users.index') }}" class="nav-link">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Users</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ route('company.index') }}" class="nav-link">
                        <i class="nav-icon fa fa-building"></i>
                        <p>
                            Manage Company
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('pic.index') }}" class="nav-link">
                        <i class="nav-icon fa fa-address-book"></i>
                        <p>
                            Manage PIC
                        </p>
                    </a>
                </li>
                @endrole

                
                @role('inputer')
                <li class="nav-item menu-open">
                    <a href="{{ route('order.transaksi') }}" class="nav-link active">
                        <i class="nav-icon fa fa-check"></i>
                        <p>
                            Input data
                        </p>
                    </a>
                </li>
               
    <!--             <li class="nav-item">
                        <a href="{{ route('produk.index') }}" class="nav-link">
                            <i class="nav-icon fa fa-server"></i>
                            <p>
                                Input data
                            </p>
                        </a>
                </li> -->
                <!-- <li class="nav-item">
                    <a href="{{ route('kategori.index') }}" class="nav-link">
                        <i class="fa fa-circle-o nav-icon"></i>
                        <p>Kategori</p>
                    </a>
                </li> -->
                <li class="nav-item has-treeview">
                        <a href="{{ route('manageinput.index') }}" class="nav-link">
                            <i class="nav-icon fa fa-archive"></i>
                            <p>
                                Manage Leads
                                {{-- <i class="right fa fa-angle-left"></i> --}}
                            </p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                    <a href="{{route('users.index')}}" class="nav-link">
                                <i class="nav-icon fa fa-user-circle-o"></i>
                                <p>Manage user</p>
                            </a>
                    </li>
                @endrole

                <li class="nav-item has-treeview">
                    <a class="nav-link" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="nav-icon fa fa-sign-out"></i>
                        <p>
                            {{ __('Logout') }}
                        </p>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
    </div>
</aside>

@section('title')
    <title>Manajemen Leads</title>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">List Data Input</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Order</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <section class="content" id="dw">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        @card
                            @slot('title')
                            Filter Leads
                            @endslot

                            <form action="{{ route('order.index') }}" method="get">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Mulai Tanggal</label>
                                            <input type="text" name="start_date" 
                                                class="form-control {{ $errors->has('start_date') ? 'is-invalid':'' }}"
                                                id="start_date"
                                                value="{{ request()->get('start_date') }}"
                                                >
                                        </div>
                                        <div class="form-group">
                                            <label for="">Sampai Tanggal</label>
                                            <input type="text" name="end_date" 
                                                class="form-control {{ $errors->has('end_date') ? 'is-invalid':'' }}"
                                                id="end_date"
                                                value="{{ request()->get('end_date') }}">
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-primary btn-sm">Cari</button>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Perusahaan</label>
                                            <select name="customer_id" class="form-control">
                                                <option value="">Pilih</option>
                                                @foreach ($customers as $cust)
                                                <option value="{{ $cust->id }}"
                                                    {{ request()->get('customer_id') == $cust->id ? 'selected':'' }}>
                                                    {{ $cust->name }} - {{ $cust->email }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Inputer</label>
                                            <select name="user_id" class="form-control">
                                                <option value="">Pilih</option>
                                                @foreach ($users as $user)
                                                <option value="{{ $user->id }}"
                                                    {{ request()->get('user_id') == $user->id ? 'selected':'' }}>
                                                    {{ $user->name }} - {{ $user->email }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            @slot('footer')

                            @endslot
                        @endcard
                    </div>
                    <div class="col-md-12">
                        @card
                            @slot('title')
                            Data Inputan
                            @endslot

                            <div class="row">
                                <div class="col-4">
                                    <div class="small-box bg-info">
                                        <div class="inner">
                                            <h3>{{ $sold }}</h3>
                                            <p>Item Terjual</p>
                                        </div>
                                        <div class="icon">
                                            <i class="ion ion-bag"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="small-box bg-success">
                                        <div class="inner">
                                            <h3>Rp {{ number_format($total) }}</h3>
                                            <p>Total Omset</p>
                                        </div>
                                        <div class="icon">
                                            <i class="ion ion-stats-bars"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="small-box bg-primary">
                                        <div class="inner">
                                            <h3>{{ $total_customer }}</h3>
                                            <p>Total pelanggan</p>
                                        </div>
                                        <div class="icon">
                                            <i class="ion ion-stats-bars"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Invoice</th>
                                            <th>Pelanggan</th>
                                            <th>No Telp</th>
                                            <th>Total Belanja</th>
                                            <th>Kasir</th>
                                            <th>Tgl Transaksi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($orders as $row)
                                        <tr>
                                            <td><strong>#{{ $row->invoice }}</strong></td>
                                            <td>{{ $row->customer->name }}</td>
                                            <td>{{ $row->customer->phone }}</td>
                                            <td>Rp {{ number_format($row->total) }}</td>
                                            <td>{{ $row->user->name }}</td>
                                            <td>{{ $row->created_at->format('d-m-Y H:i:s') }}</td>
                                            <td>
                                                <a href="{{ route('order.pdf', $row->invoice) }}" 
                                                    target="_blank"
                                                    class="btn btn-primary btn-sm">
                                                    <i class="fa fa-print"></i>
                                                </a>
                                                <a href="{{ route('order.excel', $row->invoice) }}" 
                                                    target="_blank"
                                                    class="btn btn-info btn-sm">
                                                    <i class="fa fa-file-excel-o"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td class="text-center" colspan="7">Tidak ada data transaksi</td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            @slot('footer')

                            @endslot
                        @endcard
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('js')
    <script>
        $('#start_date').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $('#end_date').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        });
    </script>
@endsection