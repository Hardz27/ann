@extends('layouts.master')

@section('title')
    <title>Manajemen Archive</title>
@endsection

@section('css')
 <!-- DataTables -->
    
 <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}" />
 <link rel="stylesheet" type="text/css" href="{{ asset('plugins/tab/css/style.css') }}" />
 <link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker.css') }}" />

    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}"> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables2/css/jquery.dataTables.min.css') }}"> --}}
    {{-- <script src="{{ asset('plugins/datatables2/jquery.dataTables.min.js') }}"></script> --}}
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Manajemen Archive</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Archive</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    
                        <div class="col-md-3 col-sm-6 col-12">
                            <a style="color:inherit" href="{{route('archive.leads')}}">
                                <div class="info-box">
                                  <span class="info-box-icon bg-danger"><i class="fa fa-history"></i></span>
                    
                                  <div class="info-box-content">
                                    <span class="info-box-text">Leads Archive</span>
                                    <span class="info-box-number">{{$archiveLeadsCount}}</span>
                                  </div>
                                  <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                                </a>
                              </div>
                              <div class="col-md-3 col-sm-6 col-12">
                                    <a style="color:inherit" href="{{route('archive.company')}}">
                                    <div class="info-box">
                                      <span class="info-box-icon bg-warning"><i class="fa fa-building"></i></span>
                        
                                      <div class="info-box-content">
                                        <span class="info-box-text">Company Archive</span>
                                        <span class="info-box-number">{{$archiveCompanyCount}}</span>
                                      </div>
                                      <!-- /.info-box-content -->
                                    </div>
                                </a>
                                    <!-- /.info-box -->
                                  </div>
                                  <div class="col-md-3 col-sm-6 col-12">
                                        <a style="color:inherit" href="{{route('archive.pic')}}">
                                        <div class="info-box">
                                          <span class="info-box-icon bg-success"><i class="fa fa-address-book"></i></span>
                            
                                          <div class="info-box-content">
                                            <span class="info-box-text">Pic Archive</span>
                                            <span class="info-box-number">{{$archivePicCount}}</span>
                                          </div>
                                          <!-- /.info-box-content -->
                                        </div>
                                    </a>
                                        <!-- /.info-box -->
                                      </div>
                                      <div class="col-md-3 col-sm-6 col-12">
                                            <a style="color:inherit" href="{{route('archive.user')}}">
                                            <div class="info-box">
                                              <span class="info-box-icon bg-info"><i class="fa fa-users"></i></span>
                                
                                              <div class="info-box-content">
                                                <span class="info-box-text">User Archive</span>
                                                <span class="info-box-number">{{$archiveUserCount}}</span>
                                              </div>
                                              <!-- /.info-box-content -->
                                            </div>
                                        </a>
                                            <!-- /.info-box -->
                                          </div>
                       

                                       
                    {{-- <div class="col-md-12">
                        @card
                            @slot('title')
                            {{-- <a href="" class="btn btn-primary btn-sm"><i class="fa fa-plus mR-5"></i>Tambah Baru</a> 
                            @endslot
                            
                            @if (session('success'))
                                @alert(['type' => 'success'])
                                    {!! session('success') !!}
                                @endalert
                            @endif --}}


                            <div class="container">
                                    <section id="fancyTabWidget" class="tabs t-tabs" style="margin: 10px;">
                                <ul class="nav nav-tabs fancyTabs dots" role="tablist">
                                            <li class="tab fancyTab active" style="border-top-left-radius: 15px;">
                                                <a id="tab0" href="#tabBody0" role="tab" aria-controls="tabBody0" aria-selected="true" data-toggle="tab" tabindex="0">
                                                    @if (auth()->user()->can('leads admin'))
                                                        <mark>{{$newInputCount}}</mark>    
                                                    @else
                                                        
                                                    @endif
                                                    
                                                    <span class="hidden-xs">New Input</span><br>
                                                    <!-- <span class="hidden-xs" style="text-align: center;width: 63%;"></span> -->
                                                <div class="whiteBlock"></div></a>
                                            </li>
                                            
                                            <li class="tab fancyTab">
                                            <!-- <div class="arrow-down"><div class="arrow-down-inner"></div></div> -->
                                            <!-- menggunakan tab-l -->
                                                <a id="tab1" href="#tabBody1" role="tab" aria-controls="tabBody1" aria-selected="true" data-toggle="tab" tabindex="0">
                                                    @if (auth()->user()->can('leads admin'))
                                                        <mark class="orange">{{$unCompliteCount}}</mark>    
                                                    @else
                                                        
                                                    @endif
                                                    
                                                    <span class="hidden-xs">Uncomplete</span> 
                                                <br>
                                                <!-- <span style="text-align: center;width: 60%;"></span> -->
                                                <div class="whiteBlock"></div></a>
                                            </li>
                                            
                                            <li class="tab fancyTab">
                                            <!-- <div class="arrow-down"><div class="arrow-down-inner"></div></div> -->
                                                <a id="tab2" href="#tabBody2" role="tab" aria-controls="tabBody2" aria-selected="true" data-toggle="tab" tabindex="0">
                                                        @if (auth()->user()->can('leads admin'))
                                                        <mark class="green">{{$compliteCount}}</mark><span class="hidden-xs">Complete</span> 
                                                    @else
                                                        
                                                    @endif
                                                    
                                                <br>
                                                <!-- <span style="text-align: center;width: 60%;"></span> -->
                                                <div class="whiteBlock"></div></a>
                                            </li>
                                            
                                            <li class="tab fancyTab" style="border-top-right-radius: 15px; height: 95px">
                                            <!-- <div class="arrow-down"><div class="arrow-down-inner"></div></div> -->
                                                <a id="tab3" href="#tabBody3" role="tab" aria-controls="tabBody3" aria-selected="true" data-toggle="tab" tabindex="0">
                                                    @if (auth()->user()->can('leads admin'))
                                                        <mark class="blue">{{$allinputCount}}</mark><span class="hidden-xs">All Data</span>
                                                    @else
                                                        
                                                    @endif
                                                    
                                                <br>
                                                <!-- <span style="text-align: center;width: 60%;"></span> -->
                                                <div class="whiteBlock"></div></a>
                                            </li> 
                                </ul>
                                
                                    <div id="myTabContent" class="tab-content fancyTabContent" aria-live="polite" 
                                        style="padding-bottom: 14px;margin-bottom: -10px;bottom: 19px;">
                                            <div class="tab-pane  fade active in active show" id="tabBody0" role="tabpanel" aria-labelledby="tab0" aria-hidden="false" tabindex="0">
                                                <div>
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12">
                                                            <div class="table-responsive">
                                                            <table id="new-inp" class="table table-hover">
                                                            <thead style="text-align: center;">
                                                                <tr>
                                                                    <td style="width: 60px;">No</td>
                                                                    <td>Inputer</td>
                                                                    <td>Company Name</td>
                                                                    <td>Domain</td>
                                                                    <td>Office Number</td>
                                                                    <td>Office Email</td>
                                                                    <td>Validation</td>
                        
                                                                    <td>Date & Time created</td>
                                                                    <td>Action</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>       
                        {{-- TABEL NEW INPUT  --}}
                                                         
                                                                 @php $no = 1; @endphp
                                                                @if ( auth()->user()->can('leads admin'))    
                                                                    @forelse ($newInput as $item)
                                                                       {{-- @if ($item->deleted_at == null) --}}
                                                              <tr>
                                                                    <td style="text-align: center;">{{ $no++ }}
                                                                        {{-- @if(session('success'))
                                                                            @if($nama == $item->company_name)
                                                                            <mark class="oranges">edited</mark>
                                                                            @endif
                                                                        @endif --}}
                                                                    </td>
                                                                    <td style="text-align: center;">
                                                                    @if ($item->created_by == 'Fahrur' ) 
                                                                        <label for="" class="badge badge-secondary">{{$item->created_by}}</label>                                                
                                                                        @else
                                                                        <label for="" class="badge badge-success">{{$item->created_by}}</label>    
                                                                    @endif
                                     
                                                                    </td> 
                                                                    <td>{{$item->company_name}}</td>

                                                                    <td>
                                                                        @foreach($item->domain as $key => $domain)
                                                                            {{$domain['domain_name']}}
                                                                        @endforeach
                                                                        
                                                                    </td>
                                                                    <td>{{$item->company_telpon_office}}</td>
                                                                    <td>

                                                                            @foreach($item->email as $key => $email)
                                                                                {{$email['email_name']}}
                                                                            @endforeach
                                                                    </td>
                                                                    <!-- <td></td> -->
                        
                                                                    <td style="text-align: center;">
                        
                                                                        <label for="" class="badge badge-danger">unvalidate</label>
                                                                        
                                                                    </td>
                                                                    <td>{{$item->created_at}}</td>
                                                                    <td style="text-align: center;">
                                                                        @if (auth()->user()->can('leads admin'))   
                                                                        <a href="{{route('archive.form_restore', $item->company_id)}}" class="btn btn-primary btn-sm btn-show-restore"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-recycle"></i></a>
                                                                        <a href="{{route('archive.detail_leads', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a>  
                                                                        {{-- <a href="{{route('archive.detail_company', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a>   --}}
                                                                        {{-- <a href="{{route('lead.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a>   --}}
                                                                            {{-- <i href="{{route('lead.validation', $item->company_id)}}" class="btn btn-success btn-sm btn-show-validation" style="padding: 4px 7px;"><i class="fa fa-check-square-o"></i></i> --}}
                                                                            {{-- <a href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a> --}}
                                                                        @endif
                                                                        @if (auth()->user()->can('leads inputer'))   
                                                                            <a href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a>
                                                                        @endif
                                                                    </td>
                                                                    
                                                                </tr>
                                                                @empty
                                                                <tr>
                                                                    <td colspan="4" class="text-center">Tidak ada data</td>
                                                                </tr> 
                                          
                                                            @endforelse
                                                     @endif
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            @php $modal= 0;     @endphp
                        
{{-- TABLE UNCOMPLITE --}}
                                        
                                            <div class="tab-pane  fade" id="tabBody1" role="tabpanel" aria-labelledby="tab1" aria-hidden="true" tabindex="0">
                                                <div class="row">
                                                        
                                                        <div class="col-md-12">
                                                            <div class="table-responsive">
                                                            <table id="uncom" class="table table-hover">
                                                            <thead style="text-align: center;">
                                                                <tr>
                                                                    <td style="width: 60px;">No</td>
                                                                    <td>Inputer</td>
                                                                    <td>Company Name</td>
                                                                    <td>Domain</td>
                                                                    <td>Office Number</td>
                                                                    <td>Office Email</td>
                                                                    <td>Validation</td>
                                                                    <td>Date & Time Validated</td>
                                                                    <td>Action</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                
                                                                @php $no = 1;@endphp
                                                                @if (auth()->user()->can('leads admin'))
                                                                    
                                                                @forelse ($unComplite as $item)
                                                                @php
                                                                            $company = DB::table('t_companies')->where('t_companies.company_id', $item->company_id)
                                                                                      ->where('t_companies.company_validation', '<', 24576)->count();
                                                                            $domain = DB::table('t_company_domains')->where('t_company_domains.company_id', $item->company_id)
                                                                                       ->where('t_company_domains.domain_validation', '=', null)->count();
                                                                            $notes = DB::table('inputers')->select('inputer_note_company')->where('company_id', $item->company_id)
                                                                                    ->where('inputer_note_company', '!=', null)->first();
                                                                            $email = DB::table('t_company_emails')->where('t_company_emails.company_id', $item->company_id)
                                                                                  ->where('t_company_emails.email_validation', '=', null)->count();   
                        
                                                                @endphp
                                
                                                                
                        
                                                                
                                                                
                                                                @if (!$item->deleted_at)
                                                                    @if($item->company_validation == 0 && ($item->domain_validation != 1 && $item->email_validation != 1))
                                                                        
                                                                    @else
                        
                                                                <tr>
                                                                    
                                                                    <td style="text-align: center;">{{ $no++ }}
                                                                    {{-- @if  (session('success'))
                                                                        @if($nama == $item->company_name)
                                                                            <mark class="oranges">edited</mark>
                                                                        @endif
                                                                    @endif --}}
                                                                    </td>
                                                                    <td style="text-align: center;">
                                                                    
                                                                            
                        
                                                                         
                                                                            @if ($item->created_by == 'Fahrur' )   
                                                                                <label for="" class="badge badge-secondary">{{$item->created_by}}</label>
                                                                            @else 
                                                                            
                                                                                <label for="" class="badge badge-success">{{$item->created_by}}</label>    
                                                                            @endif
                                                                            
                                                                        </td>
                                                                    <td >{{$item->company_name}}</td>
                                                                    <td>
                                                                            @foreach($item->domain as $key => $domain)
                                                                                {{$domain['domain_name']}}
                                                                            @endforeach
                                                                            
                                                                        </td>
                                                                    <td >{{$item->company_telpon_office}}</td>
                                                                    <td>

                                                                            @foreach($item->email as $key => $email)
                                                                                {{$email['email_name']}}
                                                                            @endforeach
                                                                    </td>
                                                                    <!-- <td></td> -->
                                                                    <td style="text-align: center;">
                                                                    {{-- @if ($validation = true) --}}
                        
                                                                        @php                                                        
                                                                        
                                                                        if($notes){
                        
                                                                            echo "<label for='' class='badge badge-danger'>*</label> ";
                                                                        }
                                                                        else{
                                                                         
                                                                        }
                                                                        
                                                                        if($item->company_validation >= 16384 && $item->company_validation < 24576){
                                                                            echo '<label for="" class="badge badge-warning">telpon</label> ';
                                                                        }
                                                                        else if($item->company_validation < 16384 && $item->company_validation >= 8192){
                                                                            echo '<label for="" class="badge badge-warning">name</label> ';
                                                                        }
                                                                        else if($item->company_validation < 8192){
                                                                            echo '<label for="" class="badge badge-warning">name</label> ';
                                                                            echo '<label for="" class="badge badge-warning">telpon</label> ';
                                                                        }
                                                                          
                                                                        if ($domain > 0 ) {
                                                                               echo "<label for='' class='badge badge-warning'>domain</label> ";
                                                                        } else {
                                                                              
                                                                        }
                                                                
                                                                         if($email > 0 ){
                                                                            echo "<label for='' class='badge badge-warning'>email</label> ";
                                                                         }
                         
                                                                         @endphp
                        
                                                                            
                                                                        
                                                                    </td>
                                                                    {{-- <td>{{$item->created_at}}</td> --}}
                                                                    <td style="text-align: center;">
                                                                        
                                                                        
                                                                            <input type="hidden" name="_method" value="DELETE">
                                                                            
                        
                                                                            {{-- @foreach ($role as $row)
                                                                                
                                                                            @if ($row->name == 'admin')
                                                                                <label for="" class="badge badge-secondary">{{$item->created_by}}</label>    
                                                                            @else
                                                                                <label for="" class="badge badge-success">{{$item->created_by}}</label>
                                                                            @endif
                                                                            @break
                                                                            
                                                                        @endforeach --}}
                        
                                                                        
                                                                            
                                                                        @if (auth()->user()->can('leads admin'))   
                                                                            
                                                                            {{-- <i href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></i> --}}
                                                                            <a href="{{route('archive.form_restore', $item->company_id)}}" class="btn btn-primary btn-sm btn-show-restore"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-recycle"></i></a>
                                                                            <a href="{{route('archive.detail_leads', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a>  
                                                                            {{-- <i href="{{route('lead.delete_lead', $item->company_id)}}" class="btn btn-danger btn-sm btn-show-delete"><i class="fa fa-archive"></i></i> --}}
                                                                        @endif
                                                                        @if (auth()->user()->can('leads inputer')) 
                                                                            <a href="{{route('lead.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a>  
                                                                            <a href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a>
                                                                        @endif
                                                                      
                                                                    </td>
                                                                </tr>
                                                                    @endif
                                                                @endif
                                                                @empty
                                                                
                                                                <tr>
                                                                    <td colspan="4" class="text-center">Tidak ada data</td>
                                                                </tr>
                                                                 
                                                                @endforelse
                                                                

                                                                
                                                                @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                            
                                                           
                                                        </div>
                                                    </div>
                                            </div>
                        
                        
                        {{-- TABLE COMPLITE --}}
                        
                                            <div class="tab-pane  fade" id="tabBody2" role="tabpanel" aria-labelledby="tab2" aria-hidden="true" tabindex="0">
                                                <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="table-responsive">
                                                            <table id="com" class="table table-hover">
                                                            <thead style="text-align: center;">
                                                                <tr>
                                                                    <td style="width: 60px;">No</td>
                                                                    <td>Inputer</td>
                                                                    <td>Company Name</td>
                                                                    <td>Domain</td>
                                                                    <td>Office Number</td>
                                                                    <td>Office Email</td>
                                                                    <td>Validation</td>
                                                                    <td>Date & Time Validated</td>
                                                                    <td>Action</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                
                                                               
                        
                                                                @php $no = 1; @endphp
                                                                @if (auth()->user()->can('leads admin'))
                                                                    
                                                                
                                                                
                        
                                                                @forelse ($complite as $item)
                                             
                                                                <tr>
                                                                    <td style="text-align: center;">{{ $no++ }}
                                                                        {{-- @if  (session('success'))
                                                                            @if($nama == $item->company_name)
                                                                                <mark class="oranges">edited</mark>
                                                                            @endif
                                                                        @endif --}}
                                                                    </td>
                                                                    <td style="text-align: center;">
                                                                            @if ($item->created_by == 'Fahrur' )   
                                                                                <label for="" class="badge badge-secondary">{{$item->created_by}}</label>
                                                                            @else 
                                                                            
                                                                                <label for="" class="badge badge-success">{{$item->created_by}}</label>    
                                                                            @endif
                                                                        </td>
                                                                    <td>{{$item->company_name}}</td>
                                                                    <td>
                                                                            @foreach($item->domain as $key => $domain)
                                                                                {{$domain['domain_name']}}
                                                                            @endforeach
                                                                            
                                                                        </td>
                                                                    <td>{{$item->company_telpon_office}}</td>
                                                                    <td>

                                                                            @foreach($item->email as $key => $email)
                                                                                {{$email['email_name']}}
                                                                            @endforeach
                                                                    </td>
                                                                    <!-- <td></td> -->
                        
                                                                    <td style="text-align:center;">
                        
                                                                        @if ($item->validation = true)
                                                                        
                                                                        <label for="" class="badge badge-success">name</label>
                                                                        <label for="" class="badge badge-success">telpon</label>    
                                                                        <label for="" class="badge badge-success">domain</label>
                                                                        <label for="" class="badge badge-success">email</label>
                                                                        @endif 
                                                                        
                                                                        
                                                                    </td>
                                                                    <td>{{$item->created_at}}</td>
                                                                    <td style="text-align: center;">
                                                                        
                                                                            
                                                                    @if (auth()->user()->can('leads admin'))  
                                                                        <a href="{{route('archive.form_restore', $item->company_id)}}" class="btn btn-primary btn-sm btn-show-restore"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-recycle"></i></a>
                                                                        <a href="{{route('archive.detail_leads', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a>  
                                                                            {{-- <i href="{{route('lead.validation', $item->company_id)}}" class="btn btn-success btn-sm btn-show-validation" style="padding: 4px 7px;"><i class="fa fa-check-square-o"></i></i> --}}
                                                                            {{-- <a href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm"  style="padding: 4px 6px;"><i class="fa fa-edit"></i></a> --}}
                                                                            {{-- <a href="{{route('lead.delete_lead', $item->company_id)}}" class="btn btn-danger btn-sm btn-show-delete"><i class="fa fa-archive"></i></a> --}}
                                                                    @endif
                                                                    @if (auth()->user()->can('leads inputer'))
                                                                            <a href="{{route('lead.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a>   
                                                                            <a href="{{ route('lead.editForm', $item->company_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a>
                                                                    @endif
                                                                    </td>
                                                                </tr>
                                                                @empty 
                                                                <tr>
                                                                    <td colspan="4" class="text-center">Tidak ada data</td>
                                                                </tr> 
                                                               @endforelse  
                                                                @endif
                                                            </tbody>
                                                        </table>
                                                    </div>                                    
                                                          
                                                        </div>
                                                    </div>
                                            </div>
                        
                        {{-- TABLE ALLDATA --}}
                                            <div class="tab-pane  fade" id="tabBody3" role="tabpanel" aria-labelledby="tab3" aria-hidden="true" tabindex="0">
                                                <div class="row">
                                                    <div class="table-responsive">
                                                            <table id="all" class="table table-hover">
                                                            <thead style="text-align: center;">
                                                                <tr>
                                                                    <td style="width: 60px;">No</td>
                                                                    <td>Inputer</td>
                                                                    <td>Company Name</td>
                                                                    <td>Domain</td>
                                                                    <td>Office Number</td>
                                                                    <td>Office Email</td>
                                                                    <td>Date & Time Validated</td>
                                                                    <td>Action</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                
                                                                @php $no = 1; @endphp
                                                                @if (auth()->user()->can('leads admin'))
                                                                    
                                                                
                                                                @forelse ($allInput as $item)
                                                                
                                                                <tr>
                                                                    <td style="text-align: center;">{{ $no++ }}
                                                                        {{-- @if  (session('success'))
                                                                            @if($nama == $item->company_name)
                                                                                <mark class="oranges">edited</mark>
                                                                            @endif
                                                                        @endif --}}
                                                                    </td>
                                                                    <td style="text-align: center;">
                                                                        
                                                                            @if ($item->created_by == 'Fahrur' )   
                                                                            <label for="" class="badge badge-secondary">{{$item->created_by}}</label>
                                                                        @else 
                                                                                                                     
                                                                            <label for="" class="badge badge-success">{{$item->created_by}}</label>    
                                                                        @endif
                                                                                
                                                                        </td> 
                                                                    <td>{{$item->company_name}}</td>
                                                                    <td>
                                                                            @foreach($item->domain as $key => $domain)
                                                                                {{$domain['domain_name']}}
                                                                            @endforeach
                                                                            
                                                                        </td>
                                                                    <td>{{$item->company_telpon_office}}</td>
                                                                    <td>

                                                                            @foreach($item->email as $key => $email)
                                                                                {{$email['email_name']}}
                                                                            @endforeach
                                                                    </td>
                                                                    
                        
                                                                    <td>{{$item->created_at}}</td>
                                                                    <td style="text-align: center;">
                                                                        @if (auth()->user()->can('leads admin'))   
                                                                            {{-- <i href="{{route('lead.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail" style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></i> --}}
                                                                            <a href="{{route('archive.form_restore', $item->company_id)}}" class="btn btn-primary btn-sm btn-show-restore"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-recycle"></i></a>
                                                                            <a href="{{route('archive.detail_leads', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a>  

                                                                            {{-- <a href="{{route('lead.delete_lead', $item->company_id)}}" class="btn btn-danger btn-sm btn-show-delete" style="margin-top: 5px;"><i class="fa fa-archive"></i></a>         --}}
                                                                        @endif
                                                                        @if (auth()->user()->can('leads inputer'))   
                                                                            {{-- <a href="{{route('lead.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"><i class="fa fa-info"></i></a> --}}
                                                                            <a href="{{route('check.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail" style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a>
                                                                        @endif
                                                                        
                                                                        
                                                                    </td>
                                                                </tr>
                                                                @empty
                                                                <tr>
                                                                    <td colspan="4" class="text-center">Tidak ada data</td>
                                                                </tr>
                                                                @endforelse
                                                                
                                                                @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            
    {{-- <div class="card">
            <div class="card-header">
              <h3 class="card-title">PIC Archive </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Company name</th>
                  <th>Company Telpon</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                  <th>CSS grade</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($archiveLeads as $item)
                        

                <tr>
                  <td>{{$item->company_name}}</td>
                  <td>{{$item->company_telpon}}</td>
                  <td>{{$item->domain_name}}</td>
                  <td>{{$item->email_name}}</td>
                  <td>{{$item->pic_name}}</td>
                </tr>
                    
                @endforeach
                </tbody>
             
              </table>
            </div>
            <!-- /.card-body -->
          </div>
      
          <!-- Control Sidebar -->
     --}}

        {{-- </div>

                            @slot('footer')

                            @endslot
                        @endcard --}}
                    </div>
                </div>
            </div>
        </section>
    </div>

    {{-- @include('users.delete') --}}

    





@endsection


@section('js')

<script src="{{ asset('plugins/tablefilter/tablefilter.js') }}"></script>
<script src="{{ asset('js/filter-new-input.js') }}"></script> <!-- ini dibutuhin di leads -->
<script src="{{ asset('js/filter-uncom.js') }}"></script> <!-- ini dibutuhin di leads -->
<script src="{{ asset('js/filter-com.js') }}"></script> <!-- ini dibutuhin di leads -->
<script src="{{ asset('js/filter-all.js') }}"></script> <!-- ini dibutuhin di leads -->
<script src="{{ asset('js/pop-up.js') }}"></script> 
<script src="{{ asset('js/validation.js') }}"></script>    
<script src="{{ asset('plugins/tab/js/index.js') }}"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/daterangepicker.min.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@if (auth()->user()->can('leads admin'))
    {{-- <script src="{{ asset('js/append_export_button.js') }}"></script> --}}
@else
    
@endif

<script src="{{ asset('js/filter_date.js') }}"></script>


{{-- 
<script>
        $(function () {
          $("#example1").DataTable();
          $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
          });
        });
</script>
    <script src="{{ asset('plugins/datatables2/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
     --}}
    {{-- <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/tablefilter/tablefilter.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/pop-up.js') }}"></script>
    <script src="{{ asset('js/validation.js') }}"></script> --}}

@endsection