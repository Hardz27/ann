@extends('layouts.master')

@section('title')
    <title>Manajemen Archive</title>
@endsection

@section('css')
 <!-- DataTables -->

    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables2/css/jquery.dataTables.min.css') }}"> --}}
    {{-- <script src="{{ asset('plugins/datatables2/jquery.dataTables.min.js') }}"></script> --}}
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Manajemen Archive</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Archive</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">


                        <div class="col-md-3 col-sm-6 col-12">
                                <a style="color:inherit" href="{{route('archive.leads')}}">
                                    <div class="info-box">
                                      <span class="info-box-icon bg-danger"><i class="fa fa-history"></i></span>
                        
                                      <div class="info-box-content">
                                        <span class="info-box-text">Leads Archive</span>
                                        <span class="info-box-number">{{$archiveLeadsCount}}</span>
                                      </div>
                                      <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                    </a>
                                  </div>
                              <div class="col-md-3 col-sm-6 col-12">
                                    <a style="color:inherit" href="{{route('archive.company')}}">
                                    <div class="info-box">
                                      <span class="info-box-icon bg-warning"><i class="fa fa-building"></i></span>
                        
                                      <div class="info-box-content">
                                        <span class="info-box-text">Company Archive</span>
                                        <span class="info-box-number">{{$archiveCompanyCount}}</span>
                                      </div>
                                      <!-- /.info-box-content -->
                                    </div>
                                </a>
                                    <!-- /.info-box -->
                                  </div>
                                  <div class="col-md-3 col-sm-6 col-12">
                                        <a style="color:inherit" href="{{route('archive.pic')}}">
                                        <div class="info-box">
                                          <span class="info-box-icon bg-success"><i class="fa fa-address-book"></i></span>
                            
                                          <div class="info-box-content">
                                            <span class="info-box-text">Pic Archive</span>
                                            <span class="info-box-number">{{$archivePicCount}}</span>
                                          </div>
                                          <!-- /.info-box-content -->
                                        </div>
                                    </a>
                                        <!-- /.info-box -->
                                      </div>
                                      <div class="col-md-3 col-sm-6 col-12">
                                            <a style="color:inherit" href="{{route('archive.user')}}">
                                            <div class="info-box">
                                              <span class="info-box-icon bg-info"><i class="fa fa-users"></i></span>
                                
                                              <div class="info-box-content">
                                                <span class="info-box-text">User Archive</span>
                                                <span class="info-box-number">{{$archiveUserCount}}</span>
                                              </div>
                                              <!-- /.info-box-content -->
                                            </div>
                                        </a>
                                            <!-- /.info-box -->
                                          </div>
                       

                       

                                       
                    <div class="col-md-12">
                        {{-- @card --}}
                            @slot('title')
                            {{-- <a href="" class="btn btn-primary btn-sm"><i class="fa fa-plus mR-5"></i>Tambah Baru</a> --}}
                            @endslot
                            
                            @if (session('success'))
                                @alert(['type' => 'success'])
                                    {!! session('success') !!}
                                @endalert
                            @endif

                            
    <div class="card card-warning">
            <div class="card-header">
              <h3 class="card-title">Company Archive</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body mailbox-messages" >
              <form action="/archive/restore_all">
              <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr>
                  <th style="width: 1%;">No</th>
                  <th>Company Name</th>
                  <th>Company Telpon</th>
                  <th>Company Address</th>
                  <th>Company Field</th>
                  <th>Company Status</th>
                  <th>Action</th>
                  <th style="width: 15% text-align: center; border: 2px solid #9A9B93;">
                                            
                      <div class="icheck-primary" style="padding-left:13%">  
                          <input   type="checkbox" id="checkall" onclick="checkAll()">
                          <label style="padding-top:5%" for="checkall"></label>
                          <button class="btn btn-danger btn-sm" name="archive" value="1" type='submit'><i class="fa fa-archive"></i></button> 
                      </div>
                  </th>
                </tr>
                </thead>
                <tbody>
                  @php
                      $no = 1;
                  @endphp
                    @foreach ($archiveCompany as $item)
                <tr>
                  <td style="text-align:center">{{$no++}}</td>
                  <td>{{$item->company_name}}</td>
                  <td>{{$item->company_telpon_office}}</td>
                  <td>{{$item->company_address}}</td>
                  <td>{{$item->company_field}}</td>
                  <td>{{$item->company_status}}</td>
                  <td style="text-align:center">
                      <a href="{{route('archive.form_restore', $item->company_id)}}" class="btn btn-primary btn-sm btn-show-restore"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-recycle"></i></a>
                      <a href="{{route('archive.detail_company', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-info"></i></a>  
                  </td>
                  <td style="width: 10%; text-align: center;">
                      <div class="icheck-primary">
                          <input type="checkbox" class="checkbox" value="{{$item->company_id}}" name="checked[]" id="check{{$no}}">
                          <label for="check{{$no}}"></label>
                      </div>    
                  </td>
                </tr>
                    {{-- @endif --}}
                @endforeach
                </tbody>
             
              </table>
            </form>
            </div>
            <!-- /.card-body -->
          </div>
      
          <!-- Control Sidebar -->
    
        </div>
                            @slot('footer')
                            @endslot
                        {{-- @endcard --}}
                    </div>
                </div>
            </div>
        </section>
    </div>

    {{-- @include('users.delete') --}}

    





@endsection





@section('js')

<script>
   function checkAll()
    {
        var checkall = $('#checkall')[0];
        
        if (checkall.checked == true) {
            $(".checkbox").prop('checked', 'checked');
        } else {
            $(".checkbox").prop('checked', '');
        }

    }
        // $(function () {
        //   $("#example1").DataTable();
        //   $('#example2').DataTable({
        //     // "paging": true,
        //     // // "lengthChange": false,
        //     // "searching": false,
        //     // // "ordering": true,
        //     // "info": true,
        //     // "autoWidth": false,
        //   });
        // });
</script>
    <script src="{{ asset('plugins/datatables2/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/pop-up.js') }}"></script> 

@endsection