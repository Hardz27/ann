
<form action="/company"  method="post">
    {{-- <div class="modal-body"> --}}

  <!-- ============================================================================= -->

  <div class="card">
    <div class="card-header">
      <h3 class="card-title">User Archive Data </h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="card-body p-0">
        
      <table class="table table-striped projects">
          <thead>
              <tr>
                  <th style="width: 2%">
                      #
                  </th>
                  <th style="width: 30%">
                      User Name
                  </th>
                  <th style="width: 50%">
                      User Email 
                  </th>
                  <th style="width: 18%" class="text-center">
                      User Role
                  </th>
              </tr>
          </thead>
          <tbody>
              
              <tr>
                  <td>
                      #
                  </td>
                  <td>
                      <a>
                        
                        {{$user->user_name}}
                      </a>
                  </td>
                  <td>
                      <ul class="list-inline">
                        <li class="list-inline-item">
                              {{$user->user_email}}
                        </li>                 
                      </ul>
                  </td>
                  <td class="project-state" style="text-align: center;">

                    @foreach ($user->getRoleNames() as $role)
                    <label for="" class="badge badge-secondary">{{$role}}</label>
                  @endforeach   
                    
{{--                     
                    @foreach ($role as $row)
                                                
                        @if ($row->name == 'admin')
                            <label for="" class="badge badge-secondary">{{$pic->created_by}}</label>    
                        @else
                            <label for="" class="badge badge-success">{{$pic->created_by}}</label>
                        @endif
                        @break
                        
                    @endforeach
                     --}}
                  </td>
              </tr>
          
          </tbody>
      </table>          
    </div>
    </div> 

    {{-- </div>   --}}
    </form>

    <div class="modal-footer" style="text-align: center;">
      <form action="{{ route('archive.restore_user', $user->user_id) }}" method="POST">
          @csrf
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          <button class="btn btn-primary">Restore to Users <i class="nav-icon fa fa-users"></i></button>
       </form>
    </div>
