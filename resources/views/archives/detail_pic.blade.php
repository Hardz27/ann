
        @php
        
        function validation($n){
             $i = 0; 
             $count = $n;
             while ($n > 0) 

             { 
                 $binaryNum; 
                 $binaryNum[$i] = $n % 2; 
                 $n = (int)($n / 2); 
                 $i++; 
             }
             for ($j = $i - 1; $j >= 0; $j--){ 
                 $bin_val[] = $binaryNum[$j];
             }


             while(count($bin_val)< 7){
                array_unshift($bin_val, 0);
           }
           
             return $bin_val;
        }
        
    //     if($pics->pics_validation != 0){
    //       $arr = validation($pics->pics_validation);
    //     }
    //     else{
    //     $arr = array(0,0,0,0,0,0,0);

    //   }

      if ($pics->pic_validation != 0) {
          $arr = validation($pics->pic_validation);
        //   dd($arr);
      } else {
        $arr = array(0,0,0,0,0,0,0);
      }
      
      
        
     @endphp


 
        
        <form action="/pic"  method="post" id="detailform">
           
        <div class="modal-body">
                
          <div class="card">
        <div class="card-header">
          <h3 class="card-title">PIC Information</h3>
          
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        
        <div class="card-body p-0">
                
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%">
                          No
                      </th>
                      <th style="width: 30%">
                          Data
                      </th>
                      <th style="width: 50%">
                          Isi
                      </th>
                      <th style="width: 18%" class="text-center">
                          Status
                      </th>
                  </tr>
              </thead>
              <tbody>

                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                        
                          <a>
                              PIC Name
                          </a>
                      </td>
                      <td>
                         
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                    {{$pics->pic_name}}
                            </li>                 
                          </ul>
                      </td>

                    {{-- <td class="project-state" style="text-align: center;">
                        @if ($companies->company_validation == true)
                            <span class="badge badge-success">Verivied</span>
                        @else
                            <span class="badge badge-danger">Unverified</span>
                        @endif
                    </td> --}}

                    <td class="project-state" style="text-align: center;">
                        @if ($arr[0] == 1)
                            <span class="badge badge-success">Verivied</span>
                        @elseif($arr[0] == 0)
                            <span class="badge badge-danger">Unverified</span>
                        @endif
                    </td>

                    </tr>

                    
                    <tr>
                        <td>
                            #
                        </td>
                        <a>
                            <td>
                                PIC posotion
                        </td>
                        </a>
                        
                        <td>
                            <ul class="list-inline">
                              <li class="list-inline-item">
                                    {{$pics->pic_position}}
                              </li>                 
                            </ul>
                        </td>
  
                        <td class="project-state" style="text-align: center;">
                            @if ($arr[1] == 1)
                                <span class="badge badge-success">Verivied</span>
                            @elseif($arr[1] == 0)
                                <span class="badge badge-danger">Unverified</span>
                            @endif
                      
                        </td>
  
                    </tr>

                   <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              PIC Division
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$pics->pic_division}}
                            </li>                 
                          </ul>
                      </td>
                    
                        
                      <td class="project-state" style="text-align: center;">
                        @if ($arr[2] == 1)
                            <span class="badge badge-success">Verivied</span>
                        @elseif($arr[2] == 0)
                            <span class="badge badge-danger">Unverified</span>
                        @endif
                        
                    </td>
                      {{-- <td class="project-state" style="text-align: center;">
                            <span class="badge badge-warning">Success</span>
                      </td> --}}

                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              PIC Phone-number
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$pics->pic_nohp}}
                            </li>                 
                          </ul>
                      </td>

                      <td class="project-state" style="text-align: center;">
                        @if ($arr[3] == 1)
                            <span class="badge badge-success">Verivied</span>
                        @elseif($arr[3] == 0)
                            <span class="badge badge-danger">Unverified</span>
                        @endif

                    </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              PIC email
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$pics->pic_email}}
                            </li>                 
                          </ul>
                      </td>

                      <td class="project-state" style="text-align: center;">
                        @if ($arr[4] == 1)
                        <a class="btn btn-primary btn-sm" target="_blank" href="https://mail.google.com/mail/u/0/#inbox?compose=new?authuser=user@gmail.com" style="margin-top: 5px;">
                            <i class="fa fa-send" style="padding-right: 5px;"></i>
                        </a>
                        {{-- <a class="btn btn-primary btn-sm btn-show-detail" href="{{route('pic.email')}}" style="margin-top: 5px;">
                            <i class="fa fa-send" style="padding-right: 5px;"></i>
                        </a> --}}
                        
                            {{-- <span class="badge badge-success">Verivied</span> --}}
                        @elseif($arr[4] == 0)
                        {{-- <a class="btn btn-primary btn-sm " href="{{route('pic.email')}}" style="margin-top: 5px;">
                            <i class="fa fa-send" style="padding-right: 5px;"></i>
                        </a> --}}
                    <a class="btn btn-primary btn-sm" target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to={{$pics->pic_email}}&su=HRD-Erporate" style="margin-top: 5px;">
                        {{-- <a class="btn btn-primary btn-sm" target="_blank" href=https://mail.google.com/mail/?view=cm&fs=1&to=fahrurozi@gmail.com&su=SUBJECT&body=BODY&bcc=someone.else@example.com" style="margin-top: 5px;"> --}}
                            <i class="fa fa-send" style="padding-right: 5px;"></i>
                        </a>
                        
                            {{-- <span class="badge badge-danger">Unverified</span> --}}
                        @endif
                       
                    </td>

                  </tr>

                 

                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              PIC Media
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$pics->pic_sosmed}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                        @if ($arr[5] == 1)
                            <span class="badge badge-success">Verivied</span>
                        @elseif($arr[5] == 0)
                            <span class="badge badge-danger">Unverified</span>
                        @endif
                    </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Pic Status
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$pics->pic_status}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                        @if ($arr[6] == 1)
                            <span class="badge badge-success">Verivied</span>
                        @elseif($arr[6] == 0)
                            <span class="badge badge-danger">Unverified</span>
                        @endif
                        
                    </td>
              
                  </tr>
              </tbody>
          </table>
        </div>
    </div>
</div>
{{-- @php
                        dd("apa")   
                       @endphp
 --}}


        <!-- /.card-body -->

