@extends('layouts.master')

@section('title')
    <title>Manage Check</title>
@endsection

@section('css')
    <style type="text/css">
        .tab-pane{ 
            height:150px;
            overflow-y:scroll;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/jquery.dataTables.min.css')}}">
@endsection




@section('content')
    <div class="content-wrapper" >
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Manage Check Leads</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Manage Check Leads</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        @card
                        
                            @slot('title')
                            <div class="row">
                                <div class="col-sm-6">
                                  <h4 class="card-title">Form</h4>
                                </div>
                              </div>
                            
                            @endslot


                            <form action="{{route('check.generate')}}" class="validate-form show-modal" method="post">

                                @csrf
                                    @if (session('error'))
                                        <div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert">&times;</a>Pastikan portal dan ul yang digunakan telah sesuai </div>
                                    @endif
                                    {{-- @if (session('generated'))
                                    @php echo "generta"; @endphp
                                        <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data bisa didaftarkan </div>
                                    @endif --}}
                                    @if (session('success'))
                                        <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data berhasil diinput</div>
                                    @endif
                                   
                                    <div class="col-sm-12">
                              <!-- textarea -->
                              <div class="form-group">
                              <label>Website Portal</label>
                              <select name="portal" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                <option selected="selected">LinkedIn</option>
                                <option>Glints</option>
                                <option>Loker.id</option>
                                <!-- <option>Delaware</option>
                                <option>Tennessee</option>
                                <option>Texas</option>
                                <option>Washington</option> -->
                              </select>
                            </div>
                              <div class="form-group">
                                <label>Textarea</label>
                                <textarea class="form-control" rows="3" name="data" placeholder="Paste here : code <ul ....."></textarea>
                              </div>
                            </div>

                                    <div class="form-group float-right">
                                            <button id="check" type="submit" class="btn btn-success btn-md">Submit Query</button>
                                    </div>
                            </form>


                            {{-- form history data --}}


                            @slot('footer')

                            @endslot
                        @endcard
                    </div>

                   
                    <div class="col-md-12">
                            @card
                                @slot('title')
                                Data Leads
                                @endslot
    <form name="domain_input" action="{{route('check.web.domain')}}" class="validate-form show-modal" method="post">
       <div class="card">
        <div class="card-header">
          <h3 class="card-title">Card</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <!-- <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> -->
          </div>
        </div>
        <div class="card-body p-0">
          <div class="table-responsive">
            @csrf                 
            <table id="exceltable" class="table table-striped projects">
              <tbody>
              
                <tr>
                  <th>No</th>
                  <th>Company name</th>
                  <th>Job Position</th>
                  <th>URL</th>
                  <th>Domain</th>
                  <th>created at</th>
                </tr>
                {{-- @if (session('generated')) --}}
                @php 
                	$i = 0;
                	$tampil = '';
                @endphp
                @foreach($companies as $row)
                @php
                	$tampil = 'yes';
                    $company = DB::table('temp_companies')->select('temp_company_id')
                    	   ->where('temp_company_name', $row->temp_company_name)
	                       ->latest('temp_company_id')
	                       ->first();
	                $job = DB::table('jobs')->where('temp_company_id', $company->temp_company_id)
	                       ->get();
	                $domain = DB::table('t_company_domains')
	                    	->where('company_id', $job[0]->company_id)
		                    ->latest('company_id')
		                    ->first();	                 	 
	                
	                if(count($job) == 1){
	                	$datetime1 = new DateTime(date('Y-m-d'));
						$datetime2 = new DateTime($job[0]->job_time);
						$difference = $datetime1->diff($datetime2);
						
						if($difference->days >= 15 ){
							$tampil = "no";
						}
						else{
							$tampil = "yes";
						}
	                	//dd($job[0]->job_time);
	            	}

					// dd($job->job_position);
                @endphp
                @if($tampil == 'yes')
                <tr>
                   <td>{{ $i+1 }}</td> 
                     <td>{{ $row->temp_company_name }}</td>
                     <input type="hidden" name="company_name[]" value="{{ $row->temp_company_name }}"/>
                     <td>
                     @foreach($job as $job_pos)
                     @php
                     	$datetime1 = new DateTime(date('Y-m-d'));
						$datetime2 = new DateTime($job_pos->job_time);
						$difference = $datetime1->diff($datetime2);					
					 @endphp
					 @if($difference->days < 15)
					 	@if($difference->days == 0)
                     		{{ $job_pos->job_position  .'('. 'Today' .')' .', ' }}
                     	@else
                     		{{ $job_pos->job_position  .'('. $difference->days.')' .', ' }}
                     	@endif
	                    @php
	                     if($job_pos->company_id){
	                     	 $domain = DB::table('t_company_domains')
	                    	   ->where('company_id', $job_pos->company_id)
		                       ->latest('company_id')
		                       ->first();
	                 	 }
	                    //dd($job_pos->job_link);
		                 @endphp
                     @endif
                     @endforeach
                     </td>
                     @if($job_pos->job_link != '-') 
                     <td> <a href="{{ $job_pos->job_link }}" target="_blank" class="btn btn-success">Check</a> </td>
                     @else
                     <td> - </td>
                     @endif
                     @if($domain)
                     <td>{{ $domain->domain_name }}</td> 
                     <input type="hidden" name="company_domain[]" value="{{ $domain->domain_name  }}"/>
                     @else
                     <td><input type="text" name="company_domain[]" class="form-control" value="-" required></td> 
                     @endif
                     <td>{{ $row->temp_company_time}}</td>
                     
                </tr>
                @php $i++; @endphp
                @endif
                @endforeach
                @if($i == 0)
                <tr>
                     <td colspan="6" class="text-center">Tidak ada data</td> 
                </tr>
              @endif
              </tbody>
            </table>
           </div>
          </div>
          <!-- /.card-body -->
        </div>
        <button type="submit" class="btn btn-primary btn-md">Save</button>
      </form>

                        @slot('footer')
                        @endslot
                    @endcard
                    </div>

                </div>
            </div>
        </section>

    </div>

    @include('checks.excel_sample')
    </div>
    </div>

@endsection

  
@section('js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-csv/0.71/jquery.csv-0.71.min.js"></script>
  <script src="{{ asset('public/js/pop-up.js') }}"></script> 
  <script src="{{ asset('public/js/jquery.dataTables.min.js') }}"></script>
  <!-- <script src="{{ asset('js/jquery.csv.js') }}"></script>
  <script src="{{ asset('js/jquery.csv.min.js') }}"></script> -->
  <script src="{{ asset('public/plugins/tablefilter/tablefilter.js') }}"></script>
  <script src="{{ asset('public/js/xlsx.core.min.js') }}"></script>  <!-- ini dibutuhin di check  -->
  <script src="{{ asset('public/js/xls.core.min.js') }}"></script>  <!-- ini dibutuhin di check  -->
  <script src="{{ asset('public/js/excel.js') }}"></script>
@endsection

