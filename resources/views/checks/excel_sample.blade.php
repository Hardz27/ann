<div class="modal fade" id="example" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="text-align: center; background: #238ae6; color: white;">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title" style="text-align: center;">Example Excel Format</h4>
          <!-- <p style="position: absolute;right: 20px;float: right;top: 6px;">Atau bisa download di sini!</p> -->
           <a class="btn btn-danger btn-sm" href="{{ asset('download/sample.xls') }}">
            Download!
            <i class="fa fa-file-excel-o"></i>
          </a>
        </div>
        <form action="#"  method="post">
        <div class="modal-body">

      <!-- ============================================================================= -->

      <div class="card">
        <div class="card-body p-0">
            <center>
              <div>
                <img src="{{ asset('images/excel_sample_img.png')}}" style="max-width: 100%">
              </div>
              <div class="caption">
                <caption>Bagian pertama sheet harus memiliki read_col[nomor kolom], contoh : read_col1, read_col2 dst.</caption><br>
                <caption>Extensi atau type file yang support adalah : .xls, .xlsx</caption><br>
              </div>
            </center>
        </div>
        </div>  
        </div>  
        </form>
        <div class="modal-footer" style="text-align: center;">
        </div>
      </div>
      </div>
    </div>