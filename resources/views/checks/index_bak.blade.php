
@extends('layouts.master')
@section('title')
<title>Manage Check</title>
@endsection

@section('css')
    <style type="text/css">
    .tab-pane{ 
        height:150px;
        overflow-y:scroll;
    }
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('public/css/jquery.dataTables.min.css')}}">
@endsection


@section('content')
<div class="content-wrapper" >
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Manage Check Company</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Check Company</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    @card
                    
                    @slot('title')
                    <div class="row">
                        <div class="col-sm-6">
                          <h4 class="card-title">Form Inputter</h4>
                      </div>
                      <div class="col-sm-6" id="preview_sample">
                          <a class="btn btn-info btn-sm" href="#" data-toggle="modal" data-target="#example">
                            Look for Excel Format!
                            <i class="fa fa-file-excel-o"></i>
                        </a>
                    </div>
                </div>
                
                @endslot


                <div class="row">
                    <div>
                        <input type="file" id="excelfile" enctype="multipart/form-data" />
                        <!-- <input type="file" name="File Upload" id="txtFileUpload" accept=".csv" /> -->
                    </div>
                    <div class="col-sm-6" style="text-align: right;">
                      <button  type="button" id="viewfile" class="btn btn-primary" onclick="ExportToTable()">
                          Export To Tables
                          <a class="fa fa-hand-o-right"></a>
                      </button>
                  </div>
              </div><br>

              <div id="OR_v2">OR</div>
              <!-- =================================================== -->
              <div id="form-input-excel" class="card-title"></div>
              <!-- =================================================== -->

              <br/>

              <form action="{{route('check.index')}}" class="validate-form show-modal" method="post">
                @csrf
                @if (session('error'))
                <div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data telah terdaftar </div>
                @endif
                @if (session('masuk'))
                <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data bisa didaftarkan </div>
                @endif
                @if (session('success'))
                <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data berhasil diinput</div>
                @endif
                
                <span class="form-control-feedback"> {{ $errors->first('domain_name') }}</span>      

                <div class="form-group has-feedback" style="margin-top: 20px;">
                    <label for="">Company Name</label>
                    <input type="text" id="company_name" name="company_name" class="form-control
                    {{$errors->has('company_name') ? ' is-invalid ' : ''}}" value="{{old('company_name')}}" style="width: 75%; float: right;" required>
                    <span class="form-control-feedback"> {{ $errors->first('company_name') }}</span>    
                </div>

                <div class="form-group has-feedback">
                    <label for="">Domain/Website</label>
                    <input type="text" id="domain_name" name="domain_name" class="form-control
                    {{$errors->has('domain_name') ? ' is-invalid' : ''}}" value="{{old('domain_name')}}" style="width: 75%; float: right;" required>{{old('domain_name')}}
                    
                    <span class="form-control-feedback"> {{ $errors->first('domain_name') }}</span>      
                </div>

                <div class="form-group" style="margin-top: 21px;margin-left: 25%;margin-bottom: -2px;">
                    <button id="check" type="submit" class="btn btn-success btn-md">Check</button>
                    {{-- @include('checks.form') --}}
                    @if (session('masuk'))

                    
                    <a href="{{route('getForm', $data1, $data2)}}" class="btn btn-primary btn-md">Add to Database</a>
                    @endif
                </div>
            </form>


            {{-- form history data --}}


            @slot('footer')

            @endslot
            @endcard
        </div>


        <div class="col-md-6">
            @if (session('checked'))
            <form id="excelmain" name="excel_form" action="{{route('check.excel')}}" class="validate-form show-modal" method="post">
                @endif
                @if (!session('checked'))
                <form id="excelmain" name="excel_form" action="{{route('check.checkExcel')}}" class="validate-form show-modal" method="post">
                  @endif
                  @card

                  @slot('title')
                  <div class="row">
                      <div class="col-sm-6">
                        <h4 class="card-title">Preview card</h4>
                    </div>
                    <div class="col-sm-6" style="text-align: right;">
                        @if (!session('checked'))
                        <button id="check2" type="submit" class="btn btn-success btn-md null">Check</button>
                        @elseif (session('checked'))
                        <button id="check3" type="submit" class="btn btn-primary btn-md">Add to Database</button>
                        @endif
                    </div>

                </div>
                @endslot

                @if (session('kosong'))
                <div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert">&times;</a>Data Kosong!</div>
                @endif
                @if (session('template'))
                <div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert">&times;</a>Template excel salah! silahkan download template di tombol "Look Excel format", Pastikan col_read dan tabel header sudah sesuai!</div>
                @endif
                @if (session('excel_error'))
                <div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert">&times;</a> Pastikan semua data belum pernah didaftakan </div>
                @endif
                @if (session('excel_success'))
                <div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert">&times;</a> {{ session('error_input') }} data gagal diinput / redundan</div>
                <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert">&times;</a>{{ session('success_input') }} data sukses diinput!</div>
                @endif

                @php
                {{ asd }}
                @endphp


                @csrf
                <p style="margin-bottom: 0;">Max data 1 kali input : 55</p>
                <div class="table-responsive">
                    <table id="exceltable" class="table table-striped projects">
                      <tbody>
                          @if (session('checked'))
                          @php $i = 0; @endphp
                          <tr>
                              <th>Company name</th>
                              <th>Company Domain</th>
                              <th>Action
                                <a class="tabledel tableBtnDel" title="" data-toggle="tooltip" data-original-title="Delete" style="margin-top: -5px;position: absolute;margin-left: 10px;font-size: 25px;"><i class="fa fa-times"></i></a>
                            </th>
                        </tr>
                        @foreach(session('checked') as $count)
                            @php
                            $nama = session('company')[$i];
                            $domain = session('data')[$i];
                        // hidden
                            $company_type = session('company_type')[$i];
                            $company_field = session('company_field')[$i];
                            $company_desc = session('company_desc')[$i];
                            $company_address = session('company_address')[$i];
                            $company_telpon_office = session('company_telpon_office')[$i];
                            $company_faximile = session('company_faximile')[$i];
                            $company_status = session('company_status')[$i];
                            $company_email = session('company_email')[$i];
                        //pic
                            $pic_name = session('pic_name')[$i];
                            $pic_position = session('pic_position')[$i];
                            $pic_division = session('pic_division')[$i];
                            $pic_nohp = session('pic_nohp')[$i];
                            $pic_email = session('pic_email')[$i];
                            $pic_sosmed = session('pic_sosmed')[$i];
                            $pic_status = session('pic_status')[$i];
                            $i++;
                            @endphp

                            @if($count==0)
                            <tr style="background: rgba(185, 19, 19, 0.72)">
                                @endif
                                <td style="width: 180px">
                                    <input type="hidden" name="excel_name[]" value="{{$nama}}" class="hidden" style="display: none;" />{{$nama}}
                                </td>
                                <td>
                                    <input type="hidden" name="excel_domain[]" value="{{$domain}}" class="hidden" style="display: none;" />{{$domain}}
                                </td>
                                <input type="hidden" name="excel_email[]" value="{{$company_email}}" class="hidden" style="display: none;" />
                                <input type="hidden" name="excel_field[]" value="{{$company_field}}" class="hidden" style="display: none;" />
                                <input type="hidden" name="excel_type[]" value="{{$company_type}}" class="hidden" style="display: none;" />
                                <input type="hidden" name="excel_desc[]" value="{{$company_desc}}" class="hidden" style="display: none;" />
                                <input type="hidden" name="excel_address[]" value="{{$company_address}}" class="hidden" style="display: none;" />
                                <input type="hidden" name="excel_telpon_office[]" value="{{$company_telpon_office}}" class="hidden" style="display: none;" />
                                <input type="hidden" name="excel_faximile[]" value="{{$company_faximile}}" class="hidden" style="display: none;" />
                                <input type="hidden" name="excel_status[]" value="{{$company_status}}" class="hidden" style="display: none;" />
                                <input type="hidden" name="excel_pic_name[]" value="{{$pic_name}}" class="hidden" style="display: none;" />
                                <input type="hidden" name="excel_pic_position[]" value="{{$pic_position}}" class="hidden" style="display: none;" />
                                <input type="hidden" name="excel_pic_division[]" value="{{$pic_division}}" class="hidden" style="display: none;" />
                                <input type="hidden" name="excel_pic_nohp[]" value="{{$pic_nohp}}" class="hidden" style="display: none;" />
                                <input type="hidden" name="excel_pic_email[]" value="{{$pic_email}}" class="hidden" style="display: none;" />
                                <input type="hidden" name="excel_pic_sosmed[]" value="{{$pic_sosmed}}" class="hidden" style="display: none;" />
                                <input type="hidden" name="excel_pic_status[]" value="{{$pic_status}}" class="hidden" style="display: none;" />
                                <td style="text-align: center;">
                                    @if($count==0)
                                    <a class="delete domainBtnDel" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i>
                                </a>
                                @endif
                            </td>
                        </tr>

                        @endforeach

                        @endif
                    </tbody>
                </table>
            </div>


            <!-- =================================================== -->

            <table id="dummy" class="table table-striped projects @if (session('checked')) null @endif">
                <tr>
                  <th>Company name</th>
                  <th>Company Domain</th>
                  <th>Action</th>
              </tr>
              <tr>
                  <td colspan="3" style="text-align: center;">Tidak ada data
                  </td>
              </tr>
          </table> 




          {{-- form history data --}}


          @slot('footer')

          @endslot
          @endcard
      </form>
  </div>                                      


{{--                    <div class="col-md-12">
@card
@slot('title')
Data Company
@endslot

<div class="card">
    <div class="card-header">
      <h3 class="card-title">Projects</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fa fa-minus"></i></button>
            <!-- <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> -->
          </div>
      </div>
      <div class="card-body p-0">
          <div class="table-responsive">
              <table class="table table-striped projects">
                  <thead>
                      <tr>
                          <th style="width: 1%">
                              #
                          </th>
                          <th style="width: 12%">
                              Company
                          </th>
                          <th style="width: 10%">
                            Contact
                        </th>
                        <th style="width: 12%">
                          Domain
                      </th>
                      
                      <th style="width: 15%">
                          Email
                      </th>
                      
                      <th style="width: 8%">
                          PIC
                      </th>
                      <th style="width: 10%" class="text-center">
                          PIC Contact
                      </th>
                      <th style="width: 10%" class="text-center">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @php
                $no = 1; 
                @endphp
                @if (auth()->user()->can('leads admin'))
                @forelse ($dataAdmin as $row)
                
                
                <tr>
                  <td>
                      {{$no++}}
                  </td>
                  <td>
                      <a>
                          {{$row->company_name}}
                      </a>
                  </td>
                  <td>
                    {{$row->company_telpon_office}}
                </td>
                <td>
                  <ul class="list-inline">
                    @foreach($row->domain as $domain)
                    {{$domain['domain_name']}} <br>
                    @endforeach
                </ul>
            </td>
            
            <td>
                @foreach($row->email as $email)
                {{$email['email_name']}}
                @endforeach
                
            </td>
            
            <td class="project_progress">
              <small>
                  {{-- @foreach($row->pic as $pic) --}}
                  {{$row->pic['pic_name']}}
                  {{-- @endforeach --}}
              </small>
          </td>
          <td class="project-state">
              {{-- @foreach($row->pic as $pic) --}}
              {{$row->pic['pic_nohp']}}
              {{-- @endforeach --}}
              
          </td>
          <td class="project-actions text-center">
              <a class="btn btn-primary btn-sm" href="{{route('check.edit', $row->company_id)}}" style="margin-top: 5px; padding: 4px 6px;">
                  <i class="fa fa-edit"></i>
              </a>
              {{-- <a class="btn btn-info btn-sm" href="#" style="margin-top: 5px;" data-toggle="modal" data-target="#myModal@php echo ($no-2);@endphp"> --}}

              <a href="{{route('check.detail', $row->company_id)}}" class="btn btn-info btn-sm btn-show-detail" style="margin-top: 5px;padding: 4px 11px;">
                <i class="fa fa-info"></i>
            </a>
        </td>
    </tr>
    
    @empty
    <tr>
        <td class="text-center" colspan="8">Tidak ada data </td>
    </tr>

    @endforelse
    @else
    @forelse ($dataInputer as $row)
    
    <tr>
        <td>
            {{$no++}}
        </td>
        <td>
            <a>
                {{$row->company_name}}
            </a>
        </td>
        <td>
          {{$row->company_telpon_office}}
      </td>
      <td>
        <ul class="list-inline">
            @foreach ($row->domain as $domain)
            {{$domain['domain_name']}}    
            @break
            @endforeach
            
        </ul>
    </td>
    
    <td>
      @foreach ($row->email as $email)
      {{$email['email_name']}}  
      @break
      @endforeach
      
  </td>
  
  <td class="project_progress">
    <small>
      {{$row->pic->pic_name}}
  </small>
</td>
<td class="project-state">
    {{-- {{$row->pic['pic_nohp']}} --}}
    {{$row->pic['pic_nohp']}}
</td>
<td class="project-actions text-center">
    <a class="btn btn-primary btn-sm" href="{{route('check.edit', $row->company_id)}}" style="margin-top: 5px; padding: 4px 6px;">
        <i class="fa fa-edit"></i>
    </a>
    {{-- <a class="btn btn-info btn-sm" href="#" style="margin-top: 5px;" data-toggle="modal" data-target="#myModal@php echo ($no-2);@endphp"> --}}
    <a href="{{route('check.detail', $row->company_id)}}" class="btn btn-info btn-sm btn-show-detail" style="margin-top: 5px;padding: 4px 11px;">
        <i class="fa fa-info"></i>
    </a>
</td>
</tr>

@empty
<tr>
  <td class="text-center" colspan="8">Tidak ada data </td>
</tr>





@endforelse

@endif
</tbody>
</table>
</div>
</div>
<!-- /.card-body -->
</div>

@slot('footer')
@endslot
@endcard
</div> --}}

</div>
</div>
</section>

</div>

@include('checks.excel_sample')
</div>
</div>

@endsection


@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-csv/0.71/jquery.csv-0.71.min.js"></script>
<script src="{{ asset('js/pop-up.js') }}"></script> 
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
  <!-- <script src="{{ asset('js/jquery.csv.js') }}"></script>
      <script src="{{ asset('js/jquery.csv.min.js') }}"></script> -->
      <script src="{{ asset('plugins/tablefilter/tablefilter.js') }}"></script>
      <script src="{{ asset('js/xlsx.core.min.js') }}"></script>  <!-- ini dibutuhin di check  -->
      <script src="{{ asset('js/xls.core.min.js') }}"></script>  <!-- ini dibutuhin di check  -->
      <script src="{{ asset('js/excel.js') }}"></script>
      <script type="text/javascript">  
    // $(document).ready(function() {

    // // The event listener for the file upload
    // document.getElementById('txtFileUpload').addEventListener('change', upload, false);

    // // Method that checks that the browser supports the HTML5 File API
    // function browserSupportFileUpload() {
    //     var isCompatible = false;
    //     if (window.File && window.FileReader && window.FileList && window.Blob) {
    //     isCompatible = true;
    //     }
    //     return isCompatible;
    // }

    // // Method that reads and processes the selected file
    // function upload(evt) {
    //     if (!browserSupportFileUpload()) {
    //         alert('The File APIs are not fully supported in this browser!');
    //         } else {
    //             var data = null;
    //             let val;
    //             var file = evt.target.files[0];
    //             var reader = new FileReader();
    //             reader.readAsText(file);

    //             reader.onload = function(event) {
    //                 var csvData = event.target.result;
    //                 data = $.csv.toArrays(csvData);
    //                 $("#dummy").find("tr").append("asdasdasd");
    //                 if (data && data.length > 0) {
    //                   alert('Imported -' + data.length + '- rows successfully!');
    //                 } else {
    //                   alert('No data to import!');
    //                 }
    //                 $.each(data, function(i, row) {
    //                     console.log(row[0].split(';'));
    //                 })
    //             };
    
    //             reader.onerror = function() {
    //                 alert('Unable to read ' + file.fileName);
    //             };
    //         }
    //     }
    // });
</script>
@endsection