                @if (auth()->user()->can('leads admin'))
                  @forelse ($dataAdmin as $row)
                  
               
                  <tr>
                      <td>
                          {{$no++}}
                      </td>
                      <td>
                          <a>
                              {{$row->company_name}}
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                              {{$row->domain['domain_name']}}
                          </ul>
                      </td>
                      <td>
                          {{$row->company_address}}
                      </td>
                      <td>
                          {{$row->email['email_name']}}
                      </td>
                      <td>
                          {{$row->company_telpon_office}}
                      </td>
                      <td class="project_progress">
                          <small>
                              {{$row->pic['pic_name']}}
                          </small>
                      </td>
                      <td class="project-state">
                          {{$row->pic['pic_status']}}
                      </td>
                      <td class="project-actions text-center">
                          <a class="btn btn-primary btn-sm" href="{{route('check.edit', $row->company_id)}}" style="margin-top: 5px; padding: 4px 6px;">
                              <i class="fa fa-edit"></i>
                          </a>
                          {{-- <a class="btn btn-info btn-sm" href="#" style="margin-top: 5px;" data-toggle="modal" data-target="#myModal@php echo ($no-2);@endphp"> --}}

                              <a href="{{route('check.detail', $row->company_id)}}" class="btn btn-info btn-sm btn-show-detail" style="margin-top: 5px;padding: 4px 11px;">
                                <i class="fa fa-info"></i>
                              </a>
                      </td>
                  </tr>
    
                  @empty
                  <tr>
                    <td class="text-center" colspan="8">Tidak ada data </td>
                  </tr>

                  @endforelse
                @else
                @forelse ($dataInputer as $row)
                  
                <tr>
                    <td>
                        {{$no++}}
                    </td>
                    <td>
                        <a>
                            {{$row->company_name}}
                        </a>
                    </td>
                    <td>
                        <ul class="list-inline">
                            {{$row->domain['domain_name']}}
                        </ul>
                    </td>
                    <td>
                        {{$row->company_address}}
                    </td>
                    <td>
                        {{$row->email['email_name']}}
                    </td>
                    <td>
                        {{$row->company_telpon_office}}
                    </td>
                    <td class="project_progress">
                        <small>
                            {{$row->pic['pic_name']}}
                        </small>
                    </td>
                    <td class="project-state">
                        {{$row->pic['pic_status']}}
                    </td>
                    <td class="project-actions text-center">
                        <a class="btn btn-primary btn-sm" href="{{route('check.edit', $row->company_id)}}" style="margin-top: 5px; padding: 4px 6px;">
                            <i class="fa fa-edit"></i>
                        </a>
                        {{-- <a class="btn btn-info btn-sm" href="#" style="margin-top: 5px;" data-toggle="modal" data-target="#myModal@php echo ($no-2);@endphp"> --}}

                        <a href="{{route('check.detail', $row->company_id)}}" class="btn btn-info btn-sm btn-show-detail" style="margin-top: 5px;padding: 4px 11px;">
                            <i class="fa fa-info"></i>
                        </a>
                    </td>
                </tr>

                @empty
                <tr>
                  <td class="text-center" colspan="8">Tidak ada data </td>
                </tr>


  


                @endforelse

                @endif