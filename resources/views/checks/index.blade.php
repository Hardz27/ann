@extends('layouts.master')
@section('title')
<title>Manage Check</title>
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('css')
<style type="text/css">
.tab-pane{ 
  height:150px;
  overflow-y:scroll;
}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
@endsection

@section('content')
<div class="content-wrapper" >
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Manage Dataset</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
            <li class="breadcrumb-item active">Manage Dataset</li>
          </ol>
        </div>
      </div>
    </div>
  </div>

  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          @card

          @slot('title')
          <div class="row">
            <div class="col-sm-6">
              <h4 class="card-title">Form Data</h4>
            </div>
            <div class="col-sm-6" id="preview_sample">
              <a class="btn btn-info btn-sm" href="#" data-toggle="modal" data-target="#example">
                Look Excel Format!
                <i class="fa fa-file-excel-o"></i>
              </a>
            </div>
          </div>
          @endslot
          @if (session('success'))
          <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data berhasil diinput</div>
          @endif
          <form action="{{route('check.import')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
              <div>
                <input type="file" id="excelfile" name="select_file" accept=".xls, .xlsx"/>
                <!-- <input type="file" name="File Upload" id="txtFileUpload" accept=".csv" /> -->
              </div>
              <div class="col-sm-6" style="text-align: right;">
                <button id="viewfile" class="btn btn-success">
                  Insert Dataset
                  <a class="fa fa-save"></a>
                </button>
              </div>
            </div><br>
          </form>

          <div id="OR_v2">OR</div>
          <!-- =================================================== -->
          <div id="form-input-excel" class="card-title"></div>
          <!-- =================================================== -->

          <br/>

          <span class="form-control-feedback"> {{ $errors->first('domain_name') }}</span>      

          <div class="form-group has-feedback">
            <label for="">Log Training</label>
            <textarea rows="7" id="log" name="log" class="form-control" value="{{old('domain_name')}}" style="width: 75%; float: right; margin-bottom: 25px" required></textarea>
          </div>

          <div class="form-group" style="margin-top: 21px;margin-left: 25%;margin-bottom: -2px; padding-top: 45px;">
            <button id="train" type="submit" class="btn btn-primary btn-md">Train!</button>
          </div>

          {{-- form history data --}}

          @slot('footer')

          @endslot
          @endcard
        </div>

        <div class="col-md-6">
          @card
          @slot('title')
          <div class="row" style="padding: 4px">
            <div class="col-sm-6">
              <h4 class="card-title">Form Data</h4>
            </div>
          </div>
          @endslot
          @if (session('success'))
          <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data berhasil diinput</div>
          @endif
          <!-- =================================================== -->

          <div class="form-group has-feedback">
            <div class="row">

              <div class="col-md-12">
                <div class="table-responsive">
                  <table id="datasets" class="table table-hover">
                    <thead style="text-align: center;">
                      <tr>
                        <td style="width: 10%">No</td>
                        <td>Soil Moisture</td>
                        <td>Air Temprature</td>
                        <td>Air Humidity</td>
                        <td>Label Action</td>
                      </tr>
                    </thead>
                    <tbody>
                    @php $num = 1; @endphp
                    @forelse($data as $datasets)                          
                      <tr>
                        <td class="text-center">{{ $num++ }}</td>
                        <td class="text-center">{{ $datasets->value_soil_moisture }}</td>
                        <td class="text-center">{{ $datasets->value_air_temprature }}</td>
                        <td class="text-center">{{ $datasets->value_air_humidity }}</td>
                        <td class="text-center">
                          <label for="" class="badge badge-danger">{{ $datasets->label }}</label>
                        </td>
                      </tr>
                    @empty
                    <tr>
                      <td colspan="5" class="text-center">Tidak ada data</td>
                    </tr>
                    @endforelse
                    </tbody>
                  </table>
                </div>
              </div>

            </div>
          </div>

          @slot('footer')

          @endslot
          @endcard
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6">
              @card

              @slot('title')
              <div class="row">
                <div class="col-sm-6">
                  <h4 class="card-title">Graph</h4>
                </div>
              </div>
              @endslot
              <form action="{{route('check.index')}}" class="validate-form show-modal" method="post">
                @csrf
                @if (session('success'))
                <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data berhasil diinput</div>
                @endif

                <span class="form-control-feedback"></span>

                <div class="form-group has-feedback">
                  <div class="row">
                    <div class="col-md-3 text-center">
                      <label for="soil_satruated">Soil Satruated</label>
                      <input type="text" id="soil_satruated" name="soil_satruated" class="form-control" value="0 0 0 0" required>
                    </div>
                    <div class="col-md-3 text-center">
                      <label for="soil_adq">Soil Adq wet</label>
                      <input type="text" id="soil_adq" name="soil_adq" class="form-control" value="0 0 0 0" required />
                    </div>
                    <div class="col-md-3 text-center">
                      <label for="soil_dry">Soil Normal</label>
                      <input type="text" id="soil_normal" name="soil_normal" class="form-control" value="0 0 0 0" required>
                    </div>
                    <div class="col-md-3 text-center">
                      <label for="soil_dry">Soil Dry</label>
                      <input type="text" id="soil_dry" name="soil_dry" class="form-control" value="0 0 0 0" required />
                    </div>
                  </div>
                  <div class="form-group float-right mt-3">
                    <button type="submit" class="btn btn-success btn-md">Save to DB</button>
                  </div>
                </div>
              </form>
              <div class="form-group float-right mr-2">
                <button id="generate_soil" type="submit" class="btn btn-primary btn-md">Generate</button>
              </div>
              <!-- LINE CHART -->
              <div class="card card-info" style="margin-top: 80px">
                <div class="card-header">
                  <h3 class="card-title">Soil Moisture</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="chart">
                    <canvas id="lineChart" style="height:250px; width: 100%"></canvas>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              @slot('footer')

              @endslot
              @endcard
            </div> {{-- COL --}}

            <div class="col-md-6">
              @card

              @slot('title')
              <div class="row">
                <div class="col-sm-6">
                  <h4 class="card-title">Graph</h4>
                </div>
              </div>
              @endslot
              <form action="{{route('check.index')}}" class="validate-form show-modal" method="post">
                @csrf
                @if (session('success'))
                <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data berhasil diinput</div>
                @endif

                <span class="form-control-feedback"></span>

                <div class="form-group has-feedback">
                  <div class="row">
                    <div class="col-md-3 text-center">
                      <label for="humidity_low">humidity low</label>
                      <input type="text" id="humidity_low" name="humidity_low" class="form-control" value="0 0 0 0" required />
                    </div>
                    <div class="col-md-3 text-center">
                      <label for="humidity_low">humidity Normal</label>
                      <input type="text" id="humidity_normal" name="humidity_normal" class="form-control" value="0 0 0 0" required>
                    </div>
                    <div class="col-md-3 text-center">
                      <label for="humidity_high">humidity high</label>
                      <input type="text" id="humidity_high" name="humidity_high" class="form-control" value="0 0 0 0" required />
                    </div>
                    <div class="col-md-3 text-center">
                      <label for="humidity_ext_high">humidity ext_high</label>
                      <input type="text" id="humidity_ext_high" name="humidity_ext_high" class="form-control" value="0 0 0 0" required>
                    </div>
                  </div>
                  <div class="form-group float-right mt-3">
                    <button type="submit" class="btn btn-success btn-md">Save to DB</button>
                  </div>
                </div>
              </form>
              <div class="form-group float-right mr-2">
                <button id="generate_humidity" type="submit" class="btn btn-primary btn-md">Generate</button>
              </div>
              <!-- LINE CHART -->
              <div class="card card-info" style="margin-top: 80px">
                <div class="card-header">
                  <h3 class="card-title">Air Humidity</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="chart">
                    <canvas id="lineChartHumid" style="height:250px; width: 100%"></canvas>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              @slot('footer')

              @endslot
              @endcard
            </div> {{-- COL --}}
          </div>
        </div>


        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6">
              @card

              @slot('title')
              <div class="row">
                <div class="col-sm-6">
                  <h4 class="card-title">Graph</h4>
                </div>
              </div>
              @endslot
              <form action="{{route('check.index')}}" class="validate-form show-modal" method="post">
                @csrf
                @if (session('success'))
                <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data berhasil diinput</div>
                @endif

                <span class="form-control-feedback"></span>

                <div class="form-group has-feedback">
                  <div class="row">
                    <div class="col-md-2 text-center">
                      <label for="temp_vcold">temp vcold</label>
                      <input type="text" id="temp_vcold" name="temp_vcold" class="form-control" value="0 0 0 0" required />
                    </div>
                    <div class="col-md-2 text-center">
                      <label for="temp_cold">temp Cold</label>
                      <input type="text" id="temp_cold" name="temp_cold" class="form-control" value="0 0 0 0" required>
                    </div>
                    <div class="col-md-3 text-center">
                      <label for="temp_cold">temp Normal</label>
                      <input type="text" id="temp_normal" name="temp_normal" class="form-control" value="0 0 0 0" required>
                    </div>
                    <div class="col-md-2 text-center">
                      <label for="temp_hot">temp hot</label>
                      <input type="text" id="temp_hot" name="temp_hot" class="form-control" value="0 0 0 0" required />
                    </div>
                    <div class="col-md-2 text-center">
                      <label for="temp_vhot">temp vhot</label>
                      <input type="text" id="temp_vhot" name="temp_vhot" class="form-control" value="0 0 0 0" required>
                    </div>
                  </div>
                  <div class="form-group float-right mt-3">
                    <button type="submit" class="btn btn-success btn-md">Save to DB</button>
                  </div>
                </div>
              </form>
              <div class="form-group float-right mr-2">
                <button id="generate_temp" type="submit" class="btn btn-primary btn-md">Generate</button>
              </div>
              <!-- LINE CHART -->
              <div class="card card-info" style="margin-top: 80px">
                <div class="card-header">
                  <h3 class="card-title">Air Temp</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="chart">
                    <canvas id="lineChartTemp" style="height:250px; width: 100%"></canvas>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              @slot('footer')

              @endslot
              @endcard
            </div> {{-- COL --}}

            <div class="col-md-6">
              @card

              @slot('title')
              <div class="row">
                <div class="col-sm-6">
                  <h4 class="card-title">Graph</h4>
                </div>
              </div>
              @endslot
              <form action="{{route('check.index')}}" class="validate-form show-modal" method="post">
                @csrf
                @if (session('success'))
                <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data berhasil diinput</div>
                @endif

                <span class="form-control-feedback"></span>

                <div class="form-group has-feedback">
                  <div class="row">
                    <div class="col-md-3 text-center">
                      <label for="humidity_low">humidity low</label>
                      <input type="text" id="humidity_low" name="humidity_low" class="form-control" value="0 0 0 0" required />
                    </div>
                    <div class="col-md-3 text-center">
                      <label for="humidity_low">humidity Normal</label>
                      <input type="text" id="humidity_normal" name="humidity_normal" class="form-control" value="0 0 0 0" required>
                    </div>
                    <div class="col-md-3 text-center">
                      <label for="humidity_high">humidity high</label>
                      <input type="text" id="humidity_high" name="humidity_high" class="form-control" value="0 0 0 0" required />
                    </div>
                    <div class="col-md-3 text-center">
                      <label for="humidity_ext_high">humidity ext_high</label>
                      <input type="text" id="humidity_ext_high" name="humidity_ext_high" class="form-control" value="0 0 0 0" required>
                    </div>
                  </div>
                  <div class="form-group float-right mt-3">
                    <button type="submit" class="btn btn-success btn-md">Save to DB</button>
                  </div>
                </div>
              </form>
              <div class="form-group float-right mr-2">
                <button id="generate_humidity" type="submit" class="btn btn-primary btn-md">Generate</button>
              </div>
              <!-- LINE CHART -->
              <div class="card card-info" style="margin-top: 80px">
                <div class="card-header">
                  <h3 class="card-title">Air Humidity</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="chart">
                    <canvas id="lineChartHumid" style="height:250px; width: 100%"></canvas>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              @slot('footer')

              @endslot
              @endcard
            </div> {{-- COL --}}
          </div>
        </div>
      </div>
    </div>
  </section>

</div>

@include('checks.excel_sample')
</div>
</div>

@endsection


@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-csv/0.71/jquery.csv-0.71.min.js"></script>
<script src="{{ asset('js/pop-up.js') }}"></script> 
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/tablefilter/tablefilter.js') }}"></script>
<script src="{{ asset('js/xlsx.core.min.js') }}"></script>  <!-- ini dibutuhin di check  -->
<script src="{{ asset('js/xls.core.min.js') }}"></script>  <!-- ini dibutuhin di check  -->
<script src="{{ asset('js/excel.js') }}"></script>
<script src="{{ asset('js/fuzzy_soil.js') }}"></script>
<script src="{{ asset('js/fuzzy_air_temp.js') }}"></script>
<script src="{{ asset('js/fuzzy_air_humidity.js') }}"></script>
<script>
var logDone = 1;
var logCount = 0;
var line = 0;
var data;
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$('#train').on('click', function() {
    // getLog()
    $.ajax({
      type : 'POST',
      url: '<?= route('check.train') ?>',
      success: function(response){
        // $('body').removeClass('loaded')
        // logDone = 0
        console.log('start train')
        // getLog()
      }
    });
  });

function update(length){

  for (let i = line; i < length; i++) {
            // $('#log').append(data[i] + '&#13;&#10;')
            setTimeout( function timer(){
              if (i > line) {
                line = i;
              }
              $('#log').append(data[line])
              console.log('line: '+line+' i: '+i+' length: '+ (data.length-1)+' is_done: '+ logDone)
              if (line == (data.length-1)) {
                $('body').addClass('loaded')
                logDone = 1
                alert('Yeaay selesai')
              }
              $('#log').animate({ 
               scrollTop: $( 
                 '#log').get(0).scrollHeight 
             }, 1); 
            }, i*100 );

            if (line > i) {
              i = line
            }
          }

        }

        function getLog(){
          $.ajax({
            type : 'GET',
            url: '<?= route('check.log') ?>',
            success: function(response){
              data = JSON.parse(response)

              if (logDone == 0) {
                update(data.length)
          // for (let i = line; i < data.length; i++) {
          //   // $('#log').append(data[i] + '&#13;&#10;')
          //    setTimeout( function timer(){
          //       if (i > line) {
          //         line = i;
          //       }
          //       // if(i < line){
          //       //   i = line;
          //       // }
          //       $('#log').append(data[line])
          //       console.log('line: '+line+' i: '+i+' length: '+ (data.length-1)+' is_done: '+ logDone)
          //       if (line == (data.length-2) && logDone == 0) {
          //         $('body').addClass('loaded')
          //         logDone = 1
          //         alert('Yeaay selesai')
          //       }
          //       $('#log').animate({ 
          //          scrollTop: $( 
          //            '#log').get(0).scrollHeight 
          //       }, 1); 
          //   }, i*100 );

          //   if (line > i) {
          //     i = line
          //   }
          // }
        }
      }
    });

   if(logDone == 0){
     setTimeout(getLog, 20000)
     console.log('GET NEW DATA! with i = '+ line)
   }
 }
</script>  

<script>
  $(function () {
    $('#datasets').DataTable({
      "scrollY": "200px",
      "scrollCollapse": true,
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": false
    });
  });
</script>  
@endsection