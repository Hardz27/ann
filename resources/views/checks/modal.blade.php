@php $modal = 0; @endphp
@foreach($companies as $key => $item)
<div class="modal fade" id="example" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Information Details</h4>
        </div>
        <form action="#"  method="post">
        <div class="modal-body">
          <div class="card">
        <div class="card-header">
          <h3 class="card-title">Company Information</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%">
                          No
                      </th>
                      <th style="width: 30%">
                          Data
                      </th>
                      <th style="width: 50%">
                          Isi
                      </th>
                      <th style="width: 18%" class="text-center">
                          Status
                      </th>
                  </tr>
              </thead>
              <tbody>

                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                        
                          <a>
                              Company Name
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                    {{$item->company_name}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-warning">Unverified</span>
                      </td>
                    </tr>

                   <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Company type
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->company_type}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-warning">Unverified</span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Company Business
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->company_field}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-warning">Unverified</span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              General Information/Description
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->company_desc}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-warning">Unverified</span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <a>
                          <td>
                              Office Number
                      </td>
                      </a>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->company_telpon_office}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-warning">Success</span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Faximile
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->company_faximile}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-success">Success</span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Address
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->company_address}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-success">Success</span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Company status
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->company_status}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-success">Success</span>
                      </td>
                  </tr>
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>

      <div class="card collapsed-card">
        <div class="card-header">
          <h3 class="card-title">Company Contacts</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%">
                          #
                      </th>
                      <th style="width: 30%">
                          Field
                      </th>
                      <th style="width: 40%">
                          Value
                      </th>
                      <th style="width: 28%" class="text-center">
                          Status
                      </th>
                  </tr>
              </thead>
              <tbody>
                  
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Domain/Web Company
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->domain['domain_name']}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-success">Success</span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              E-Mail Company
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->email['email_name']}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-success">Success</span>
                      </td>
                  </tr>
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>

      <!-- ============================================================================= -->

      <div class="card collapsed-card">
        <div class="card-header">
          <h3 class="card-title">Personal Information Company</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body p-0">
            
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%">
                          #
                      </th>
                      <th style="width: 30%">
                          Field
                      </th>
                      <th style="width: 50%">
                          Value
                      </th>
                      <th style="width: 18%" class="text-center">
                          Status
                      </th>
                  </tr>
              </thead>
              <tbody>
                  
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Personal Name
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->pic['pic_name']}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <a class="btn btn-primary btn-sm" href="{{route('pic.view', $item->pic['pic_id'])}}" style="margin-top: 5px;">
                              <i class="fa fa-info" style="padding-right: 5px;"></i>
                          </a>
                      </td>
                  </tr>
                  <!-- <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Position
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->pic['pic_position']}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-success">Success</span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Divisi
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->pic['pic_division']}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-success">Success</span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              E-Mail
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->pic['pic_email']}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-success">Success</span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Phone Number
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->pic['pic_nohp']}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-success">Success</span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Social Media URL/LinkedIn
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->pic['pic_sosmed']}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-success">Success</span>
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Status
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->pic['pic_status']}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                          <span class="badge badge-success">Success</span>
                      </td>
                  </tr> -->
              
              </tbody>
          </table>          
        </div>
        </div>    
        </form>
        <div class="modal-footer">
            
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </div>
      </div>
    </div>
  </div>
  @php $modal++;@endphp
@endforeach           