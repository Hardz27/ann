@extends('layouts.master')

@section('title')
    <title>Transaksi</title>
@endsection

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Manage Check</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Manage Check</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <section class="content" id="dw">
            <div class="container-fluid">
                <div class="col-md-7">
                <div class="card card-default">
                        <div class="card-header">
                          <h3 class="card-title">Input Company</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form class="form-horizontal" method="POST" action="{{route('check.index')}}">
                        {{ csrf_field() }}
                        @if (session('error'))
                            @alert(['type' => 'danger'])
                                {{ session('error') }}
                            @endalert
                        @endif

                          <div class="card-body">

                            <div class="form-group row">
                              <label for="company_name" class="col-sm-3 control-label">Company Name</label>
                              <div class="col-sm-8">
                                <input id="company_name" name="company_name" type="text" class="form-control {{$errors->has('company_name') ? ' has-errors' : '' }}"  
                                     placeholder="Company Name" value="{{old('company_name')}}">
                                    
                                @if ($errors->has('company_name'))
                                <span class="help-block">
                                    <strong>{{$errors->first('company_name')}}</strong>
                                </span>
                                @endif
                              </div>
                            </div>

                        <div class="form-group row">
                              <label for="domain_name" class="col-sm-3 control-label">Domain/Website</label>
                              <div class="col-sm-8">
                                <input id="domain_name" name="domain_name" type="text" class="form-control {{$errors->has('domain_name') ? 'has-errors' : ''}}"
                              placeholder="Domain Name" value="{{old('domain_name')}}">
                                
                                @if ($errors->has('domain_name'))
                                    <span class="help-block">
                                    <strong>{{$errors->first('domain_name')}}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            
                        </div>

                        <!-- /.card-body -->
                          <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Check!</button>

                            <button type="submit" class="btn btn-success float-right">Add to Data</button>
                          </div>
                          <!-- /.card-footer -->
                        </form>
                      </div>
                    </div>
                    </div>
                    <div class="col-md-12">
                            @card
                                @slot('title')
                                Data Company
                                @endslot
    
                                <div class="row">
                                    
                                </div>
    
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Domain</th>
                                                <th>City</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>PIC</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {{-- @forelse ($orders as $row) --}}
                                            <tr>
                                                {{-- <td><strong>#{{ $row->invoice }}</strong></td> --}} <td><strong>invoice</strong></td>
                                                 <td> customers name</td>    {{-- <td>{{ $row->customer->name }}</td> --}}
                                                 <td> customers phone</td>   {{-- <td>{{ $row->customer->phone }}</td> --}}
                                                 <td> Rp total</td>    {{--<td>Rp {{ number_format($row->total) }}</td> --}}
                                                <td></td>
                                                <td></td>
                                                <td> customers name</td>
                                                <td>
                                                    {{-- <a href="{{ route('order.pdf', $row->invoice) }}"  --}}
                                                            <a href="" 
                                                        target="_blank"
                                                        class="btn btn-primary btn-sm">
                                                        <i class="fa fa-print"></i>
                                                    </a>
                                                    <a href="" 
                                                        target="_blank"
                                                        class="btn btn-info btn-sm">
                                                        <i class="fa fa-file-excel-o"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            {{-- @empty --}}
                                            <tr>
                                                <td class="text-center" colspan="8">Tidak ada data inputan</td>
                                            </tr>
                                            {{-- @endforelse --}}
                                        </tbody>
                                    </table>
                                </div>
                                @slot('footer')
    
                                @endslot
                            @endcard
                        </div>
        </section>
    </div>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/accounting.js/0.4.1/accounting.min.js"></script>
    <script src="{{ asset('js/transaksi.js') }}"></script>
@endsection