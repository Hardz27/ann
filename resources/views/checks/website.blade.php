@extends('layouts.master')

@section('title')
    <title>Manage Check</title>
@endsection

@section('css')
    <style type="text/css">
        .tab-pane{ 
            height:150px;
            overflow-y:scroll;
        }
    </style>
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('public/css/jquery.dataTables.min.css')}}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')
    <div class="content-wrapper" >
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dataset Generator</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Dataset Generator</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        @card
                        
                            @slot('title')
                            <div class="row">
                                <div class="col-sm-6">
                                  <h4 class="card-title">Form Input</h4>
                                </div>
                              </div>
                            
                            @endslot <!-- Last Key : nq -->
                            <form action="{{route('check.generateNormalized')}}" class="validate-form show-modal" method="post">

                                @csrf
                                    @if (session('error'))
                                        <div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert">&times;</a>Pastikan portal dan ul yang digunakan telah sesuai </div>
                                    @endif
                                    {{-- @if (session('generated'))
                                    @php echo "generta"; @endphp
                                        <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data bisa didaftarkan </div>
                                    @endif --}}
                                    @if (session('success'))
                                        <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data berhasil diinput</div>
                                    @endif
                                   
                                    <div class="col-sm-12">
                              <!-- textarea -->
                              <!-- <div class="form-group">
                              <label>Website Portal</label>

                              <select id="category" name="category" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                <option value="">Pilih</option>
                                <option value="LI" >Linked</option>
                                <option value="GL" {{ old('category') =='pic' ? 'selected' : ''  }}>Glints</option>
                                <option value="JO" {{ old('category') =='Jobstreet' ? 'selected' : ''  }}>Jobstreet</option>
                                <option value="LO" {{ old('category') =='Loker.id' ? 'selected' : ''  }}>Loker.id</option>
                              </select>
                            </div> -->
                            
                              <div class="form-group">
                                <label>Textarea</label>
                                <textarea class="form-control" rows="3" name="data" placeholder="Paste here : code <ul ....."></textarea>
                              </div>
                            </div>

                                    <div class="form-group float-right">
                                            <button id="check" type="submit" class="btn btn-success btn-md">Submit Query</button>
                                    </div>
                            </form>


                            {{-- form history data --}}


                            @slot('footer')

                            @endslot
                        @endcard
                    </div>


                    
              <div class="col-md-6">
                @card
                
                    @slot('title')
        <div style="float:left">Form Filter</div>
                <div class="row">
                    <div class="col-sm-6" style='text-align: right;'></div> 
                </div>
                
                    
                    @endslot
                    <form action="" method="get">
                        <div class="row">
                                <div class="col-md-4">
                                        <div class="form-group">
                                        <label for="">Category</label>
                                        <select id="category" name="category" value="" class="form-control">   
                                              <option value="">Pilih</option>
                                              <option value="LI">Linked</option>
                                              <option value="GL">Glints</option>
                                              <option value="JO">Jobstreet</option>
                                              <option value="LO">Loker.id</option>
                                              <option value="ALL">All Data</option>

                                               {{-- <option value="pic" @if(old('category') == 'pic') selected @endif>Pic</option> --}}
                                           
                                           </select>
                                        </div>
                                    </div>
                            <div class="col-md-5">
                              <div class="form-group has-feedback" >
                                <label for="">Date Range</label>
                                <input type="text" id="due_date" name="due_date" value="{{ request()->get('due_date') }}"  class="form-control" placeholder="Masukkan tanggal">
                              </div>
                            </div>
                              <div class="col-md-3">
                                <div class="form-group" style="padding-top:20%; padding-left:18%">
                                  <button class="btn btn-primary btn-md">Search Data</button>&nbsp;&nbsp;  
                                  {{-- <button id="reset" class="cancelBtn btn">Reset</button> --}}
                                  {{-- <button class="cancelBtn btn btn-md btn-default" type="button">Reset</button> --}}
                                </div>
                                
                              </div>
{{--                               
                              <div class="col-md-4">
                                      <div class="form-group">
                                          <button id="reset" class="btn pull-right">Reset</button>    
                                      </div>
                                     
                              </div> --}}
                          </div>
                    </form>

                    @slot('footer')

                    @endslot
                @endcard
            </div>                



               
            <!-- ============================= Bagian Tak Terbaca ======================================== -->
{{-- <div class="col-md-12">
    <form name="domain_input" action="{{route('check.web.domain')}}" class="validate-form show-modal" method="post">
       <div class="card">
        <div class="card-header">
          <h3 class="card-title">List Company</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body">
            @csrf                 
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="width: 1%;">No</th>
                  <th>Company name</th>
                  <th>Job Position</th>
                  <th>URL</th>
                  <th>Domain</th>
                  <th>created at</th>
                </tr>
              </thead>
              <tbody>
                @php 
                	$i = 0;
                	$tampil = '';
                @endphp
                @foreach($companies as $row)
                    @php
                    	$tampil = 'no';
                        $company = DB::table('temp_companies')->select('temp_company_id')
                          ->where('temp_company_name', $row->temp_company_name)
    	                    ->latest('temp_company_id')
    	                    ->first();
    	                $job = DB::table('jobs')->where('temp_company_id', $company->temp_company_id)
    	                       ->get();
    	                $domain = DB::table('t_company_domains')
    	                    	->where('company_id', $job[0]->company_id)
    		                    ->latest('company_id')
    		                    ->first();	                 	 
    	                
    	                if(count($job) == 1){
    	                	$datetime1 = new DateTime(date('Y-m-d'));
            						$datetime2 = new DateTime($job[0]->job_time);
            						$difference = $datetime1->diff($datetime2);
            						
            						if($difference->days >= 15 ){
            							$tampil = "no";
            						}
            						else{
            							$tampil = "yes";
            						}
    	                	//dd($job[0]->job_time);
    	            	  }

    					       // dd($job->job_position);
                    @endphp
                @if($tampil == 'yes')
                <tr>
                   <td class="text-center">{{ $i+1 }}</td> 
                     <td>{{ $row->temp_company_name }}</td>
                     <input type="hidden" name="company_name[]" value="{{ $row->temp_company_name }}"/>
                     <td>
                     @foreach($job as $job_pos)
                       @php
                         	$datetime1 = new DateTime(date('Y-m-d'));
              						$datetime2 = new DateTime($job_pos->job_time);
              						$difference = $datetime1->diff($datetime2);					
            					 @endphp
            					 @if($difference->days < 15)
              					 	@if($difference->days == 0)
                         		{{ $job_pos->job_position  .'('. $row->temp_company_code . ' - Today' .')' .', ' }}
                         	@else
                         		{{ $job_pos->job_position  .'('. $row->temp_company_code . ' - ' . $difference->days.')' .', ' }}
                         	@endif
    	                    @php
    	                     if($job_pos->company_id){
    	                     	 $domain = DB::table('t_company_domains')
    	                    	   ->where('company_id', $job_pos->company_id)
    		                       ->latest('company_id')
    		                       ->first();
    	                 	 }
    	                    
    		                 @endphp
                       @endif
                    @endforeach
                     </td>
                     @if($row->temp_company_link != '-') 
                     <td class="text-center"> <a href="{{ $row->temp_company_link }}" target="_blank" class="btn btn-success">Check</a> </td>
                     @else
                     <td> - </td>
                     @endif
                     @if($domain)
                     <td>{{ $domain->domain_name }}</td> 
                     <input type="hidden" name="company_domain[]" value="{{ $domain->domain_name  }}"/>
                     @else
                     <td><input type="text" name="company_domain[]" class="form-control" value="-" required></td> 
                     @endif
                     <td>{{ $row->created_at->format('Y-M-d') }}</td>
                     
                </tr>
                    @php $i++; @endphp
                  @endif
                @endforeach
                @if($i == 0)
                <tr>
                     <td colspan="6" class="text-center">Tidak ada data</td> 
                </tr>
              @endif
              
              </tbody>
              
            </table>
            <button type="submit" class="btn btn-primary btn-md">Save</button>
           </div>
          </div>
          <!-- /.card-body -->
          
        </div>  --}}
        
      </form>

                      
                    </div>

                </div>
            </div>
        </section>

    </div>

    @include('checks.excel_sample')
    </div>
    </div>

@endsection

  
@section('js')
  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-csv/0.71/jquery.csv-0.71.min.js"></script> --}}
  <script src="{{ asset('js/pop-up.js') }}"></script> 
  {{-- <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script> --}}
  <!-- <script src="{{ asset('js/jquery.csv.js') }}"></script>
  <script src="{{ asset('js/jquery.csv.min.js') }}"></script> -->
  {{-- <script src="{{ asset('plugins/tablefilter/tablefilter.js') }}"></script> --}}
  <script src="{{ asset('js/xlsx.core.min.js') }}"></script>  <!-- ini dibutuhin di check  -->
  <script src="{{ asset('js/xls.core.min.js') }}"></script>  <!-- ini dibutuhin di check  -->
  <script src="{{ asset('js/excel.js') }}"></script>
  <script src="{{ asset('js/moment.min.js') }}"></script>
  <script src="{{ asset('js/daterangepicker.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables2/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
  

  <script>
    
       $(function () {
          $("#example1").DataTable();
          $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
          });
        });


    $('#category  option[value=""]').prop("selected", true);

// var e = document.getElementById("category");
// var strUser = e.options[e.selectedIndex].value;

    $(function() {
      $('input[name="due_date"]').daterangepicker({
        opens: 'left',
        autoUpdateInput: false,
        locale: { 
          format: 'DD-MM-YYYY',
    
        }
      });
      $('input[name="due_date"]').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
      });
    
      $('input[name="due_date"]').on('cancel.daterangepicker', function(ev, picker) {
          $(this).val('');
      });
    });

$("#reset").on("click", function () {
    $('#category option').removeAttr('selected');
    $('#due_date').datepicker('setDate', null);
    $('input[name="due_date"]').on('cancel.daterangepicker', function(ev, picker) {
          $(this).val('');
      });
});
    </script> 
@endsection

