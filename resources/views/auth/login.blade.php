@extends('layouts.auth')

@section('title')
    <title>Login</title>
@endsection

@section('content')
	
<div class="welcome">
  <span id="splash-overlay" class="splash"></span>
  <span id="welcome" class="z-depth-4"></span>

  <main class="valign-wrapper">
    <span style="padding-left: 0px; padding-right: 0px;" class="container grey-text text-lighten-1 ">

      <div class="limiter">
        
        <div class="container-login100">
            <div class="wrap-login100">
            <!-- <div class="animated animatedFadeInUp fadeInUp" style="height: 200px; background-image: url('images/erporate.png');">
            </div> -->
           <form method="POST" action="{{ route('login') }}" class="login100-form validate-form">
                <span class="login100-form-title p-b-43">
                    Leads Project
                </span>
                @csrf
                @if (session('error'))
                    @alert(['type' => 'danger'])
                        {{ session('error') }}
                    @endalert
                @endif

               
                <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                    <input type="email"
                        name="user_email" 
                        class="input100 {{ $errors->has('user_email') ? ' is-invalid' : '' }}" 
                        
                        value="{{ old('user_email') }}">
                    
                    <span class="focus-input100"></span>
                    <span class="label-input100">Email</span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Password is required">
                    <input type="password" 
                        name="user_password"
                        class="input100 {{ $errors->has('user_password') ? ' is-invalid' : '' }} ">

                    
                    <span class="focus-input100"></span>
                    <span class="label-input100">Password</span>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                </div>
            </form>

            <div class="login100-more" style="background-image: url('images/bg-01.jpg');">
            </div>
            <img class="login100-more animated animatedFadeInUp fadeInUp" style="position: absolute; left: 5%; z-index: 99999;top: 35%;width: 35%;" src="{{ asset('images/erporate.png') }}">
            
            </div>

        </div>
    </div>


            <!-- <div class="social-auth-links text-center mb-3">
                <p>- OR -</p>
                <a href="#" class="btn btn-block btn-primary">
                    <i class="fa fa-facebook mr-2"></i> Sign in using Facebook
                </a>
                <a href="#" class="btn btn-block btn-danger">
                    <i class="fa fa-google-plus mr-2"></i> Sign in using Google+
                </a>
            </div>

            <p class="mb-1">
                <a href="#">I forgot my password</a>
            </p>
            <p class="mb-0">
                <a href="#" class="text-center">Register a new membership</a>
            </p> -->
        </div>
    <!-- </div> -->

    <!--   <div class="center-align">
        
      </div> -->

    </span>
  </main>

</div>




    
@endsection