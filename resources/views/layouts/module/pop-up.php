<script data-config="">
 var filtersConfig = {
  base_path: 'tablefilter/',
  auto_filter: {
                    delay: 110 //milliseconds
              },
              filters_row_index: 1,
              state: true,
              alternate_rows: true,
              rows_counter: true,
              btn_reset: true,
              status_bar: true,
              msg_filter: 'Filtering...'
            };
            var tf = new TableFilter('demo', filtersConfig);
            tf.init();
          </script>

<!-- here stars scrolling icon -->
            <script type="text/javascript">
                $(document).ready(function() {
                    /*
                        var defaults = {
                        containerID: 'toTop', // fading element id
                        containerHoverID: 'toTopHover', // fading element hover id
                        scrollSpeed: 1200,
                        easingType: 'linear' 
                        };
                    */
                                        
                    $().UItoTop({ easingType: 'easeOutQuart' });
                                        
                    });
            </script>
            <!-- start-smoth-scrolling -->
            <!-- <script type="text/javascript" src="js/move-top.js"></script> -->
            <script type="text/javascript" src="js/easing.js"></script>

            <script type="text/javascript">
                jQuery(document).ready(function($) {
                    $(".scroll").click(function(event){     
                        event.preventDefault();
                        $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                    });
                });
            </script>
            <script>
                $(document).ready(function() {
                $('#demo').DataTable( {
                    "pagingType": "full_numbers"
                } );
            } );
            </script>

            <!-- Add new ro -->
            <!-- ================================================================= -->

<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    var counter = 1;
    var actions = $("table td:last-child").html();
    // Append table with add row form on add new button click
    $(".add-new").click(function(){
        
        var index = $("table tbody tr:last-child").index();
        var row =
          '<div class="panel panel-default">' +
            '<div class="panel-heading">' +
                '<h4 class="panel-title">' +
          '<a data-toggle="collapse" data-parent="#accordion" href="#collapse' + counter + '">Personal data information ' + (counter + 1) +'</a>' +
          '<a data-toggle="collapse" data-parent="#accordion" href="#collapse' + counter + '" style="float: right; font-size: 20px;">-</a>' +
        '</h4>' +
      '</div>' +
      '<div id="collapse' + counter + '" class="panel-collapse collapse in">' +
        ' <div class="panel-body" style="padding: 10px;">' +
            
            '<thead><tr><td></td></tr></thead>' +
                '<tr>' +
                    '<td><p id="kolom">Personal name</td>' + 
                    '<td><input type="text" class="form-control" name="pic_name" placeholder="Personal name"></td>' +
                '</tr>' +
                '<tr>' +
                    '<td><p id="kolom">Position</td>' + 
                    '<td><input type="text" class="form-control" name="pic_position" placeholder="Postion"></td>' +
                '</tr>' +
                '<tr>' +
                    '<td><p id="kolom">Divisi</td>' + 
                    '<td><input type="text" class="form-control" name="pic_divisi" placeholder="Division"></td>' +
                '</tr>' +
                '<tr>' +
                    '<td><p id="kolom">Email</td>' + 
                    '<td><input type="text" class="form-control" name="pic_email" placeholder="Email Address"></td>' +
                '</tr>' + 
                '<tr>' +
                    '<td><p id="kolom">Phone number</td>' + 
                    '<td><input type="text" class="form-control" name="pic_phone" placeholder="Phone number"></td>' +
                '</tr>' +
                '<tr>' +
                    '<td><p id="kolom">Social media URL/LinkedIn</td>' + 
                    '<td><input type="text" class="form-control" name="pic_url" placeholder="Social Media URL"></td>' +
                '</tr>' +
                '<tr>' +
                    '<td><p id="kolom">Staus</td>' + 
                    '<td><select name="pic_status" class="form-control">' +
                            '<option value="pilihan1">Pilihan 1</option>' +
                            '<option value="pilihan2">Pilihan 2</option>' +
                            '<option value="pilihan3">Pilihan 3</option>' +
                            '<option value="pilihan4">Pilihan 4</option>' +
                        '</select></td>' +
                '</tr></div>' +
             '</div>' +
            '</div>';
        $("#person").append(row);     
        $("table tbody tr").eq(index + 1).find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();
        counter++;
    });
    // Add row on add button click
    $(document).on("click", ".add", function(){
        var empty = false;
        var input = $(this).parents("tr").find('input[type="text"]');
        input.each(function(){
            if(!$(this).val()){
                $(this).addClass("error");
                empty = true;
            } else{
                $(this).removeClass("error");
            }
        });
        $(this).parents("tr").find(".error").first().focus();
        if(!empty){
            input.each(function(){
                $(this).parent("td").html($(this).val());
            });         
            $(this).parents("tr").find(".add, .edit").toggle();
            $(".add-new").removeAttr("disabled");
        }       
    });
    // Edit row on edit button click
    $(document).on("click", ".edit", function(){        
        $(this).parents("tr").find("td:not(:last-child)").each(function(){
            $(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">');
        });     
        $(this).parents("tr").find(".add, .edit").toggle();
        $(".add-new").attr("disabled", "disabled");
    });
    // Delete row on delete button click
    $(document).on("click", ".delete", function(){
        $(this).parents("tr").remove();
        $(this).parents("tr").remove();
        $(".add-new").removeAttr("disabled");
    });
});
</script>

        <!-- Add new row for company contact -->
    <!-- ======================================== -->

    <script>
    $(document).ready(function () {
        var domain = 0;
        var email = 0;

        $("#addDomain").on("click", function () {
            var newRow = $('<tr>');
            var cols = "";

            cols += '<td style="border-top: none; width: 30%;"></td>';
            cols += '<td style="border-top: none; width: 60%;"><div class="col-sm-10"><input type="text" style="width: 110%;" class="form-control" name="com_domain_'+ domain +'" placeholder="Your Company Domain" required="required" /></div></td>';
            cols += '<td style="border-top: none; width: 10%;"><a class="delete domainBtnDel" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a></td>';
            newRow.append(cols);
            $("#sub_domain").append(newRow);
            domain++;
        });

// <td style="border-top: none;"><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>

        $("#sub_domain").on("click", ".domainBtnDel", function (event) {
            $(this).closest("tr").remove();       
            domain -= 1;
        });

        // ========================================================================
        // =                               Add E-Mail                             =
        // ========================================================================

        $("#addEmail").on("click", function () {
            var newRow = $('<tr>');
            var cols = "";

            cols += '<td style="border-top: none; width: 30%;"></td>';
            cols += '<td style="border-top: none; width: 60%;"><div class="col-sm-10"><input type="text" style="width: 110%;" class="form-control" name="com_domain_'+ email +'" placeholder="Your Company E-Mail" required="required" /></div></td>';
            cols += '<td style="border-top: none; width: 10%;"><a class="delete emailBtnDel" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a></td>';
            newRow.append(cols);
            $("#sub_email").append(newRow);
            email++;
        });

// <td style="border-top: none;"><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>

        $("#sub_email").on("click", ".emailBtnDel", function (event) {
            $(this).closest("tr").remove();       
            email -= 1;
        });


    });

    </script>

    
<script>

    var str= company_name;

    var patt=/www./g;

    if(patt.test(str)==true)
    {
        var str2= str.replace(patt,"");
    }

</script>