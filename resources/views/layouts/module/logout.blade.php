
<div class="modal fade" id="logout" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="text-align: center; background: #dfb038; color: white;">
          <center>
            <h4 class="modal-title">Are you sure want to logged out?</h4>
          </center>
        </div>
        <form action="#"  method="post">
        <div class="modal-body">

      <!-- ============================================================================= -->

      <div class="card">
        <div class="card-body p-0">
            
        </div>
        </div>  
        </div>  
        </form>
        <div class="modal-footer" style="text-align: center;">
          <form action="{{ route('logout')}}" method="POST">
              @csrf
              <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
              <button class="btn btn-danger" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();b();">Yes log me out!<i class="fa fa-sign-out"></i></button>
           </form>
        </div>
      </div>
      </div>
    </div>
  </div>
