
@if (!isset($page))
    @php $page = ""; @endphp
@endif

@php
    $newInput = DB::table('t_companies')
        ->join('t_company_domains', 't_companies.company_id', '=' ,'t_company_domains.company_id')
        ->join('t_company_emails', 't_companies.company_id', '=' , 't_company_emails.company_id')
        ->select('t_company_domains.*','t_company_emails.*','t_companies.*')
        ->where('t_companies.company_validation', '=', false)
        ->where('t_companies.deleted_at', '=', null)
        ->where('t_company_emails.email_validation', '=', false)
        ->where('t_company_domains.domain_validation', '=', false)
        ->orderBy('t_companies.created_at', 'DESC')
        ->orderBy('t_companies.company_name', 'ASC')
        ->get();
@endphp
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('home')}}" class="brand-link">
        <img src="{{ asset('dist/img/erporate1.png') }}" alt="POS" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">ERPORATE-LEADS</span>
    </a>

    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                {{-- <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image"> --}}
            </div>
            <div class="info">
                @if (auth()->user()->can('leads admin'))
                    <!-- <a href="#" class="d-block">{{'Admin ' . auth()->user()->user_name}}</a> -->
                    <a href="#" class="d-block">{{'Admin Hardz'}}</a>
                @else
                    <a href="#" class="d-block">{{'Inputer ' . auth()->user()->user_name}}</a>
                @endif
                 
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                <a href="{{ route('home') }}" class='nav-link
                            @if ($page == "Home")
                                @php echo "active" @endphp
                            @endif
                '">
                        <i class="nav-icon fa fa-dashboard"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>

                
@if (auth()->user()->can('manage check')||auth()->user()->can('manage leads'))   
                <li class="nav-item has-treeview">
                    <a href="#" class='nav-link
                            @if ($page == "Check")
                                @php echo "active" @endphp
                            @endif
                            '>
                        <i class="nav-icon fa fa-pencil-square-o"></i>
                        <p>
                            Manage Cek
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('check.index') }}" class="nav-link">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Input excel & Form</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('check.website') }}" class="nav-link">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Input Website Portal</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- <li class="nav-item">
                    <a href="{{ route('leads.index') }}" class='nav-link
                            @if ($page == "Leads")
                                @php echo "active" @endphp
                            @endif
                            '>
                        <i class="nav-icon fa fa-check-square-o""></i>
                        <p>
                            Manage Leads
                            @if(auth()->user()->can('leads admin'))
                                @if($newInput->count() > 0)
                                    @php echo '<span class="right badge badge-danger">New</span>'; @endphp
                                @endif
                            @endif

                            
                            {{-- <i class="right fa fa-angle-left"></i> --}}
                        </p>
                    </a>
                </li> -->

@endif   

               
                
                @role('admin')
               
                <!-- <li class="nav-item">
                    <a href="{{ route('company.index') }}" class='nav-link
                            @if ($page == "Company")
                                @php echo "active" @endphp
                            @endif
                    '">
                        <i class="nav-icon fa fa-building-o"></i>
                        <p>
                            Manage Company
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('pic.index') }}" class='nav-link
                            @if ($page == "Pic")
                                @php echo "active" @endphp
                            @endif
                    '">
                        <i class="nav-icon fa fa-address-book-o"></i>
                        <p>
                            Manage PIC
                        </p>
                    </a>
                </li> -->
                
           

     
                <!-- <li class="nav-item has-treeview">
                    <a href="#" class='nav-link
                            @if ($page == "User" || $page == "Role")
                                @php echo "active" @endphp
                            @endif
                    '">
                        <i class="nav-icon fa fa-users"></i>
                        <p>
                            Manage User
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="" class="nav-link">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Role</p>
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a href="{{ route('users.roles_permission') }}" class="nav-link">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Role Permission</p>
                            </a>
                        </li>
                        

                        <li class="nav-item">
                            <a href="" class="nav-link">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Users</p>
                            </a>
                        </li>
                    </ul>
                </li>
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <li class="nav-header">MASTER DATA</li>
            </div>
                <li class="nav-item">
                    <a href="{{ route('salary.index') }}" class='nav-link
                            @if ($page == "Salary")
                                @php echo "active" @endphp
                            @endif
                            '>
                        <i class="nav-icon fa fa-money"></i>
                        <p>
                            Manage Salary
                        </p>
                    </a>
                </li>
                     
                <li class="nav-item">
                    <a href="{{ route('archive.leads') }}" class='nav-link
                            @if ($page == "Archive")
                                @php echo "active" @endphp
                            @endif
                    '">
                        <i class="nav-icon fa fa-archive"></i>
                        <p>
                            Manage Archive
                        </p>
                    </a>
                </li> -->

                


                
    
                @endrole

                
                @role('inputer')
{{--
                    <li class="nav-item has-treeview">
                    <a href="{{route('users.index')}}" class="nav-link">
                                <i class="fa fa-user-circle-o"></i>
                                <p>Profile</p>
                            </a>
                    </li>
--}}             
                @endrole

                <li class="nav-item has-treeview">
                    <a class="nav-link" href="#" data-toggle="modal" data-target="#logout">
                    <!-- <a class="nav-link" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();b();"> -->
                        <i class="nav-icon fa fa-sign-out"></i>
                        <p>
                            {{ __('Logout') }}
                        </p>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
    </div>
</aside>

@include('layouts.module.logout')