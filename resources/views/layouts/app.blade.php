<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Daengweb') }}</title>

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->


    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <!-- <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}"> -->


    <!-- =========================================================== -->
    <!-- =                         Baru                              -->
    <!-- =========================================================== -->
    @yield('css2')
	
</head>
<body>
    
    
    
            @yield('content2')
            

     

    <!-- <script src="{{ asset('js/pop-up.js') }}"></script>     -->
    <script src="{{ asset('js/validattion2.js') }}"></script>    
    

    
</body>
</html>
