<div class="header navbar">
  <div class="header-container">
    <ul class="nav-left">
      <li>
        <a id='sidebar-toggle' class="sidebar-toggle" href="javascript:void(0);">
          <i class="ti-menu"></i>
        </a>
      </li>
      <!-- <li class="search&#45;box"> -->
      <!--     <a class="search&#45;toggle no&#45;pdd&#45;right" href="javascript:void(0);"> -->
      <!--         <i class="search&#45;icon ti&#45;search pdd&#45;right&#45;10"></i> -->
      <!--         <i class="search&#45;icon&#45;close ti&#45;close pdd&#45;right&#45;10"></i> -->
      <!--     </a> -->
      <!-- </li> -->
      <!-- <li class="search&#45;input"> -->
      <!--     <input class="form&#45;control" type="text" placeholder="Search..."> -->
      <!-- </li> -->
    </ul>
    <ul class="nav-right">
      <!-- NOTIFIKASI -->
      {{--<li class="notifications dropdown">
        <span class="counter bgc-blue">3</span>
        <a href="" class="dropdown-toggle no-after" data-toggle="dropdown">
          <i class="ti-email"></i>
        </a>
        <ul class="dropdown-menu">
          <li class="pX-20 pY-15 bdB">
            <i class="ti-email pR-10"></i>
            <span class="fsz-sm fw-600 c-grey-900">Emails</span>
          </li>
          <li>
            <ul class="ovY-a pos-r scrollable lis-n p-0 m-0 fsz-sm">
              <li>
                <a href="" class='peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100'>
                  <div class="peer mR-15">
                    <img class="w-3r bdrs-50p" src="/images/3.jpg" alt="">
                  </div>
                  <div class="peer peer-greed">
                    <div>
                      <div class="peers jc-sb fxw-nw mB-5">
                        <div class="peer">
                          <p class="fw-500 mB-0">Lee Doe</p>
                        </div>
                        <div class="peer">
                          <small class="fsz-xs">25 mins ago</small>
                        </div>
                      </div>
                      <span class="c-grey-600 fsz-sm"> Want to create your own customized data generator for your app... </span>
                    </div>
                  </div>
                </a>
              </li>
            </ul>
          </li>
          <li class="pX-20 pY-15 ta-c bdT">
            <span>
              <a href="" class="c-grey-600 cH-blue fsz-sm td-n">View All Email <i class="fs-xs ti-angle-right mL-10"></i></a>
            </span>
          </li>
        </ul>
      </li> --}}
      <li class="dropdown">
        <a href="" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown">
          <!-- <div class="peer mR-10">
            <img class="w-2r bdrs-50p" src="{{ auth()->user()->avatar }}" alt="">
          </div> -->
          <div class="peer">
            <span class="fsz-sm c-grey-900 d-flex align-items-center">{{ auth()->user()->name }} <i class="mL-5 material-icons">keyboard_arrow_down</i></span>
          </div>
        </a>
        <ul class="dropdown-menu fsz-sm">
          {{-- <li>
            <a href="" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
              <i class="ti-settings mR-10"></i>
              <span>Setting</span>
            </a>
          </li>
          <li role="separator" class="divider"></li> --}}
          <li>
            <a href="/logout" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
              <i class="ti-power-off mR-10"></i>
              <span>Logout</span>
            </a>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</div>
