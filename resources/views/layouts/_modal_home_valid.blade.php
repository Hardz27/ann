<div class="modal fade" id="modal-valid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="modal-dialog" role="document" >
     <div class="modal-content" >
       <div class="modal-header" style="text-align: center; background: #28A745; color: white;">
         <h4 class="modal-title" id="modal-title-valid">Validated Data Detail</h4>
       </div>
       <div class="modal-body" id="modal-body-valid" >
         
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       </div>
     </div>
   </div> 
 </div>