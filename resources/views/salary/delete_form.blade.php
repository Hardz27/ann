    {{-- <div class="modal-body"> --}}

  <!-- ============================================================================= -->

  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Salary Delete</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="card-body p-0">
        
      <table class="table table-striped projects">
          <thead>
              <tr>
                  <th style="width: 2%">
                      #
                  </th>
                  <th style="width: 30%">
                      Tanggal berlaku
                  </th>
                  <th style="width: 50%">
                      Tanggal berakhir
                  </th>
                  <th style="width: 18%" class="text-center">
                      Nilai Salary
                  </th>
              </tr>
          </thead>
          <tbody>
              
              <tr>
                  <td>
                      #
                  </td>
                  <td>
                      <a>
                        
                        {{$salary->tgl_berlaku}}
                      </a>
                  </td>
                  <td>
                      <ul class="list-inline">
                        <li class="list-inline-item">
                              {{$salary->tgl_deadline}}
                        </li>                 
                      </ul>
                  </td>
                  <td class="project-state" style="text-align: center;">

                    <label for="" class="badge badge-secondary">{{$salary->salary_value}}</label>

                  </td>
              </tr>
          
          </tbody>
      </table>          
    </div>
    </div> 

    {{-- </div>   --}}

    <div class="modal-footer" style="text-align: center;">
      <form action="{{ route('salary.destroy', $salary->salary_id) }}" method="GET">
          @csrf
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          <button class="btn btn-danger">Delete<i class="fa fa-archive"></i></button>
       </form>
    </div>
