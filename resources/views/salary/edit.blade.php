@extends('layouts.master')

@section('title')
    <title>Edit Users</title>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/daterangepicker.css') }}" />
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Edit Users</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('salary.index') }}">Salary</a></li>
                            <li class="breadcrumb-item active">Edit</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        @card
                            @slot('title')
                            <br>
                            <button onclick="goBack()" class="btn btn-primary" id="goback" style="margin-top: -1.5%"><i class="fa fa-angle-double-left"></i>Go Back</button>
                            <br>
                            @endslot
                            
                            @if (session('error'))
                                @alert(['type' => 'danger'])
                                    {!! session('error') !!}
                                @endalert
                            @endif
                            
                            <form action="{{ route('salary.update', $salary->salary_id) }}" method="get">
                                @csrf
                                <div class="form-group">
                                    <label for="">Nilai salary</label>
                                    <input type="text" name="salary_value" id="nilai"
                                        value="{{ $salary->salary_value }}"
                                        class="form-control {{ $errors->has('salary_value') ? 'is-invalid':'' }}" required>
                                    <p class="text-danger">{{ $errors->first('salary_value') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="">Tanggal berlaku - Tanggal Berakhir</label>
                                    <input type="text" name="due_date" id="date" value="{{ $salary->tgl_berlaku ." - " . $salary->tgl_deadline }}"
                                       class="form-control {{ $errors->has('due_date') ? 'is-invalid':'' }}">
                                    <p class="text-danger">{{ $errors->first('due_date') }}</p>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary btn-sm">
                                        <i class="fa fa-send"></i> Update
                                    </button>
                                </div>
                            </form>
                            @slot('footer')

                            @endslot
                        @endcard
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('js')
  <script src="{{ asset('public/js/moment.min.js') }}"></script>
  <script src="{{ asset('public/js/daterangepicker.min.js') }}"></script>
  
  <script>
    var rupiah = document.getElementById("nilai");
    rupiah.addEventListener("keyup", function(e) {
  
      rupiah.value = formatRupiah(this.value, "Rp. ");
    });


    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
      var number_string = angka.replace(/[^,\d]/g, "").toString(),
        split = number_string.split(","),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

      // tambahkan titik jika yang di input sudah menjadi angka ribuan
      if (ribuan) {
        separator = sisa ? "." : "";
        rupiah += separator + ribuan.join(".");
      }

      rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
      return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
    }

  </script>

  <script>
$(function() {
  $('input[name="due_date"]').daterangepicker({
    opens: 'center',
    locale: { 
      format: 'DD-MMMM-YYYY',

    }
  });
});
</script>
  
@endsection