@php
  
  @endphp
           
    <div class="modal-body">

            <div class="">
                    <!-- Widget: user widget style 1 -->
                    <div class="card card-widget widget-user">
                      <!-- Add the bg color to the header using any of the bg-* classes -->
                    <!-- foreach ($user->getRoleNames() as $role) -->
                        <!-- if ($role == 'admin') -->
                        <div class="widget-user-header text-black"
                        style="background: url('../dist/img/photo4.png') center center;">
                              <h3 class="widget-user-username" style="text-align:center">Detail History</h3>
                        </div>
                        <div class="widget-user-image">
                                <!-- <img class="img-circle elevation-2" src="../dist/img/user1-128x128.jpg" alt="User Avatar"> -->
                              </div>
                        <!-- else -->
                        <!-- <div class="widget-user-header text-black"
                        style="background: url('../dist/img/boxed-bg.png') center center;">
                            <h3 class="widget-user-username" style="text-align:center">{user->user_name}</h3>
                        </div>
                        <div class="widget-user-image">
                                <img class="img-circle elevation-2" src="../dist/img/user3-128x128.jpg" alt="User Avatar">
                              </div> -->
                        <!-- endif -->
                      <!-- endforeach -->
                     
                      <div class="card-footer">
                        <div class="row">
                          <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <span class="description-text">Tanggal berlaku</span>
                                <h5 class="description-header">{{$salary->tgl_berlaku}}</h5>


                              
                              
                            </div>
                            <!-- /.description-block -->
                          </div>
                          <!-- /.col -->
                          <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <span class="description-text">Tanggal berakhir</span>
                                <h5 class="description-header">{{$salary->tgl_deadline}}</h5>
                              
                            </div>
                            <!-- /.description-block -->
                          </div>
                          <!-- /.col -->
                          
                      
                        <!-- foreach ($user->getRoleNames() as $role) -->
                            <!-- if ($role == 'admin') -->
                            <div class="col-sm-4">
                                <div class="description-block">
                                  
                                  @if($salary->deleted_at != null)
                                  <span class="description-text">Restore</span><br/>
                                  <a href="{{route('salary.restore', $salary->salary_id)}}" class="btn btn-primary btn-sm"  style="margin-top: 5px; padding: 4px 11px;"><i class="fa fa-recycle"></i></a>
                                  @else
                                  <span class="description-text">Total salary</span><br/>
                                  <span class="badge badge-secondary">{{$jumlah}}</span>
                                  @endif
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- else -->
                            <!-- <div class="col-sm-4">
                                <div class="description-block">
                                    <span class="description-text">ROLE</span><br/>
                                      <span class="badge badge-success">{role}</span>
                                    </div>
                                     /.description-block -->
                               <!-- </div> -->
                            <!-- endif -->
                        <!-- endforeach -->
                        


                          <!-- /.col -->
                        </div>
                        <!-- /.row -->
                      </div>
                    </div>
                    <!-- /.widget-user -->
                  </div>

           
      <div class="card">
    <div class="card-header">
      <h3 class="card-title">User Iputers</h3>
      
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    
    <div class="card-body p-0">
            
      <table class="table table-striped projects">
          <thead>
              <tr>
                  <th style="width: 2%">
                      No
                  </th>
                  <th style="width: 30%">
                      Data
                  </th>
                  <th style="width: 50%">
                      Value
                  </th>
                  
              </tr>
          </thead>
          <tbody>
            @foreach($report as $data)
                <tr>
                    <td>
                        #
                    </td>
                    <a>
                        <td>
                          {{$data->karyawan}}
                        </td>
                    </a>
                    
                    <td>
                        <ul class="list-inline">
                          <li class="list-inline-item">
                                Total input : 
                               <mark class="badge badge-primary">{{$data->total_input}}</mark>
                                &nbsp;| 
                          </li>                 
                          <li class="list-inline-item">
                              Salary : 
                               <mark class="badge badge-success">{{$data->total_salary}}</mark>
                          </li>                 
                        </ul>
                    </td>
                   
                </tr>
                @endforeach

                
                <!-- <tr>
                    <td>
                        #
                    </td>
                    <a>
                        <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                              Unvalid Input
                            </li>
                            <li class="list-inline-item">
                              Unvalid Input2
                            </li>
                          </ul>
                        </td>
                    </a>
                    
                    <td>
                        <ul class="list-inline">
                          <li class="list-inline-item">
                            <mark class="badge badge-warning">{inputerunCompliteCount}</mark>
                          </li>                 
                        </ul>
                    </td>
                   
                </tr>

               <tr>
                  <td>
                      #
                  </td>
                  <td>
                      <a>
                        Valid Input
                      </a>
                  </td>
                  <td>
                      <ul class="list-inline">
                        <li class="list-inline-item">
                            <mark class="badge badge-success">{inputercompliteCount}</mark>
                        </li>                 
                      </ul>
                  </td>
              </tr>
              <tr>
                  <td>
                      #
                  </td>
                  <td>
                      <a>
                          Total Input
                      </a>
                  </td>
                  <td>
                      <ul class="list-inline">
                        <li class="list-inline-item">
                            <mark class="badge badge-info">{inputerallinputCount}</mark>
                        </li>                 
                      </ul>
                  </td>
              </tr>
              <tr>
                    <td>
                        #
                    </td>
                    <td>
                        <a>
                            Salary
                        </a>
                    </td>
                    <td>
                        <ul class="list-inline">
                          <li class="list-inline-item">
                              
                                <mark class="badge badge-dark"></mark>
                              
                                <mark class="badge badge-dark">0</mark>
                              
                              
                          </li>                 
                        </ul>
                    </td>
                </tr> -->

          </tbody>
      </table>
    </div>
</div>
</div>