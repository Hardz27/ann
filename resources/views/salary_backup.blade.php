
@extends('layouts.master')

@section('title')
    <title>Manage Salary</title>
@endsection

@section('css')
    <style type="text/css">
        .tab-pane{ 
            height:150px;
            overflow-y:scroll;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker.css') }}" />
@endsection




@section('content')
    <div class="content-wrapper" >
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Manage Salary</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Manage Salary</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        @card
                        
                            @slot('title')
                            <div class="row">
                                <div class="col-sm-6">
                                  <h4 class="card-title">Form nilai Salary</h4>
                                </div>
                              </div>
                            
                            @endslot

                                  <!-- <div id="OR_v2">OR</div> -->
                            <!-- =================================================== -->
                            <!-- <div id="form-input-excel" class="card-title"></div> -->
                            <!-- =================================================== -->

                            <form action="{{route('salary.add')}}" class="validate-form show-modal" method="post">

                                @csrf
                                    @if (session('error'))
                                        <div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data telah terdaftar </div>
                                    @endif
                                    @if (session('success'))
                                        <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert">&times;</a> Data berhasil diinput</div>
                                    @endif
                                   

                                    <div class="form-group has-feedback" style="margin-top: 20px;">
                                        <label for="">Nilai Salary</label>
                                        <input type="text" name="salary_value" id="nilai" class="form-control" style="width: 60%; float: right;" placeholder="Contoh input:10000" required>
                                    </div>
                                    <div class="form-group has-feedback" style="margin-top: 20px;">
                                        <label for="">Tanggal Mulai-berkahir</label>
                                        <input type="text" name="due_date" id="date" class="form-control" style="width: 60%; float: right;" required>
                                    </div>


                                    <div class="form-group" style="float :right;">
                                        <button id="check" type="submit" class="btn btn-success btn-md">Simpan</button>
                                    </div>
                            </form>


                            {{-- form history data --}}


                            @slot('footer')

                            @endslot
                        @endcard
                    </div>
                <div class="col-md-6">
                   <div class="card">
                      <div class="card-header">
                        <h3 class="card-title">History Salary</h3>

                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                          <!-- <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fas fa-times"></i></button> -->
                        </div>
                      </div>
                      <div class="card-body p-0">
                        <div class="table-responsive">
                        <table class="table table-striped projects"  id="salary">
                            <thead>
                                <tr>
                                    <th style="width: 1%">
                                        No
                                    </th>
                                    <th style="width: 10%">
                                        Nilai Salary
                                    </th>
                                    <th style="width: 15%">
                                        Tanggal Berlaku
                                    </th>
                                    <th style="width: 15%">
                                        Tanggal Berakhir
                                    </th>
                                    <th style="width: 12%">
                                        Dibuat oleh
                                    </th>
                                    <!-- <th style="width: 10%" class="text-center">
                                        action
                                    </th> -->
                                </tr>
                            </thead>
                            <tbody>
                              @php
                                  $no = 1; 
//                                  dd(count($salary_value),$salary_value);
                              @endphp
                              @if(count($salary_value) == 0 || $salary_value == false)
                                  <tr>
                                    <td class="text-center" colspan="8">Tidak ada data </td>
                                  </tr>
                              @else
                                @foreach($salary_value as $item)
                                 @if(strtotime(date('d-M-Y')) >= strtotime($item->tgl_berlaku) &&  strtotime(date('d-M-Y')) <= strtotime($item->tgl_deadline))
                                  <tr style="background-color: #a9f491">
                                 @else
                                  <tr>
                                 @endif
                                    <td>
                                        {{$no++}}
                                    </td>
                                    <td>
                                        <a>
                                            Rp. {{$item->salary_value}}
                                        </a>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            {{$item->tgl_berlaku}}
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            {{$item->tgl_deadline}}
                                        </ul>
                                    </td>
                                    <td>
                                        {{$item->created_by}}
                                    </td>
                                    <!-- <td class="text-center">
                                        <a href="" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a>
                                    </td> -->
                                  </tr> 
                                  
                                @endforeach
                              @endif
                            </tbody>
                        </table>
                        {{ $salary_value->links() }}
                      </div>
                      </div>
                      <!-- /.card-body -->
                    </div>
                  </div>



                </div>
                  <div class="row">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-8">
                   <div class="card">
                      <div class="card-header">
                        <h3 class="card-title">Report Salary</h3>

                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                          <!-- <button type="button" class="btn btn-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fas fa-times"></i></button> -->
                        </div>
                      </div>
                      <div class="card-body p-0">
                        <div class="table-responsive">
                        <table class="table table-striped projects"  id="history">
                            <thead>
                                <tr>
                                    <th style="width: 1%">
                                        No
                                    </th>
                                    <th style="width: 10%">
                                        Nilai Salary
                                    </th>
                                    <th style="width: 15%">
                                        Tanggal Berlaku
                                    </th>
                                    <th style="width: 15%">
                                        Tanggal Berakhir
                                    </th>
                                    <th style="width: 12%">
                                        Total Salary
                                    </th>
                                    <th style="width: 10%" class="text-center">
                                        action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                              @php
                                  $no = 1; 
                                  $total = 0; 
                              @endphp
                              @if(count($report) == 0 || $report == false)
                                  <tr>
                                    <td class="text-center" colspan="8">Tidak ada data </td>
                                  </tr>
                              @else
                                @foreach($report as $items)
                                @php $total += $items->total; @endphp
                                  <tr>
                                    <td>
                                        {{$no++}}
                                    </td>
                                    <td>
                                            Rp. {{$items->nilai}}
                                        </a>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            {{$items->tanggal_berlaku}}
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            {{$items->tanggal_deadline}}
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            {{formatRupiah($items->total)}}
                                        </ul>
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('salary.detail') }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-view"></i></a>
                                    </td>
                                  </tr> 
                                  
                                @endforeach
                                <tr style="background: #72e3d7;">
                                  <td colspan="4"> Total Keseluruhan</td>
                                  <td colspan="2">{{formatRupiah($total)}}</td>
                                </tr>
                              @endif
                            </tbody>
                        </table>
                        
                      </div>
                      </div>
                      <!-- /.card-body -->
                    </div>
                  </div>
                  <div class="col-md-2">
                  </div>
                  </div>
            </div>
        </section>

    </div>

    @include('checks.excel_sample')
    </div>
    </div>

@endsection

  
@section('js')
  <script src="{{ asset('js/pop-up.js') }}"></script> 
  <script src="{{ asset('plugins/tablefilter/tablefilter.js') }}"></script>
  <script src="{{ asset('js/filter-salary.js') }}"></script> <!-- ini dibutuhin di leads -->
  <script src="{{ asset('js/filter-history.js') }}"></script> <!-- ini dibutuhin di leads -->
  <script src="{{ asset('js/xlsx.core.min.js') }}"></script>  <!-- ini dibutuhin di check  -->
  <script src="{{ asset('js/xls.core.min.js') }}"></script>  <!-- ini dibutuhin di check  -->
  <script src="{{ asset('js/excel.js') }}"></script>
  <script src="{{ asset('js/moment.min.js') }}"></script>
  <script src="{{ asset('js/daterangepicker.min.js') }}"></script>
  
  <script>
    var rupiah = document.getElementById("nilai");
    rupiah.addEventListener("keyup", function(e) {
  
      rupiah.value = formatRupiah(this.value, "Rp. ");
    });


    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
      var number_string = angka.replace(/[^,\d]/g, "").toString(),
        split = number_string.split(","),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

      // tambahkan titik jika yang di input sudah menjadi angka ribuan
      if (ribuan) {
        separator = sisa ? "." : "";
        rupiah += separator + ribuan.join(".");
      }

      rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
      return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
    }

  </script>

  <script>
$(function() {
  $('input[name="due_date"]').daterangepicker({
    opens: 'left',
    locale: { format: 'DD-MM-YYYY' }
  });
});
</script>
  
@endsection