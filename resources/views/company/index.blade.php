@extends('layouts.master')

@section('title')
    <title>Manajemen Company</title>
@endsection

@section('css')
{{-- <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css"> --}}
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}">

    <style>
        th{
            border: 1px solid black;
        }
    </style>
    {{-- <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker-bs3.css') }}"> --}}
    
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Manajemen Company</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Company</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

{{--
        <div class="col-md-12">
                @card
                
                    @slot('title')
                    <div style="float:left">Filter Company</div>
                    @php
                    date_default_timezone_set("Asia/Jakarta");
                @endphp
                <div class="row">
                    <div class="col-sm-6" style='text-align: right;'></div>
                    <div class="col-sm-6" style='text-align: right; padding-left:20%'> @php echo   date("l d/m/Y ");  @endphp</div>
                </div>
                
                    
                    @endslot

                    <form action="" method="get">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Mulai Tanggal</label>
                                    <input type="text" name="start_date" 
                                        class="form-control {{ $errors->has('start_date') ? 'is-invalid':'' }}"
                                        id="start_date"
                                        value="{{ request()->get('start_date') }}"
                                        >

                                </div>
                               
                               
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                            <label for="">Sampai Tanggal</label>
                                            <input type="text" name="end_date" 
                                                class="form-control {{ $errors->has('end_date') ? 'is-invalid':'' }}"
                                                id="end_date"
                                                value="{{ request()->get('end_date') }}">
                                        </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">User</label>
                                    <select id="user" name="user_id" class="form-control myeditable">
                                        <option value="" selected="selected">Pilih</option>
                                        @foreach ($users as $user)
                                        <option id="user" value="{{ $user->user_id }}"
                                            {{ request()->get('user_id') == $user->user_id ? 'selected':'' }}>
                                            {{ $user->user_name }} - {{ $user->user_email }}
                                        </option>
                                        @endforeach
                                    </select>
                                   

                                </div>
                                
                                
                                
                                <div class="row" style="padding-left:30%">
                                    <div class="col-md-2">
                                    <div class="form-group"   >
                                        <button class="btn btn-primary btn-md">Cari</button>
                                        
                                    </div>
                                    </div>
                                    <div class="col-md-4">
                                            <div class="form-group" style="padding-right:10%">
                                                <button id="reset" class="btn pull-right">Reset</button>    
                                            </div>
                                            </div>
                                </div>

                            </div>
                            
                        </div>
                    </form> 

                    @slot('footer')

                    @endslot
                @endcard
            </div>
            --}}

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        @card
                            @slot('title')
                            Data Company
                            
                            {{-- @php
                                    date_default_timezone_set("Asia/Jakarta");
                                @endphp
                                <div class="row">
                                    <div class="col-sm-6">@php echo date("l d/m/Y ");@endphp</div>
                                    <div class="col-sm-6" style='text-align: right;'> @php echo date("h:i:sa")  @endphp</div>
                                </div> --}}
                            @endslot
                            
                            @if (session('success'))
                                @alert(['type' => 'success'])
                                    {!! session('success') !!}
                                @endalert
                            @endif

                            {{-- <div class="col-12">
                                    <div class="card"> --}}
                                      {{-- <div class="card-header">
                                        <h3 class="card-title"></h3>
                                      </div> --}}
                                      <!-- /.card-header -->
                                      
                                      <div class="card-body">
                                        
                                        <div class="table-responsive mailbox-messages">
                                        <form action="/company/delete_check" method="get">
                                        <table id="example2" class="table table-bordered table-hover">
                                            
                                          <thead>
                                          <tr>
                                            <th style="width: 60px; border: 2px solid #9A9B93;" >No</th>
                                            <th style="text-align: center; border: 2px solid #9A9B93;">Company</th>
                                            <th style="text-align: center; border: 2px solid #9A9B93;">Contact</th>
                                            <th style="text-align: center; border: 2px solid #9A9B93;">Address</th>
                                            <th style="text-align: center; border: 2px solid #9A9B93;">Status</th>
                                            <th style="text-align: center; border: 2px solid #9A9B93;">Type</th>
                                            <th style="text-align: center; border: 2px solid #9A9B93;">Aksi</th>
                                            <th style="width: 15% text-align: center; border: 2px solid #9A9B93;">
                                            
                                                <div class="icheck-primary" style="padding-left:13%">  
                                                    <input   type="checkbox" id="checkall" onclick="checkAll()">
                                                    <label style="padding-top:5%" for="checkall"></label>
                                                    <button class="btn btn-danger btn-sm" name="archive" value="1" type='submit'><i class="fa fa-archive"></i></button> 
                                                </div>
                                            </th>
                                          </tr>
                                          </thead>
                                          @php
                                              $no = 1;
                                          @endphp
                                          @forelse ($companies as $item)    
                                        <tbody>
                                        <tr>
                                        <td style="width: 3%; text-align: center;">{{$no++}}</td>    
                                        <td>{{$item->company_name}}</td>
                                        <td>{{$item->company_telpon_office}}</td>
                                        <td>{{$item->company_address}}</td>
                                        <td>{{$item->company_status}}</td>
                                        <td>{{$item->company_type}}</td>
                                        <td style="text-align: center;">
                                                {{-- <form action="{{ route('company.index') }}" method="POST"> --}}
                                                    {{-- @csrf --}}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    {{-- <i href="#" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal@php echo ($no-2);@endphp"><i class="fa fa-info"></i></i> --}}
                                                    
                                                    <a href="{{route('company.detail', $item->company_id)}}" class="btn btn-info btn-sm btn-show-detail" style="padding: 4px 11px;"><i class="fa fa-info"></i></a>
                                                    <a href="{{ route('company.edit',  $item->company_id) }}" class="btn btn-primary btn-sm" style="padding: 4px 6px;"><i class="fa fa-edit"></i></a>
                                                    {{-- <a href="{{route('company.delete_form', $item->company_id)}}" class="btn btn-danger btn-sm btn-show-delete"><i class="fa fa-archive"></i></a> --}}
                                                    
                                                    
                                                    {{-- <a href="#" data-toggle="modal" data-target="#delete@php echo ($no-2);@endphp" class="btn btn-danger btn-sm"><i class="fa fa-archive"></i></a> --}}
                                                {{-- </form> --}}
                                            </td>
                                            <td style="width: 10%; text-align: center;">
                                                    <div class="icheck-primary">
                                                        <input type="checkbox" class="checkbox" value="{{$item->company_id}}" name="checked[]" id="check{{$no}}">
                                                        <label for="check{{$no}}"></label>
                                                    </div>
                                            
                                                     

                                           </td>
                                          </tr>
                                          @empty
                                          <tr>
                                              <td colspan="7" class="text-center">Tidak ada data</td>
                                          </tr> 
                                          @endforelse
                                   
                                          </tbody>
                                          <tfoot>
                                               
                                          </tfoot>
                                          
                                        </table>
                                        </form>
                                    </div>
                                        <br>
                                        {{-- <div class="card-tools valid">
                                                <ul class="pagination-valid pagination-md m-0 float-right">
                                                  {{$companies->links()}}
                                                </ul>
                                            </div> --}}
                                      
                                      </div>
                                      <!-- /.card-body -->
                                    {{-- </div> --}}
                                    <!-- /.card -->
 
                        
                            @slot('footer')

                            @endslot
                        @endcard
                    {{-- </div> --}}

 
                
            </div>
        </section>

        @include('company.delete')
        {{-- @include('company.modal') --}}

    </div>
@endsection

@section('js')
{{-- <script>
function() {
  var clicks = $(this).data('clicks')
  if (clicks) {
    //Uncheck all checkboxes
    $('.mailbox-messages input[type=\'checkbox\']').prop('checked', false)
    $('.checkbox-toggle .fa.fa-check-square-o').removeClass('fa-check-square-o').addClass('fa-square-o')
  } else {
    //Check all checkboxes
    $('.mailbox-messages input[type=\'checkbox\']').prop('checked', true)
    $('.checkbox-toggle .fa.fa-square-o').removeClass('fa-square-o').addClass('fa-check-square-o')
  }
  $(this).data('clicks', !clicks)
}
</script> --}}

<script>
    function checkAll()
    {
        var checkall = $('#checkall')[0];
        
        if (checkall.checked == true) {
            $(".checkbox").prop('checked', 'checked');
        } else {
            $(".checkbox").prop('checked', '');
        }

    }
    $("#reset").on("click", function () {
    // $('#user option').prop('selected', function() {
    //     return this.defaultSelected;
    // });
    $('#user option').removeAttr('selected');
    $('#start_date').datepicker('setDate', null);
    $('#end_date').datepicker('setDate', null);

    // $("#start_date").val("");
});
</script>

<script>
        $(function () {
          $("#example1").DataTable();
          $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": true,
          });
        });
</script>
    {{-- <script src="{{ asset('plugins/datatables2/jquery.dataTables.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script> --}}
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    {{-- <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('plugins/tablefilter/tablefilter.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/filter-company.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/datepicker.js') }}"></script> --}}
    <script src="{{ asset('js/pop-up.js') }}"></script>
    <script src="{{ asset('js/validation.js') }}"></script>
    <script language='javascript'>
        function happycode(){
           $('#company').find('.rdiv .row').append('<a href="/company/export_excel" class="export btn btn-success btn-sm">Export to excel<i class="fa fa-file-excel-o" style="padding-left: 10px;"></i></a>');
        }
        
    </script>
    <script>
        //call after page loaded
        window.onload=happycode ; 
    </script>
<script>
$("#start_date").datepicker({ 
    autoclose: true,
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    minDate: new Date(),
    maxDate: '+2y',
    onSelect: function(date){

        var selectedDate = new Date(date);
        var msecsInADay = 86400000;
        var endDate = new Date(selectedDate.getTime() + msecsInADay);

       //Set Minimum Date of EndDatePicker After Selected Date of StartDatePicker
        $("#end_date").datepicker( "option", "minDate", endDate );
        $("#end_date").datepicker( "option", "maxDate", '+2y' );

    }
});


$("#end_date").datepicker({ 
    autoclose: true,
    dateFormat: 'yy-mm-dd',
    changeMonth: true
});
// $('.datepicker').datepicker('update');


$("#start_date").datepicker( "refresh" )


$("#end_date").datepicker( "refresh" )

 </script>


  
@endsection