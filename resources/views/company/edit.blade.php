@extends('layouts.master')

@section('title')
    <title>Form Input</title>
@endsection

@section('css')
    <style type="text/css">
        .tab-pane{
            height:150px;
            overflow-y:scroll;
        }
    </style>
@endsection



@section('content')
            <!--Login modal-->
    <div class="content-wrapper" >
        <div class="content" style="background: white;">
            <div class="header">
                <br>
                <h4 class="modal-title">
                    <br><br>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-1 extra-w3layouts"> </div>
                    <div class="col-md-10 extra-w3layouts" style="border: 1px dotted #C2C2C2;padding-right: 30px; margin-bottom: 100px;padding-bottom: 30px;">
                @if (session('error'))
                    @alert(['type' => 'danger'])
                        {!! session('error') !!}
                    @endalert
                @endif

                @if (session('success'))
                    @alert(['type' => 'success'])
                        {!! session('success') !!}
                    @endalert
                @endif
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" style="padding: 10px 20px;margin-top: 40px;">
                            <p id="header-name">Company Information</p>
                            <button onclick="goBack()" class="btn btn-primary" id="goback"><i class="fa fa-angle-double-left"></i>Go Back</button>                           
                           
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content" style="padding-left: 15px;">
                            <div class="active" id="Login">

                        <form action="{{route('company.update', $company->company_id)}}"  class="form-horizontal" method="post">
                                @csrf
                                {{-- {{csrf_field()}} --}}
                                <div class="panel-group" id="accordion">
                                <table id="company" class="table table-hover">
                                
                                <div class="form-group colinp" style="margin-bottom: 0px;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p id="kolom">Company names</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p id="kolom">Company Address</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group colinp" style="margin-bottom: -10px;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" name="company_name" value="{{ $company->company_name }}" class="form-control {{ $errors->has('company_name') ? 'is-invalid': '' }}" id="company_name" placeholder="Company name" required />
                                            <p class="text-danger">{{ $errors->first('company_name') }}</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" value="{{$company->company_address}}" class="form-control {{$errors->has('company_address') ? 'is-invalid': '' }}" name="company_address" placeholder="Company Address"  required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group colinp" style="margin-bottom: 0px;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p id="kolom">Company Business</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p id="kolom">General Information/Description</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group colinp" style="margin-bottom: -10px;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="company_field" type="text" value="{{$company->company_field}}" class="form-control {{$errors->first('company_field') ? 'is-invalid': ''}}" placeholder="Company Business" required/>
                                            <p class="text-danger">{{$errors->first('company_field')}}</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" value="{{$company->company_desc}}" class="form-control {{$errors->has('company_desc') ? 'is-invalid':  '' }}" name="company_desc" placeholder="Company Short Description"  required/>
                                            <p class="{{$errors->first('company_desc')}}"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group colinp" style="margin-bottom: 0px;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p id="kolom">Office Number</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p id="kolom">Faximile</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group colinp" style="margin-bottom: -10px;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" name="company_telpon_office" value="{{$company->company_telpon_office}}" class="form-control {{$errors->has('company_telpon_office') ? 'is-invalid': ''}}" placeholder="Company Office Number"  required/>
                                            <p class="text-danger">{{$errors->first('company_telpon_office')}}</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" name="company_faximile" value="{{$company->company_faximile}}" class="form-control {{$errors->has('company_faximile') ? 'is-invalid': '' }}" placeholder="Company Faximile" required/>
                                            <p class='text-danger'>{{$errors->first('company_faximile')}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group colinp" style="margin-bottom: 0px;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p id="kolom">Company Type</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p id="kolom">Company Status</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group colinp" style="margin-bottom: -10px;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <select name="company_type" class="form-control {{$errors->has('company_type') ? 'is-invalid': ''}}" >
                                                <option value="{{$company->company_type}}">{{$company->company_type}}</option>
                                                <option value="tekno">Tekno</option>
                                                <option value="nontekno">Nontekno</option>      
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <select name="company_status" class="form-control {{$errors->has('company_status') ? 'is-invalid': ''}}">
                                                  <option value="{{$company->company_status}}">{{$company->company_status}}</option>
                                                
                                                    <option value="aktif">Aktif</option>      
                                                    <option value="nonaktif">Nonaktif</option>      
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </table>
                            
                            </div>
                            <div style="float: right;">
                            <br>
                            <button type="submit" class="btn btn-success btn-md" style="background: #28a745;">Submit</button>
                            <button type="reset" class="btn btn-danger btn-md" value="Reset">Reset</button>
                            
                        </div>
                                </form>
                            </div>
                            
                        </div>
                        <div id="OR"  ><img src="{{ asset('images/icon.png') }}" style="max-width: 100%;"></div>
                    </div>
                    <div class="col-md-1 extra-w3layouts"> </div>
                </div>
                
            </div>
        </div>
    </div>



    @endsection

@section('js')


<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
