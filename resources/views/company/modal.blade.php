{{-- 
@php

function validation($n){
     $i = 0; 
     $count = $n;
     while ($n > 0) 
     { 
         $binaryNum; 
         $binaryNum[$i] = $n % 2; 
         $n = (int)($n / 2); 
         $i++; 
     }
     for ($j = $i - 1; $j >= 0; $j--){ 
         $bin_val[] = $binaryNum[$j];
     }

     while(count($bin_val)< 8 ){
        array_unshift($bin_val, 0);
   }
   
     return $bin_val;
}

if($companies->company_validation != 0){
  $arr = validation($companies->company_validation);
}
else{
$arr = array(0,0,0,0,0,0,0,0);
}

@endphp


@php $modal = 0; @endphp
@foreach($companies as $key => $item)
<div class="modal fade" id="myModal@php echo $modal;@endphp" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Information Details</h4>
        </div>
        <form action="#"  method="post">
        <div class="modal-body">
          <div class="card">
        <div class="card-header">
          <h3 class="card-title">Company Information</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%">
                          No
                      </th>
                      <th style="width: 30%">
                          Data
                      </th>
                      <th style="width: 50%">
                          Isi
                      </th>
                      <th style="width: 18%" class="text-center">
                          Status
                      </th>
                  </tr>
              </thead>
              <tbody>

                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                        
                          <a>
                              Company Name
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                    {{$item->company_name}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                        @if ($arr[0] == 1)
                            <span class="badge badge-success">Verivied</span>
                        @elseif($arr[0] == 0)
                            <span class="badge badge-danger">Unverified</span>
                        @endif
                      </td>
                    </tr>

                    <tr>
                        <td>
                            #
                        </td>
                        <a>
                            <td>
                                Office Number
                        </td>
                        </a>
                        <td>
                            <ul class="list-inline">
                              <li class="list-inline-item">
                                    {{$item->company_telpon_office}}
                              </li>                 
                            </ul>
                        </td>
                        <td class="project-state" style="text-align: center;">
                            @if ($arr[1] == 1)
                                <span class="badge badge-success">Verivied</span>
                            @elseif($arr[1] == 0)
                                <span class="badge badge-danger">Unverified</span>
                            @endif
                        </td>
                    </tr>


                   <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Company type
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->company_type}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                        @if ($arr[2] == 1)
                            <span class="badge badge-success">Verivied</span>
                        @elseif($arr[2] == 0)
                            <span class="badge badge-danger">Unverified</span>
                        @endif
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Company Business
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->company_field}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                        @if ($arr[3] == 1)
                            <span class="badge badge-success">Verivied</span>
                        @elseif($arr[3] == 0)
                            <span class="badge badge-danger">Unverified</span>
                        @endif
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              General Information/Description
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->company_desc}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                        @if ($arr[4] == 1)
                            <span class="badge badge-success">Verivied</span>
                        @elseif($arr[4] == 0)
                            <span class="badge badge-danger">Unverified</span>
                        @endif
                      </td>
                  </tr>

                

                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Faximile
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->company_faximile}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                        @if ($arr[5] == 1)
                            <span class="badge badge-success">Verivied</span>
                        @elseif($arr[5] == 0)
                            <span class="badge badge-danger">Unverified</span>
                        @endif
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Address
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->company_address}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                        @if ($arr[6] == 1)
                            <span class="badge badge-success">Verivied</span>
                        @elseif($arr[6] == 0)
                            <span class="badge badge-danger">Unverified</span>
                        @endif
                      </td>
                  </tr>
                  <tr>
                      <td>
                          #
                      </td>
                      <td>
                          <a>
                              Company status
                          </a>
                      </td>
                      <td>
                          <ul class="list-inline">
                            <li class="list-inline-item">
                                  {{$item->company_status}}
                            </li>                 
                          </ul>
                      </td>
                      <td class="project-state" style="text-align: center;">
                        @if ($arr[7] == 1)
                            <span class="badge badge-success">Verivied</span>
                        @elseif($arr[7] == 0)
                            <span class="badge badge-danger">Unverified</span>
                        @endif
                      </td>
                  </tr>
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
        <!-- /.card-body -->
      </div>
      </div>
      @php $modal++;@endphp
      @endforeach --}}