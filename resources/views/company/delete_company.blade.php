<form action="/company"  method="post">
    {{-- <div class="modal-body"> --}}

  <!-- ============================================================================= -->

  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Company Data</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="card-body p-0">
        
      <table class="table table-striped projects">
          <thead>
              <tr>
                  <th style="width: 2%">
                      #
                  </th>
                  <th style="width: 30%">
                      Nama Perusahaan
                  </th>
                  <th style="width: 50%">
                      Dibuat Pada
                  </th>
                  <th style="width: 18%" class="text-center">
                      Dibuat Oleh
                  </th>
              </tr>
          </thead>
          <tbody>
              
              <tr>
                  <td>
                      #
                  </td>
                  <td>
                      <a>
                        {{$company->company_name}}
                      </a>
                  </td>
                  <td>
                      <ul class="list-inline">
                        <li class="list-inline-item">
                              {{$company->created_at}}
                        </li>                 
                      </ul>
                  </td>
                  <td class="project-state" style="text-align: center;">
                    {{-- <label for="" class="badge badge-secondary">{{$company->created_by}}</label> --}}
                    
                    @foreach ($role as $row)
                                                
                        @if ($row->name == 'admin')
                            <label for="" class="badge badge-secondary">{{$company->created_by}}</label>    
                        @else
                            <label for="" class="badge badge-success">{{$company->created_by}}</label>
                        @endif
                        @break
                        
                    @endforeach
                    
                  </td>
              </tr>
          
          </tbody>
      </table>          
    </div>
    </div> 

    {{-- </div>   --}}
    </form>

    <div class="modal-footer" style="text-align: center;">
      <form action="{{ route('company.destroy', $company->company_id) }}" method="POST">
          @csrf
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          <button class="btn btn-danger">Send to archive <i class="fa fa-archive"></i></button>
       </form>
    </div>
