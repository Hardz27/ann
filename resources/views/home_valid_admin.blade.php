
@extends('layouts.app')
@section('css2')
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endsection
@section('content2')

<form action="/company"  method="post" id="detailform">
<div class="modal-body">
  <div class="card">
<div class="card-header">
  <h3 class="card-title">Company Information</h3>

  <div class="card-tools">
    <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
      <i class="fa fa-minus"></i></button>
  </div>
</div>

<div class="card-body p-0">
  <table class="table table-striped projects">
      <thead>
          <tr>
              <th style="width: 2%">
                  No
              </th>
              <th style="width: 30%">
                  Company
              </th>
              <th style="width: 50%">
                  Domain
              </th>
              <th style="width: 18%" class="text-center">
                  Status
              </th>
          </tr>
      </thead>
      <tbody>
        @php
            $no = 1;
        @endphp
        @forelse ($adminComplite as $item)
            
          <tr>
              <td>
                  {{$no++}}
              </td>
              <td>
                
                  <a>
                    {{$item->company_name}}
                  </a>
              </td>
              <td>
                  <ul class="list-inline">
                    <li class="list-inline-item">
                            {{$item->domain_name}}
                    </li>                 
                  </ul>
              </td>
            <td class="project-state" style="text-align: center;">
                <span >{{$item->created_by}}</span>
          
            </td>

            </tr>
            
            @empty
            <tr>
                <td colspan="4" class="text-center">Tidak ada data</td>
            </tr> 
            @endforelse
       
      </tbody>
  </table>
</div>
</div>

<div class="card-tools valid">
<ul class="pagination-valid pagination-sm m-0 float-right">
  {{$adminComplite->links()}}
</ul>
</div>
</div>
@endsection

<!-- /.card-body -->

