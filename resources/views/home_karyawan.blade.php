
@extends('layouts.app')
@section('css2')
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endsection
@section('content2')
<form action="/pic"  method="post" id="detailform">



  
    <!-- USERS LIST -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Latest Members</h3>

        <div class="card-tools">
          <span class="badge badge-danger">{{count($karyawan)}} Karyawan</span>

            <button type="button" class="btn btn-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>

        </div>
      </div>
     
      <div class="card-body">
        <ul class="users-list">
          @php
          
          @endphp
            @foreach ($karyawan as $item)
          <li>
              
            <img src="dist/img/user1-128x128.jpg" alt="User Image">
            <a class="users-list-name" href="#">{{$item->user_name}}</a>
            <span class="users-list-date">Rp. {{$item->salary}}</span>
            
          </li>
          
          @endforeach
        </ul>
        
      </div>
   
      <div class="card-footer text-center">
        <a href="{{route('users.index')}}">View All Users</a>
      </div>
      
    </div>

</div>


            </div >
            
                <div class="card-tools karyawan">
                    <ul class="pagination-karyawan pagination-sm m-0 float-left">            
                            
                    </ul>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            
            </div>
           
            
@endsection

<!-- /.card-body -->

