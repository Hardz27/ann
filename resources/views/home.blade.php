@extends('layouts.master')

@section('title')
    <title>Dashboard</title>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main content -->
        <section class="content" id="dw">
            <div class="container-fluid">
                <div class="row">


                        
                    

                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-white">
                            <div class="inner" style="float:inline">
                                @if (auth()->user()->can('leads admin'))
                              <div class="row">
                            <div class="col-md-4">
                                <h3>{{ $NewInput }}</h3>  
                                <p>new Input </p>
                            </div>
                            <div class="col-md-4">
                                <h3>{{ $unCompliteCount }}</h3>  
                                <p>Unvalid</p>
                                
                                
                                
                            </div>

                            <div class="col-md-4">
                                <h3>{{ $validasi }}</h3>  
                                <p>Valid Data</p>
                                <div class="icon">
                                    <i class="fa fa-check-square-o" style="margin-top: 25px;"></i>
                                </div>
                            </div>
                            </div>
                            </div>

         
                                {{-- <a href="{{route('home.unvalid_admin')}}" class="small-box-footer btn-home-unvalid"> more info --}}
                                        <a href="{{route('leads.index')}}" class="small-box-footer"> more info
                                    <i class="fa fa-arrow-circle-right">
                                    </i>
                                </a>
                                  
                                
                                @else
                                    <h3>{{ $inputerunCompliteCount }}</h3> 
                                    <p>Unvalidated Data</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-window-close-o" style="margin-top: 25px;"></i>
                                </div>
                                <a href="{{route('home.unvalid')}}" class="small-box-footer btn-home-unvalid"> more info
                                    <i class="fa fa-arrow-circle-right">
                                    </i>
                                </a>
                                @endif
                          
                        </div>
                    </div>
                   
                     

                    <div class="col-lg-3 col-6">
                            <div class="small-box bg-success">
                                <div class="inner">
                                    @if (auth()->user()->can('leads admin'))
                                    <h3>{{ $picCount }}</h3>
                                    <p>PIC Data</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-address-book-o" style="margin-top: 25px;"></i>
                                </div>
                                {{-- <a href="{{route('home.valid_admin')}}" class="small-box-footer btn-home-valid"> more info --}}
                                        <a href="{{route('pic.index')}}" class="small-box-footer"> more info
                                    <i class="fa fa-arrow-circle-right">
                                    </i>
                                </a>
                                    @else
                                    <h3>{{ $inputerValidasi }}</h3>  
                                    <p>Validasi</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-check-square-o" style="margin-top: 25px;"></i>
                                </div>
                                <a href="{{route('home.valid')}}" class="small-box-footer btn-home-valid"> more info
                                    <i class="fa fa-arrow-circle-right">
                                    </i>
                                </a>  
                                    @endif
                                    
                                    
                            </div>
                        </div>

                    <div class="col-lg-3 col-6">
                            <div class="small-box bg-info">
                                <div class="inner">
                                    @if (auth()->user()->can('leads admin'))
                                        <h3>{{ $TotalInput }}</h3> 
                                        <p>Total Input</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-list-alt" style="margin-top: 25px;"></i>
                                    </div>
                                    <a href="#" class="small-box-footer"> more info
                                        <i class="fa fa-arrow-circle-right">
                                        </i>
                                    </a>   
                                    @else
                                        <h3>{{ $inputerTotalInput }}</h3>
                                        <p>Total Input</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-list-alt" style="margin-top: 25px;"></i>
                                    </div>
                                    <a href="#" class="small-box-footer"> more info
                                        <i class="fa fa-arrow-circle-right">
                                        </i>
                                    </a>
                                    @endif
                             
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">
                            <div class="small-box bg-danger">
                                <div class="inner">
                                    {{-- @foreach ($role as $item) --}}
                                    @if (auth()->user()->can('leads admin'))
                                        <h3>{{formatRupiah($total_salary).',00'}}</h3>    
                                        <p>{{ $user.' Karyawan' }} </p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-usd" style="margin-top: 25px;"></i>
                                    </div>
                                    <a href="{{route('home.karyawan')}}" class="small-box-footer btn-home-karyawan"> more info
                                            {{-- <a href="#" class="small-box-footer "> more info --}}
                                        <i class="fa fa-arrow-circle-right">
                                        </i>
                                    </a>
                                    @else
                                    {{-- @php
                                        dd($gaji);    
                                    @endphp --}}
                                    
                                        <h3>{{formatRupiah($gaji).',00'}}</h3>

                                        <p>Salary</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-usd" style="margin-top: 25px;"></i>
                                    </div>
                                    <a href="#" class="small-box-footer"> more info
                                        <i class="fa fa-arrow-circle-right">
                                        </i>
                                    </a>
                                    @endif  
                            </div>
                        </div>


                    

                </div>
                <div class="row">
                    <canvas id="dw-chart"></canvas>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('js')
    <script src="{{ asset('js/pop-up.js') }}"></script> 
    <script src="{{ asset('js/dashboard.js') }}"></script>
    <!-- <script src="{{ asset('dist/js/pages/dashboard.js') }}"></script> -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <!-- <script src="{{ asset('dist/js/adminlte.js') }}"></script> -->
@endsection