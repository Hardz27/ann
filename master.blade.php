<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    @yield('title')
    

    
    <!-- metatags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Erpo Leads" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"> <!-- ini dibutuhin di semua page  -->
    <link rel='stylesheet prefetch' href="{{ asset('css/font-awesome.min.css') }}"> <!-- ini dibutuhin di semua page  -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}"> <!-- ini dibutuhin di semua page  -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert.css') }}">

    @yield('css')
</head>
<body class='hold-transition sidebar-mini 
                            @if ($page == "Check" || $page == "Leads")
                                @php echo "sidebar-collapse" @endphp
                            @endif'>
<div id="loader-wrapper">
    <div id="loader"></div>
</div>

    <div class="wrapper">
        <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{route('home')}}" class="nav-link">Home</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Contact</a>
                </li>
            </ul>

            <!-- SEARCH FORM -->
            

            {{-- <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="fa fa-comments-o"></i>
                        <span class="badge badge-danger navbar-badge">3</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <a href="#" class="dropdown-item">
                            <div class="media">
                                <img src="dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        Brad Diesel
                                        <span class="float-right text-sm text-danger"><i class="fa fa-star"></i></span>
                                    </h3>
                                    <p class="text-sm">Call me whenever you can...</p>
                                    <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 Hours Ago</p>
                                </div>
                            </div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
                    </div>
                </li>
            </ul> --}}
        </nav>

        @include('layouts.module.sidebar')

        @yield('content')

        @include('layouts._modal')
        @include('layouts._modal_delete')
        @include('layouts._modal_validation')

        @include('layouts.module.footer')

    </div>
    

    <!-- jQuery -->

    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('dist/js/adminlte.js') }}"></script> <!-- ini dibutuhin di home  ================================ -->
    <script src="{{ asset('dist/js/pages/dashboard.js') }}"></script> <!-- ini dibutuhin di home  -->
    <script src="{{ asset('js/sweetalert.min.js') }}"></script> <!-- ini dibutuhin di logout/sidebar  -->
    <script src="{{ asset('js/add-personal.js') }}"></script>
    <script src="{{ asset('js/add-contact.js') }}"></script>
    <script src="{{ asset('js/alert.js') }}"></script> 
    <script src="{{ asset('js/loading.js') }}"></script> <!-- ini dibutuhin di semua page -->
    <script src="{{ asset('js/modernizr-2.6.2.min.js') }}"></script> 
    @yield('js')
    <script type="application/x-javascript">
        addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); }
    </script> 
    

</body>
</html>

